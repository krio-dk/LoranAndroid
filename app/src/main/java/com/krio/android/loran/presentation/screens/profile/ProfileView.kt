package com.krio.android.loran.presentation.screens.profile

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 04.09.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface ProfileView : MvpView {

    fun setSubscriptionBageColor(color: Int)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showChromeTab(url: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun sendEmail(email: String, subject: String, mailText: String, chooserText: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

}