package com.krio.android.loran.entity.thirdparty.tmdb.serial

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
data class TmdbSerialsPagedList(
        @SerializedName("page") val page: Int,
        @SerializedName("results") val results: List<TmdbSerialItem>,
        @SerializedName("total_results") val totalResults: Int,
        @SerializedName("total_pages") val totalPages: Int
)
