package com.krio.android.loran.presentation.screens.search

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.presentation.displayitems.search.DisplayEmptyItem
import com.krio.android.loran.entity.presentation.displayitems.search.DisplaySearchItem
import com.krio.android.loran.model.interactor.CatalogInteractor
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.global.Paginator
import com.krio.android.loran.presentation.screens.serial.SerialPresenter
import com.krio.android.loran.toothpick.DI
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 25.02.2018.
 */
@InjectViewState
class SearchPresenter @Inject constructor(
        private val router: Router,
        private val interactor: CatalogInteractor,
        private val errorHandler: ErrorHandler
) : BasePresenter<SearchView>() {

    private var paginator: Paginator<DisplaySearchItem>? = null

    private val searchRelay = PublishRelay.create<String>()
    private var isNeedScrollToTop = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.enableSearchViewListener()

        paginator = Paginator({ page -> interactor.getSearchDefaultItems(page) }, viewController)
        refreshShows()

        subscribeToQueryChanges()
    }

    private fun subscribeToQueryChanges() {
        searchRelay
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { query ->
                    paginator = if (query.isNotEmpty()) {
                        Paginator({ page -> interactor.getSearchItems(query, page) }, viewController)
                    } else {
                        Paginator({ page -> interactor.getSearchDefaultItems(page) }, viewController)
                    }

                    refreshShows()
                    isNeedScrollToTop = true
                }
                .connect()
    }

    override fun onDestroy() {
        super.onDestroy()
        paginator?.release()
    }

    fun refreshShows() {
        viewState.resetEndlessScroll()
        paginator?.refresh()
    }

    fun loadNextPage() {
        viewState.showPageError(false)
        paginator?.loadNewPage()
    }

    fun searchTvShow(query: String) {
        viewState.showEmptyError(false, null)
        viewState.showPageError(false)
        searchRelay.accept(query)
    }

    fun onShowClicked(displaySearchItem: DisplaySearchItem) {
        router.navigateTo(
            Screens.SerialScreen(
                DI.CATALOG_SCOPE,
                SerialPresenter.InitParams(
                    displaySearchItem.serialTmdbId,
                    displaySearchItem.serialName,
                    displaySearchItem.backdropPath
                )
            )
        )
    }

    fun onBackPressed() = router.exit()

    private val viewController = object : Paginator.ViewController<DisplaySearchItem> {
        override fun showEmptyProgress(show: Boolean) {
            viewState.showHorizontalProgress(show)
        }

        override fun showEmptyError(show: Boolean, error: Throwable?) {
            viewState.showSearchResult(false, listOf())
            if (error != null) {
                errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
            } else {
                viewState.showEmptyError(show, null)
            }
        }

        override fun showErrorMessage(error: Throwable) {
            errorHandler.proceed(error) {
                viewState.showPageError(true, it)
            }
        }

        override fun showEmptyView(show: Boolean) {
            viewState.showSearchResult(show, listOf(DisplayEmptyItem()))
        }

        override fun showData(show: Boolean, data: List<DisplaySearchItem>) {
            viewState.showSearchResult(show, data)
            if (isNeedScrollToTop) {
                viewState.scrollToTop()
                isNeedScrollToTop = false
            }
        }

        override fun showRefreshProgress(show: Boolean) {
            // do nothing
        }

        override fun showPageProgress(show: Boolean) {
            viewState.showPageProgress(show)
        }
    }

}