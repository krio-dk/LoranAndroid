package com.krio.android.loran.entity.domain.core.main

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.materialdata.SerialMaterialData

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class Serial(
        @SerializedName("serial_tmdb_id") val serialTmdbId: Int,
        @SerializedName("serial_name") val serialName: String,
        @SerializedName("seasons") val seasons: MutableList<Season> = mutableListOf(),
        @SerializedName("material_data") val materialData: SerialMaterialData = SerialMaterialData()
)