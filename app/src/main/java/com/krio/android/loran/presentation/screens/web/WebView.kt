package com.krio.android.loran.presentation.screens.web

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 04.09.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface WebView : MvpView {
    fun showWebPage(url: String)
    fun showTitle(title: String)
    fun showSubtitle(subtitle: String)
    fun showProgress(show: Boolean)
}