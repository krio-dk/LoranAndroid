package com.krio.android.loran.entity.thirdparty.tmdb.person

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class TmdbProfilesItem(
        @SerializedName("aspect_ratio") val aspectRatio: Double,
        @SerializedName("file_path") val filePath: String,
        @SerializedName("height") val height: Int,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("width") val width: Int
)