package com.krio.android.loran.entity.loran.body.purchase

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 09.02.2018.
 */
data class PurchaseBody(
        @SerializedName("signed_data") val signedData: String,
        @SerializedName("signature") val signature: String
)