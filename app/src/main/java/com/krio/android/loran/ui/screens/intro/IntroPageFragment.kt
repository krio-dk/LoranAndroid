package com.krio.android.loran.ui.screens.intro

import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.R
import kotlinx.android.synthetic.main.fragment_intro_page.*

/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
class IntroPageFragment : Fragment() {

    companion object {
        private const val ARGS_PARAM_IMAGE_RES = "image_res"
        private const val ARGS_PARAM_TEXT_RES = "text_res"

        fun newInstance(@DrawableRes imageRes: Int, @StringRes textRes: Int) = IntroPageFragment().apply {
            arguments = Bundle().apply {
                putInt(ARGS_PARAM_IMAGE_RES, imageRes)
                putInt(ARGS_PARAM_TEXT_RES, textRes)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_intro_page, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.let {
            setImage(it.getInt(ARGS_PARAM_IMAGE_RES))
            setText(it.getInt(ARGS_PARAM_TEXT_RES))
        }
    }

    private fun setImage(@DrawableRes imageRes: Int) {
        GlideApp.with(this)
                .load(imageRes)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView)
    }

    private fun setText(@StringRes textRes: Int) {
        textView?.text = getString(textRes)
    }
}