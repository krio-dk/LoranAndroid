package com.krio.android.loran.ui.global.containers

import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
interface ContainerDelegate {
    val scope: String
    val startScreen: SupportAppScreen
}