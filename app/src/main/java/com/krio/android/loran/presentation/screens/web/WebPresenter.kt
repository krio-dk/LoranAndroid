package com.krio.android.loran.presentation.screens.web

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 04.09.2018.
 */
@InjectViewState
class WebPresenter @Inject constructor(
        private val initParams: InitParams,
        private val router: Router,
        private val errorHandler: ErrorHandler
) : BasePresenter<WebView>() {

    data class InitParams(
            val url: String,
            val title: String
    ) : Serializable

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showTitle(initParams.title)
        viewState.showWebPage(initParams.url)
    }

    fun onStartLoading() {
        viewState.showProgress(true)
    }

    fun onStopLoading() {
        viewState.showProgress(false)
    }

    fun onBackPressed() = router.exit()
}
