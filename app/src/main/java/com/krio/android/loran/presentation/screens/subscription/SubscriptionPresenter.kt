package com.krio.android.loran.presentation.screens.subscription

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.android.billingclient.api.BillingClient
import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.subscription.*
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.SubscriptionInteractor
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.billing.BillingManager
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@InjectViewState
class SubscriptionPresenter @Inject constructor(
        @AppNavigation private val router: Router,
        private val interactor: SubscriptionInteractor,
        private val resourceManager: ResourceManager,
        private val billingManager: BillingManager,
        private val prefs: Prefs,
        private val refreshRelay: PublishRelay<Boolean>,
        private val errorHandler: ErrorHandler
) : BasePresenter<SubscriptionView>() {

    var clearAdapterRelay = PublishRelay.create<Boolean>()

    private var refreshDisposable: Disposable? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        Analytics.logEvent("subscription_screen_opened") {
            prefs.userId?.let { putInt("user_id", it) }
        }

        subscribeToPurchasesUpdates()
        observeRefresh()
        getData()
    }

    fun refresh() {
        getData()
    }

    private fun subscribeToPurchasesUpdates() {
        billingManager.purchasesUpdates()
            .doOnNext {
                viewState.showData(true, emptyList())
                viewState.showEmptyProgress(true)
            }
            .flatMap { interactor.verifyPurchases(it).toObservable() }
            .subscribe { response ->
                if (response.verified) {
                    refreshRelay.accept(true)
                }
            }
            .connect()
    }

    private fun getData() {
        interactor.getSubscriptions()
            .doOnSubscribe {
                viewState.showData(false, emptyList())
                viewState.showEmptyError(false, null)
                viewState.showEmptyProgress(true)
            }
            .doAfterTerminate {
                viewState.showEmptyProgress(false)
            }
            .subscribe(
                { displaySubscriptions ->
                    val data = mutableListOf<Any>().apply {
                        displaySubscriptions.activeSubscription?.let { displaySubscriptionActiveItem ->
                            add(displaySubscriptionActiveItem)
                            add(DisplaySubscriptionCaptionItem(resourceManager.getString(R.string.subscription_text_other)))

                        } ?: add(DisplaySubscriptionDescriptionItem())

                        addAll(displaySubscriptions.availableSubscriptions)
                        add(DisplaySubscriptionFooterItem())
                    }

                    viewState.showData(true, data)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showEmptyError(true, it) }
                }
            )
            .connect()
    }

    fun onSubscribeClicked(displaySubscriptionItem: DisplaySubscriptionItem, activity: Activity) {

        Analytics.logEvent("subscribe_clicked") {
            putString("subscription_title", displaySubscriptionItem.title)
            prefs.userId?.let { putInt("user_id", it) }
        }

        billingManager.queryActiveSubscriptions()
            .flatMap { purchases ->
                val activeSubscription = if (purchases.isNotEmpty()) purchases[0] else null
                billingManager.launchBillingFlow(BillingClient.SkuType.SUBS, displaySubscriptionItem.id, activeSubscription?.sku)
            }
            .subscribe(
                { launchBillingFlow ->
                    launchBillingFlow(activity)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    fun onDetailsClicked(displaySubscriptionItem: DisplaySubscriptionActiveItem, activity: Activity) {

        Analytics.logEvent("subscription_details_clicked") {
            putString("subscription_title", displaySubscriptionItem.title)
            prefs.userId?.let { putInt("user_id", it) }
        }

        val browserIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("https://play.google.com/store/account/subscriptions?sku=${displaySubscriptionItem.id}&package=com.krio.android.loran")
        )
        activity.startActivity(browserIntent)
    }

    fun onLinkToGooglePlayClicked(activity: Activity) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/account/subscriptions"))
        activity.startActivity(browserIntent)
    }

    private fun observeRefresh() {
        refreshDisposable = refreshRelay.subscribe {
            clearAdapterRelay.accept(true)
            getData()
        }.apply { connect() }
    }

    fun onBackPressed() = router.exit()
}
