package com.krio.android.loran.extensions

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import androidx.annotation.Dimension
import androidx.annotation.StringRes
import com.krio.android.loran.R
import com.krio.android.loran.model.system.resource.ResourceManager
import retrofit2.HttpException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Dmitriy Kolmogorov on 19.02.2018.
 */
fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}


fun Throwable.userMessage(resourceManager: ResourceManager) = when (this) {
    is HttpException -> when (this.code()) {
        304 -> resourceManager.getString(R.string.error_not_modified)
        400 -> resourceManager.getString(R.string.error_bad_request)
        401 -> resourceManager.getString(R.string.error_unauthorized)
        403 -> resourceManager.getString(R.string.error_forbidden)
        404 -> resourceManager.getString(R.string.error_not_found)
        405 -> resourceManager.getString(R.string.error_method_not_allowed)
        409 -> resourceManager.getString(R.string.error_conflict)
        422 -> resourceManager.getString(R.string.error_unprocessable)
        500 -> resourceManager.getString(R.string.error_server_error)
        else -> resourceManager.getString(R.string.error_unknown)
    }
    is IOException -> resourceManager.getString(R.string.error_network)
    else -> resourceManager.getString(R.string.error_unknown)
}

fun String.timeZoneDateTime(): String {
    val dateFormatter = SimpleDateFormat(if (length == 10) "yyyy-MM-dd" else "yyyy-MM-dd HH:mm:ss", Locale.getDefault()).apply {
        timeZone = TimeZone.getTimeZone("Europe/Moscow")
    }

    val date = dateFormatter.parse(this)
    return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(date)
}

fun Context.dpToPx(@Dimension(unit = Dimension.DP) dp: Float) = dpToPx(resources.displayMetrics, dp)

private fun dpToPx(displayMetrics: DisplayMetrics, @Dimension(unit = Dimension.DP) dp: Float) =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        displayMetrics
    )


fun Resources.getHtmlSpannedString(@StringRes id: Int): Spanned = getString(id).toHtmlSpan()

fun Resources.getHtmlSpannedString(@StringRes id: Int, vararg formatArgs: Any): Spanned = getString(id, *formatArgs).toHtmlSpan()

fun String.toHtmlSpan(): Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
} else {
    Html.fromHtml(this)
}