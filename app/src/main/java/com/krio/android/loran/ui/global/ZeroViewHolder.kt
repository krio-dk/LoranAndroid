package com.krio.android.loran.ui.global

import android.view.ViewGroup
import com.krio.android.loran.R
import com.krio.android.loran.extensions.visible
import kotlinx.android.synthetic.main.layout_zero_data.view.*

/**
 * Created by Dmitriy Kolmogorov on 19.02.2018.
 */
class ZeroViewHolder(
        private val view: ViewGroup,
        private val refreshListener: () -> Unit = {}
) {
    private val res = view.resources

    init {
        view.refreshButton.setOnClickListener { refreshListener() }
    }

    fun showEmptyData(msg: String? = null, imageRes: Int) {
        view.imageView.setImageResource(imageRes)
        view.description.text = msg ?: res.getText(R.string.empty_data_description)
        view.visible(true)
        view.refreshButton.visible(false)
    }

    fun showEmptyError(msg: String? = null) {
        view.imageView.setImageResource(R.drawable.ic_error_svg)
        view.description.text = msg ?: res.getText(R.string.empty_error_description)
        view.visible(true)
        view.refreshButton.visible(true)
    }

    fun hide() {
        view.visible(false)
    }
}