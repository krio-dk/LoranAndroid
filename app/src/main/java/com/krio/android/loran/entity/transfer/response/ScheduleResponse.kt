package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.main.ScheduleItem

/**
 * Created by Dmitriy Kolmogorov on 15.04.2018.
 */
data class ScheduleResponse(
        @SerializedName("schedule_items") val scheduleItems: List<ScheduleItem>? = null
)