package com.krio.android.loran.model.initialization

import android.content.Context
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.android.billingclient.api.Purchase
import com.krio.android.loran.entity.domain.core.main.InitMessage
import com.krio.android.loran.entity.loran.body.purchase.PurchaseBody
import com.krio.android.loran.entity.transfer.body.InitBody
import com.krio.android.loran.fcm.PushTokenSender
import com.krio.android.loran.fcm.PushTokenWorker
import com.krio.android.loran.fcm.PushTokenWorkerFactory
import com.krio.android.loran.model.data.server.loran.LoranApi
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.data.utils.LanguageUtils
import com.krio.android.loran.model.system.billing.BillingManager
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.model.system.secure.PiracyManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
class Initializer @Inject constructor(
    private val context: Context,
    private val prefs: Prefs,
    private val loranApi: LoranApi,
    private val billingManager: BillingManager,
    private val resourceManager: ResourceManager,
    private val piracyManager: PiracyManager,
    private val pushTokenSender: PushTokenSender,
    private val workerFactory: PushTokenWorkerFactory
) {

    companion object {
        private const val UPDATE_PUSH_TOKEN_WORK_TAG = "updatePushTokenWorkTag"
    }

    private var activePurchases = emptyList<Purchase>()
    private var history = emptyList<Purchase>()

    fun init(): Single<InitResult> = requireSubscriptionsSupported()

    fun requireSubscriptionsSupported(): Single<InitResult> {
        Timber.d("requireSubscriptionsSupported")
        return billingManager.isSubscriptionsSupported()
                .flatMap { isSupported ->
                    if (isSupported) {
                        queryActiveSubscriptions()
                    } else {
                        Single.error(SubscriptionsNotSupportedException())
                    }
                }
    }

    fun queryActiveSubscriptions(): Single<InitResult> {
        Timber.d("queryActiveSubscriptions")
        return billingManager.queryActiveSubscriptions()
                .flatMap { purchases ->
                    activePurchases = purchases
                    querySubscriptionHistory()
                }
    }

    fun querySubscriptionHistory(): Single<InitResult> {
        Timber.d("querySubscriptionHistory")
        return billingManager.querySubscriptionHistory()
                .flatMap { purchases ->
                    history = purchases
                    sendPurchasesToServer()
                }
    }

    fun sendPurchasesToServer(): Single<InitResult> {
        Timber.d("sendPurchasesToServer")
        val body = InitBody(
                activePurchases = activePurchases.map { PurchaseBody(it.originalJson, it.signature) },
                history = history.map { PurchaseBody(it.originalJson, it.signature) },
                userId = prefs.userId,
                language = LanguageUtils.language
        )

        return loranApi.init(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext { Single.error(ServerErrorException(it)) }
                .flatMap { response ->
                    if (response.isPurchasesValid) {
                        saveUserId(response.userId)
                        configureUpdatePushTokenWork()
                        sendPushToken()
                        complete(
                            message = response.message,
                            verifySignature = response.verifySignature ?: true,
                            verifyInstaller = response.verifyInstaller ?: false,
                            verifyPirateApps = response.verifyPirateApps ?: false,
                            verifyThirdPartyStore = response.verifyThirdPartyStore ?: false
                        )

                    } else {
                        Single.error(PurchasesNotValidException())
                    }
                }
    }

    private fun saveUserId(userId: Int?) {
        prefs.userId = userId
    }

    private fun configureUpdatePushTokenWork() {
        Timber.d("configureUpdatePushTokenWork")

        val workManager = WorkManager.getInstance(context)
        workManager.cancelAllWorkByTag(UPDATE_PUSH_TOKEN_WORK_TAG)

        val updatePushTokenWorkRequest = PeriodicWorkRequest.Builder(PushTokenWorker::class.java, 6, TimeUnit.HOURS)
            .setInitialDelay(6, TimeUnit.HOURS)
            .addTag(UPDATE_PUSH_TOKEN_WORK_TAG)
            .build()

        workManager.enqueue(updatePushTokenWorkRequest)
    }

    private fun sendPushToken() {
        pushTokenSender.sendPushToken()
    }


    private fun complete(
        message: InitMessage? = null,
        verifySignature: Boolean,
        verifyInstaller: Boolean,
        verifyPirateApps: Boolean,
        verifyThirdPartyStore: Boolean
    ): Single<InitResult> {
        Timber.d("complete")
        return piracyManager
            .check(verifySignature, verifyInstaller, verifyPirateApps, verifyThirdPartyStore)
            .andThen(Single.just(InitResult(message)))
    }
}