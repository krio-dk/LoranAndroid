package com.krio.android.loran

import ru.terrakok.cicerone.Screen

/**
 * Created by Dmitriy Kolmogorov on 13.02.2018.
 */
object Tabs {
    object SerialTab: Screen()
    object ScheduleTab: Screen()
    object TimelineTab: Screen()
    object FavoritelTab: Screen()
    object ProfileTab: Screen()
}