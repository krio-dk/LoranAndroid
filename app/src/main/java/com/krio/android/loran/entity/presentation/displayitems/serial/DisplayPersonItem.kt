package com.krio.android.loran.entity.presentation.displayitems.serial

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class DisplayPersonItem(
        val personTmdbId: Int,
        val name: String,
        val character: String,
        val profilePath: String
)