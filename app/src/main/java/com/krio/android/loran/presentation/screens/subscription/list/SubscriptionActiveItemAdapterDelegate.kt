package com.krio.android.loran.presentation.screens.subscription.list

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionActiveItem
import com.krio.android.loran.extensions.visible
import kotlinx.android.synthetic.main.item_subscription.view.*

/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
class SubscriptionActiveItemAdapterDelegate(
        private val detailsClickListener: (DisplaySubscriptionActiveItem) -> Unit
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplaySubscriptionActiveItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subscription_active, parent, false)
        return SubscriptionActiveViewHolder(view, detailsClickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as SubscriptionActiveViewHolder).bind(items[position] as DisplaySubscriptionActiveItem)
    }

    class SubscriptionActiveViewHolder(
            val view: View,
            detailsClickListener: (DisplaySubscriptionActiveItem) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        private lateinit var displayItem: DisplaySubscriptionActiveItem

        init {
            view.detailsButton.setOnClickListener {
                detailsClickListener.invoke(displayItem)
            }
        }

        fun bind(item: DisplaySubscriptionActiveItem) {
            this.displayItem = item

            view.backdrop.setImageResource(R.drawable.placeholder_backdrop_logo)
            view.title.text = displayItem.title
            view.totalPrice.text = displayItem.totalPrice
            view.periodDescription.text = displayItem.periodDescription
            view.freeTrialPeriod.text = displayItem.freeTrial
            view.commonDescription.text = displayItem.commonDescription
            view.monthPrice.text = displayItem.monthPrice

            view.subscribeButton.visible(false)
            view.detailsButton.visible(true)
            view.activeSubscriptionIndicator.visible(true)

            when (displayItem.grade) {
                1 -> view.backdropContainer.foreground = ContextCompat.getDrawable(view.context, R.drawable.gradient_bronze)
                2 -> view.backdropContainer.foreground = ContextCompat.getDrawable(view.context, R.drawable.gradient_silver)
                3 -> view.backdropContainer.foreground = ContextCompat.getDrawable(view.context, R.drawable.gradient_gold)
            }
        }
    }
}