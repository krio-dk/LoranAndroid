package com.krio.android.loran.entity.domain.core.ad

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Dmitriy Kolmogorov on 06.06.2019.
 */
data class AdAppConfiguration(
    @SerializedName("show_video") val showVideo: Boolean
) : Serializable