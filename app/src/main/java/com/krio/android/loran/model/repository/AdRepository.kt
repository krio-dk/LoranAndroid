package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.transfer.body.AdAppConfigurationBody
import com.krio.android.loran.entity.transfer.body.AdScreenConfigurationBody
import com.krio.android.loran.entity.transfer.response.AdAppConfigurationResponse
import com.krio.android.loran.entity.transfer.response.AdScreenConfigurationResponse
import com.krio.android.loran.entity.transfer.response.NeedShowRewardedDialogResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 06.06.2019.
 */
class AdRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun getAdScreenConfiguration(
        screen: String,
        userId: Int?
    ): Single<AdScreenConfigurationResponse> = loranApi
        .getAdScreenConfiguration(AdScreenConfigurationBody(screen, userId))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun getAdAppConfiguration(
        userId: Int?
    ): Single<AdAppConfigurationResponse> = loranApi
        .getAdAppConfiguration(AdAppConfigurationBody(userId))
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun needShowRewardedVideoDialog(
        serialTmdbId: Int,
        userId: Int
    ): Single<NeedShowRewardedDialogResponse> = loranApi
        .needShowRewardedVideoDialog(serialTmdbId, userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}