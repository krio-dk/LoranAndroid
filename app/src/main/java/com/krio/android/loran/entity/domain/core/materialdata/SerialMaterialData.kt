package com.krio.android.loran.entity.domain.core.materialdata

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.main.SerialStatus

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class SerialMaterialData(
        @SerializedName("backdrop_path") var backdropPath: String? = null,
        @SerializedName("first_air_date") var firstAirDate: String? = null,
        @SerializedName("homepage") var homepage: String? = null,
        @SerializedName("origin_country") var originCountry: List<String>? = null,
        @SerializedName("original_language") var originalLanguage: String? = null,
        @SerializedName("original_name") var originalName: String? = null,
        @SerializedName("overview") var overview: String? = null,
        @SerializedName("poster_path") var posterPath: String? = null,
        @SerializedName("status") var status: SerialStatus? = null,
        @SerializedName("popularity") var popularity: Double? = null,
        @SerializedName("vote_average") var voteAverage: Double? = null,
        @SerializedName("vote_count") var voteCount: Int? = null,
        @SerializedName("networks") var networks: List<String>? = null
)