package com.krio.android.loran.presentation.screens.person.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.ProfileSize
import com.krio.android.loran.model.data.glide.GlideApp
import kotlinx.android.synthetic.main.item_photo.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class PhotoItemAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is String

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        return PhotoViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as PhotoViewHolder).bind(items[position] as String)
    }

    class PhotoViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(path: String) {
            GlideApp.with(view.context)
                    .load(ImagesConfiguration.getProfileFullPath(ProfileSize.PROFILE_SIZE_ORIGINAL, path))
                    .onlyRetrieveFromCache(true)
                    .placeholder(R.drawable.placeholder_season_cover)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(view.imageView)

        }
    }
}