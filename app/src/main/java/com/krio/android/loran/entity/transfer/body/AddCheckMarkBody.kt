package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 08.06.2018.
 */
data class AddCheckMarkBody(
    @SerializedName ("serial_tmdb_id") val serialTmdbId: Int,
    @SerializedName ("season_number") val seasonNumber: Int,
    @SerializedName ("episode_number") val episodeNumber: Int,
    @SerializedName ("user_id") val userId: Int?
)