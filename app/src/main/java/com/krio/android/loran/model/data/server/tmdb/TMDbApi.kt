package com.krio.android.loran.model.data.server.tmdb

import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbCredits
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPerson
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPersonImages
import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeason
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerial
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerialsPagedList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
interface TMDbApi {

    companion object {
        const val API_VERSION = "3"
    }

    @GET("${API_VERSION}/tv/popular")
    fun getPopularSerials(
            @Query("page") page: Int,
            @Query("language") language: String
    ): Single<TmdbSerialsPagedList>

    @GET("${API_VERSION}/tv/top_rated")
    fun getTopRatedSerials(
            @Query("page") page: Int,
            @Query("language") language: String
    ): Single<TmdbSerialsPagedList>

    @GET("${API_VERSION}/search/tv")
    fun searchSerials(
            @Query("query") query: String,
            @Query("page") page: Int,
            @Query("language") language: String
    ): Single<TmdbSerialsPagedList>

    @GET("${API_VERSION}/tv/{serial_id}")
    fun getSerialDetails(
            @Path("serial_id") serialTmdbId: Int,
            @Query("language") language: String
    ): Single<TmdbSerial>

    @GET("${API_VERSION}/tv/{serial_id}/season/{season_number}")
    fun getSeasonDetails(
            @Path("serial_id") serialTmdbId: Int,
            @Path("season_number") seasonNumber: Int,
            @Query("language") language: String
    ): Single<TmdbSeason>

    @GET("${API_VERSION}/tv/{serial_id}/credits")
    fun getCredits(
            @Path("serial_id") serialTmdbId: Int,
            @Query("language") language: String
    ): Single<TmdbCredits>


    @GET("${API_VERSION}/person/{person_id}")
    fun getPerson(
            @Path("person_id") personId: Int,
            @Query("language") language: String
    ): Single<TmdbPerson>

    @GET("${API_VERSION}/person/{person_id}/images")
    fun getPersonImages(
            @Path("person_id") personId: Int
    ): Single<TmdbPersonImages>
}