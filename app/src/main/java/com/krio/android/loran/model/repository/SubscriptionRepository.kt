package com.krio.android.loran.model.repository

import com.android.billingclient.api.Purchase
import com.krio.android.loran.entity.loran.body.purchase.PurchaseBody
import com.krio.android.loran.entity.transfer.body.VerifyPurchasesBody
import com.krio.android.loran.entity.transfer.response.ActiveSubscriptionResponse
import com.krio.android.loran.entity.transfer.response.VerifyPurchasesResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 21.08.2018.
 */
class SubscriptionRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun getSubscriptionsIds(): Single<List<String>> =
            loranApi.getSubscriptions()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun getActiveSubscription(userId: Int): Single<ActiveSubscriptionResponse> =
            loranApi.getActiveSubscription(userId )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    fun verifyPurchases(purchases: List<Purchase>, userId: Int?): Single<VerifyPurchasesResponse> {
        val body = VerifyPurchasesBody(purchases.map { PurchaseBody(it.originalJson, it.signature) }, userId)
        return loranApi.verifyPurchases(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

}