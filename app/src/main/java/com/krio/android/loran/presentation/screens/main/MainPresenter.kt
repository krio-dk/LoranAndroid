package com.krio.android.loran.presentation.screens.main

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.Tabs
import com.krio.android.loran.presentation.global.BasePresenter
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
@InjectViewState
class MainPresenter @Inject constructor(
    private val initParams: InitParams,
    private val router: Router
) : BasePresenter<MainView>() {

    data class InitParams(val tabIndex: Int) : Serializable

    private var showMainContent = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        when (initParams.tabIndex) {
            0 -> viewState.selectPostersTab()
            1 -> viewState.selectTimelineTab()
            2 -> viewState.selectScheduleTab()
            3 -> viewState.selectFavoritesTab()
            4 -> viewState.selectProfileTab()
        }

        showMainContent = true
        viewState.showMainContent(true)
    }

    fun onActivityCreated() {
        if (showMainContent) {
            viewState.showMainContent(true)
        }
    }

    fun onPostersSelected() = router.replaceScreen(Tabs.SerialTab)
    fun onTimelineSelected() = router.replaceScreen(Tabs.TimelineTab)
    fun onScheduleSelected() = router.replaceScreen(Tabs.ScheduleTab)
    fun onFavoriteSelected() = router.replaceScreen(Tabs.FavoritelTab)
    fun onProfileSelected() = router.replaceScreen(Tabs.ProfileTab)

    fun onBackPressed() = router.exit()

}