package com.krio.android.loran.entity.presentation.displayitems.subscription

/**
 * Created by Dmitriy Kolmogorov on 19.08.2018.
 */
class DisplaySubscriptionActiveItem (
        val id: String,
        val title: String,
        val totalPrice: String,
        val periodDescription: String,
        val freeTrial: String,
        val commonDescription: String,
        val monthPrice: String,
        val grade: Int
)