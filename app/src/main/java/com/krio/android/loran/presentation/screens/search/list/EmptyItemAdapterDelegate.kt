package com.krio.android.loran.presentation.screens.search.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.search.DisplayEmptyItem

/**
 * Created by Dmitriy Kolmogorov on 17.02.2018.
 */
class EmptyItemAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayEmptyItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_empty, parent, false)
        return EmptyViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>,
                                  position: Int,
                                  viewHolder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) {
    }

    class EmptyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}