package com.krio.android.loran.entity.presentation.displayitems.serial

import com.krio.android.loran.entity.domain.core.main.SerialStatus

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class DisplaySerialDetailsItem(
        val firstAirDate: String?,
        val countries: List<String>?,
        val originalLanguage: String?,
        val originalName: String?,
        val overview: String?,
        val posterPath: String?,
        val status: SerialStatus?,
        val rating: Double?
)