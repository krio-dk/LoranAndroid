package com.krio.android.loran.model.system.notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.krio.android.loran.R
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.StillSize
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import com.krio.android.loran.ui.screens.launch.LaunchActivity
import javax.inject.Inject


class NotificationManager @Inject constructor(
        private val context: Context,
        private val prefs: Prefs
) {

    private val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private var notificationId: Int = 0

    fun showNotification(
            serialName: String,
            episodeName: String,
            translator: String,
            translationDate: String,
            seasonNumber: Int,
            episodeNumber: Int,
            backdropPath: String?,
            stillPath: String?
    ) {

        val details = context.getString(R.string.notification_details, seasonNumber, episodeNumber)

        val formattedDate = DateTimeUtils.getFormattedStringDateTime(translationDate)

        val collapsedLayout = RemoteViews(context.packageName, R.layout.notification_collapsed).apply {
            setTextViewText(R.id.translator, translator)
            setTextViewText(R.id.translationDate, formattedDate)
            setTextViewText(R.id.serialName, serialName)
            setTextViewText(R.id.details, details)
        }

        val expandedLayout = RemoteViews(context.packageName, R.layout.notification_expanded).apply {
            setTextViewText(R.id.serialName, serialName)
            setTextViewText(R.id.translator, translator)
            setTextViewText(R.id.episodeName, episodeName)
            setTextViewText(R.id.translationDate, formattedDate)
            setTextViewText(R.id.seasonNumber, seasonNumber.toString())
            setTextViewText(R.id.episodeNumber, episodeNumber.toString())
        }

        loadBackdrop(stillPath, backdropPath) { backdrop ->

            Analytics.logEvent("push_backdrop_received") {
                prefs.userId?.let { putInt("user_id", it) }
                putString("serial_name", serialName)
                putString("translator_name", translator)
                putInt("season_number", seasonNumber)
                putInt("episode_number", episodeNumber)
            }

            expandedLayout.setImageViewBitmap(R.id.backdrop, backdrop)
            showNotification(serialName, "$details ($translator)", collapsedLayout, expandedLayout)
        }
    }

    private fun loadBackdrop(stillPath: String?, backdropPath: String?, onLoaded: (Bitmap) -> Unit) {
        if (stillPath != null && stillPath.isNotBlank()) {
            loadImage(ImagesConfiguration.getStillFullPath(StillSize.STILL_SIZE_780, stillPath), onLoaded)
        } else {
            if (backdropPath != null && backdropPath.isNotBlank()) {
                loadImage(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_780, backdropPath), onLoaded)
            } else {
                onLoaded(BitmapFactory.decodeResource(context.resources, R.drawable.placeholder_backdrop))
            }
        }
    }

    private fun loadImage(path: String, onLoaded: (Bitmap) -> Unit) {
        GlideApp.with(context)
                .asBitmap()
                .load(path)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        onLoaded(resource)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // do nothing
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        Analytics.logEvent("push_backdrop_error") {
                            prefs.userId?.let { putInt("user_id", it) }
                            putString("path", path)
                            putString("drawable_present", if (errorDrawable == null) "false" else "true" )
                        }

                        onLoaded(BitmapFactory.decodeResource(context.resources, R.drawable.placeholder_backdrop))
                    }
                })
    }

    private fun showNotification(
            title: String,
            text: String,
            collapsedLayout: RemoteViews,
            expandedLayout: RemoteViews) {

        val pendingIntent = PendingIntent.getActivity(
                context,
                0,
                Intent(context, LaunchActivity::class.java).apply { action = "${context.packageName}.intent.action.TIMELINE" },
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notification = NotificationCompat.Builder(context, context.getString(R.string.default_notification_channel_id))
                .setSmallIcon(R.drawable.icon_notification)
                .setContentTitle(title)
                .setContentText(text)
                .setCustomContentView(collapsedLayout)
                .setCustomBigContentView(expandedLayout)
                .setContentIntent(pendingIntent)
                .setColor(ContextCompat.getColor(context, R.color.colorNotificationInfo))
                .setAutoCancel(true)
                //.setDefaults(Notification.DEFAULT_ALL)
                .build()

        notificationManager.notify(notificationId++, notification)

        Analytics.logEvent("push_shown") {
            prefs.userId?.let { putInt("user_id", it) }
            putString("title", title)
            putString("text", text)
        }
    }
}