package com.krio.android.loran.entity.thirdparty.tmdb.person


import com.google.gson.annotations.SerializedName

data class TmdbCredits(
        @SerializedName("cast") val tmdbCast: List<TmdbCastItem>,
        @SerializedName("id") val id: Int
)
