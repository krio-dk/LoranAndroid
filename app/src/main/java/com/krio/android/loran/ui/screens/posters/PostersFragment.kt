package com.krio.android.loran.ui.screens.posters

import android.graphics.Rect
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_ID
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayProgressItem
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayRefreshItem
import com.krio.android.loran.entity.presentation.displayitems.posters.DisplayPosterItem
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.presentation.global.list.ProgressItemAdapterDelegate
import com.krio.android.loran.presentation.global.list.RefreshItemAdapterDelegate
import com.krio.android.loran.presentation.screens.posters.PostersPresenter
import com.krio.android.loran.presentation.screens.posters.PostersView
import com.krio.android.loran.presentation.screens.posters.list.PosterItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.EndlessScrollListener
import com.krio.android.loran.ui.global.ZeroViewHolder
import kotlinx.android.synthetic.main.fragment_posters.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick


class PostersFragment : BaseFragment(), PostersView {

    companion object {
        const val VIEW_TYPE_POSTER = 1
        const val VIEW_TYPE_PROGRESS = 2
        const val VIEW_TYPE_REFRESH = 3
        const val VISIBLE_THRESHOLD = 9
    }

    override val layoutRes = R.layout.fragment_posters

    private val adapter = PostersAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    private var menuItemSearch: MenuItem? = null

    @InjectPresenter
    lateinit var presenter: PostersPresenter

    @ProvidePresenter
    fun providePresenter(): PostersPresenter {
        return Toothpick.openScope(DI.CATALOG_SCOPE).getInstance(PostersPresenter::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbarTitle.setText(R.string.posters_title)

        recyclerView.apply {
            setHasFixedSize(true)

            layoutManager = gridLayoutManager
            adapter = this@PostersFragment.adapter

            addOnScrollListener(endlessScrollListener)
            addOnScrollListener(topButtonVisibilityController)

            addItemDecoration(itemDecorator)
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.refreshShows()
        }

        topButton.setOnClickListener { recyclerView.scrollToPosition(0) }
    }

    private val gridLayoutManager
        get() = GridLayoutManager(context, 3).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (adapter.getItemViewType(position)) {
                        VIEW_TYPE_POSTER -> 1
                        VIEW_TYPE_REFRESH,
                        VIEW_TYPE_PROGRESS -> 3
                        else -> throw Exception()
                    }
                }
            }
        }

    private val endlessScrollListener = EndlessScrollListener(VISIBLE_THRESHOLD, VIEW_TYPE_PROGRESS) {
        presenter.loadNextPage()
    }

    private val topButtonVisibilityController
        get() = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0)
                    topButton.visible(false)
                else if (dy < 0)
                    topButton.visible(true)

                val firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                if (firstVisibleItemPosition <= 2) {
                    topButton.visible(false)
                }
            }
        }

    private val itemDecorator
        get() = object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val layoutParams = view.layoutParams as GridLayoutManager.LayoutParams
                val spanIndex = layoutParams.spanIndex

                val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)
                val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)

                val topPadding = when (layoutParams.viewAdapterPosition) {
                    0, 1, 2 -> fullPadding
                    else -> 0
                }

                when (spanIndex) {
                    0 -> outRect.set(fullPadding, topPadding, 0, halfPadding)
                    1 -> outRect.set(halfPadding, topPadding, halfPadding, halfPadding)
                    2 -> outRect.set(0, topPadding, fullPadding, halfPadding)
                }
            }
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.posters, menu)
        menuItemSearch = menu.findItem(R.id.menuSearch)
        menuItemSearch?.isVisible = adapter.items.isNotEmpty()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuSearch -> {
                presenter.onSearchClicked()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showPageProgress(show: Boolean) {
        adapter.showProgress(show)
    }

    override fun showPageError(show: Boolean, message: String?) {
        adapter.showRefresh(show, message)
    }

    override fun showEmptyView(show: Boolean) {
        if (show) zeroViewHolder?.showEmptyData(imageRes = R.drawable.ic_movie_svg)
        else zeroViewHolder?.hide()
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun showPosters(show: Boolean, displayPosters: List<DisplayPosterItem>) {
        recyclerView?.visible(show)
        adapter.setData(displayPosters)
    }

    override fun showMenuItemSearch(show: Boolean) {
        menuItemSearch?.isVisible = show
    }

    override fun enableToolbarScrolling(enable: Boolean) {
        toolbarFrameLayout?.let {
            val params = toolbarFrameLayout.layoutParams as AppBarLayout.LayoutParams
            params.scrollFlags = if (enable) {
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            } else 0


            toolbar?.visible(false)
            toolbar?.visible(true)
        }
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun resetEndlessScroll() {
        endlessScrollListener.reset()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class PostersAdapter : ListDelegationAdapter<MutableList<Any>>() {

        init {
            setHasStableIds(true)
            items = mutableListOf()
            delegatesManager.addDelegate(VIEW_TYPE_POSTER, PosterItemAdapterDelegate { presenter.onPosterClicked(it) })
            delegatesManager.addDelegate(VIEW_TYPE_PROGRESS, ProgressItemAdapterDelegate())
            delegatesManager.addDelegate(VIEW_TYPE_REFRESH, RefreshItemAdapterDelegate { presenter.loadNextPage() })
        }

        fun setData(displayPosters: List<DisplayPosterItem>) {
            val progress = isProgress()

            items.clear()
            items.addAll(displayPosters)
            if (progress) items.add(DisplayProgressItem())

            notifyDataSetChanged()
        }

        fun showProgress(isVisible: Boolean) {
            val currentProgress = isProgress()

            if (isVisible && !currentProgress) items.add(DisplayProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())

            notifyDataSetChanged()
        }

        fun showRefresh(show: Boolean, message: String?) {
            val currentRefresh = isRefresh()

            if (show && !currentRefresh) items.add(DisplayRefreshItem(message
                    ?: getString(R.string.error_unknown)))
            else if (!show && currentRefresh) items.remove(items.last())

            notifyDataSetChanged()
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is DisplayProgressItem

        private fun isRefresh() = items.isNotEmpty() && items.last() is DisplayRefreshItem

        override fun getItemId(position: Int): Long {
            super.getItemId(position)
            val item = items[position]
            return when (item) {
                is DisplayPosterItem -> item.serialTmdbId.toLong()
                else -> NO_ID
            }
        }
    }
}
