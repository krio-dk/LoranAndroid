package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.loran.body.purchase.PurchaseBody

/**
 * Created by Dmitriy Kolmogorov on 10.02.2018.
 */
class VerifyPurchasesBody (
        @SerializedName("purchases") val purchases: List<PurchaseBody>,
        @SerializedName("user_id") val userId: Int?
)