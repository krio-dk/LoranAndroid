package com.krio.android.loran.entity.domain.dataitems.materialdata

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 07.06.2018.
 */
data class TimelineItemMaterialData(
        @SerializedName("serial_name") var serialName: String? = null,
        @SerializedName("episode_name") var episodeName: String? = null,
        @SerializedName("episode_original_air_date") var episodeOriginalAirDate: String? = null,
        @SerializedName("serial_backdrop_path") var serialBackdropPath: String? = null,
        @SerializedName("serial_poster_path") var serialPosterPath: String? = null,
        @SerializedName("episode_still_path") var episodeStillPath: String? = null,
        @SerializedName("networks") var networks: List<String>? = null
)