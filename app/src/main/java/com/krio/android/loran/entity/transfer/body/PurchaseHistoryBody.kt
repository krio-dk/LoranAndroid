package com.krio.android.loran.entity.loran.body.purchase

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 10.02.2018.
 */
data class PurchaseHistoryBody(
        @SerializedName("product_id") val productId: String,
        @SerializedName("purchase_token") val purchaseToken: String,
        @SerializedName("purchase_time") val purchaseTime: Long
)