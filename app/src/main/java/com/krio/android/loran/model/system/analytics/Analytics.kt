package com.krio.android.loran.model.system.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.krio.android.loran.BuildConfig
import timber.log.Timber

object Analytics {

    private var firebaseAnalytics: FirebaseAnalytics? = null

    fun init(context: Context) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(context)
    }

    fun logEvent(name: String, params: Bundle.() -> Unit = {}) {
        if (!BuildConfig.DEBUG) {
            firebaseAnalytics?.logEvent(name, Bundle().apply { params(this) })
        } else {
            Timber.d("Log analytics event: disabled ($name)")
        }
    }
}