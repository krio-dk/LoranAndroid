package com.krio.android.loran.presentation.screens.search

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface SearchView : MvpView {
    fun showPageProgress(show: Boolean)
    fun showPageError(show: Boolean, message: String? = null)
    fun showEmptyProgress(show: Boolean)
    fun showHorizontalProgress(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showSearchResult(show: Boolean, tvShows: List<Any>)
    fun enableSearchViewListener()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun scrollToTop()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun resetEndlessScroll()
}