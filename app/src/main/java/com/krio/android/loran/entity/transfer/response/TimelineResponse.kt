package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.main.TimelineItem

/**
 * Created by Dmitriy Kolmogorov on 15.04.2018.
 */
data class TimelineResponse(
        @SerializedName("timeline_items") val timelineItems: List<TimelineItem>? = null
)