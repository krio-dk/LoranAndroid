package com.krio.android.loran.entity.thirdparty.tmdb.person

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 03.03.2018.
 */

data class TmdbCastItem(
        @SerializedName("character") val character: String,
        @SerializedName("credit_id") val creditId: String,
        @SerializedName("gender") val gender: Int?,
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("order") val order: Int,
        @SerializedName("profile_path") var profilePath: String?
)


