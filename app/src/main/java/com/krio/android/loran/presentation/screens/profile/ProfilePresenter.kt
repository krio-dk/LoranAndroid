package com.krio.android.loran.presentation.screens.profile

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.BuildConfig
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.SubscriptionInteractor
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.screens.web.WebPresenter
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import ru.terrakok.cicerone.Router
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 04.09.2018.
 */
@InjectViewState
class ProfilePresenter @Inject constructor(
    private val subscriptionInteractor: SubscriptionInteractor,
    private val innerRouter: Router,
    @AppNavigation private val appRouter: Router,
    private val refreshRelay: PublishRelay<Boolean>,
    private val prefs: Prefs,
    private val resourceManager: ResourceManager,
    private val errorHandler: ErrorHandler
) : BasePresenter<ProfileView>() {

    companion object {
        private const val useChromeTab = true
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        observeRefresh()
        updateSubscriptionBageColor()
    }

    fun onSupportClick() {
        val mailText = """
            |${resourceManager.getString(R.string.support_email_text_identification_data)}
            |User Id: ${prefs.userId}
            |Platform: Android
            |App Version: ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})
            |
            |${resourceManager.getString(R.string.support_email_text_feedback)}
            |
            |
        """.trimMargin()

        viewState.sendEmail(
                resourceManager.getString(R.string.support_email),
                resourceManager.getString(R.string.support_email_subject),
                mailText,
                resourceManager.getString(R.string.support_email_chooser_title)
        )
    }

    fun onIntroClick() = appRouter.navigateTo(Screens.IntroScreen(false))

    fun onAboutClick() = appRouter.navigateTo(Screens.AboutScreen)

    fun onTermsOfUseClick() {
        if (useChromeTab) {
            viewState.showChromeTab(resourceManager.getString(R.string.link_terms_of_use))
        } else {
            innerRouter.navigateTo(
                Screens.WebScreen(
                    DI.PROFILE_SCOPE,
                    WebPresenter.InitParams(
                            resourceManager.getString(R.string.link_terms_of_use),
                            resourceManager.getString(R.string.terms_of_use_caption)
                    )
                )
            )
        }
    }

    fun onPrivacyPolicyClick() {
        if (useChromeTab) {
            viewState.showChromeTab(resourceManager.getString(R.string.link_privacy_policy))
        } else {
            innerRouter.navigateTo(
                Screens.WebScreen(
                    DI.PROFILE_SCOPE,
                    WebPresenter.InitParams(
                        resourceManager.getString(R.string.link_privacy_policy),
                        resourceManager.getString(R.string.privacy_policy_caption)
                    )
                )
            )
        }
    }

    fun onSubscriptionClick() = appRouter.navigateTo(Screens.SubscriptionScreen)

    private fun observeRefresh() {
        refreshRelay.subscribe {
            updateSubscriptionBageColor()
        }.connect()
    }

    private fun updateSubscriptionBageColor() {
        subscriptionInteractor.getSubscriptions()
                .subscribe(
                        { (_, activeSubscription) ->
                            when (activeSubscription?.grade) {
                                1 -> viewState.setSubscriptionBageColor(R.color.colorSubscriptionBronze)
                                2 -> viewState.setSubscriptionBageColor(R.color.colorSubscriptionSilver)
                                3 -> viewState.setSubscriptionBageColor(R.color.colorSubscriptionGold)
                                else -> viewState.setSubscriptionBageColor(R.color.colorDarkSecondaryText)
                            }
                        },
                        { error ->
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                ).connect()
    }

    fun onBackPressed() = innerRouter.exit()
}
