package com.krio.android.loran.toothpick

object DI {
    const val APP_SCOPE = "app_scope"
    const val MAIN_SCOPE = "main_scope"
    const val CATALOG_SCOPE = "catalog_scope"
    const val FAVORITE_SCOPE = "favorites_scope"
    const val PROFILE_SCOPE = "profile_scope"

    fun serialRootScope(parentScope: String) = "${parentScope}_serial_container_scope"
    fun serialDetailsScope(parentScope: String) = "${parentScope}_serial_details_scope"
    fun serialSeasonsScope(parentScope: String) = "${parentScope}_serial_seasons_scope"
    fun serialEpisodesScope(parentScope: String) = "${parentScope}_serial_episodes_scope"
    fun personScope(parentScope: String) = "${parentScope}_person_scope"
    fun subscriptionScope(parentScope: String) = "${parentScope}_subscription_scope"
    fun webScope(parentScope: String) = "${parentScope}_web_scope"
    fun aboutScope(parentScope: String) = "${parentScope}_about_scope"
    fun introScope(parentScope: String) = "${parentScope}_intro_scope"
}