package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 08.09.2018.
 */
data class AddCheckMarkResponse (
        @SerializedName("success") val success: Boolean,
        @SerializedName("user_id") val userId: Int? = null
)