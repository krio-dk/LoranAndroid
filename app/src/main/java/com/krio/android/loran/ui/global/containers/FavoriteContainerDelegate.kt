package com.krio.android.loran.ui.global.containers

import com.krio.android.loran.Screens
import com.krio.android.loran.toothpick.DI

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
class FavoriteContainerDelegate : ContainerDelegate {
    override val startScreen = Screens.FavoriteScreen
    override val scope = DI.FAVORITE_SCOPE
}