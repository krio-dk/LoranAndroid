package com.krio.android.loran.presentation.screens.seasons

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.presentation.displayitems.seasons.DisplaySeasonItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.*
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.screens.episodes.EpisodesPresenter
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import com.krio.android.loran.toothpick.module.qualifier.ParentScope
import com.krio.android.loran.ui.screens.episodes.EpisodesFragment
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@InjectViewState
class SeasonsPresenter @Inject constructor(
    private val initParams: InitParams,
    @ParentScope private val parentScope: String,
    private val imagesInteractor: ImagesInteractor,
    private val serialInteractor: SerialInteractor,
    private val favoriteInteractor: FavoriteInteractor,
    private val subscriptionInteractor: SubscriptionInteractor,
    private val adInteractor: AdInteractor,
    private val refreshRelay: PublishRelay<Boolean>,
    private val prefs: Prefs,
    private val router: Router,
    @AppNavigation private val appRouter: Router,
    private val appodealManager: AppodealManager,
    private val resourceManager: ResourceManager,
    private val errorHandler: ErrorHandler
) : BasePresenter<SeasonsView>() {

    var refreshDisposable: Disposable? = null

    private var rewardedVideoDisposable: Disposable? = null

    private var isSubscriptionPresent = false

    data class InitParams(
            val tmdbId: Int,
            val serialName: String,
            val backdropPath: String?,
            val translatorId: Int,
            val translatorName: String,
            var isFavorite: Boolean
    ) : Serializable

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        Analytics.logEvent("seasons_screen_opened") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            prefs.userId?.let { putInt("user_id", it) }
        }

        viewState.showTitle(initParams.serialName)
        viewState.showTranslator(initParams.translatorName)
        showBackdrop()
        observeRefresh()
        getData()
    }

    private fun showBackdrop() {
        val backdropPath = initParams.backdropPath
        if (backdropPath != null) {
            viewState.showBackdrop(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_780, backdropPath))
        }
    }

    fun getData() {
        serialInteractor.getSeasonItems(initParams.tmdbId, initParams.translatorId)
                .flatMap { displaySeasonItems ->
                    imagesInteractor
                            .preloadPosters(displaySeasonItems.mapNotNull { it.posterPath }, PosterSize.POSTER_SIZE_780)
                            .flatMap { Single.just(displaySeasonItems) }
                }
                .flatMap { data ->
                    Single.zip(
                        Single.just(data),
                        subscriptionInteractor.getActiveSubscription(),
                        BiFunction<List<DisplaySeasonItem>, String, Pair<List<DisplaySeasonItem>, String>> { displayItems, currentSubscription ->
                            Pair(displayItems, currentSubscription)
                        }
                    )
                }
                .doOnSubscribe {
                    viewState.showEmptyError(false, null)
                    viewState.showEmptyProgress(true)
                }
                .doAfterTerminate {
                    viewState.showEmptyProgress(false)
                }
                .subscribe(
                        { (seasonItems, currentSubscription) ->
                            isSubscriptionPresent = currentSubscription.isNotEmpty()
                            viewState.showData(seasonItems)
                            updateFavoritesButton()
                        },
                        { error ->
                            errorHandler.proceed(error) { viewState.showEmptyError(true, it) }
                        }
                )
                .connect()
    }

    private fun updateFavoritesButton() {
        if (initParams.isFavorite) viewState.showFavoritesButton(resourceManager.getString(R.string.seasons_button_remove_from_favorites), R.color.colorSeenTint)
        else viewState.showFavoritesButton(resourceManager.getString(R.string.seasons_button_add_to_favorites), R.color.colorLinkText)
    }

    fun onFavoritesButtonClicked() {
        if (initParams.isFavorite) {
            removeFromFavorite(initParams.translatorId)
        } else {

            Analytics.logEvent("add_to_favorite_clicked") {
                putString("serial_name", initParams.serialName)
                putString("translator_name", initParams.translatorName)
                putString("from", "seasons screen")
                prefs.userId?.let { putInt("user_id", it) }
            }

            if (isSubscriptionPresent) {
                addToFavorite(initParams.translatorId, adWatched = false)
            } else {
                checkIfNeedToShowRewardedVideoDialog()
            }
        }
    }

    private fun checkIfNeedToShowRewardedVideoDialog() {
        adInteractor.needShowRewardedVideoDialog(initParams.tmdbId)
            .subscribe(
                { needShow ->
                    if (needShow) showRewardedVideoDialog()
                    else addToFavorite(initParams.translatorId,  adWatched = false)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    private fun showRewardedVideoDialog() {
        appodealManager.cacheRewardedVideo()
        subscriptionInteractor
            .getLowestSubscriptionPrice()
            .subscribe(
                { totalPrice ->
                    viewState.showRewardedVideoDialog(
                        initParams.serialName,
                        initParams.translatorId,
                        initParams.translatorName,
                        totalPrice
                    )

                    Analytics.logEvent("rewarded_video_dialog_shown") {
                        putString("serial_name", initParams.serialName)
                        putString("translator_name", initParams.translatorName)
                        putString("from", "seasons screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    fun onWatchAdClicked(translatorId: Int, translatorName: String) {

        Analytics.logEvent("watch_video_ad_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", translatorName)
            putString("from", "seasons screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        rewardedVideoDisposable = appodealManager.showRewardedVideo(viewState::setRewardedVideoProgress)
            .subscribe (
                { finished ->
                    println("Rewarded Video: presenter, finished = $finished")

                    Analytics.logEvent("rewarded_video_closed") {
                        putBoolean("finished", finished)
                        putString("from", "seasons screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    if (finished) {
                        addToFavorite(translatorId, adWatched = true, translatorName = translatorName)
                    } else {
                        viewState.showRewardedVideoError(true, resourceManager.getString(R.string.rewarded_video_dialog_error_not_finished))
                    }
                },
                { error ->
                    println("Rewarded Video: presenter, error = $error")

                    Analytics.logEvent("rewarded_video_failed") {
                        putString("from", "seasons screen")
                        putString("error", error.message)
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    if (error !is AppodealManager.AdDisabledException && error !is AppodealManager.RewardedVideoAdNotInitializedException) {
                        addToFavorite(translatorId, adWatched = false, translatorName = translatorName)
                    }

                    when (error) {
                        is AppodealManager.AdDisabledException,
                        is AppodealManager.RewardedVideoAdNotInitializedException -> {
                            viewState.hideRewardedVideoDialog()
                            viewState.showMessage(resourceManager.getString(R.string.common_restart_app))
                        }
                        is AppodealManager.CantShowRewardedVideoException -> {
                            // don't show anything
                        }
                        else -> {
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                    }
                }
            )
            .also { it.connect() }
    }

    fun onDismissRewardedVideoDialog() {
        rewardedVideoDisposable?.dispose()
    }

    private fun removeFromFavorite(translatorId: Int) {

        Analytics.logEvent("remove_from_favorite_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            putString("from", "seasons screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        favoriteInteractor.removeFromFavorite(initParams.tmdbId, translatorId)
            .subscribe(
                { (success) ->
                    if (success == true) {
                        initParams.isFavorite = false
                        updateFavoritesButton()
                        acceptRefresh()
                    }
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    private fun addToFavorite(translatorId: Int, adWatched: Boolean, translatorName: String? = null) {
        favoriteInteractor.addToFavorite(initParams.tmdbId, translatorId, adWatched)
            .subscribe(
                { (success) ->
                    if (success) {

                        Analytics.logEvent("add_to_favorite_success") {
                            putString("serial_name", initParams.serialName)
                            putString("translator_name", translatorName)
                            putString("from", "seasons screen")
                            prefs.userId?.let { putInt("user_id", it) }
                        }

                        translatorName?.let {
                            if (!adWatched) viewState.showMessage(resourceManager.getString(R.string.rewarded_video_message_lucky))
                            viewState.showMessage(resourceManager.getString(R.string.rewarded_video_message_done, initParams.serialName, it))
                        }

                        viewState.hideRewardedVideoDialog()
                        initParams.isFavorite = true
                        updateFavoritesButton()
                        acceptRefresh()

                    } else {

                        Analytics.logEvent("add_to_favorite_blocked") {
                            putString("serial_name", initParams.serialName)
                            putString("translator_name", translatorName)
                            putString("from", "seasons screen")
                            prefs.userId?.let { putInt("user_id", it) }
                        }

                        viewState.showRewardedVideoError(true, resourceManager.getString(R.string.rewarded_video_dialog_error_show))
                    }
                },
                { error ->

                    Analytics.logEvent("add_to_favorite_error") {
                        putString("serial_name", initParams.serialName)
                        putString("translator_name", translatorName)
                        putString("from", "seasons screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    viewState.hideRewardedVideoDialog()
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    fun onSeasonClicked(displaySeasonItem: DisplaySeasonItem) {

        val presenterParams = EpisodesPresenter.InitParams(
                initParams.tmdbId,
                initParams.serialName,
                initParams.backdropPath,
                initParams.translatorId,
                initParams.translatorName,
                displaySeasonItem.seasonNumber,
                displaySeasonItem.seasonName
        )

        val params = EpisodesFragment.InitParams(
                closeSerialRootScopeOnExit = false,
                presenterParams = presenterParams
        )

        router.navigateTo(Screens.EpisodesScreen(parentScope, params))
    }

    private fun updateFavoriteStatus() {
        serialInteractor.getTranslationItems(initParams.tmdbId, true)
            .subscribe(
                { displayTranslationItems ->
                    displayTranslationItems.find { it.translatorId == initParams.translatorId }?.let {
                        initParams.isFavorite = it.isFavorite
                        updateFavoritesButton()
                    }
                },
                { error ->
                    errorHandler.proceed(error)
                }
            )
            .connect()
    }

    fun onSubscriptionDetailsClicked() {
        appRouter.navigateTo(Screens.SubscriptionScreen)
    }

    private fun observeRefresh() {
        refreshDisposable = refreshRelay.subscribe {
            updateFavoriteStatus()
        }.apply { connect() }
    }

    private fun acceptRefresh() {
        refreshDisposable?.dispose()
        refreshRelay.accept(true)
        observeRefresh()
    }

    fun onBackPressed() {
        router.exit()
    }
}