package com.krio.android.loran.ui.screens.launch

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.core.main.InitMessage
import com.krio.android.loran.entity.domain.core.main.InitMessageType.GRACE_PERIOD
import com.krio.android.loran.extensions.dpToPx
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.system.secure.PiracyManager
import com.krio.android.loran.presentation.screens.launch.InitErrorDialogConfigurator
import com.krio.android.loran.presentation.screens.launch.LaunchPresenter
import com.krio.android.loran.presentation.screens.launch.LaunchView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseActivity
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.containers.ContainerFragment
import com.krio.android.loran.ui.screens.main.MainFragment
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import timber.log.Timber
import toothpick.Toothpick
import java.lang.ref.WeakReference
import javax.inject.Inject


class LaunchActivity : BaseActivity(), LaunchView {

    companion object {
        private const val CONTENT_READY_FLAG = "is_content_ready"
    }

    override val layoutRes = R.layout.activity_main

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var piracyManager: PiracyManager

    @InjectPresenter
    lateinit var presenter: LaunchPresenter

    private lateinit var smokeAnimatorSet: AnimatorSet

    private var allowStartSmokeAnimation = false
    var isContentReady = false

    init {
        val heapSize = Runtime.getRuntime().maxMemory()
        Timber.i("Heap size: $heapSize")
    }

    @ProvidePresenter
    fun providePresenter(): LaunchPresenter {
        return Toothpick.openScope(DI.APP_SCOPE).getInstance(LaunchPresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.APP_SCOPE))
        super.onCreate(savedInstanceState)
        initAnimators()

        isContentReady = savedInstanceState?.getBoolean(CONTENT_READY_FLAG) ?: false

        if (savedInstanceState == null || !isContentReady) {
            piracyManager.activity = WeakReference(this)
            presenter.onInitialOnCreate()
        } else {
            showMainContainer(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(CONTENT_READY_FLAG, isContentReady)
    }

    private fun initAnimators() {
        val showAnimator = ObjectAnimator.ofFloat(videoView, "alpha", 0.5f).apply {
            duration = 5000
            startDelay = 1000
        }

        val hideAnimator = ObjectAnimator.ofFloat(videoView, "alpha", 0f).apply {
            duration = 5000
            startDelay = 10000
        }

        smokeAnimatorSet = AnimatorSet().apply {
            play(showAnimator).before(hideAnimator)
        }
    }

    override fun showInitAnimation() {
        allowStartSmokeAnimation = true
        videoView.apply {
            setRawData(R.raw.intro_video)
            prepare {
                if (allowStartSmokeAnimation) {
                    start()
                    smokeAnimatorSet.start()
                }
            }
        }
    }

    override fun hideInitAnimation(duration: Long, onAnimationEnd: () -> Unit) {
        allowStartSmokeAnimation = false
        smokeAnimatorSet.cancel()

        ObjectAnimator.ofFloat(videoView, "alpha", 0f).apply {
            this.duration = duration
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    onAnimationEnd()
                }
            })
            start()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        when (intent?.action) {
            // TODO: handle onNewIntent
            // "$packageName.intent.action.TIMELINE" -> selectTimelineTab()
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onStop() {
        piracyManager.disposeWatchYoutube()
        super.onStop()
    }

    override fun sendEmail(email: String, subject: String, mailText: String, chooserText: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, mailText)
        }

        startActivity(Intent.createChooser(emailIntent, chooserText))
    }

    override fun showInitMessage(initMessage: InitMessage) {
        val builder = AlertDialog.Builder(this, R.style.DialogTheme)
            .setTitle(initMessage.title)
            .setMessage(initMessage.text)
            .setCancelable(false)
            .setOnDismissListener { presenter.onInitDialogDismiss() }

        when (initMessage.type) {
            GRACE_PERIOD -> {
                builder.apply {
                    setNegativeButton(R.string.initialization_init_message_button_skip, null)
                    setPositiveButton(R.string.initialization_init_message_button_subscription_link) { _, _ ->
                        presenter.onToSubscriptionClick(
                            this@LaunchActivity
                        )
                    }
                }
            }
        }

        builder.create().show()
    }

    override fun showInitErrorDialog(configurator: InitErrorDialogConfigurator) {
        val positiveButtonText = getString(R.string.initialization_button_retry)
        val negativeButtonText = getString(R.string.initialization_button_exit)
        val neutralButtonText: String = getString(R.string.initialization_button_support)

        val positiveButtonClickListener: (DialogInterface, Int) -> Unit = { _, _ -> presenter.onRetryClick(configurator.error) }
        val negativeButtonClickListener: (DialogInterface, Int) -> Unit = { _, _ -> presenter.onExitClick() }
        val neutralButtonClickListener: (DialogInterface, Int) -> Unit = { _, _ -> presenter.onSupportClick() }

        val builder = AlertDialog.Builder(this, R.style.DialogTheme)
            .setTitle(R.string.initialization_failed_title)
            .setMessage(configurator.message)
            .setCancelable(false)
            .setNegativeButton(negativeButtonText, negativeButtonClickListener)

        if (configurator.showRetryButton) {
            builder.setPositiveButton(positiveButtonText, positiveButtonClickListener)
        }

        if (configurator.showSupportButton) {
            builder.setNeutralButton(neutralButtonText, neutralButtonClickListener)
        }

        builder.create().show()
    }

    override fun setContentReadyFlag() {
        isContentReady = true
    }

    override fun showMainContainer(show: Boolean) {
        mainContainer.visible(show)
    }

    fun showSnackMessage(msg: String) {
        val messageView = LayoutInflater.from(this).inflate(R.layout.layout_message, messagesContainer, false)
        val textView = messageView.findViewById<TextView>(R.id.messageTextView)
        textView.text = msg

        messagesContainer.addView(messageView)
        messagesContainer.postDelayed({ messagesContainer.removeView(messageView) }, 4000)
    }

    fun configureSnackMargin() {
        val layoutParams = messagesContainer.layoutParams as ViewGroup.MarginLayoutParams

        layoutParams.bottomMargin = supportFragmentManager.fragments
            .find { it is MainFragment }
            ?.let { this.dpToPx(56f).toInt() }
            ?: this.dpToPx(0f).toInt()

        messagesContainer.layoutParams = layoutParams
    }

    private val navigator: SupportAppNavigator by lazy {
        object : SupportAppNavigator(this, supportFragmentManager, R.id.mainContainer) {

            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                // Fix incorrect order lifecycle callback of MainFragment
                fragmentTransaction.setReorderingAllowed(true)
            }
        }
    }

    override fun onBackPressed() {
        when (supportFragmentManager.findFragmentById(R.id.mainContainer)) {
            is ContainerFragment -> {
                val currentFragment = supportFragmentManager.findFragmentById(R.id.mainContainer) as ContainerFragment
                currentFragment.onBackPressed()
            }

            is BaseFragment -> {
                val currentFragment = supportFragmentManager.findFragmentById(R.id.mainContainer) as BaseFragment
                currentFragment.onBackPressed()
            }

            else -> presenter.onBackPressed()
        }
    }
}
