package com.krio.android.loran.toothpick.provider.tmdb

import com.krio.android.loran.BuildConfig
import com.krio.android.loran.model.data.auth.AuthParameterHolder
import com.krio.android.loran.model.data.server.interceptor.AuthParameterInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class TMDbOkHttpClientProvider @Inject constructor(
        private val authParameterHolder: AuthParameterHolder
) : Provider<OkHttpClient> {

    override fun get() = OkHttpClient.Builder().apply {
        addNetworkInterceptor(AuthParameterInterceptor(authParameterHolder))
        readTimeout(30, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            addNetworkInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        }

    }.build()
}

