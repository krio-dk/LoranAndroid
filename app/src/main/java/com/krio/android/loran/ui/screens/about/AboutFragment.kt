package com.krio.android.loran.ui.screens.about

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.presentation.screens.about.AboutPresenter
import com.krio.android.loran.presentation.screens.about.AboutView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick


class AboutFragment : BaseFragment(), AboutView {

    override val layoutRes = R.layout.fragment_about

    @InjectPresenter
    lateinit var presenter: AboutPresenter

    @ProvidePresenter
    fun providePresenter(): AboutPresenter {
        val parentScope = DI.APP_SCOPE
        val scope = Toothpick.openScopes(parentScope, DI.aboutScope(parentScope))

        return scope.getInstance(AboutPresenter::class.java).apply {
            Toothpick.closeScope(DI.aboutScope(parentScope))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.baseline_close_white_24)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbarTitle.setText(R.string.about_caption)
    }

    override fun showVersion(versionNumber: String) {
        version.text = getString(R.string.about_version, versionNumber)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
