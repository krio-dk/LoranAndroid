package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.thirdparty.tmdb.*
import com.krio.android.loran.model.repository.ImagesRepository
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
class ImagesInteractor @Inject constructor(
        private val repository: ImagesRepository
) {

    init {
        Timber.d("Init")
    }

    fun preloadStills(stillPaths: List<String>): Single<Boolean> {
        val fullPaths = stillPaths.map { ImagesConfiguration.getStillFullPath(StillSize.STILL_SIZE_780, it) }
        return repository.preloadImages(fullPaths)
    }

    fun preloadPosters(posterPaths: List<String>, posterSize: PosterSize): Single<Boolean> {
        val fullPaths = posterPaths.map { ImagesConfiguration.getPosterFullPath(posterSize, it) }
        return repository.preloadImages(fullPaths)
    }

    fun preloadProfiles(profilesPath: List<String>): Single<Boolean> {
        val fullPaths = profilesPath.map { ImagesConfiguration.getProfileFullPath(ProfileSize.PROFILE_SIZE_ORIGINAL, it) }
        return repository.preloadImages(fullPaths)
    }

    fun preloadBackdrops(backdropPaths: List<String>, backdropSize: BackdropSize): Single<Boolean> {
        val fullPaths = backdropPaths.map { ImagesConfiguration.getBackdropFullPath(backdropSize, it) }
        return repository.preloadImages(fullPaths)
    }
}