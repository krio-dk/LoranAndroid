package com.krio.android.loran.presentation.screens.serial

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface SerialView : MvpView {
    fun showTitle(title: String)
    fun showData(data: List<Any>)
    fun showBackdrop(url: String)

    fun showEmptyProgress(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)

    fun updateFavoriteStatus(translatorId: Int, isFavorite: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(SkipStrategy::class)
    fun showRewardedVideoDialog(serialName: String, translatorId: Int, translatorName: String, subscriptionCost: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun hideRewardedVideoDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showRewardedVideoError(show: Boolean, message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setRewardedVideoProgress(value: Long)
}