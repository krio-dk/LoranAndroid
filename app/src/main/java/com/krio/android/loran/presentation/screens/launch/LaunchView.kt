package com.krio.android.loran.presentation.screens.launch

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.krio.android.loran.model.initialization.Initializer
import com.krio.android.loran.entity.domain.core.main.InitMessage

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(OneExecutionStateStrategy::class)
interface LaunchView : MvpView {
    fun showInitAnimation()
    fun hideInitAnimation(duration: Long, onAnimationEnd: () -> Unit)

    fun setContentReadyFlag()
    fun showMainContainer(show: Boolean)

    fun showInitMessage(initMessage: InitMessage)
    fun showInitErrorDialog(configurator: InitErrorDialogConfigurator)

    fun sendEmail(email: String, subject: String, mailText: String, chooserText: String)
}