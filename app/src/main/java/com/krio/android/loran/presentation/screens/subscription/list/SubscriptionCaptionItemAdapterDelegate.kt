package com.krio.android.loran.presentation.screens.subscription.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionCaptionItem
import kotlinx.android.synthetic.main.item_date.view.*

/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
class SubscriptionCaptionItemAdapterDelegate() : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplaySubscriptionCaptionItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subscription_caption, parent, false)
        return SubscriptionCaptionViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as SubscriptionCaptionViewHolder).bind(items[position] as DisplaySubscriptionCaptionItem)
    }

    class SubscriptionCaptionViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: DisplaySubscriptionCaptionItem) {
            view.date.text = item.text
        }
    }
}