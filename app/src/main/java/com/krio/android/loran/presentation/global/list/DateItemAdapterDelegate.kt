package com.krio.android.loran.presentation.global.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayDateItem
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import kotlinx.android.synthetic.main.item_date.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class DateItemAdapterDelegate() : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayDateItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_date, parent, false)
        return DateViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as DateViewHolder).bind(items[position] as DisplayDateItem)
    }

    class DateViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


        fun bind(dateItem: DisplayDateItem) {
            if (dateItem.date.isNotEmpty()) {
                view.date.text = DateTimeUtils.getFormattedStringDate(dateItem.date)
            } else {
                // view.date.text = "Давным-давно, в далёкой-далёкой галактике..."
            }
        }
    }
}