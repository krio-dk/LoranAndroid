package com.krio.android.loran.entity.presentation.displayitems.subscription

/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
data class DisplaySubscriptionCaptionItem (val text: String)