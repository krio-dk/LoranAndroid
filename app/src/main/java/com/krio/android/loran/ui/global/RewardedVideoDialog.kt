package com.krio.android.loran.ui.global

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View.*
import android.view.ViewGroup
import com.krio.android.loran.R
import com.krio.android.loran.extensions.getHtmlSpannedString
import com.krio.android.loran.extensions.visible
import kotlinx.android.synthetic.main.dialog_rewarded_video.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class RewardedVideoDialog : DialogFragment() {

    companion object {
        private const val ARGS_PARAM_SERIAL_NAME = "serial_name"
        private const val ARGS_PARAM_SUBSCRIPTION_COST = "subscription_cost"

        fun newInstance(serialName: String, subscriptionCost: String) = RewardedVideoDialog().apply {
            arguments = Bundle().apply {
                putString(ARGS_PARAM_SERIAL_NAME, serialName)
                putString(ARGS_PARAM_SUBSCRIPTION_COST, subscriptionCost)
            }
        }
    }

    var watchAdClickListener: () -> Unit = { }
    var dismissListener: () -> Unit = { }
    var subscriptionDetailsClickListener: () -> Unit = { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.dialog_rewarded_video, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(arguments!!) {
            val serialName = getString(ARGS_PARAM_SERIAL_NAME)!!
            val subscriptionCost = getString(ARGS_PARAM_SUBSCRIPTION_COST)!!

            description.text = resources.getHtmlSpannedString(R.string.rewarded_video_dialog_description, serialName)
            alternativeDescription.text = resources.getHtmlSpannedString(R.string.rewarded_video_dialog_alternative, subscriptionCost)
        }

        cancelButton.setOnClickListener { dismiss()}
        detailsButton.setOnClickListener {
            subscriptionDetailsClickListener.invoke()
            dismiss()
        }
        watchAdButton.setOnClickListener {
            watchAdClickListener.invoke()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        dismissListener.invoke()
    }

    fun enableWatchAdButton(enabled: Boolean) {
        watchAdButton.isEnabled = enabled
    }

    fun showProgress(show: Boolean) {
        progressLayout.visibility = if (show) VISIBLE else GONE
    }

    fun setProgressValue(value: Long) {
        progressValue.text = if (value > 0) getString(R.string.rewarded_video_dialog_timer_value, value.toString()) else "..."
    }

    fun showError(show: Boolean, message: String? = null) {
        errorText.text = message
        errorContainer.visible(show)
    }
}