package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 08/04/2019.
 */
data class HasFavoritesResponse(
        @SerializedName("has_favorites") val hasFavorites: Boolean
)