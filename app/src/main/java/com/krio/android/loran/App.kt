package com.krio.android.loran

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.work.WorkManager
import com.jakewharton.threetenabp.AndroidThreeTen
import com.krio.android.loran.fcm.PushTokenWorkerFactory
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.AppModule
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration


/**
 * Created by Dmitriy Kolmogorov on 04.01.2018.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this);

        initLogger()
        initToothpick()
        initAppScope()
        initWorkManager()
        initAnalytics()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channel = NotificationChannel(
                getString(R.string.default_notification_channel_id),
                getString(R.string.default_notification_channel_name),
                NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationManager.createNotificationChannel(channel);
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }
    }

    private fun initAppScope() {
        Toothpick.openScope(DI.APP_SCOPE).apply {
            installModules(AppModule(this@App))
        }
    }

    private fun initWorkManager() {
        val config = androidx.work.Configuration.Builder()
            .setWorkerFactory(Toothpick.openScope(DI.APP_SCOPE).getInstance(PushTokenWorkerFactory::class.java))
            .build()

        WorkManager.initialize(this, config)

    }

    private fun initAnalytics() {
        Analytics.init(this)
    }
}