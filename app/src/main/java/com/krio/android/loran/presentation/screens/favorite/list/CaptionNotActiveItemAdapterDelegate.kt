package com.krio.android.loran.presentation.screens.favorite.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteCaptionNotActive
import kotlinx.android.synthetic.main.item_favorite_caption_not_active.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class CaptionNotActiveItemAdapterDelegate (private val detailsClickListener: () -> Unit): AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
        items[position] is DisplayFavoriteCaptionNotActive

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_caption_not_active, parent, false)
        return DateViewHolder(view, detailsClickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as DateViewHolder).bind(items[position] as DisplayFavoriteCaptionNotActive)
    }

    class DateViewHolder(val view: View, detailsClickListener: () -> Unit) : RecyclerView.ViewHolder(view) {

        init {
            view.notTrackedDetailsButton.setOnClickListener { detailsClickListener.invoke() }
        }

        fun bind(item: DisplayFavoriteCaptionNotActive) {
            
        }
    }
}