package com.krio.android.loran.entity.presentation.displayitems.schedule

import com.krio.android.loran.entity.domain.dataitems.main.ScheduleType

/**
 * Created by Dmitriy Kolmogorov on 27.03.2018.
 */
data class DisplayScheduleItem(
        val tmdbId: Int,
        val serialName: String,
        val seasonNumber: Int,
        val episodeNumber: Int,
        var isSeen: Boolean,
        val translatorId: Int,
        val translatorName: String,
        val scheduleDate: String,
        val scheduleType: ScheduleType,
        val episodeName: String?,
        val originalAirDate: String?,
        val backdropPath: String?,
        val posterPath: String?
)