package com.krio.android.loran.model.data.server.loran

import com.krio.android.loran.entity.transfer.body.*
import com.krio.android.loran.entity.transfer.response.*
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
interface LoranApi {

    companion object {
        const val API_VERSION = "1"
    }

    // Authentication and Purchases

    @POST("android/${API_VERSION}/init")
    fun init(@Body body: InitBody): Single<InitResponse>


    @POST("android/${API_VERSION}/verifyPurchases")
    fun verifyPurchases(@Body body: VerifyPurchasesBody): Single<VerifyPurchasesResponse>

    @FormUrlEncoded
    @POST("android/${API_VERSION}/pushToken")
    fun sendPushToken(
            @Field("token") token: String,
            @Field("app_instance_id") appInstanceId: String,
            @Field("user_id") userId: Int
    ): Single<ResultResponse>

    // Serial

    @POST("${API_VERSION}/serial")
    fun getSerial(@Body body: GetSerialBody): Single<SerialResponse>

    // Favorite translation

    @POST("2/addFavorite")
    fun addFavorite(@Body body: AddFavoriteBody): Single<UserIdResponse>

    @FormUrlEncoded
    @POST("${API_VERSION}/removeFavorite")
    fun removeFavorite(
            @Field("serial_tmdb_id") serialTmdbId: Int,
            @Field("translator_id") translatorId: Int,
            @Field("user_id") userId: Int
    ): Single<ResultResponse>

    @GET("${API_VERSION}/favoritesIdentifiers")
    fun getFavoritesIdentifiers(@Query("user_id") userId: Int): Single<List<Int>>

    // Check Mark

    @POST("${API_VERSION}/addCheckMark")
    fun addCheckMark(@Body body: AddCheckMarkBody): Single<AddCheckMarkResponse>

    @FormUrlEncoded
    @POST("${API_VERSION}/removeCheckMark")
    fun removeCheckMark(
            @Field("serial_tmdb_id") serialTmdbId: Int,
            @Field("season_number") seasonNumber: Int,
            @Field("episode_number") episodeNumber: Int,
            @Field("user_id") userId: Int
    ): Single<ResultResponse>

    @POST("${API_VERSION}/addGroupCheckMark")
    fun addGroupCheckMark(@Body body: AddGroupCheckMarkBody): Single<AddCheckMarkResponse>

    @FormUrlEncoded
    @POST("${API_VERSION}/removeGroupCheckMark")
    fun removeGroupCheckMark(
            @Field("serial_tmdb_id") serialTmdbId: Int,
            @Field("season_number") seasonNumber: Int,
            @Field("user_id") userId: Int
    ): Single<ResultResponse>


    // Tabs

    @GET("${API_VERSION}/timeline")
    fun getTimeline(
            @Query("page") page: Int,
            @Query("user_id") userId: Int
    ): Single<TimelineResponse>

    @GET("${API_VERSION}/schedule")
    fun getSchedule(
            @Query("page") page: Int,
            @Query("user_id") userId: Int
    ): Single<ScheduleResponse>

    @GET("${API_VERSION}/favorites")
    fun getFavorites(@Query("user_id") userId: Int): Single<FavoritesResponse>

    @GET("${API_VERSION}/favorite")
    fun getFavorite(
            @Query("serial_tmdb_id") serialTmdbId: Int,
            @Query("user_id") userId: Int
    ): Single<FavoriteItemResponse>

    @GET("${API_VERSION}/hasFavorites")
    fun hasFavorites(@Query("user_id") userId: Int): Single<HasFavoritesResponse>


    // Serial activation

    @FormUrlEncoded
    @POST("2/activateSerial")
    fun activateSerial(
            @Field("serial_tmdb_id") serialTmdbId: Int,
            @Field("ad_watched") adWatched: Boolean,
            @Field("user_id") userId: Int
    ): Single<ActivateSerialResponse>

    @FormUrlEncoded
    @POST("${API_VERSION}/deactivateSerial")
    fun deactivateSerial(
        @Field("serial_tmdb_id") serialTmdbId: Int,
        @Field("user_id") userId: Int
    ): Single<ResultResponse>


    // Subscriptions

    @GET("android/${API_VERSION}/subscriptions")
    fun getSubscriptions(): Single<List<String>>

    @GET("android/${API_VERSION}/activeSubscription")
    fun getActiveSubscription(@Query("user_id") userId: Int): Single<ActiveSubscriptionResponse>


    // Ad

    @POST("${API_VERSION}/adScreenConfiguration")
    fun getAdScreenConfiguration(@Body body: AdScreenConfigurationBody): Single<AdScreenConfigurationResponse>

    @POST("${API_VERSION}/adAppConfiguration")
    fun getAdAppConfiguration(@Body body: AdAppConfigurationBody): Single<AdAppConfigurationResponse>

    @GET("${API_VERSION}/needShowRewardedVideoDialog")
    fun needShowRewardedVideoDialog(
        @Query("serial_tmdb_id") serialTmdbId: Int,
        @Query("user_id") userId: Int
    ): Single<NeedShowRewardedDialogResponse>
}