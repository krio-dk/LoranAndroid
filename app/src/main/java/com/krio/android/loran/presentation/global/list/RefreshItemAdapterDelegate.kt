package com.krio.android.loran.presentation.global.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayRefreshItem
import kotlinx.android.synthetic.main.item_refresh.view.*

/**
 * Created by Dmitriy Kolmogorov on 17.02.2018.
 */
class RefreshItemAdapterDelegate(private val clickListener: () -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayRefreshItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_refresh, parent, false)
        return ProgressViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>,
                                  position: Int,
                                  viewHolder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) {
        (viewHolder as ProgressViewHolder).bind(items[position] as DisplayRefreshItem)
    }

    class ProgressViewHolder(val view: View, clickListener: () -> Unit) : RecyclerView.ViewHolder(view) {
        init {
            view.refreshButton.setOnClickListener { clickListener.invoke() }
        }

        fun bind(item: DisplayRefreshItem) {
            view.description.text = item.description
        }
    }
}