package com.krio.android.loran.entity.domain.core.main

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class Translation(
        @SerializedName("translator_id") val translatorId: Int,
        @SerializedName("translator_name") val translatorName: String,
        @SerializedName("translation_air_date") val translationAirDate: String? = null,
        @SerializedName("is_favorite") val isFavorite: Boolean
)