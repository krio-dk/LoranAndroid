package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 08.06.2018.
 */
data class AddFavoriteBody(
    @SerializedName ("serial_tmdb_id") val serialTmdbId: Int,
    @SerializedName ("translator_id") val translatorId: Int,
    @SerializedName ("ad_watched") val adWatched: Boolean,
    @SerializedName ("user_id") val userId: Int?
)