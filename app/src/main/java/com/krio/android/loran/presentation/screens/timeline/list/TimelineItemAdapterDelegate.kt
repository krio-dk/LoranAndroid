package com.krio.android.loran.presentation.screens.timeline.list

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.timeline.DisplayTimelineItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.StillSize
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import kotlinx.android.synthetic.main.item_timeline_update.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class TimelineItemAdapterDelegate(private val clickListener: (DisplayTimelineItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayTimelineItem


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_timeline_update, parent, false)
        return TimelineViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as TimelineViewHolder).bind(items[position] as DisplayTimelineItem)
    }

    class TimelineViewHolder(val view: View, clickListener: (DisplayTimelineItem) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var displayTimelineItem: DisplayTimelineItem

        init {
            view.setOnClickListener {
                clickListener.invoke(displayTimelineItem)
            }
        }

        fun bind(timelineItem: DisplayTimelineItem) {

            this.displayTimelineItem = timelineItem

            view.backdrop.setImageResource(R.drawable.placeholder_backdrop)

            val stillPath = timelineItem.stillPath
            if (stillPath != null) {
                GlideApp.with(view.context)
                        .load(ImagesConfiguration.getStillFullPath(StillSize.STILL_SIZE_780, stillPath))
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(view.backdrop)
            } else {
                val backdropPath = timelineItem.backdropPath
                if (backdropPath != null) {
                    GlideApp.with(view.context)
                            .load(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_780, backdropPath))
                            .transition(DrawableTransitionOptions.withCrossFade())
                            .into(view.backdrop)
                }
            }

            if (timelineItem.isSeen) {
                val colorMatrix = ColorMatrix().apply { setSaturation(0f) }
                val filter = ColorMatrixColorFilter(colorMatrix)
                view.backdrop.colorFilter = filter

            } else {
                view.backdrop.clearColorFilter()
            }

            val textColor = ContextCompat.getColor(view.context, if (timelineItem.isSeen) R.color.colorDarkSecondaryText else R.color.colorDarkPrimaryText)
            with (view) {
                episodeName.setTextColor(textColor)
                serialName.setTextColor(textColor)
                translator.setTextColor(textColor)
                episodeNumber.setTextColor(textColor)
                seasonNumber.setTextColor(textColor)
            }

            view.translationDate.text = DateTimeUtils.getFormattedStringDateTime(timelineItem.translationDate)

            view.episodeName.text = timelineItem.episodeName
            view.serialName.text = timelineItem.serialName

            view.translator.text = timelineItem.translatorName

            view.episodeNumber.text = timelineItem.episodeNumber.toString()
            view.seasonNumber.text = timelineItem.seasonNumber.toString()

            view.ribbon.text = view.context.getString(if (timelineItem.isSeen) R.string.timeline_text_seen else R.string.timeline_text_not_seen)

            view.ribbon.setTextColor(ContextCompat.getColor(view.context, if (timelineItem.isSeen) R.color.colorLinkText else R.color.colorWhitePrimaryText))
            view.ribbon.setBackgroundColor(ContextCompat.getColor(view.context, if (timelineItem.isSeen) R.color.colorWhiteBackground else R.color.colorLinkText))

            view.ribbonBackground.setBackgroundColor(ContextCompat.getColor(view.context, if (timelineItem.isSeen) R.color.colorRibbonWhite else R.color.colorRibbonBlue))
        }
    }
}