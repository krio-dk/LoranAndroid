package com.krio.android.loran.ui.screens.person

import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import at.favre.lib.dali.Dali
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPerson
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.screens.person.PersonPresenter
import com.krio.android.loran.presentation.screens.person.PersonView
import com.krio.android.loran.presentation.screens.person.list.PhotoItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.ZeroViewHolder
import kotlinx.android.synthetic.main.fragment_person.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick
import toothpick.config.Module
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
class PersonFragment : BaseFragment(), PersonView {

    companion object {
        private const val ARGS_INIT_PARAMS = "cast_item"
        private const val ARGS_PARAM_PARENT_SCOPE = "parent scope"

        fun newInstance(parentScope: String, initParams: PersonPresenter.InitParams) = PersonFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGS_INIT_PARAMS, initParams)
                putString(ARGS_PARAM_PARENT_SCOPE, parentScope)
            }
        }
    }

    override val layoutRes = R.layout.fragment_person

    private val adapter = PhotoAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    @InjectPresenter
    lateinit var presenter: PersonPresenter

    @ProvidePresenter
    fun providePresenter(): PersonPresenter {
        val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String
        val scope = Toothpick.openScopes(parentScope, DI.personScope(parentScope)).apply {
            installModules(object : Module() {
                init {
                    bind(PersonPresenter.InitParams::class.java).toInstance(arguments!![ARGS_INIT_PARAMS] as PersonPresenter.InitParams)
                }
            })
        }

        return scope.getInstance(PersonPresenter::class.java).apply {
            Toothpick.closeScope(DI.personScope(parentScope))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        recyclerView.apply {
            layoutManager = object : LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false) {
                override fun canScrollHorizontally(): Boolean {
                    return (adapter?.itemCount ?: 0) > 1
                }
            }

            adapter = this@PersonFragment.adapter

            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    parent.adapter?.let { adapter ->

                        if (adapter.itemCount == 1) {
                            val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)

                            val topPadding = fullPadding
                            val bottomPadding = fullPadding

                            outRect.set(fullPadding, topPadding, fullPadding, bottomPadding)

                        } else {
                            val layoutParams = view.layoutParams as RecyclerView.LayoutParams

                            val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)
                            val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)

                            val topPadding = fullPadding
                            val bottomPadding = fullPadding

                            val count = adapter.itemCount

                            when (layoutParams.viewAdapterPosition) {
                                0 -> outRect.set(fullPadding, topPadding, halfPadding, bottomPadding)
                                count - 1 -> outRect.set(0, topPadding, fullPadding, bottomPadding)
                                else -> outRect.set(0, topPadding, halfPadding, bottomPadding)
                            }
                        }
                    }
                }
            })
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.getData()
        }
    }

    override fun showTitle(title: String) {
        toolbarTitle?.text = title
        toolbarTitle?.typeface = null
    }

    override fun showData(tmdbPerson: TmdbPerson) {

        var birthdayDate: Date? = null
        if (!tmdbPerson.birthday.isNullOrEmpty()) {

            val format = if (tmdbPerson.birthday != null && tmdbPerson.birthday.length == 4) {
                SimpleDateFormat("yyyy", Locale.getDefault())
            } else {
                SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            }

            birthdayDate = format.parse(tmdbPerson.birthday)
            birthday?.text = SimpleDateFormat("d MMMM yyyy", Locale.getDefault()).format(birthdayDate)
        } else {
            birthday?.text = "-"
        }

        if (tmdbPerson.deathday.isNullOrEmpty()) {

            extraCaption?.text = getString(R.string.person_text_age)

            if (birthdayDate != null) {
                extra?.text = getDiffYears(birthdayDate, Date()).toString()
            } else {
                extra?.text = "-"
            }

        } else {
            extraCaption?.text = getString(R.string.person_text_day_of_death)
            val deathdayDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(tmdbPerson.deathday)
            val formattedDeathdayDate = SimpleDateFormat("d MMMM yyyy", Locale.getDefault()).format(deathdayDate)

            if (birthdayDate != null) {
                extra?.text = "$formattedDeathdayDate (${getDiffYears(birthdayDate, deathdayDate)})"
            } else {
                extra?.text = "-"
            }
        }

        placeOfBirth?.text = tmdbPerson.placeOfBirth

        cardView?.visible(true)
    }

    fun getDiffYears(first: Date, last: Date): Int {
        val a = getCalendar(first)
        val b = getCalendar(last)
        var diff = b.get(YEAR) - a.get(YEAR)
        if (a.get(MONTH) > b.get(MONTH) || a.get(MONTH) == b.get(MONTH) && a.get(DATE) > b.get(DATE)) {
            diff--
        }
        return diff
    }

    fun getCalendar(date: Date): Calendar {
        val cal = Calendar.getInstance(Locale.US)
        cal.time = date
        return cal
    }

    override fun showPhotos(photos: List<String>) {
        if (photos.size == 1) {
            recyclerView?.layoutParams?.width = ViewGroup.LayoutParams.WRAP_CONTENT
        }
        adapter.setData(photos)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun showBackdrop(url: String) {
        GlideApp.with(this)
                .asBitmap()
                .load(url)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        Dali.create(activity)
                                .load(resource)
                                .placeholder(R.drawable.placeholder_backdrop)
                                .blurRadius(25)
                                .downScale(2)
                                .concurrent()
                                .reScale()
                                .skipCache()
                                .into(backdrop)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // do nothing
                    }
                })
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class PhotoAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(PhotoItemAdapterDelegate())
        }

        fun setData(photos: List<Any>) {
            items.clear()
            items.addAll(photos)
            notifyDataSetChanged()
        }
    }
}