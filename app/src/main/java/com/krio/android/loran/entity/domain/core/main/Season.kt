package com.krio.android.loran.entity.domain.core.main

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.materialdata.SeasonMaterialData

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class Season (
        @SerializedName("season_number") val seasonNumber: Int,
        @SerializedName("episodes") val episodes: MutableList<Episode> = mutableListOf(),
        @SerializedName("material_data") val materialData: SeasonMaterialData = SeasonMaterialData()
)