package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.transfer.response.ScheduleResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class ScheduleRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun getSchedule(
            page: Int,
            userId: Int
    ): Single<ScheduleResponse> = loranApi
            .getSchedule(page, userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}