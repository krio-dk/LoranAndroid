package com.krio.android.loran.presentation.screens.posters.list


import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.posters.DisplayPosterItem
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.model.data.glide.GlideApp
import kotlinx.android.synthetic.main.item_poster.view.*


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class PosterItemAdapterDelegate(private val clickListener: (DisplayPosterItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayPosterItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_poster, parent, false)
        return PosterViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as PosterViewHolder).bind(items[position] as DisplayPosterItem)
    }

    class PosterViewHolder(val view: View, clickListener: (DisplayPosterItem) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var displayPosterItem: DisplayPosterItem

        init {
            view.setOnClickListener { clickListener.invoke(displayPosterItem) }
        }

        fun bind(posterItem: DisplayPosterItem) {
            this.displayPosterItem = posterItem

            GlideApp.with(view.context)
                    .load(ImagesConfiguration.getPosterFullPath(PosterSize.POSTER_SIZE_342, posterItem.posterPath))
                    .placeholder(R.drawable.placeholder_cover)
                    .into(view.imageView)

        }
    }
}