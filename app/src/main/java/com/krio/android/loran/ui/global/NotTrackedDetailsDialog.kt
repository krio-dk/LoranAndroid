package com.krio.android.loran.ui.global

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.krio.android.loran.R
import kotlinx.android.synthetic.main.dialog_subscription.*


/**
 * Created by Dmitriy Kolmogorov on 16.06.2019.
 */
class NotTrackedDetailsDialog : DialogFragment() {

    var subscriptionClickListener: () -> Unit = { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.dialog_not_tracked_details, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cancelButton.setOnClickListener { dismiss() }
        detailsButton.setOnClickListener {
            subscriptionClickListener.invoke()
            dismiss()
        }
    }
}