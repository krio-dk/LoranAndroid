package com.krio.android.loran.toothpick.provider

import android.content.Context
import at.favre.lib.dali.Dali
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class DaliProvider @Inject constructor(private val context: Context) : Provider<Dali> {

    override fun get(): Dali = Dali.create(context)
}