package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.domain.dataitems.main.ScheduleItem
import com.krio.android.loran.entity.presentation.displayitems.schedule.DisplayScheduleItem
import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeason
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerial
import com.krio.android.loran.entity.transfer.response.ScheduleResponse
import com.krio.android.loran.extensions.timeZoneDateTime
import com.krio.android.loran.model.data.server.loran.LoranConfig.TRANSLATOR_ORIGINAL_ID
import com.krio.android.loran.model.data.server.tmdb.TmdbResourceNotFoundException
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.data.utils.LanguageUtils
import com.krio.android.loran.model.data.utils.LanguageUtils.isRussianSpeaking
import com.krio.android.loran.model.repository.ScheduleRepository
import com.krio.android.loran.model.repository.SerialRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import retrofit2.HttpException
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class ScheduleInteractor @Inject constructor(
        private val scheduleRepository: ScheduleRepository,
        private val serialRepository: SerialRepository,
        private val prefs: Prefs
) {

    init {
        Timber.d("Init")
    }

    private var presentTmdbSerials = emptyList<TmdbSerial>()
    private var presentSeasons = emptyList<Pair<Int, TmdbSeason>>()

    fun getScheduleItems(page: Int): Single<List<DisplayScheduleItem>> {
        return getSchedule(page)
                .map { response ->
                    response.scheduleItems?.map {

                        val networks = it.materialData.networks

                        val translatorName = if (it.translatorId == TRANSLATOR_ORIGINAL_ID && networks != null) {
                            when  {
                                isRussianSpeaking() -> "${networks.joinToString()} (${it.translatorName})"
                                else -> networks.joinToString()
                            }
                        } else {
                            it.translatorName
                        }

                        DisplayScheduleItem(
                                it.serialTmdbId,
                                it.materialData.serialName ?: it.serialName,
                                it.seasonNumber,
                                it.episodeNumber,
                                it.isSeen,
                                it.translatorId,
                                translatorName,
                                it.scheduleDate.timeZoneDateTime(),
                                it.scheduleType,
                                it.materialData.episodeName,
                                it.materialData.episodeOriginalAirDate,
                                it.materialData.serialBackdropPath,
                                it.materialData.serialPosterPath
                        )
                    } ?: emptyList()
                }
    }

    private fun getSchedule(page: Int): Single<ScheduleResponse> {
        return prefs.userId?.let { userId ->
            scheduleRepository.getSchedule(page, userId)
                    .flatMap { response ->
                        response.scheduleItems?.let { scheduleItems ->
                            fillMissingData(scheduleItems).flatMap {
                                presentTmdbSerials = it.first
                                presentSeasons = it.second
                                Single.just(response)
                            }

                        } ?: Single.just(ScheduleResponse())
                    }
        } ?: Single.just(ScheduleResponse())
    }

    fun fillMissingData(scheduleItems: List<ScheduleItem>): Single<Pair<List<TmdbSerial>, List<Pair<Int, TmdbSeason>>>> {
        return Single.zip(
                getScheduleSerials(scheduleItems, presentTmdbSerials),
                getScheduleSeasons(scheduleItems, presentSeasons),
                BiFunction<List<TmdbSerial>, List<Pair<Int, TmdbSeason>>, Pair<List<TmdbSerial>, List<Pair<Int, TmdbSeason>>>> { newSerials, newSeasons ->
                    val allSerials = presentTmdbSerials + newSerials
                    val allSeasons = presentSeasons + newSeasons

                    scheduleItems.forEach { scheduleItem ->
                        val serial = allSerials.find { it.id == scheduleItem.serialTmdbId }
                        val season = allSeasons.find { it.first == serial?.id && it.second.seasonNumber == scheduleItem.seasonNumber }?.second
                        val episode = season?.episodes?.find { episode -> episode.episodeNumber == scheduleItem.episodeNumber }

                        scheduleItem.materialData.serialName = serial?.name
                        scheduleItem.materialData.episodeName = episode?.name
                        scheduleItem.materialData.episodeOriginalAirDate = episode?.airDate
                        scheduleItem.materialData.serialBackdropPath = serial?.backdropPath
                        scheduleItem.materialData.serialPosterPath = serial?.posterPath
                        scheduleItem.materialData.networks = serial?.networks?.map { it.name }
                    }

                    Pair(allSerials, allSeasons)
                }
        )
    }

    private fun getScheduleSerials(scheduleItems: List<ScheduleItem>, presentTmdbSerials: List<TmdbSerial>): Single<List<TmdbSerial>> {
        val presentSerialsItems = presentTmdbSerials.map { it.id }
        val newSerialItems = scheduleItems.filterNot { presentSerialsItems.contains(it.serialTmdbId) }
        val uniqueSerialItems = newSerialItems.distinctBy { it.serialTmdbId }

        return Observable
                .fromIterable(uniqueSerialItems)
                .flatMap { scheduleItem ->
                    delayedRequest(serialRepository.getTmdbSerial(scheduleItem.serialTmdbId))
                            .toObservable()
                            .onErrorResumeNext(Observable.empty())
                }
                .toList()
    }

    private fun getScheduleSeasons(scheduleItems: List<ScheduleItem>, presentSeasons: List<Pair<Int, TmdbSeason>>): Single<List<Pair<Int, TmdbSeason>>> {
        val presentSeasonsItems = presentSeasons.map { Pair(it.first, it.second.seasonNumber) }
        val newSeasonItems = scheduleItems.filterNot { presentSeasonsItems.contains(Pair(it.serialTmdbId, it.seasonNumber)) }
        val uniqueSeasonItems = newSeasonItems.map { Pair(it.serialTmdbId, it.seasonNumber) }.distinct()

        return Observable.fromIterable(uniqueSeasonItems)
                .flatMap { pair ->
                    Observable.zip(
                            Observable.just(pair.first),
                            delayedRequest(serialRepository.getTmdbSeasonDetails(pair.first, pair.second)).toObservable(),
                            BiFunction<Int, TmdbSeason, Pair<Int, TmdbSeason>> { tmdbId, season -> Pair(tmdbId, season) }
                    ).onErrorResumeNext(Observable.empty())
                }
                .toList()
    }

    private fun <T> delayedRequest(request: Single<T>, delay: Long = 0): Single<T> {
        return Single
                .timer(delay, TimeUnit.SECONDS)
                .flatMap { request }
                .onErrorResumeNext { error ->
                    if (error is HttpException && error.code() == 404) {
                        Single.error(TmdbResourceNotFoundException())
                    } else {
                        delayedRequest(request, 1)
                    }
                }
    }
}