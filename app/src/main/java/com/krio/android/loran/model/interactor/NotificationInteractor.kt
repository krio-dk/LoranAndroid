package com.krio.android.loran.model.interactor

import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.core.materialdata.SerialMaterialData
import com.krio.android.loran.model.repository.SerialRepository
import com.krio.android.loran.model.system.resource.ResourceManager
import io.reactivex.Single
import javax.inject.Inject

class NotificationInteractor @Inject constructor(
        private val repository: SerialRepository,
        private val resourceManager: ResourceManager
) {

    fun getSerialName(serialTmdbId: Int): Single<String> = repository
            .getTmdbSerial(serialTmdbId)
            .map { it.name }

    fun getSerialMaterialData(serialTmdbId: Int): Single<SerialMaterialData> = repository
            .getTmdbSerial(serialTmdbId)
            .map { tmdbSerial ->
                SerialMaterialData(
                        backdropPath = tmdbSerial.backdropPath,
                        firstAirDate = tmdbSerial.firstAirDate,
                        homepage = tmdbSerial.homepage,
                        originCountry = tmdbSerial.originCountry,
                        originalLanguage = tmdbSerial.originalLanguage,
                        originalName = tmdbSerial.originalName,
                        overview = tmdbSerial.overview,
                        posterPath = tmdbSerial.posterPath,
                        status = tmdbSerial.status,
                        popularity = tmdbSerial.popularity,
                        voteAverage = tmdbSerial.voteAverage,
                        voteCount = tmdbSerial.voteCount,
                        networks = tmdbSerial.networks.map { it.name }
                )
            }

    fun getEpisodeStillPath(serialTmdbId: Int, seasonNumber: Int, episodeNumber: Int): Single<String> = repository
            .getTmdbSeasonDetails(serialTmdbId, seasonNumber)
            .flatMap {
                Single.just(it.episodes.find { it.episodeNumber == episodeNumber }?.stillPath
                        ?: "")
            }


    fun getEpisodeName(serialTmdbId: Int, seasonNumber: Int, episodeNumber: Int): Single<String> = repository
            .getTmdbSeasonDetails(serialTmdbId, seasonNumber)
            .flatMap {
                Single.just(it.episodes.find { it.episodeNumber == episodeNumber }?.name
                        ?: "${resourceManager.getString(R.string.episodes_item_caption)} #$episodeNumber")
            }
}