package com.krio.android.loran.presentation.screens.serial.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplaySerialDetailsItem
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import kotlinx.android.synthetic.main.item_serial.view.*
import java.util.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class SerialDetailsItemAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplaySerialDetailsItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_serial, parent, false)
        return SerialDetailsViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as SerialDetailsViewHolder).bind(items[position] as DisplaySerialDetailsItem)
    }

    class SerialDetailsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(serialDetailsItem: DisplaySerialDetailsItem) {

            if (serialDetailsItem.posterPath != null) {
                GlideApp.with(view.context)
                        .load(ImagesConfiguration.getPosterFullPath(PosterSize.POSTER_SIZE_342, serialDetailsItem.posterPath))
                        .placeholder(R.drawable.placeholder_cover)
                        .into(view.imageView)
            }

            view.originalName.text = serialDetailsItem.originalName

            view.score.text = serialDetailsItem.rating.toString()

            serialDetailsItem.status?.let {
                view.status.text = view.context.getString(serialDetailsItem.status.description())
            }


            serialDetailsItem.countries?.let {
                view.originCountry.text = serialDetailsItem.countries.joinToString { country -> Locale(serialDetailsItem.originalLanguage, country).displayCountry }
                view.originalLanguage.text = Locale(serialDetailsItem.originalLanguage).displayLanguage
            }

            serialDetailsItem.firstAirDate?.let {
                view.firstAirDate.text = DateTimeUtils.getFormattedStringDate(serialDetailsItem.firstAirDate)
            }


            view.overview.text = if (serialDetailsItem.overview.isNullOrBlank()) view.context.getString(R.string.common_text_no_overview) else serialDetailsItem.overview


            if (serialDetailsItem.overview != null && serialDetailsItem.overview.length > 250) {
                view.more.visible(true)
                view.overview.maxLines = 5
            } else {
                view.more.visible(false)
            }

            view.more.setOnClickListener {
                if (view.overview.maxLines == 5) {
                    view.overview.maxLines = Int.MAX_VALUE
                    view.more.text = view.context.getString(R.string.serial_text_hide)
                } else {
                    view.overview.maxLines = 5
                    view.more.text = view.context.getString(R.string.serial_text_more)
                }
            }
        }
    }
}