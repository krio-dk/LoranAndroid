package com.krio.android.loran.model.data.server.tmdb

/**
 * Created by Dmitriy Kolmogorov on 16.09.2018.
 */
class TmdbResourceNotFoundException : Error()