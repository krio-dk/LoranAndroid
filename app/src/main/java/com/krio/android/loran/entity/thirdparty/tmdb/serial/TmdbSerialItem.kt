package com.krio.android.loran.entity.thirdparty.tmdb.serial

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 02.03.2018.
 */
data class TmdbSerialItem(
        @SerializedName("poster_path") var posterPath: String?,
        @SerializedName("popularity") val popularity: Double,
        @SerializedName("id") val tmdbId: Int,
        @SerializedName("backdrop_path") var backdropPath: String?,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("overview") val overview: String,
        @SerializedName("first_air_date") val firstAirDate: String,
        @SerializedName("origin_country") val originCountry: List<String>,
        @SerializedName("original_language") val originalLanguage: String,
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("name") val name: String,
        @SerializedName("original_name") val originalName: String
)