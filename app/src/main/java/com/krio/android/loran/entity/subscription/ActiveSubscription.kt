package com.krio.android.loran.entity.subscription

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 09/03/2019.
 */
data class ActiveSubscription (
        @SerializedName("product_id") var productId: String,
        @SerializedName("store") var store: SubscriptionStore
)
