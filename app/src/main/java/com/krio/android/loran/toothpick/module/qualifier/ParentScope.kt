package com.krio.android.loran.toothpick.module.qualifier

import javax.inject.Qualifier

/**
 * Created by Dmitriy Kolmogorov on 2019-06-01.
 */
@Qualifier
annotation class ParentScope