package com.krio.android.loran.ui.screens.seasons

import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.favre.lib.dali.Dali
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.seasons.DisplaySeasonItem
import com.krio.android.loran.extensions.dpToPx
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.screens.seasons.SeasonsPresenter
import com.krio.android.loran.presentation.screens.seasons.SeasonsView
import com.krio.android.loran.presentation.screens.seasons.list.SeasonItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.SerialModule
import com.krio.android.loran.toothpick.module.qualifier.ParentScope
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.RewardedVideoDialog
import com.krio.android.loran.ui.global.ZeroViewHolder
import kotlinx.android.synthetic.main.fragment_seasons.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick
import toothpick.config.Module
import java.io.Serializable

/**
 * Created by Dmitriy Kolmogorov on 07.03.2018.
 */
class SeasonsFragment : BaseFragment(), SeasonsView {

    companion object {
        private const val ARGS_INIT_PARAMS = "init_params"
        private const val ARGS_PARAM_PARENT_SCOPE = "parent scope"

        private const val REWARDED_VIDEO_DIALOG_TAG = "rewarded_video_dialog"

        fun newInstance(parentScope: String, initParams: InitParams) = SeasonsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGS_INIT_PARAMS, initParams)
                putString(ARGS_PARAM_PARENT_SCOPE, parentScope)
            }
        }
    }

    data class InitParams(
            val closeSerialRootScopeOnExit: Boolean,
            val presenterParams: SeasonsPresenter.InitParams
    ) : Serializable

    override val layoutRes = R.layout.fragment_seasons

    private val adapter = SeasonsAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    @InjectPresenter
    lateinit var presenter: SeasonsPresenter

    @ProvidePresenter
    fun providePresenter(): SeasonsPresenter {

        val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String
        val initParams = arguments!![ARGS_INIT_PARAMS] as InitParams

        Toothpick.openScopes(parentScope, DI.serialRootScope(parentScope)).apply {
            installModules(SerialModule())
        }

        val scope = Toothpick.openScopes(DI.serialRootScope(parentScope), DI.serialSeasonsScope(parentScope)).apply {
            installModules(object : Module() {
                init {
                    bind(SeasonsPresenter.InitParams::class.java).toInstance(initParams.presenterParams)
                    bind(String::class.java).withName(ParentScope::class.java).toInstance(parentScope)
                }
            })
        }

        return scope.getInstance(SeasonsPresenter::class.java).also {
            Toothpick.closeScope(DI.serialSeasonsScope(parentScope))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        recyclerView.apply {
            setHasFixedSize(true)
            adapter = this@SeasonsFragment.adapter

            layoutManager = object : LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false) {
                override fun canScrollHorizontally(): Boolean {
                    return (adapter?.itemCount ?: 0) > 1
                }
            }

            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    parent.adapter?.let { adapter ->
                        if (adapter.itemCount == 1) {
                            val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)

                            val topPadding = fullPadding
                            val bottomPadding = requireContext().dpToPx(64f).toInt()

                            outRect.set(fullPadding, topPadding, fullPadding, bottomPadding)

                        } else {
                            val layoutParams = view.layoutParams as RecyclerView.LayoutParams

                            val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)
                            val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)

                            val topPadding = fullPadding
                            val bottomPadding = requireContext().dpToPx(64f).toInt()

                            val count = adapter.itemCount

                            when (layoutParams.viewAdapterPosition) {
                                0 -> outRect.set(fullPadding, topPadding, halfPadding, bottomPadding)
                                count - 1 -> outRect.set(0, topPadding, fullPadding, bottomPadding)
                                else -> outRect.set(0, topPadding, halfPadding, bottomPadding)
                            }
                        }
                    }
                }
            })
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.getData()
        }

        favoritesButton.setOnClickListener { presenter.onFavoritesButtonClicked() }
    }

    override fun showTitle(title: String) {
        toolbarTitle?.text = title
        toolbarTitle?.typeface = null
    }

    override fun showTranslator(translatorName: String) {
        translator?.text = translatorName
    }

    override fun showBackdrop(url: String) {
        GlideApp.with(this)
                .asBitmap()
                .load(url)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        Dali.create(activity)
                                .load(resource)
                                .placeholder(R.drawable.placeholder_backdrop)
                                .blurRadius(10)
                                .downScale(2)
                                .concurrent()
                                .reScale()
                                .skipCache()
                                .into(backdrop)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // do nothing
                    }
                })
    }

    override fun showData(displaySeasonItems: List<DisplaySeasonItem>) {
        recyclerView?.visible(true)
        if (displaySeasonItems.size == 1) {
            recyclerView?.layoutParams?.width = ViewGroup.LayoutParams.WRAP_CONTENT
        }
        adapter.setData(displaySeasonItems)
    }

    override fun showFavoritesButton(text: String, textColor: Int) {
        favoritesButton.text = text
        favoritesButton.setTextColor(ContextCompat.getColor(requireContext(), textColor))
        favoritesButton.visible(true)
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun showRewardedVideoDialog(serialName: String, translatorId: Int, translatorName: String, subscriptionCost: String) {
        if (isAdded) {
            val dialogFragment = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG)
            if (dialogFragment == null) {
                val dialog = RewardedVideoDialog.newInstance(serialName, subscriptionCost)
                dialog.dismissListener = { presenter.onDismissRewardedVideoDialog() }
                dialog.subscriptionDetailsClickListener = { presenter.onSubscriptionDetailsClicked() }
                dialog.watchAdClickListener = {
                    dialog.showError(false)
                    dialog.showProgress(true)
                    dialog.enableWatchAdButton(false)
                    presenter.onWatchAdClicked(translatorId, translatorName)
                }
                dialog.isCancelable = false
                dialog.show(childFragmentManager, REWARDED_VIDEO_DIALOG_TAG)
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    override fun hideRewardedVideoDialog() {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.dismiss()
        }
    }

    override fun showRewardedVideoError(show: Boolean, message: String) {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.showError(show, message)
            dialog?.showProgress(false)
            dialog?.enableWatchAdButton(true)
        }
    }

    override fun setRewardedVideoProgress(value: Long) {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.setProgressValue(value)
        }
    }

    override fun onBackPressed() {
        val initParams = arguments!![ARGS_INIT_PARAMS] as InitParams
        if (initParams.closeSerialRootScopeOnExit) {
            val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String
            Toothpick.closeScope(DI.serialRootScope(parentScope))
        }

        presenter.onBackPressed()
    }

    inner class SeasonsAdapter : ListDelegationAdapter<MutableList<Any>>() {

        val delegate = SeasonItemAdapterDelegate({ presenter.onSeasonClicked(it) })

        init {
            items = mutableListOf()
            delegatesManager.addDelegate(delegate)
        }

        fun setData(displaySeasonItems: List<DisplaySeasonItem>) {
            items.clear()
            items.addAll(displaySeasonItems)
            notifyDataSetChanged()
        }
    }

}