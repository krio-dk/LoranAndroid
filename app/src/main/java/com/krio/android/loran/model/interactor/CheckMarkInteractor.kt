package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.transfer.response.AddCheckMarkResponse
import com.krio.android.loran.entity.transfer.response.ResultResponse
import com.krio.android.loran.entity.transfer.response.UserIdResponse
import com.krio.android.loran.fcm.PushTokenSender
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.CheckMarkRepository
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 10.03.2018.
 */
class CheckMarkInteractor @Inject constructor(
        private val repository: CheckMarkRepository,
        private val prefs: Prefs,
        private val pushTokenSender: PushTokenSender
) {

    init {
        Timber.d("Init")
    }

    fun addCheckMark(serialTmdbId: Int, seasonNumber: Int, episodeNumber: Int): Single<AddCheckMarkResponse> =
            repository
                .addCheckMark(serialTmdbId, seasonNumber, episodeNumber, prefs.userId)
                .doOnSuccess { result ->
                    if (result.success && prefs.userId == null) {
                        result.userId?.let { userId ->
                            prefs.userId = userId
                            pushTokenSender.sendPushToken()
                        }
                    }
                }

    fun removeCheckMark(serialTmdbId: Int, seasonNumber: Int, episodeNumber: Int): Single<ResultResponse> {
        return prefs.userId?.let { userId ->
            repository.removeCheckMark(serialTmdbId, seasonNumber, episodeNumber, userId)
        } ?: Single.just(ResultResponse())
    }

    fun addGroupCheckMark(serialTmdbId: Int, seasonNumber: Int, episodesNumbers: List<Int>): Single<AddCheckMarkResponse> =
            repository
                .addGroupCheckMark(serialTmdbId, seasonNumber, episodesNumbers, prefs.userId)
                .doOnSuccess { result ->
                    if (result.success && prefs.userId == null) {
                        result.userId?.let { userId ->
                            prefs.userId = userId
                            pushTokenSender.sendPushToken()
                        }
                    }
                }

    fun removeGroupCheckMark(serialTmdbId: Int, seasonNumber: Int): Single<ResultResponse> {
        return prefs.userId?.let { userId ->
            repository.removeGroupCheckMark(serialTmdbId, seasonNumber, userId)
        } ?: Single.just(ResultResponse())
    }

}