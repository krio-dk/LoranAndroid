package com.krio.android.loran.presentation.screens.about

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.BuildConfig
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import ru.terrakok.cicerone.Router
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 04.09.2018.
 */
@InjectViewState
class AboutPresenter @Inject constructor(@AppNavigation private val router: Router) : BasePresenter<AboutView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showVersion(BuildConfig.VERSION_NAME)
    }

    fun onBackPressed() = router.exit()
}
