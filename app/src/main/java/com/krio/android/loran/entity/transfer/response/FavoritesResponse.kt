package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.main.FavoriteItem

/**
 * Created by Dmitriy Kolmogorov on 15.04.2018.
 */
data class FavoritesResponse(
    @SerializedName("favorite_items") val favoriteItems: List<FavoriteItem>? = null,
    @SerializedName("max_active_serials_count") val maxActiveSerialsCount: Int? = null
)