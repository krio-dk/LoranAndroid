package com.krio.android.loran.entity.domain.dataitems.main

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.materialdata.ScheduleItemMaterialData

/**
 * Created by krio on 05.06.2018.
 */
class ScheduleItem (
        @SerializedName("serial_tmdb_id") val serialTmdbId: Int,
        @SerializedName("serial_name") val serialName: String,
        @SerializedName("season_number") val seasonNumber: Int,
        @SerializedName("episode_number") val episodeNumber: Int,
        @SerializedName("is_seen") val isSeen: Boolean,
        @SerializedName("translator_id") val translatorId: Int,
        @SerializedName("translator_name") val translatorName: String,
        @SerializedName("schedule_date") val scheduleDate: String,
        @SerializedName("schedule_type") val scheduleType: ScheduleType,
        @SerializedName("material_data") val materialData: ScheduleItemMaterialData = ScheduleItemMaterialData()
)
