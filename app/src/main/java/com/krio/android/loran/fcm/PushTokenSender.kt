package com.krio.android.loran.fcm

import com.google.firebase.iid.FirebaseInstanceId
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.PushTokenRepository
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 20.09.2018.
 */
class PushTokenSender @Inject constructor(
        private var pushTokenRepository: PushTokenRepository,
        private var prefs: Prefs
) {

    fun sendPushToken() {
        prefs.userId?.let { userId ->
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                pushTokenRepository
                        .sendPushToken(userId, instanceIdResult.token)
                        .subscribe(
                                { response -> Timber.i("Send push token result: ${response.success}") },
                                { error -> Timber.e(error, "Send push token error") }
                        )
            }
        }
    }

}