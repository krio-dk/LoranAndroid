package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.transfer.body.AddCheckMarkBody
import com.krio.android.loran.entity.transfer.response.ActivateSerialResponse
import com.krio.android.loran.entity.transfer.response.CheckActivationPermissionResponse
import com.krio.android.loran.entity.transfer.response.ResultResponse
import com.krio.android.loran.entity.transfer.response.UserIdResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 02.09.2018.
 */
class ActivateSerialRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun activateSerial(
            serialTmdbId: Int,
            adWatched: Boolean,
            userId: Int
    ): Single<ActivateSerialResponse> = loranApi
            .activateSerial(serialTmdbId, adWatched, userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun deactivateSerial(
        serialTmdbId: Int,
        userId: Int
    ): Single<ResultResponse> = loranApi
        .deactivateSerial(serialTmdbId, userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}