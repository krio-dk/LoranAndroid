package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.presentation.displayitems.posters.DisplayPosterItem
import com.krio.android.loran.entity.presentation.displayitems.search.DisplaySearchItem
import com.krio.android.loran.model.repository.CatalogRepository
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class CatalogInteractor @Inject constructor(private val repository: CatalogRepository) {

    init {
        Timber.d("Init")
    }

    fun getPopularSerialsPosterItems(page: Int): Single<List<DisplayPosterItem>> = repository
            .getPopularTmdbSerials(page)
            .toObservable()
            .flatMap { Observable.fromIterable(it) }
            .filter { it.posterPath != null && it.backdropPath != null }
            .map { tmdbSerialItem ->
                DisplayPosterItem(
                        tmdbSerialItem.tmdbId,
                        tmdbSerialItem.name,
                        tmdbSerialItem.posterPath!!,
                        tmdbSerialItem.backdropPath
                )
            }
            .toList()


    fun getSearchDefaultItems(page: Int): Single<List<DisplaySearchItem>> =
            getTopRatedSerials(page)
                    .map {
                        it.map { tmdbSerialItem ->
                            DisplaySearchItem(
                                    tmdbSerialItem.tmdbId,
                                    tmdbSerialItem.name,
                                    tmdbSerialItem.backdropPath,
                                    tmdbSerialItem.firstAirDate,
                                    tmdbSerialItem.originalName,
                                    tmdbSerialItem.overview,
                                    tmdbSerialItem.posterPath
                            )
                        }
                    }

    fun getSearchItems(query: String, page: Int): Single<List<DisplaySearchItem>> =
            searchSerials(query, page)
                    .map {
                        it.map { tmdbSerialItem ->
                            DisplaySearchItem(
                                    tmdbSerialItem.tmdbId,
                                    tmdbSerialItem.name,
                                    tmdbSerialItem.backdropPath,
                                    tmdbSerialItem.firstAirDate,
                                    tmdbSerialItem.originalName,
                                    tmdbSerialItem.overview,
                                    tmdbSerialItem.posterPath
                            )
                        }
                    }

    private fun getPopularSerials(page: Int) = repository
            .getPopularTmdbSerials(page)
            .toObservable()
            .flatMap { Observable.fromIterable(it) }
            .filter { it.posterPath != null }
            .toList()

    private fun getTopRatedSerials(page: Int) = repository
            .getTopRatedTmdbSerials(page)
            .toObservable()
            .flatMap { Observable.fromIterable(it) }
            .filter { it.posterPath != null }
            .toList()

    private fun searchSerials(query: String, page: Int) = repository
            .searchTmdbSerials(query, page)
            .toObservable()
            .flatMap { Observable.fromIterable(it) }
            .filter { it.posterPath != null }
            .sorted { o1, o2 -> o2.popularity.compareTo(o1.popularity) }
            .toList()
}