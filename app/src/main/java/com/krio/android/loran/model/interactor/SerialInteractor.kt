package com.krio.android.loran.model.interactor

import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.core.main.Episode
import com.krio.android.loran.entity.domain.core.main.Season
import com.krio.android.loran.entity.domain.core.main.Serial
import com.krio.android.loran.entity.domain.core.main.Translation
import com.krio.android.loran.entity.presentation.displayitems.episodes.DisplayEpisodeItem
import com.krio.android.loran.entity.presentation.displayitems.seasons.DisplaySeasonItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplaySerialDetailsItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayTranslationItem
import com.krio.android.loran.entity.thirdparty.tmdb.episode.TmdbEpisode
import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeason
import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeasonsItem
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerial
import com.krio.android.loran.entity.transfer.response.SerialResponse
import com.krio.android.loran.model.data.server.loran.LoranConfig.TRANSLATOR_ORIGINAL_ID
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.SerialRepository
import com.krio.android.loran.model.system.resource.ResourceManager
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 14.04.2018.
 */
class SerialInteractor @Inject constructor(
    private val repository: SerialRepository,
    private val resourceManager: ResourceManager,
    private val prefs: Prefs
) {

    init {
        Timber.d("Init")
    }

    private var serial: Serial? = null

    private val translatorOriginalName: String
        get() = resourceManager.getString(R.string.common_text_original)


    fun getSerialDetailsItem(serialTmdbId: Int): Single<DisplaySerialDetailsItem> {
        return getSerial(serialTmdbId)
            .map { serial ->
                DisplaySerialDetailsItem(
                    posterPath = serial.materialData.posterPath,
                    originalName = serial.materialData.originalName,
                    firstAirDate = serial.materialData.firstAirDate,
                    rating = serial.materialData.voteAverage,
                    status = serial.materialData.status,
                    countries = serial.materialData.originCountry,
                    originalLanguage = serial.materialData.originalLanguage,
                    overview = serial.materialData.overview
                )
            }
    }

    fun getTranslationItems(serialTmdbId: Int, refresh: Boolean): Single<List<DisplayTranslationItem>> {
        return getSerial(serialTmdbId, refresh).map { serial ->
            mapSerialToTranslatedSeasons(serial)
                .map {
                    DisplayTranslationItem(
                        it.key.translatorId,
                        it.key.translatorName,
                        it.value.size,
                        it.value.toMutableList().flatMap { it.episodes }.count(),
                        it.key.isFavorite
                    )
                }
                .sortedWith(compareByDescending<DisplayTranslationItem> { it.isFavorite }.thenByDescending { it.episodesCount }.thenBy { it.translatorName })
                .toMutableList()
                .also { items ->
                    items.find { it.translatorId == TRANSLATOR_ORIGINAL_ID }?.let { originalItem ->
                        items.remove(originalItem)
                        items.add(0, originalItem)
                    }
                }
        }
    }

    private fun getSerial(serialTmdbId: Int, refresh: Boolean = false): Single<Serial> {
        if (refresh) {
            return loadSerial(serialTmdbId)
        } else {
            return if (serial == null) loadSerial(serialTmdbId) else Single.just(serial)
        }
    }

    // Serial
    private fun loadSerial(serialTmdbId: Int): Single<Serial> =
        Single.zip(
            repository.getTmdbSerial(serialTmdbId),
            repository.getLoranSerial(serialTmdbId, prefs.userId),
            BiFunction<TmdbSerial, SerialResponse, Pair<TmdbSerial, Serial?>> { tmdbSerial, loranSerialResponse ->
                return@BiFunction Pair(tmdbSerial, loranSerialResponse.serial)
            })
            .flatMap { pair ->
                val tmdbSerial = pair.first
                val serial = pair.second

                if (serial != null) {
                    Single.just(Pair(tmdbSerial, serial))
                } else {
                    getOriginalSeasons(tmdbSerial)
                        .flatMap { seasons ->
                            val serial = Serial(
                                serialTmdbId = tmdbSerial.id,
                                serialName = tmdbSerial.name,
                                seasons = seasons.toMutableList()
                            )
                            Single.just(Pair(tmdbSerial, serial))
                        }
                }
            }
            .flatMap { pair ->
                val tmdbSerial = pair.first
                val serial = pair.second

                fillSerialMaterialData(serial, tmdbSerial)
                fillSeasonsMaterialData(serial.seasons, tmdbSerial.seasons)

                this.serial = serial
                Single.just(serial)
            }

    private fun fillSerialMaterialData(serial: Serial, tmdbSerial: TmdbSerial) {
        serial.materialData.apply {
            backdropPath = tmdbSerial.backdropPath
            firstAirDate = tmdbSerial.firstAirDate
            homepage = tmdbSerial.homepage
            originCountry = tmdbSerial.originCountry
            originalLanguage = tmdbSerial.originalLanguage
            originalName = tmdbSerial.originalName
            overview = tmdbSerial.overview
            posterPath = tmdbSerial.posterPath
            status = tmdbSerial.status
            popularity = tmdbSerial.popularity
            voteAverage = tmdbSerial.voteAverage
            voteCount = tmdbSerial.voteCount

        }
    }


    private fun fillSeasonsMaterialData(seasons: List<Season>, tmdbSeasons: List<TmdbSeasonsItem>) {
        seasons.forEach { season ->
            tmdbSeasons.find { it.seasonNumber == season.seasonNumber }?.let { tmdbSeasonsItem ->
                season.materialData.apply {
                    seasonName = "${resourceManager.getString(R.string.seasons_item_caption)} ${tmdbSeasonsItem.seasonNumber}"
                    posterPath = tmdbSeasonsItem.posterPath
                    airDate = tmdbSeasonsItem.airDate
                }
            }
        }
    }


    private fun getOriginalSeasons(tmdbSerial: TmdbSerial): Single<List<Season>> =
        getNumberOfSerialReleasedEpisodes(tmdbSerial)
            .toObservable()
            .flatMap { numberOfReleasedEpisodes ->
                Observable.fromIterable(tmdbSerial.seasons)
                    .filter { it.seasonNumber != 0 }
                    .map { seasonItem ->
                        val isLastSeason = seasonItem.seasonNumber == tmdbSerial.numberOfSeasons

                        val episodesNumbers = if (isLastSeason) {
                            val numberOfEpisodesExcludeLastSeasonAndSpecial = tmdbSerial.seasons
                                .filterNot { it.seasonNumber == 0 }
                                .filterNot { it.seasonNumber == tmdbSerial.numberOfSeasons }
                                .sumBy { it.episodeCount }

                            val numberOfReleasedEpisodesInLastSeason = (numberOfReleasedEpisodes - numberOfEpisodesExcludeLastSeasonAndSpecial).toInt()
                            listOf(1..numberOfReleasedEpisodesInLastSeason).flatten()

                        } else {
                            listOf(1..seasonItem.episodeCount).flatten()
                        }

                        Season(
                            seasonItem.seasonNumber,
                            episodesNumbers.map { episodeNumber ->
                                Episode(
                                    episodeNumber,
                                    false,
                                    mutableListOf(
                                        Translation(
                                            translatorId = TRANSLATOR_ORIGINAL_ID,
                                            translatorName = translatorOriginalName,
                                            isFavorite = false
                                        )
                                    )
                                )
                            }.toMutableList()
                        )
                    }
            }
            .toList()

    private fun getNumberOfSerialReleasedEpisodes(serial: TmdbSerial): Single<Long> {
        return repository
            .getTmdbSeasonDetails(serial.id, serial.numberOfSeasons)
            .toObservable()
            .flatMap { season -> Observable.fromIterable(season.episodes) }
            .filter { episode ->
                if (episode.airDate.isNullOrBlank()) {
                    true
                } else {
                    val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                    val episodeAirDate = format.parse(episode.airDate)
                    val currentDate = Date()
                    episodeAirDate > currentDate
                }
            }
            .count()
            .map { numberOfNotReleasedEpisodes ->
                val numberOfEpisodesWithoutSpecial = serial.seasons.filterNot { it.seasonNumber == 0 }.sumBy { it.episodeCount }
                numberOfEpisodesWithoutSpecial - numberOfNotReleasedEpisodes
            }
    }

    // Seasons

    fun getSeasonItems(serialTmdbId: Int, translatorId: Int): Single<List<DisplaySeasonItem>> {
        return getSerial(serialTmdbId)
            .map { serial ->

                val translatedSeasons = mapSerialToTranslatedSeasons(serial)
                val key = translatedSeasons.keys.find { it.translatorId == translatorId }

                translatedSeasons[key]?.map {
                    DisplaySeasonItem(
                        it.seasonNumber,
                        it.episodes.count(),
                        it.materialData.airDate,
                        it.materialData.seasonName,
                        it.materialData.posterPath
                    )
                }
            }
    }

    // Episodes

    fun getEpisodeItems(serialTmdbId: Int, seasonNumber: Int, translatorId: Int, refresh: Boolean): Single<List<DisplayEpisodeItem>> {
        return Single.zip(
            getSerial(serialTmdbId, refresh),
            repository.getTmdbSeasonDetails(serialTmdbId, seasonNumber),
            BiFunction<Serial, TmdbSeason, Pair<Serial, TmdbSeason>> { serial, tmdbSeason ->
                Pair(serial, tmdbSeason)
            }

        ).map { pair ->

            val serial = pair.first
            val tmdbSeason = pair.second

            val translatedSeasons = mapSerialToTranslatedSeasons(serial)
            val key = translatedSeasons.keys.find { it.translatorId == translatorId }
            val season = translatedSeasons[key]?.find { it.seasonNumber == seasonNumber }

            fillEpisodesMaterialData(season!!.episodes, tmdbSeason.episodes)

            season.episodes.map {
                DisplayEpisodeItem(
                    it.episodeNumber,
                    it.isSeen,
                    it.materialData.airDate,
                    it.materialData.episodeName,
                    it.materialData.overview,
                    it.materialData.stillPath
                )
            }
        }.observeOn(AndroidSchedulers.mainThread())
    }

    private fun fillEpisodesMaterialData(episodes: List<Episode>, tmdbEpisodes: List<TmdbEpisode>) {
        episodes.forEach { episode ->
            tmdbEpisodes.find { it.episodeNumber == episode.episodeNumber }?.let { tmdbEpisode ->
                episode.materialData.apply {
                    episodeName = tmdbEpisode.name
                    stillPath = tmdbEpisode.stillPath
                    airDate = tmdbEpisode.airDate
                    overview = tmdbEpisode.overview
                }
            }
        }
    }


    // Extra

    private fun mapSerialToTranslatedSeasons(serial: Serial): Map<TranslatedSeasonKey, List<Season>> {
        val translatedSeasons = mutableMapOf<TranslatedSeasonKey, MutableList<Season>>()

        serial.seasons.forEach { season ->
            season.episodes.forEach { episode ->
                episode.translations.forEach { translation ->

                    val key = TranslatedSeasonKey(translation.translatorId, translation.translatorName, translation.isFavorite)

                    val resultSeasons = translatedSeasons[key]
                        ?: mutableListOf<Season>().also { emptyList ->
                            translatedSeasons[key] = emptyList
                        }

                    val resultSeason = resultSeasons.find {
                        it.seasonNumber == season.seasonNumber
                    }
                        ?: Season(season.seasonNumber, mutableListOf(), season.materialData).also { newSeason ->
                            resultSeasons.add(newSeason)
                        }

                    val resultEpisode = resultSeason.episodes.find {
                        it.episodeNumber == episode.episodeNumber
                    }
                        ?: Episode(episode.episodeNumber, episode.isSeen, mutableListOf(translation)).also { newEpisode ->
                            resultSeason.episodes.add(newEpisode)
                        }

                    if (!resultEpisode.translations.contains(translation)) {
                        resultEpisode.translations.add(translation)
                    }
                }
            }
        }

        return translatedSeasons
    }

    data class TranslatedSeasonKey(
        val translatorId: Int,
        val translatorName: String,
        val isFavorite: Boolean
    )
}