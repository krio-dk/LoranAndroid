package com.krio.android.loran.presentation.screens.intro

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface IntroView : MvpView {

    fun showNextPage()
    fun setCloseButtonText(text: String)

    @StateStrategyType(SkipStrategy::class)
    fun showConfirmExitDialog(onExitConfirmed: () -> Unit)
}