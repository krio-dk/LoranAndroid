package com.krio.android.loran.presentation.global

import com.krio.android.loran.extensions.userMessage
import com.krio.android.loran.model.system.resource.ResourceManager
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 19.02.2018.
 */
class ErrorHandler @Inject constructor(private val resourceManager: ResourceManager) {

    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        Timber.e(error)
        messageListener(error.userMessage(resourceManager))
    }
}
