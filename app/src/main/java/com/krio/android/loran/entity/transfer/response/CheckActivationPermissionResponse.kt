package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 07.09.2018.
 */
data class CheckActivationPermissionResponse(
        @SerializedName("permission_granted") val permissionGranted: Boolean,
        @SerializedName("total_activation_count") val totalActivationCount: Int,
        @SerializedName("total_duration_millis") val totalDurationMillis: Long,
        @SerializedName("remaining_activations_count") val remainingActivationsCount: Int,
        @SerializedName("remaining_time") val remainingTime: Long? = null
)