package com.krio.android.loran.model.system.ad

import android.app.Activity
import com.appodeal.ads.*
import com.appodeal.ads.utils.Log
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.BuildConfig
import com.krio.android.loran.entity.domain.core.ad.AdAppConfiguration
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.analytics.Analytics
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.io.Serializable
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

/**
 * Created by Dmitriy Kolmogorov on 2019-05-26.
 */
class AppodealManager(
    private val activity: WeakReference<Activity>,
    private val initParams: InitParams,
    private val prefs: Prefs
) {

    data class InitParams(
        val hasConsent: Boolean,
        val adAppConfiguration: AdAppConfiguration?
    ) : Serializable

    companion object {
        private const val appKey = "7d2e1833bfac19b45f7599232d1a0df4e811809abc7dc3cd"
    }

    private var isInitializePass = false

    private val nativeAdLoadedRelay = PublishRelay.create<Boolean>()

    private val rewardedVideoClosedRelay = PublishRelay.create<Boolean>()
    private var rewardedVideoLoading = false

    init {
        initParams.adAppConfiguration?.let {
            configure(it)
            setNativeCallbacks()
            prefs.userId?.let { userId -> initialize(userId) }
        }
    }

    private fun configure(adAppConfiguration: AdAppConfiguration) {
        if (adAppConfiguration.showVideo) {
            Appodeal.setNativeAdType(Native.NativeAdType.Auto)
            Appodeal.muteVideosIfCallsMuted(true)
        } else {
            Appodeal.setNativeAdType(Native.NativeAdType.NoVideo)
        }

        // Trying to use auto cache and check display rate
        // Appodeal.setAutoCache(Appodeal.NATIVE, false)

        Appodeal.setAutoCache(Appodeal.REWARDED_VIDEO, false)

        if (BuildConfig.DEBUG) {
             Appodeal.setTesting(false)
             Appodeal.setLogLevel(Log.LogLevel.verbose)
        }

        activity.get()?.let {
            Appodeal.disableNetwork(it, AppodealNetworks.VUNGLE)
        }
    }

    private fun setNativeCallbacks() {
        Appodeal.setNativeCallbacks(object : NativeCallbacks {
            override fun onNativeLoaded() {
                println("onNativeLoaded, available: ${Appodeal.getAvailableNativeAdsCount()}")
                nativeAdLoadedRelay.accept(true)
            }

            override fun onNativeClicked(p0: NativeAd?) {
                println("onNativeClicked")
            }

            override fun onNativeFailedToLoad() {
                println("onNativeFailedToLoad, available: ${Appodeal.getAvailableNativeAdsCount()}")
            }

            override fun onNativeShown(nativeAd: NativeAd?) {
                println("onNativeShown, available: ${Appodeal.getAvailableNativeAdsCount()}")
            }

            override fun onNativeShowFailed(p0: NativeAd?) {
                println("onNativeShowFailed, available: ${Appodeal.getAvailableNativeAdsCount()}")
            }

            override fun onNativeExpired() {
                println("onNativeExpired")
            }

        })

        Appodeal.setRewardedVideoCallbacks(object: RewardedVideoCallbacks {
            override fun onRewardedVideoFinished(amount: Double, name: String?) {
                println("Rewarded Video: onFinished, amount: $amount, name: $name")
            }

            override fun onRewardedVideoClosed(finished: Boolean) {
                println("Rewarded Video: onClosed, finished: $finished")
                if (finished) Analytics.logEvent("rewarded_video_watched")
                rewardedVideoClosedRelay.accept(finished)
            }

            override fun onRewardedVideoExpired() {
                println("Rewarded Video: onExpired")
            }

            override fun onRewardedVideoLoaded(p0: Boolean) {
                println("Rewarded Video: onoLoaded")
                rewardedVideoLoading = false
            }

            override fun onRewardedVideoClicked() {
                println("Rewarded Video: onClicked")
            }

            override fun onRewardedVideoFailedToLoad() {
                println("Rewarded Video: onFailedToLoad")
                rewardedVideoLoading = false
            }

            override fun onRewardedVideoShown() {
                println("Rewarded Video: onShown")
            }

            override fun onRewardedVideoShowFailed() {
                println("Rewarded Video: onShowFailed")
            }

        })
    }

    private fun initialize(userId: Int) {
        activity.get()?.let {
            Appodeal.setUserId(userId.toString())
            Appodeal.initialize(it, appKey, Appodeal.NATIVE or Appodeal.REWARDED_VIDEO, initParams.hasConsent)
            isInitializePass = true
            // Appodeal.cache(it, Appodeal.NATIVE)
        }
    }

    private fun performInitializationCheck() {
        if (!isInitializePass) prefs.userId?.let { initialize(it) }
    }

    fun nativeAdLoadedObservable(): Observable<Boolean> = nativeAdLoadedRelay

    fun getNativeAdItem(): NativeAdItem {
        return initParams.adAppConfiguration?.let {
            if (isInitializePass) {
                if (Appodeal.isInitialized(Appodeal.NATIVE)) {
                    println("getNativeAdItem, available: ${Appodeal.getAvailableNativeAdsCount()}")
                    // Trying to use auto cache and check display rate
                    // if (Appodeal.getAvailableNativeAdsCount() <= 1) {
                    //     activity.get()?.let { Appodeal.cache(it, Appodeal.NATIVE) }
                    // }
                    Appodeal.getNativeAds(1).firstOrNull()?.let { NativeAdItem.Filled(it) } ?: NativeAdItem.Empty

                } else {
                    return NativeAdItem.NativeAdNotInitialized
                }

            } else {
                performInitializationCheck()
                return NativeAdItem.Empty
            }

        } ?: NativeAdItem.AdDisabled
    }

    fun cacheRewardedVideo() {
        initParams.adAppConfiguration?.let {
            if (isInitializePass) {
                if (Appodeal.isInitialized(Appodeal.REWARDED_VIDEO)) {
                    println("Rewarded Video: cache request, isLoaded = ${Appodeal.isLoaded(Appodeal.REWARDED_VIDEO)}, isLoading = $rewardedVideoLoading")
                    if (!Appodeal.isLoaded(Appodeal.REWARDED_VIDEO) && !rewardedVideoLoading) {
                        activity.get()?.let {
                            println("Rewarded Video: CACHE")
                            rewardedVideoLoading = true
                            Appodeal.cache(it, Appodeal.REWARDED_VIDEO)
                        }
                    }
                }
            } else {
                performInitializationCheck()
            }
        }
    }

    fun showRewardedVideo(progress: (value: Long) -> Unit): Single<Boolean> {
        initParams.adAppConfiguration?.let {
            if (isInitializePass) {
                if (Appodeal.isInitialized(Appodeal.REWARDED_VIDEO)) {
                    cacheRewardedVideo()

                    val relay = PublishRelay.create<Single<Boolean>>()
                    var intervalDisposable: Disposable? = null
                    return relay
                        .firstOrError()
                        .flatMap { it }
                        .doOnDispose { intervalDisposable?.dispose() }
                        .doOnSubscribe {
                            activity.get()?.let { activity ->
                                var shown = false
                                intervalDisposable =
                                    Observable.intervalRange(0, 26, 0, 1, TimeUnit.SECONDS)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .doOnComplete {
                                            println("Rewarded Video: doOnComplete, shown = $shown")
                                            if (shown) relay.accept(
                                                rewardedVideoClosedRelay.take(1).firstOrError()
                                            )
                                            else relay.accept(
                                                Single.error(
                                                    CantShowRewardedVideoException()
                                                )
                                            )
                                        }
                                        .doOnDispose {
                                            println("Rewarded Video: doOnDispose, shown = $shown")
                                            progress(0)
                                            if (shown) relay.accept(
                                                rewardedVideoClosedRelay.take(1).firstOrError()
                                            )
                                        }
                                        .doOnTerminate {
                                            println("Rewarded Video: doOnTerminate, shown = $shown")
                                        }
                                        .doAfterTerminate {
                                            println("Rewarded Video: doAfterTerminate, shown = $shown")
                                        }
                                        .subscribe { seconds ->
                                            println("Rewarded Video: sec = $seconds")
                                            progress(25 - seconds)

                                            if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO)) {
                                                shown =
                                                    Appodeal.show(activity, Appodeal.REWARDED_VIDEO)
                                            }

                                            println("Rewarded Video: shown = $shown")
                                            if (shown) intervalDisposable?.dispose()
                                            else if (!rewardedVideoLoading) cacheRewardedVideo()
                                        }

                            } ?: relay.accept(Single.error(CantShowRewardedVideoException()))
                        }

                } else {
                    return Single.error(RewardedVideoAdNotInitializedException())
                }

            } else {
                performInitializationCheck()
                return Single.error(CantShowRewardedVideoException())
            }

        } ?: return Single.error(AdDisabledException())
    }

    class CantShowRewardedVideoException : Error("Cant show rewarded video")
    class AdDisabledException : Error("Ad disabled")
    class RewardedVideoAdNotInitializedException : Error("Rewarded video ad not initialized")

    sealed class NativeAdItem {
        object AdDisabled: NativeAdItem()
        object NativeAdNotInitialized: NativeAdItem()
        object Empty: NativeAdItem()
        data class Filled(val nativeAd: NativeAd): NativeAdItem()
    }

}