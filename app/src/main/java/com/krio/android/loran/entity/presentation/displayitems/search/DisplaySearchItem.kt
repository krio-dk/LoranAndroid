package com.krio.android.loran.entity.presentation.displayitems.search

/**
 * Created by Dmitriy Kolmogorov on 14.04.2018.
 */
data class DisplaySearchItem(
        val serialTmdbId: Int,
        val serialName: String,
        val backdropPath: String?,
        val firstAirDate: String?,
        val serialOriginalName: String?,
        val overview: String?,
        val posterPath: String?
)