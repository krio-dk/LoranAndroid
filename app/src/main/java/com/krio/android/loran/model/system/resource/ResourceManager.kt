package com.krio.android.loran.model.system.resource

import android.content.Context
import androidx.annotation.StringRes
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class ResourceManager @Inject constructor(private val context: Context) {
    fun getString(@StringRes id: Int) = context.getString(id)
    fun getString(@StringRes id: Int, param1: String, param2: String? = null) = context.getString(id, param1, param2)
    fun getQuantityString(id: Int, quantity: Int): String = context.resources.getQuantityString(id, quantity, quantity)
}
