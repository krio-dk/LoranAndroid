package com.krio.android.loran.presentation.screens.favorite

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.*
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteCaptionActive
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteItem

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface FavoriteView : MvpView {
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showEmptyProgress(show: Boolean)
    fun showRefreshProgress(show: Boolean)
    fun showNotActiveWarning(show: Boolean)
    fun enableToolbarScrolling(enable: Boolean)
    fun showData(show: Boolean, data: List<Any>)


    @StateStrategyType(OneExecutionStateStrategy::class)
    fun removeItem(item: DisplayFavoriteItem)


    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateCaptionActiveItem(item: DisplayFavoriteCaptionActive)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun removeCaptionNotActiveItem()


    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateItem(item: DisplayFavoriteItem)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun cancelUpdating()


    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)


    @StateStrategyType(SkipStrategy::class)
    fun showRemoveFromFavoritesDialog(onRemoveConfirmed: () -> Unit)

    @StateStrategyType(SkipStrategy::class)
    fun showActiveSerialsLimitReachedDialog()

    @StateStrategyType(SkipStrategy::class)
    fun showRewardedVideoDialog(serialTmdbId: Int, serialName: String, subscriptionCost: String)


    @StateStrategyType(OneExecutionStateStrategy::class)
    fun hideRewardedVideoDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showRewardedVideoError(show: Boolean, message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setRewardedVideoProgress(value: Long)
}