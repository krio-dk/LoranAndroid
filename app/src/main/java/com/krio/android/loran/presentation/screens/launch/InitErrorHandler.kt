package com.krio.android.loran.presentation.screens.launch

import com.krio.android.loran.R
import com.krio.android.loran.extensions.userMessage
import com.krio.android.loran.model.initialization.PurchasesNotValidException
import com.krio.android.loran.model.initialization.ServerErrorException
import com.krio.android.loran.model.initialization.SubscriptionsNotSupportedException
import com.krio.android.loran.model.system.billing.BadBillingResponseException
import com.krio.android.loran.model.system.billing.BillingClientNotReadyException
import com.krio.android.loran.model.system.billing.QueryType
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.model.system.secure.PiracyDetectedException
import com.krio.android.loran.model.system.secure.PiracyErrorException
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import com.github.javiersantos.piracychecker.enums.PiracyCheckerError.*

/**
 * Created by Dmitriy Kolmogorov on 15.09.2018.
 */
class InitErrorHandler(private val resourceManager: ResourceManager) {

    fun proceed(error: Throwable, messageListener: (InitErrorDialogConfigurator) -> Unit) {
        Timber.e(error)
        when (error) {
            is BillingClientNotReadyException -> messageListener(BillingClientNotReadyDialogConfigurator(error))
            is BadBillingResponseException -> messageListener(BadBillingResponseDialogConfigurator(error))
            is SubscriptionsNotSupportedException -> messageListener(SubscriptionsNotSupportedDialogConfigurator(error))
            is ServerErrorException -> messageListener(ServerErrorDialogConfigurator(error))
            is PurchasesNotValidException -> messageListener(PurchasesNotValidDialogConfigurator(error))
            is PiracyDetectedException -> messageListener(PiracyDetectedConfigurator(error))
            is PiracyErrorException -> messageListener(PiracyErrorConfigurator(error))
            else -> messageListener(CommonDialogConfigurator(error))
        }
    }

    private inner class BillingClientNotReadyDialogConfigurator(
            override val error: BillingClientNotReadyException
    ) : InitErrorDialogConfigurator {
        override val message: String = resourceManager.getString(R.string.initialization_failed_message_billing_client_not_ready)
        override val showRetryButton = true
        override val showSupportButton = false
    }

    private inner class BadBillingResponseDialogConfigurator(
            override val error: BadBillingResponseException
    ) : InitErrorDialogConfigurator {
        override val message: String = when (error.queryType) {
            QueryType.QUERY_ACTIVE_SUBSCRIPTIONS -> resourceManager.getString(R.string.initialization_failed_message_query_active_purchases)
            QueryType.QUERY_PURCHASE_HISTORY -> resourceManager.getString(R.string.initialization_failed_message_query_history)
            QueryType.QUERY_PURCHASES_DETAILS -> throw UnsupportedOperationException("Querying purchases details is not a part of initialization process")
        }

        override val showRetryButton = true
        override val showSupportButton = false
    }

    private inner class SubscriptionsNotSupportedDialogConfigurator(override val error: SubscriptionsNotSupportedException) : InitErrorDialogConfigurator {
        override val message: String = resourceManager.getString(R.string.initialization_failed_message_subscriptions_not_supported)
        override val showRetryButton = false
        override val showSupportButton = true
    }

    private inner class ServerErrorDialogConfigurator(override val error: ServerErrorException) : InitErrorDialogConfigurator {
        override val message: String = when (error.reason) {
            is SocketTimeoutException,
            is ConnectException -> resourceManager.getString(R.string.initialization_failed_message_server_unavailable)
            else -> resourceManager.getString(R.string.initialization_failed_message_server_error)
        }
        override val showRetryButton = true
        override val showSupportButton = false
    }


    private inner class PurchasesNotValidDialogConfigurator(override val error: PurchasesNotValidException) : InitErrorDialogConfigurator {
        override val message: String = resourceManager.getString(R.string.initialization_failed_message_purchases_not_valid)
        override val showRetryButton = false
        override val showSupportButton = true
    }

    private inner class CommonDialogConfigurator(override val error: Throwable) : InitErrorDialogConfigurator {
        override val message: String = error.userMessage(resourceManager)
        override val showRetryButton = true
        override val showSupportButton = false
    }

    private inner class PiracyDetectedConfigurator(override val error: PiracyDetectedException) : InitErrorDialogConfigurator {
            override val message: String = when {
                error.reason == INVALID_INSTALLER_ID -> resourceManager.getString(R.string.initialization_init_message_piracy_invalid_installer)
                error.reason == PIRATE_APP_INSTALLED && error.app?.name != null -> resourceManager.getString(R.string.initialization_init_message_piracy_pirate_app_installed, error.app.name ?: "")
                error.reason == THIRD_PARTY_STORE_INSTALLED && error.app?.name != null -> resourceManager.getString(R.string.initialization_init_message_piracy_pirate_third_party_store_installed, error.app.name ?: "")
                else -> resourceManager.getString(R.string.initialization_init_message_piracy_detected)
            }
            override val showRetryButton = false
            override val showSupportButton = true
    }

    private inner class PiracyErrorConfigurator(override val error: PiracyErrorException) : InitErrorDialogConfigurator {
        override val message: String = resourceManager.getString(R.string.initialization_init_message_piracy_error)
        override val showRetryButton = true
        override val showSupportButton = true
    }

}