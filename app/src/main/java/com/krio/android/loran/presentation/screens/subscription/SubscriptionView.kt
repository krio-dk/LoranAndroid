package com.krio.android.loran.presentation.screens.subscription

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface SubscriptionView : MvpView {
    fun showEmptyProgress(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun enableToolbarScrolling(enable: Boolean)
    fun showData(show: Boolean, data: List<Any>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}