package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbCredits
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPerson
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPersonImages
import com.krio.android.loran.model.data.server.tmdb.TMDbApi
import com.krio.android.loran.model.data.utils.LanguageUtils
import com.krio.android.loran.model.system.resource.ResourceManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 10.03.2018.
 */
class PersonRepository @Inject constructor(
        private val tmdbApi: TMDbApi,
        private val resourceManager: ResourceManager
) {

    private val defaultLanguage: String
        get() = LanguageUtils.language

    private val cachedTmdbCredits = mutableMapOf<Int, TmdbCredits>()
    private val cachedTmdbPerson = mutableMapOf<Int, TmdbPerson>()
    private val cachedTmdbPersonImages = mutableMapOf<Int, TmdbPersonImages>()

    init {
        Timber.d("Init")
    }

    fun getTmdbCredits(
            serialTmdbId: Int,
            language: String = this.defaultLanguage
    ): Single<TmdbCredits> {
        return cachedTmdbCredits[serialTmdbId]?.let {
            Single.just(it)
        } ?: tmdbApi
                .getCredits(serialTmdbId, language)
                .doAfterSuccess { cachedTmdbCredits.put(serialTmdbId, it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTmdbPerson(
            personId: Int,
            language: String = this.defaultLanguage
    ): Single<TmdbPerson> {
        return cachedTmdbPerson[personId]?.let {
            Single.just(it)
        } ?: tmdbApi
                .getPerson(personId, language)
                .doAfterSuccess { cachedTmdbPerson.put(personId, it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTmdbPersonImages(personId: Int): Single<TmdbPersonImages> {
        return cachedTmdbPersonImages[personId]?.let {
            Single.just(it)
        } ?: tmdbApi
                .getPersonImages(personId)
                .doAfterSuccess { cachedTmdbPersonImages.put(personId, it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}