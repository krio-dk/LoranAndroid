package com.krio.android.loran.presentation.screens.episodes.list

import android.content.res.ColorStateList
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.episodes.DisplayEpisodeItem
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.StillSize
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import kotlinx.android.synthetic.main.item_episode.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class EpisodeItemAdapterDelegate(private val clickListener: (DisplayEpisodeItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayEpisodeItem


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_episode, parent, false)
        return EpisodeViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as EpisodeViewHolder).bind(items[position] as DisplayEpisodeItem)
    }

    class EpisodeViewHolder(val view: View, clickListener: (DisplayEpisodeItem) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var episodeItem: DisplayEpisodeItem

        init {
            view.setOnClickListener { clickListener.invoke(episodeItem) }
        }

        fun bind(episodeItem: DisplayEpisodeItem) {

            this.episodeItem = episodeItem

            view.backdrop.clearColorFilter()
            view.backdrop.setImageResource(R.drawable.placeholder_backdrop)

            val stillPath = episodeItem.stillPath
            if (stillPath != null) {
                GlideApp.with(view.context)
                        .load(ImagesConfiguration.getStillFullPath(StillSize.STILL_SIZE_780, stillPath))
                        .placeholder(R.drawable.placeholder_backdrop)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(view.backdrop)
            }

            if (episodeItem.isSeen) {
                val colorMatrix = ColorMatrix().apply { setSaturation(0f) }
                val filter = ColorMatrixColorFilter(colorMatrix)
                view.backdrop.colorFilter = filter
            }

            view.name.text = episodeItem.episodeName ?: "${view.context.getString(R.string.episodes_item_caption)} #${episodeItem.episodeNumber}"

            view.number.apply {
                text = episodeItem.episodeNumber.toString()
                setTextColor(ContextCompat.getColor(view.context, if (episodeItem.isSeen) R.color.colorLinkText else R.color.colorWhitePrimaryText))
            }

            view.numberContainer.setCardBackgroundColor(ContextCompat.getColor(view.context, if (episodeItem.isSeen) R.color.colorWhiteBackground else R.color.colorLinkText))

            view.date.text = episodeItem.airDate?.let {
                DateTimeUtils.getFormattedStringDate(episodeItem.airDate)
            } ?: view.context.getString(R.string.common_shruggie)

            view.overview.text = if (episodeItem.overview.isNullOrBlank()) view.context.getString(R.string.common_text_no_overview) else episodeItem.overview

            val color = ContextCompat.getColor(view.context, if (episodeItem.isSeen) R.color.colorLinkText else R.color.colorWhitePrimaryText)
            ImageViewCompat.setImageTintList(view.checkMark, ColorStateList.valueOf(color))

            view.checkMarkContainer.setCardBackgroundColor(ContextCompat.getColor(view.context, if (episodeItem.isSeen) R.color.colorWhiteBackground else R.color.colorLinkText))

            val textColor =  ContextCompat.getColor(view.context, if(episodeItem.isSeen) R.color.colorDarkSecondaryText else R.color.colorDarkPrimaryText)
            with(view) {
                name.setTextColor(textColor)
                overview.setTextColor(textColor)
            }

            /*
            if (episodeItem.isUpdating) {
                view.checkMark.visibility = GONE
                view.loaderFrame.visibility = VISIBLE

            } else {
                view.loaderFrame.visibility = GONE
                view.checkMark.visibility = VISIBLE
            }
            */
        }
    }
}