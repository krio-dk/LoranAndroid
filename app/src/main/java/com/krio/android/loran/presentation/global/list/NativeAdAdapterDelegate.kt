package com.krio.android.loran.presentation.global.list

import android.view.LayoutInflater
import android.view.MotionEvent.ACTION_UP
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appodeal.ads.NativeAd
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.core.ad.Ad
import com.krio.android.loran.extensions.dpToPx
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.system.ad.AppodealManager
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.item_native_ad.view.*
import java.util.concurrent.TimeUnit

/**
 * Created by Dmitriy Kolmogorov on 29.05.2019.
 */
class NativeAdAdapterDelegate(
    private val appodealManager: AppodealManager,
    private val closeClickListener: () -> Unit
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =  items[position] is Ad

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_native_ad, parent, false)
        return NativeAdViewHolder(view, appodealManager, closeClickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as NativeAdViewHolder).bind()
    }

    class NativeAdViewHolder(
        private val view: View,
        private val appodealManager: AppodealManager,
        private val closeClickListener: () -> Unit
    ) : RecyclerView.ViewHolder(view) {

        var disposable: Disposable? = null
        var nativeAd: NativeAd? = null

        init {
            view.closeButton.setOnTouchListener { _, motionEvent ->
                if (motionEvent.action == ACTION_UP) closeClickListener.invoke()
                true
            }
        }

        fun bind() {
            resetViewParameters()

            val nativeAdItem = appodealManager.getNativeAdItem()

            if (nativeAdItem is AppodealManager.NativeAdItem.Filled) {
                nativeAd = nativeAdItem.nativeAd
                handleNativeAd(nativeAdItem.nativeAd)
            } else {
                disposable = appodealManager
                    .nativeAdLoadedObservable()
                    .filter { true }
                    .map { appodealManager.getNativeAdItem() }
                    .filter { it is AppodealManager.NativeAdItem.Filled }
                    .map { it as AppodealManager.NativeAdItem.Filled }
                    .take(1)
                    .timeout(5, TimeUnit.SECONDS)
                    .subscribe(
                        {
                            nativeAd = it.nativeAd
                            handleNativeAd(it.nativeAd)
                        },
                        { /* do nothing  */ }
                    )
            }
        }

        fun unregisterViewForInteraction() {
            view.nativeAdView.unregisterViewForInteraction()
            // Destroying leads to crash with Admob native ad items
            // nativeAd?.destroy()
        }

        private fun resetViewParameters() {
            (view.layoutParams as RecyclerView.LayoutParams).apply {
                height = 0
                setMargins(leftMargin, 0, rightMargin, 0)
            }

            disposable?.dispose()

            // Destroying leads to crash with Admob native ad items
            // nativeAd?.destroy()
        }

        private fun handleNativeAd(nativeAd: NativeAd) {

            (view.layoutParams as RecyclerView.LayoutParams).apply {
                height = RecyclerView.LayoutParams.WRAP_CONTENT
                setMargins(leftMargin, 0, rightMargin, view.context.dpToPx(8f).toInt())
            }

            view.title.text = nativeAd.title
            view.description.text = nativeAd.description

            if (nativeAd.rating == 0f) {
                view.rating.visibility = View.INVISIBLE
            } else {
                view.rating.visibility = View.VISIBLE
                view.rating.rating = nativeAd.rating
                view.rating.stepSize = 0.1f
            }

            view.button.text = nativeAd.callToAction

            val providerView = nativeAd.getProviderView(view.context)
            if (providerView != null) {
                if (providerView.parent != null && providerView.parent is ViewGroup) {
                    (providerView.parent as ViewGroup).removeView(providerView)
                }

                view.providerViewContainer.removeAllViews()

                val layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )

                view.providerViewContainer.addView(providerView, layoutParams)
                view.providerViewContainer.visible(true)
            } else {
                view.providerViewContainer.visible(false)
            }

            // Hide NativeMediaView
            // view.nativeAdView.nativeMediaView = view.mediaView
            // view.mediaViewCardView.visibility = View.VISIBLE

            view.nativeAdView.apply {
                titleView = view.title
                descriptionView = view.description
                ratingView = view.rating
                callToActionView = view.button
                this.providerView = providerView
                setNativeIconView(view.icon)
                registerView(nativeAd)
                visibility = View.VISIBLE
            }
        }

    }
}