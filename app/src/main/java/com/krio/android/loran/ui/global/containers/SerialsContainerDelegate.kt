package com.krio.android.loran.ui.global.containers

import com.krio.android.loran.Screens
import com.krio.android.loran.toothpick.DI

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
class SerialsContainerDelegate : ContainerDelegate {
    override val startScreen = Screens.PostersScreen
    override val scope = DI.CATALOG_SCOPE
}