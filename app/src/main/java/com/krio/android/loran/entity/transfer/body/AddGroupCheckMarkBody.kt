package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 25.01.2020.
 */
data class AddGroupCheckMarkBody(
    @SerializedName ("serial_tmdb_id") val serialTmdbId: Int,
    @SerializedName ("season_number") val seasonNumber: Int,
    @SerializedName ("episodes_numbers") val episodesNumbers: List<Int>,
    @SerializedName ("user_id") val userId: Int?
)