package com.krio.android.loran.ui.global.containers


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.krio.android.loran.R
import com.krio.android.loran.Tabs
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.InnerNavigationModule
import com.krio.android.loran.ui.global.BaseFragment
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
class ContainerFragment : Fragment() {

    companion object {
        private const val ARGS_PARAM_TAB = "tab"

        fun newInstance(tab: String) = ContainerFragment().apply {
            arguments = Bundle().apply {
                putString(ARGS_PARAM_TAB, tab)
            }
        }
    }

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    lateinit var containerDelegate: ContainerDelegate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_container, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        when (arguments!![ARGS_PARAM_TAB]) {
            Tabs.SerialTab.screenKey -> containerDelegate = SerialsContainerDelegate()
            Tabs.FavoritelTab.screenKey -> containerDelegate = FavoriteContainerDelegate()
            Tabs.ProfileTab.screenKey -> containerDelegate = ProfileContainerDelegate()
        }

        Toothpick.openScopes(DI.MAIN_SCOPE, containerDelegate.scope).apply {
            installModules(InnerNavigationModule())
            Toothpick.inject(this@ContainerFragment, this)
        }

        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.container) == null) {
            router.replaceScreen(containerDelegate.startScreen)
        }
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    fun onBackPressed() {
        val currentFragment = childFragmentManager.findFragmentById(R.id.container) as? BaseFragment
        currentFragment?.onBackPressed()
                ?: Toothpick.openScope(DI.MAIN_SCOPE).getInstance(Router::class.java).exit()
    }

    private val navigator: SupportAppNavigator by lazy {
        object : SupportAppNavigator(activity, childFragmentManager, R.id.container) {

            override fun activityBack() {
                Toothpick.openScope(DI.MAIN_SCOPE).getInstance(Router::class.java).exit()
            }

            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                //fix incorrect order lifecycle callback of MainFragment
                fragmentTransaction.setReorderingAllowed(true)
            }
        }
    }
}