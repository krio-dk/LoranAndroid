package com.krio.android.loran.model.data.server.interceptor

import com.krio.android.loran.model.data.auth.AuthParameterHolder
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class AuthParameterInterceptor(private val authData: AuthParameterHolder) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalHttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
                .addQueryParameter(authData.authParameterName, authData.authParameterValue)
                .build()

        val requestBuilder = original.newBuilder().url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}
