package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 09.02.2018.
 */
data class VerifyPurchasesResponse(
        @SerializedName("verified") var verified: Boolean,
        @SerializedName("user_id") var userId: Int?
)