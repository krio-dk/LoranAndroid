package com.krio.android.loran.ui.screens.gdpr

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.text.style.UnderlineSpan
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.presentation.screens.gdpr.GdprPresenter
import com.krio.android.loran.presentation.screens.gdpr.GdprView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_gdpr.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
class GdprFragment : BaseFragment(), GdprView {

    override val layoutRes = R.layout.fragment_gdpr

    @InjectPresenter
    lateinit var presenter: GdprPresenter

    @ProvidePresenter
    fun providePresenter(): GdprPresenter = Toothpick.openScopes(DI.APP_SCOPE).getInstance(GdprPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbarTitle?.text = getString(R.string.common_app_name)
        toolbarTitle?.typeface = null

        val learnMore = "Learn more."
        val mainText = getString(R.string.gdpr_main_text, getString(R.string.common_app_name))
        val startPosition = mainText.indexOf(learnMore)
        val endPosition = startPosition + learnMore.length
        val spannableMain = SpannableString(mainText).apply {
            setSpan(
                URLSpan("https://www.appodeal.com/privacy-policy"),
                startPosition,
                endPosition,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        text.apply {
            movementMethod = LinkMovementMethod.getInstance()
            text = spannableMain
        }

        yesButton.setOnClickListener { presenter.onYesClicked() }

        val no = getString(R.string.gdpr_disagree).toUpperCase()
        val spannableNo = SpannableString(no).apply {
            setSpan(
                UnderlineSpan(),
                0,
                length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        noButton.apply {
            setText(spannableNo, TextView.BufferType.SPANNABLE)
            setOnClickListener { presenter.onNoClicked() }
        }
    }
}