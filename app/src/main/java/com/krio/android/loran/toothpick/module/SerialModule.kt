package com.krio.android.loran.toothpick.module

import com.krio.android.loran.model.interactor.SerialInteractor
import toothpick.config.Module

/**
 * Created by krio on 13.06.2018.
 */
class SerialModule: Module()  {
    init {
        // Interactor
        bind(SerialInteractor::class.java).singletonInScope()
    }
}