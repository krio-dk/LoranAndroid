package com.krio.android.loran.model.system.billing

import android.app.Activity
import android.content.Context
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.*
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
class BillingManager @Inject constructor(context: Context) : PurchasesUpdatedListener {

    private val billingClient = newBuilder(context).setListener(this).build()

    private val purchasesUpdatedRelay = PublishRelay.create<List<Purchase>>()

    fun purchasesUpdates(): Observable<List<Purchase>> = purchasesUpdatedRelay

    fun isSubscriptionsSupported(): Single<Boolean> = executeRequest {
        Timber.d("Checking if feature of type subscriptions is supported.")
        val responseCode = billingClient.isFeatureSupported(FeatureType.SUBSCRIPTIONS)
        Single.just(responseCode == BillingResponse.OK)
    }

    fun queryActiveSubscriptions() = queryActivePurchases(SkuType.SUBS)

    fun querySubscriptionHistory() = queryPurchaseHistory(SkuType.SUBS)

    fun querySubscriptionsDetails(subscriptionsIds: List<String>) = querySkuDetails(SkuType.SUBS, subscriptionsIds)

    fun launchBillingFlow(
            @SkuType skuType: String,
            skuId: String,
            oldSku: String? = null
    ): Single<(Activity) -> Int> = executeRequest {
        Timber.d("Launch billing flow.")
        val purchaseParams = BillingFlowParams.newBuilder().setType(skuType).setSku(skuId).setOldSku(oldSku)
        val function = { activity: Activity ->
            billingClient.launchBillingFlow(activity, purchaseParams.build())
        }
        Single.just(function)
    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        Timber.d("Purchases Updated. Response code: $responseCode")
        if (responseCode == BillingClient.BillingResponse.OK) {
            purchasesUpdatedRelay.accept(purchases)
        }
    }

    private fun queryActivePurchases(@SkuType skuType: String): Single<List<Purchase>> = executeRequest {
        Timber.d("Query active purchases.")
        val purchasesResult = billingClient.queryPurchases(skuType)
        if (purchasesResult.responseCode == BillingResponse.OK) {
            Single.just(purchasesResult.purchasesList)
        } else {
            Single.error(BadBillingResponseException(QueryType.QUERY_ACTIVE_SUBSCRIPTIONS))
        }
    }

    private fun queryPurchaseHistory(@SkuType skuType: String): Single<List<Purchase>> = executeRequest {
        Timber.d("Query purchase history.")
        val relay = BehaviorRelay.create<Single<List<Purchase>>>()

        relay.firstOrError().flatMap { it }.doOnSubscribe {
            billingClient.queryPurchaseHistoryAsync(skuType) { responseCode, purchasesList ->
                if (responseCode == BillingResponse.OK) {
                    relay.accept(Single.just(purchasesList))
                } else {
                    relay.accept(Single.error(BadBillingResponseException(QueryType.QUERY_PURCHASE_HISTORY)))
                }
            }
        }
    }

    private fun querySkuDetails(
            @SkuType skuType: String,
            skusList: List<String>
    ): Single<List<SkuDetails>> = executeRequest {
        Timber.d("Query sku details.")
        val relay = BehaviorRelay.create<Single<List<SkuDetails>>>()
        val params = SkuDetailsParams.newBuilder()
                .setSkusList(skusList)
                .setType(skuType)
                .build()

        relay.firstOrError().flatMap { it }.doOnSubscribe {
            billingClient.querySkuDetailsAsync(params) { responseCode, skuDetailsList ->
                if (responseCode == BillingResponse.OK) {
                    relay.accept(Single.just(skuDetailsList))
                } else {
                    relay.accept(Single.error(BadBillingResponseException(QueryType.QUERY_PURCHASES_DETAILS)))
                }
            }
        }
    }

    private fun <T> executeRequest(action: () -> Single<T>): Single<T> {
        return if (billingClient.isReady) {
            action()
        } else {
            startConnection(action)
        }
    }

    private fun <T> startConnection(action: () -> Single<T>): Single<T> {
        Timber.d("Start billing connection.")
        val relay = BehaviorRelay.create<Single<T>>()
        val stateListener = RelayBillingClientStateListenerExtra(action, relay)

        return relay.firstOrError().flatMap { it }.doOnSubscribe {
            billingClient.startConnection(stateListener)
        }
    }

    inner class RelayBillingClientStateListenerExtra<T>(
            private val action: () -> Single<T>,
            private val relay: Relay<Single<T>>
    ) : BillingClientStateListener {

        override fun onBillingSetupFinished(@BillingClient.BillingResponse billingResponseCode: Int) {
            Timber.d("Billing setup finished. Response code: $billingResponseCode")
            if (billingResponseCode == BillingResponse.OK) {
                relay.accept(action())
            } else {
                relay.accept(Single.error(BillingClientNotReadyException()))
            }
        }

        override fun onBillingServiceDisconnected() {
            Timber.d("Billing service disconnected.")
            relay.accept(Single.error(BillingClientNotReadyException()))
        }
    }
}