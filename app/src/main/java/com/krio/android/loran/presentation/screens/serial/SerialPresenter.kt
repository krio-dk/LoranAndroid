package com.krio.android.loran.presentation.screens.serial

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayActorsItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayPersonItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplaySerialDetailsItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayTranslationItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.*
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.screens.person.PersonPresenter
import com.krio.android.loran.presentation.screens.seasons.SeasonsPresenter
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import com.krio.android.loran.toothpick.module.qualifier.ParentScope
import com.krio.android.loran.ui.screens.seasons.SeasonsFragment
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function3
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@InjectViewState
class SerialPresenter @Inject constructor(
        private val initParams: InitParams,
        @ParentScope private val parentScope: String,
        private val serialInteractor: SerialInteractor,
        private val personInteractor: PersonInteractor,
        private val favoriteInteractor: FavoriteInteractor,
        private val subscriptionInteractor: SubscriptionInteractor,
        private val adInteractor: AdInteractor,
        private val refreshRelay: PublishRelay<Boolean>,
        private val prefs: Prefs,
        private val router: Router,
        @AppNavigation private val appRouter: Router,
        private val appodealManager: AppodealManager,
        private val resourceManager: ResourceManager,
        private val errorHandler: ErrorHandler
) : BasePresenter<SerialView>() {

    var refreshDisposable: Disposable? = null

    private var rewardedVideoDisposable: Disposable? = null

    private var translationItems = listOf<DisplayTranslationItem>()

    private var isSubscriptionPresent = false

    data class InitParams(
            val tmdbId: Int,
            val serialName: String,
            val backdropPath: String?
    ) : Serializable

    data class Items(
            val displaySerialDetailsItem: DisplaySerialDetailsItem,
            val displayTranslationItems: List<DisplayTranslationItem>,
            val displayActorsItem: DisplayActorsItem
    )

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        Analytics.logEvent("serial_screen_opened") {
            putString("serial_name", initParams.serialName)
            prefs.userId?.let { putInt("user_id", it) }
        }

        viewState.showTitle(initParams.serialName)

        showBackdrop()
        observeRefresh()
        getData()
    }

    private fun showBackdrop() {
        val backdropPath = initParams.backdropPath
        if (backdropPath != null) {
            viewState.showBackdrop(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_780, backdropPath))
        }
    }

    fun getData(refresh: Boolean = false) {
        getMergedData(initParams.tmdbId, refresh)
                .doOnSubscribe {
                    viewState.showEmptyError(false, null)
                    viewState.showEmptyProgress(true)
                }
                .doAfterTerminate {
                    viewState.showEmptyProgress(false)
                }
                .subscribe(
                        { items ->

                            val data = mutableListOf<Any>().apply {
                                add(items.displaySerialDetailsItem)
                                addAll(items.displayTranslationItems)
                                add(items.displayActorsItem)
                            }

                            translationItems = items.displayTranslationItems

                            viewState.showData(data)
                        },
                        { error ->
                            errorHandler.proceed(error) { viewState.showEmptyError(true, it) }
                        }
                )
                .connect()
    }


    private fun getMergedData(serialTmdbId: Int, refresh: Boolean) =
            Single.zip(
                    getSerialData(serialTmdbId, refresh),
                    personInteractor.getActorsItem(serialTmdbId),
                    subscriptionInteractor.getActiveSubscription(),
                    Function3<Pair<DisplaySerialDetailsItem, List<DisplayTranslationItem>>, DisplayActorsItem, String, Items> {  pair, actorsItem, activeSubscription ->
                        isSubscriptionPresent = activeSubscription.isNotEmpty()
                        Items(pair.first, pair.second, actorsItem)
                    }
            )

    private fun getSerialData(serialTmdbId: Int, refresh: Boolean): Single<Pair<DisplaySerialDetailsItem, List<DisplayTranslationItem>>> =
            serialInteractor.getSerialDetailsItem(serialTmdbId)
                    .flatMap { serialDetailsItem ->
                        serialInteractor.getTranslationItems(serialTmdbId, refresh)
                                .map { translationItems ->
                                    Pair(serialDetailsItem, translationItems)
                                }
                    }

    fun onFavoriteClicked(displayTranslationItem: DisplayTranslationItem) {
        if (displayTranslationItem.isFavorite) {
            removeFromFavorite(displayTranslationItem.translatorId)
        } else {

            Analytics.logEvent("add_to_favorite_clicked") {
                putString("serial_name", initParams.serialName)
                translationItems.find { it.translatorId == displayTranslationItem.translatorId }?.translatorName?.let {
                    putString("translator_name", it)
                }
                putString("from", "serial screen")
                prefs.userId?.let { putInt("user_id", it) }
            }

            if (isSubscriptionPresent) {
                addToFavorite(displayTranslationItem.translatorId, adWatched = false)
            } else {
                checkIfNeedToShowRewardedVideoDialog(displayTranslationItem)
            }
        }
    }

    private fun checkIfNeedToShowRewardedVideoDialog(displayTranslationItem: DisplayTranslationItem) {
        adInteractor.needShowRewardedVideoDialog(initParams.tmdbId)
            .subscribe(
                { needShow ->
                    if (needShow) showRewardedVideoDialog(displayTranslationItem)
                    else addToFavorite(displayTranslationItem.translatorId, adWatched = false)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    private fun showRewardedVideoDialog(displayTranslationItem: DisplayTranslationItem) {
        appodealManager.cacheRewardedVideo()
        subscriptionInteractor
            .getLowestSubscriptionPrice()
            .subscribe(
                { totalPrice ->
                    viewState.showRewardedVideoDialog(
                        initParams.serialName,
                        displayTranslationItem.translatorId,
                        displayTranslationItem.translatorName,
                        totalPrice
                    )

                    Analytics.logEvent("rewarded_video_dialog_shown") {
                        putString("serial_name", initParams.serialName)
                        putString("translator_name", displayTranslationItem.translatorName)
                        putString("from", "serial screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    fun onWatchAdClicked(translatorId: Int, translatorName: String) {

        Analytics.logEvent("watch_video_ad_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", translatorName)
            putString("from", "serial screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        rewardedVideoDisposable = appodealManager.showRewardedVideo(viewState::setRewardedVideoProgress)
            .subscribe (
                { finished ->
                    println("Rewarded Video: presenter, finished = $finished")

                    Analytics.logEvent("rewarded_video_closed") {
                        putBoolean("finished", finished)
                        putString("from", "serial screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    if (finished) {
                        addToFavorite(translatorId, adWatched = true, translatorName = translatorName)
                    } else {
                        viewState.showRewardedVideoError(true, resourceManager.getString(R.string.rewarded_video_dialog_error_not_finished))
                    }
                },
                { error ->

                    println("Rewarded Video: presenter, error = $error")

                    Analytics.logEvent("rewarded_video_failed") {
                        putString("from", "serial screen")
                        putString("error", error.message)
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    if (error !is AppodealManager.AdDisabledException && error !is AppodealManager.RewardedVideoAdNotInitializedException) {
                        addToFavorite(translatorId, adWatched = false, translatorName = translatorName)
                    }

                    when (error) {
                        is AppodealManager.AdDisabledException,
                        is AppodealManager.RewardedVideoAdNotInitializedException -> {
                            viewState.hideRewardedVideoDialog()
                            viewState.showMessage(resourceManager.getString(R.string.common_restart_app))
                        }
                        is AppodealManager.CantShowRewardedVideoException -> {
                            // don't show anything
                        }
                        else -> {
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                    }
                }
            )
            .also { it.connect() }
    }

    fun onDismissRewardedVideoDialog() {
        rewardedVideoDisposable?.dispose()
    }

    private fun removeFromFavorite(translatorId: Int) {

        Analytics.logEvent("remove_from_favorite_clicked") {
            putString("serial_name", initParams.serialName)
            translationItems.find { it.translatorId == translatorId }?.translatorName?.let {
                putString("translator_name", it)
            }
            putString("from", "serial screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        viewState.updateFavoriteStatus(translatorId, false)

        favoriteInteractor.removeFromFavorite(initParams.tmdbId, translatorId)
                .subscribe(
                        { (success) ->
                            if (success == true) {
                                acceptRefresh()
                            }
                        },
                        { error ->
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                            viewState.updateFavoriteStatus(translatorId, true)
                        }
                )
                .connect()
    }

    private fun addToFavorite(translatorId: Int, adWatched: Boolean, translatorName: String? = null) {

        viewState.updateFavoriteStatus(translatorId, true)

        favoriteInteractor.addToFavorite(initParams.tmdbId, translatorId, adWatched)
                .subscribe(
                        { (success) ->
                            if (success) {

                                Analytics.logEvent("add_to_favorite_success") {
                                    putString("serial_name", initParams.serialName)
                                    putString("translator_name", translatorName)
                                    putString("from", "serial screen")
                                    prefs.userId?.let { putInt("user_id", it) }
                                }

                                translatorName?.let {
                                    if (!adWatched) viewState.showMessage(resourceManager.getString(R.string.rewarded_video_message_lucky))
                                    viewState.showMessage(resourceManager.getString(R.string.rewarded_video_message_done, initParams.serialName, it))
                                }

                                viewState.hideRewardedVideoDialog()
                                acceptRefresh()

                            } else {

                                Analytics.logEvent("add_to_favorite_blocked") {
                                    putString("serial_name", initParams.serialName)
                                    putString("translator_name", translatorName)
                                    putString("from", "serial screen")
                                    prefs.userId?.let { putInt("user_id", it) }
                                }

                                viewState.updateFavoriteStatus(translatorId, false)
                                viewState.showRewardedVideoError(true, resourceManager.getString(R.string.rewarded_video_dialog_error_show))
                            }
                        },
                        { error ->

                            Analytics.logEvent("add_to_favorite_error") {
                                putString("serial_name", initParams.serialName)
                                putString("translator_name", translatorName)
                                putString("from", "serial screen")
                                prefs.userId?.let { putInt("user_id", it) }
                            }

                            viewState.updateFavoriteStatus(translatorId, false)
                            viewState.hideRewardedVideoDialog()

                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                )
                .connect()
    }

    fun onTranslationClicked(displayTranslationItem: DisplayTranslationItem) {

        val presenterParams = SeasonsPresenter.InitParams(
                initParams.tmdbId,
                initParams.serialName,
                initParams.backdropPath,
                displayTranslationItem.translatorId,
                displayTranslationItem.translatorName,
                displayTranslationItem.isFavorite
        )

        val params = SeasonsFragment.InitParams(
                closeSerialRootScopeOnExit = false,
                presenterParams = presenterParams
        )

        router.navigateTo(Screens.SeasonsScreen(parentScope, params))
    }

    fun onPersonClicked(displayPersonItem: DisplayPersonItem) {
        router.navigateTo(
            Screens.PersonScreen(
                parentScope,
                PersonPresenter.InitParams(
                    displayPersonItem.name,
                    displayPersonItem.profilePath,
                    displayPersonItem.personTmdbId
                )
            )
        )
    }

    fun onSubscriptionDetailsClicked() {
        appRouter.navigateTo(Screens.SubscriptionScreen)
    }

    private fun observeRefresh() {
        refreshDisposable = refreshRelay.subscribe {
            viewState.showData(emptyList())
            getData(refresh = true)
        }.apply { connect() }
    }

    private fun acceptRefresh() {
        refreshDisposable?.dispose()
        refreshRelay.accept(true)
        observeRefresh()
    }


    fun onBackPressed() {
        router.exit()
    }
}