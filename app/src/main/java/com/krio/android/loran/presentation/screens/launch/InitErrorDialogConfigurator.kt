package com.krio.android.loran.presentation.screens.launch

/**
 * Created by Dmitriy Kolmogorov on 15.09.2018.
 */
interface InitErrorDialogConfigurator {
    val error: Throwable
    val message: String
    val showRetryButton: Boolean
    val showSupportButton: Boolean
}