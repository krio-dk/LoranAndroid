package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.subscription.ActiveSubscription

/**
 * Created by Dmitriy Kolmogorov on 09/03/2019.
 */
data class ActiveSubscriptionResponse (
        @SerializedName("is_subscription_purchased") var isSubscriptionPurchased: Boolean,
        @SerializedName("active_subscription") var activeSubscription: ActiveSubscription?
)