package com.krio.android.loran.entity.presentation.displayitems.seasons

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class DisplaySeasonItem(
        val seasonNumber: Int,
        val episodesCount: Int,
        val seasonAirDate: String?,
        val seasonName: String?,
        val posterPath: String?
)