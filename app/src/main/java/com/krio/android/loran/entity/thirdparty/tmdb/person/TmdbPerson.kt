package com.krio.android.loran.entity.thirdparty.tmdb.person

import com.google.gson.annotations.SerializedName

data class TmdbPerson(
        @SerializedName("adult") val adult: Boolean,
        @SerializedName("biography") val biography: String,
        @SerializedName("birthday") val birthday: String?,
        @SerializedName("deathday") val deathday: String?,
        @SerializedName("gender") val gender: Int,
        @SerializedName("homepage") val homepage: String?,
        @SerializedName("id") val id: Int,
        @SerializedName("imdb_id") val imdbId: String,
        @SerializedName("name") val name: String,
        @SerializedName("place_of_birth") val placeOfBirth: String?,
        @SerializedName("popularity") val popularity: Double,
        @SerializedName("profile_path") val profilePath: String?
)