package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.domain.dataitems.main.TimelineItem
import com.krio.android.loran.entity.presentation.displayitems.timeline.DisplayTimelineItem
import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeason
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerial
import com.krio.android.loran.entity.transfer.response.TimelineResponse
import com.krio.android.loran.extensions.timeZoneDateTime
import com.krio.android.loran.model.data.server.loran.LoranConfig.TRANSLATOR_ORIGINAL_ID
import com.krio.android.loran.model.data.server.tmdb.TmdbResourceNotFoundException
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.data.utils.LanguageUtils.isRussianSpeaking
import com.krio.android.loran.model.repository.SerialRepository
import com.krio.android.loran.model.repository.TimelineRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import retrofit2.HttpException
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class TimelineInteractor @Inject constructor(
        private val repository: TimelineRepository,
        private val serialRepository: SerialRepository,
        private val prefs: Prefs
) {

    init {
        Timber.d("Init")
    }

    private var presentTmdbSerials = emptyList<TmdbSerial>()
    private var presentSeasons = emptyList<Pair<Int, TmdbSeason>>()

    fun getTimelineItems(page: Int): Single<List<DisplayTimelineItem>> {
        return getTimeline(page)
                .map { response ->
                    response.timelineItems?.map {

                        val networks = it.materialData.networks

                        val translatorName = if (it.translatorId == TRANSLATOR_ORIGINAL_ID && networks != null) {
                            when {
                                isRussianSpeaking() -> "${networks.joinToString()} (${it.translatorName})"
                                else -> networks.joinToString()
                            }
                        } else {
                            it.translatorName
                        }

                        DisplayTimelineItem(
                                it.serialTmdbId,
                                it.materialData.serialName ?: it.serialName,
                                it.seasonNumber,
                                it.episodeNumber,
                                it.isSeen,
                                it.translatorId,
                                translatorName,
                                it.translationDate.timeZoneDateTime(),
                                it.materialData.episodeName,
                                it.materialData.episodeOriginalAirDate,
                                it.materialData.serialBackdropPath,
                                it.materialData.serialPosterPath,
                                it.materialData.episodeStillPath
                        )
                    } ?: emptyList()
                }
    }

    private fun getTimeline(page: Int): Single<TimelineResponse> {
        return prefs.userId?.let { userId ->
            repository.getTimeline(page, userId)
                    .flatMap { response ->
                        response.timelineItems?.let { timeLineItems ->
                            fillMissingData(timeLineItems).flatMap {
                                presentTmdbSerials = it.first
                                presentSeasons = it.second
                                Single.just(response)
                            }

                        } ?: Single.just(TimelineResponse())
                    }

        } ?: Single.just(TimelineResponse())
    }

    private fun fillMissingData(timelineItems: List<TimelineItem>): Single<Pair<List<TmdbSerial>, List<Pair<Int, TmdbSeason>>>> {
        return Single.zip(
                getTimelineSerials(timelineItems, presentTmdbSerials),
                getTimelineSeasons(timelineItems, presentSeasons),
                BiFunction<List<TmdbSerial>, List<Pair<Int, TmdbSeason>>, Pair<List<TmdbSerial>, List<Pair<Int, TmdbSeason>>>> { newSerials, newSeasons ->
                    val allSerials = presentTmdbSerials + newSerials
                    val allSeasons = presentSeasons + newSeasons

                    timelineItems.forEach { timelineItem ->
                        val serial = allSerials.find { it.id == timelineItem.serialTmdbId }
                        val season = allSeasons.find { it.first == serial?.id && it.second.seasonNumber == timelineItem.seasonNumber }?.second
                        val episode = season?.episodes?.find { episode -> episode.episodeNumber == timelineItem.episodeNumber }

                        timelineItem.materialData.serialName = serial?.name
                        timelineItem.materialData.episodeName = episode?.name
                        timelineItem.materialData.episodeOriginalAirDate = episode?.airDate
                        timelineItem.materialData.serialBackdropPath = serial?.backdropPath
                        timelineItem.materialData.episodeStillPath = episode?.stillPath
                        timelineItem.materialData.networks = serial?.networks?.map { it.name }
                    }

                    Pair(allSerials, allSeasons)
                }
        )
    }

    private fun getTimelineSerials(timelineItems: List<TimelineItem>, presentTmdbSerials: List<TmdbSerial>): Single<List<TmdbSerial>> {
        val presentSerialsItems = presentTmdbSerials.map { it.id }
        val newSerialItems = timelineItems.filterNot { presentSerialsItems.contains(it.serialTmdbId) }
        val uniqueSerialItems = newSerialItems.distinctBy { it.serialTmdbId }

        return Observable
                .fromIterable(uniqueSerialItems)
                .flatMap { timelineItem ->
                    delayedRequest(serialRepository.getTmdbSerial(timelineItem.serialTmdbId))
                            .toObservable()
                            .onErrorResumeNext(Observable.empty())
                }
                .toList()
    }

    private fun getTimelineSeasons(timelineItems: List<TimelineItem>, presentSeasons: List<Pair<Int, TmdbSeason>>): Single<List<Pair<Int, TmdbSeason>>> {
        val presentSeasonsItems = presentSeasons.map { Pair(it.first, it.second.seasonNumber) }
        val newSeasonItems = timelineItems.filterNot { presentSeasonsItems.contains(Pair(it.serialTmdbId, it.seasonNumber)) }
        val uniqueSeasonItems = newSeasonItems.map { Pair(it.serialTmdbId, it.seasonNumber) }.distinct()

        return Observable.fromIterable(uniqueSeasonItems)
                .flatMap { pair ->
                    Observable.zip(
                            Observable.just(pair.first),
                            delayedRequest(serialRepository.getTmdbSeasonDetails(pair.first, pair.second)).toObservable(),
                            BiFunction<Int, TmdbSeason, Pair<Int, TmdbSeason>> { tmdbId, season -> Pair(tmdbId, season) }
                    ).onErrorResumeNext(Observable.empty())
                }
                .toList()
    }

    private fun <T> delayedRequest(request: Single<T>, delay: Long = 0): Single<T> {
        return Single
                .timer(delay, TimeUnit.SECONDS)
                .flatMap { request }
                .onErrorResumeNext { error ->
                    if (error is HttpException && error.code() == 404) {
                        Single.error(TmdbResourceNotFoundException())
                    } else {
                        delayedRequest(request, 1)
                    }
                }
    }
}