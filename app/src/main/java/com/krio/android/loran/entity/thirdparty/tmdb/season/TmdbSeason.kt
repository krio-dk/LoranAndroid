package com.krio.android.loran.entity.thirdparty.tmdb.season

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.thirdparty.tmdb.episode.TmdbEpisode

/**
 * Created by Dmitriy Kolmogorov on 02.03.2018.
 */
data class TmdbSeason(
        @SerializedName("_id") val _id: String,
        @SerializedName("air_date") val airDate: String,
        @SerializedName("episodes") val episodes: List<TmdbEpisode>,
        @SerializedName("name") val name: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("id") val id: Int,
        @SerializedName("poster_path") val posterPath: String?,
        @SerializedName("season_number") val seasonNumber: Int
)