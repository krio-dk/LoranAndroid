package com.krio.android.loran.entity.domain.core.materialdata

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class EpisodeMaterialData(
        @SerializedName("air_date") var airDate: String? = null,
        @SerializedName("episode_name") var episodeName: String? = null,
        @SerializedName("overview") var overview: String? = null,
        @SerializedName("still_path") var stillPath: String? = null
)