package com.krio.android.loran.entity.domain.dataitems.main

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 05.06.2018.
 */
data class FavoriteTranslationItem(
        @SerializedName("translator_id") val translatorId: Int,
        @SerializedName("translator_name") val translatorName: String,
        @SerializedName("next_episode") val nextEpisode: FavoriteNextEpisodeItem? = null
)