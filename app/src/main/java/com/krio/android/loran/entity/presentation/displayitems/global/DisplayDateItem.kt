package com.krio.android.loran.entity.presentation.displayitems.global

/**
 * Created by Dmitriy Kolmogorov on 17.03.2018.
 */
data class DisplayDateItem(val date: String)