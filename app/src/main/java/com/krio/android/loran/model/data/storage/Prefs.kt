package com.krio.android.loran.model.data.storage

import android.content.Context
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class Prefs @Inject constructor(private val context: Context) {

    companion object {
        private const val LORAN_PREFS = "loran_prefs"
        private const val USER_ID = "user_id"
        private const val SHOW_INTRO = "show_intro"
        private const val RESULT_GDPR = "result_gdpr"
        private const val INSTALL_NOT_FROM_GOOGLE_PLAY_EVENT_SEND = "install_not_from_google_play_event_send"
    }

    private fun getSharedPreferences(prefsName: String) = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    var userId: Int?
        get() {
            return if (getSharedPreferences(LORAN_PREFS).contains(USER_ID)) {
                getSharedPreferences(LORAN_PREFS).getInt(USER_ID, 0)
            } else {
                null
            }
        }
        set(value) {
            if (value != null) {
                getSharedPreferences(LORAN_PREFS).edit().putInt(USER_ID, value).apply()
            } else {
                getSharedPreferences(LORAN_PREFS).edit().remove(USER_ID).apply()
            }
        }

    var wasIntroShowing: Boolean
        get() = getSharedPreferences(LORAN_PREFS).getBoolean(SHOW_INTRO, false)
        set(value) {
            getSharedPreferences(LORAN_PREFS).edit().putBoolean(SHOW_INTRO, value).apply()
        }

    val wasConsentShowing: Boolean
        get() = getSharedPreferences(LORAN_PREFS).contains(RESULT_GDPR)

    var resultGdpr: Boolean
        get() = getSharedPreferences(LORAN_PREFS).getBoolean(RESULT_GDPR, false)
        set(value) {
            getSharedPreferences(LORAN_PREFS).edit().putBoolean(RESULT_GDPR, value).apply()
        }

    var wasInstallNotFromGooglePlayEventSend: Boolean
        get() = getSharedPreferences(LORAN_PREFS).getBoolean(INSTALL_NOT_FROM_GOOGLE_PLAY_EVENT_SEND, false)
        set(value) {
            getSharedPreferences(LORAN_PREFS).edit().putBoolean(INSTALL_NOT_FROM_GOOGLE_PLAY_EVENT_SEND, value).apply()
        }
}