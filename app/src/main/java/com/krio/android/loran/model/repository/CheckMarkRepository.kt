package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.transfer.body.AddCheckMarkBody
import com.krio.android.loran.entity.transfer.body.AddGroupCheckMarkBody
import com.krio.android.loran.entity.transfer.response.AddCheckMarkResponse
import com.krio.android.loran.entity.transfer.response.ResultResponse
import com.krio.android.loran.entity.transfer.response.UserIdResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 10.03.2018.
 */
class CheckMarkRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun addCheckMark(
            serialTmdbId: Int,
            seasonNumber: Int,
            episodeNumber: Int,
            userId: Int?
    ): Single<AddCheckMarkResponse> = loranApi
            .addCheckMark(AddCheckMarkBody(serialTmdbId, seasonNumber, episodeNumber, userId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun removeCheckMark(
            serialTmdbId: Int,
            seasonNumber: Int,
            episodeNumber: Int,
            userId: Int
    ): Single<ResultResponse> = loranApi
            .removeCheckMark(serialTmdbId, seasonNumber, episodeNumber, userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun addGroupCheckMark(
            serialTmdbId: Int,
            seasonNumber: Int,
            episodesNumbers: List<Int>,
            userId: Int?
    ): Single<AddCheckMarkResponse> = loranApi
            .addGroupCheckMark(AddGroupCheckMarkBody(serialTmdbId, seasonNumber, episodesNumbers, userId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun removeGroupCheckMark(
            serialTmdbId: Int,
            seasonNumber: Int,
            userId: Int
    ): Single<ResultResponse> = loranApi
            .removeGroupCheckMark(serialTmdbId, seasonNumber, userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}