package com.krio.android.loran.entity.domain.dataitems.main

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 20.06.2018.
 */
enum class ScheduleType {
    @SerializedName("Concrete")
    CONCRETE,

    @SerializedName("Calculated")
    CALCULATED,

    @SerializedName("Original")
    ORIGINAL
}