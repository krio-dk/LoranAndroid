package com.krio.android.loran.presentation.screens.gdpr

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface GdprView : MvpView