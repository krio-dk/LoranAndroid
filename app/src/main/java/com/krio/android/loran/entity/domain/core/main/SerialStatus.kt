package com.krio.android.loran.entity.domain.core.main

import androidx.annotation.StringRes
import com.google.gson.annotations.SerializedName
import com.krio.android.loran.R

/**
 * Created by Dmitriy Kolmogorov on 03.03.2018.
 */
enum class SerialStatus {
    @SerializedName("Returning Series")
    RETURNING_SERIES,
    @SerializedName("Planned")
    PLANNED,
    @SerializedName("In Production")
    IN_PRODUCTION,
    @SerializedName("Ended")
    ENDED,
    @SerializedName("Canceled")
    CANCELED,
    @SerializedName("Pilot")
    PILOT;

    @StringRes
    fun description(): Int {
        return when (this) {
            RETURNING_SERIES -> R.string.serial_status_returning_series
            PLANNED -> R.string.serial_status_planned
            IN_PRODUCTION -> R.string.serial_status_in_production
            ENDED -> R.string.serial_status_ended
            CANCELED -> R.string.serial_status_canceled
            PILOT -> R.string.serial_status_pilot
        }
    }
}