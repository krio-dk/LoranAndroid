package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayActorsItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayPersonItem
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPerson
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPersonImages
import com.krio.android.loran.model.repository.PersonRepository
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 10.03.2018.
 */
class PersonInteractor @Inject constructor(private val repository: PersonRepository) {

    init {
        Timber.d("Init")
    }

    fun getActorsItem(tmdbSerialId: Int): Single<DisplayActorsItem> = repository
            .getTmdbCredits(tmdbSerialId)
            .map { tmdbCastItem ->

                val personItems = tmdbCastItem.tmdbCast.filter { it.profilePath != null }.map {
                    DisplayPersonItem(it.id, it.name, it.character, it.profilePath!!)
                }

                DisplayActorsItem(personItems)
            }

    fun getPerson(personId: Int): Single<TmdbPerson> = repository.getTmdbPerson(personId)

    fun getPersonImages(personId: Int): Single<TmdbPersonImages> = repository.getTmdbPersonImages(personId)

}