package com.krio.android.loran.ui.global

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
abstract class BaseActivity : MvpAppCompatActivity() {

    companion object {
        private val PROGRESS_DIALOG_TAG = "progress_dialog"
    }

    abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
    }

    protected fun showProgressDialog() {
        val progressDialogFragment = supportFragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG)
        if (progressDialogFragment == null) {
            ProgressDialog().show(supportFragmentManager, PROGRESS_DIALOG_TAG)
            supportFragmentManager.executePendingTransactions()
        }

    }

    protected fun hideProgressDialog() {
        val progressDialogFragment = supportFragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG) as? ProgressDialog
        progressDialogFragment?.let {
            it.dismissAllowingStateLoss()
            supportFragmentManager.executePendingTransactions()
        }
    }
}