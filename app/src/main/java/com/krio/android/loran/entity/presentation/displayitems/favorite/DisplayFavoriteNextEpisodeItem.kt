package com.krio.android.loran.entity.presentation.displayitems.favorite

import com.krio.android.loran.entity.domain.dataitems.main.ScheduleType

data class DisplayFavoriteNextEpisodeItem(
        val seasonNumber: Int,
        val episodeNumber: Int,
        val isReleased: Boolean,
        val scheduleDate: String?,
        val scheduleType: ScheduleType?
)