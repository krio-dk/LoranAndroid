package com.krio.android.loran.entity.domain.core.main

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
enum class InitMessageType {
    @SerializedName("grace_period")
    GRACE_PERIOD
}