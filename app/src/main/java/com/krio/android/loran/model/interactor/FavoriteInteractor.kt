package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.domain.dataitems.main.FavoriteItem
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteItem
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteNextEpisodeItem
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteTranslationItem
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerial
import com.krio.android.loran.entity.transfer.response.FavoriteItemResponse
import com.krio.android.loran.entity.transfer.response.FavoritesResponse
import com.krio.android.loran.entity.transfer.response.ResultResponse
import com.krio.android.loran.entity.transfer.response.UserIdResponse
import com.krio.android.loran.fcm.PushTokenSender
import com.krio.android.loran.model.data.server.loran.LoranConfig.TRANSLATOR_ORIGINAL_ID
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.FavoriteRepository
import com.krio.android.loran.model.repository.SerialRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class FavoriteInteractor @Inject constructor(
        private val favoriteRepository: FavoriteRepository,
        private val serialRepository: SerialRepository,
        private val prefs: Prefs,
        private val pushTokenSender: PushTokenSender
) {

    init {
        Timber.d("Init")
    }

    fun getFavoriteItems(): Single<Pair<List<DisplayFavoriteItem>, Int?>> {
        return getFavorites()
                .map { response ->
                    val displayFavoriteItems = response.favoriteItems?.map {

                        val translationItems = it.translations
                                .map {

                                    val nextEpisodeItem = it.nextEpisode?.let {
                                        DisplayFavoriteNextEpisodeItem(
                                                it.seasonNumber,
                                                it.episodeNumber,
                                                it.isReleased,
                                                it.scheduleDate,
                                                it.scheduleType
                                        )
                                    }

                                    DisplayFavoriteTranslationItem(
                                            it.translatorId,
                                            it.translatorName,
                                            nextEpisodeItem
                                    )
                                }
                                .toMutableList()
                                .also { items ->
                                    items.find { it.translatorId == TRANSLATOR_ORIGINAL_ID }?.let { originalItem ->
                                        items.remove(originalItem)
                                        items.add(0, originalItem)
                                    }
                                }

                        DisplayFavoriteItem(
                                it.serialTmdbId,
                                it.materialData.serialName ?: it.serialNameEn,
                                it.lastSeenSeasonNumber,
                                it.lastSeenEpisodeNumber,
                                translationItems.toMutableList(),
                                it.materialData.backdropPath,
                                it.materialData.firstAirDate,
                                it.materialData.originCountry,
                                it.materialData.originalLanguage,
                                it.materialData.posterPath,
                                it.materialData.status,
                                it.materialData.voteAverage,
                                it.isActive
                        )
                    } ?: emptyList()

                    Pair(displayFavoriteItems, response.maxActiveSerialsCount)
                }
    }

    fun getFavoriteItem(serialTmdbId: Int): Single<DisplayFavoriteItem> {
        return getFavorite(serialTmdbId)
                .map { response ->
                    return@map response.favoriteItem?.let {

                        val translationItems = it.translations
                                .map {

                                    val nextEpisodeItem = it.nextEpisode?.let {
                                        DisplayFavoriteNextEpisodeItem(
                                                it.seasonNumber,
                                                it.episodeNumber,
                                                it.isReleased,
                                                it.scheduleDate,
                                                it.scheduleType
                                        )
                                    }

                                    DisplayFavoriteTranslationItem(
                                            it.translatorId,
                                            it.translatorName,
                                            nextEpisodeItem
                                    )
                                }
                                .toMutableList()
                                .also { items ->
                                    items.find { it.translatorId == TRANSLATOR_ORIGINAL_ID }?.let { originalItem ->
                                        items.remove(originalItem)
                                        items.add(0, originalItem)
                                    }
                                }

                        DisplayFavoriteItem(
                                it.serialTmdbId,
                                it.materialData.serialName ?: it.serialNameEn,
                                it.lastSeenSeasonNumber,
                                it.lastSeenEpisodeNumber,
                                translationItems.toMutableList(),
                                it.materialData.backdropPath,
                                it.materialData.firstAirDate,
                                it.materialData.originCountry,
                                it.materialData.originalLanguage,
                                it.materialData.posterPath,
                                it.materialData.status,
                                it.materialData.voteAverage,
                                it.isActive
                        )
                    }
                }
    }

    fun addToFavorite(serialTmdbId: Int, translatorId: Int, adWatched: Boolean): Single<UserIdResponse> = favoriteRepository
            .addSerialInTranslationToFavorite(serialTmdbId, translatorId, adWatched, prefs.userId)
            .doOnSuccess { result ->
                if (result.success) {
                    result.userId?.let { userId ->
                        prefs.userId = userId
                        pushTokenSender.sendPushToken()
                    }
                }
            }

    fun removeFromFavorite(serialTmdbId: Int, translatorId: Int): Single<ResultResponse> {
        return prefs.userId?.let { userId ->
            favoriteRepository.removeSerialInTranslationFromFavorite(serialTmdbId, translatorId, userId)
        } ?: Single.just(ResultResponse())
    }

    fun hasFavorites(): Single<Boolean> {
        return prefs.userId?.let { userId ->
            favoriteRepository.hasFavorites(userId).map { it.hasFavorites }
        } ?: Single.just(false)
    }

    fun getFavoritesIdentifiers(): Single<List<Int>> {
        return prefs.userId?.let { userId ->
            favoriteRepository.getFavoritesIdentifiers(userId)
        } ?: Single.just(emptyList())
    }

    private fun getFavorites(): Single<FavoritesResponse> {
        return prefs.userId?.let { userId ->
            favoriteRepository.getFavorites(userId)
                    .flatMap { response ->
                        response.favoriteItems?.let { favoriteItems ->
                            fillMaterialData(favoriteItems).flatMap {
                                Single.just(response)
                            }

                        } ?: Single.just(FavoritesResponse())
                    }

        } ?: Single.just(FavoritesResponse())
    }

    private fun getFavorite(serialTmdbId: Int): Single<FavoriteItemResponse> {
        return prefs.userId?.let { userId ->
            favoriteRepository.getFavorite(serialTmdbId, userId)
                    .flatMap { response ->
                        response.favoriteItem?.let { favoriteItem ->
                            fillMaterialData(listOf(favoriteItem)).flatMap {
                                Single.just(response)
                            }

                        } ?: Single.just(FavoriteItemResponse())
                    }

        } ?: Single.just(FavoriteItemResponse())
    }

    private fun fillMaterialData(favoriteItems: List<FavoriteItem>): Single<Unit> {
        return Observable.fromIterable(favoriteItems)
                .flatMap { favoriteItem ->
                    Observable.zip(
                            Observable.just(favoriteItem),
                            serialRepository.getTmdbSerial(favoriteItem.serialTmdbId).toObservable(),
                            BiFunction<FavoriteItem, TmdbSerial, Pair<FavoriteItem, TmdbSerial>> { _, tmdbSerial ->
                                Pair(favoriteItem, tmdbSerial)
                            }
                    )
                }
                .flatMap { pair ->

                    val favoriteItem = pair.first
                    val tmdbSerial = pair.second

                    favoriteItem.materialData.apply {
                        serialName = tmdbSerial.name
                        backdropPath = tmdbSerial.backdropPath
                        firstAirDate = tmdbSerial.firstAirDate
                        originCountry = tmdbSerial.originCountry
                        originalLanguage = tmdbSerial.originalLanguage
                        posterPath = tmdbSerial.posterPath
                        status = tmdbSerial.status
                        voteAverage = tmdbSerial.voteAverage
                    }

                    Observable.just(Unit)
                }
                .toList()
                .map { Unit }
    }
}