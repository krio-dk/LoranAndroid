package com.krio.android.loran.model.repository

import com.google.firebase.iid.FirebaseInstanceId
import com.krio.android.loran.entity.transfer.response.ResultResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class PushTokenRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun sendPushToken(userId: Int, token: String?): Single<ResultResponse> {
        return token?.let {
            loranApi.sendPushToken(it, FirebaseInstanceId.getInstance().id, userId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

        } ?: Single.just(ResultResponse(false))
    }
}