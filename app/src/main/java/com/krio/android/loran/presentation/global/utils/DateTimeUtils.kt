package com.krio.android.loran.presentation.global.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
object DateTimeUtils {

    fun getFormattedStringDate(stringDate: String): String {
        val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(stringDate)
        return SimpleDateFormat("d MMMM yyyy", Locale.getDefault()).format(date)
    }

    fun getFormattedStringDateTime(stringDate: String): String {
        val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(stringDate)
        val formattedDate = SimpleDateFormat("HH:mm, d MMM yyyy", Locale.getDefault()).format(date)

        return if (formattedDate.contains("00:00")) formattedDate.substring(7) else formattedDate
    }
}