package com.krio.android.loran.model.data.auth

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
interface AuthParameterHolder {
    val authParameterName: String
    val authParameterValue: String
}
