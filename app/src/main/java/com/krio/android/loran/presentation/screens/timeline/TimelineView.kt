package com.krio.android.loran.presentation.screens.timeline

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.krio.android.loran.entity.presentation.displayitems.timeline.DisplayTimelineItem

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface TimelineView : MvpView {
    fun showPageProgress(show: Boolean)
    fun showPageError(show: Boolean, message: String? = null)
    fun showEmptyProgress(show: Boolean)
    fun showEmptyView(show: Boolean, message: String? = null)
    fun showEmptyError(show: Boolean, message: String?)
    fun showData(show: Boolean, data: List<Any>)
    fun showRefreshProgress(show: Boolean)
    fun enableToolbarScrolling(enable: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateItem(item: DisplayTimelineItem)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun resetEndlessScroll()
}