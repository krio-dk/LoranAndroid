package com.krio.android.loran.entity.domain.dataitems.main

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.materialdata.TimelineItemMaterialData

/**
 * Created by krio on 05.06.2018.
 */
data class TimelineItem(
        @SerializedName("serial_tmdb_id") val serialTmdbId: Int,
        @SerializedName("serial_name") val serialName: String,
        @SerializedName("season_number") val seasonNumber: Int,
        @SerializedName("episode_number") val episodeNumber: Int,
        @SerializedName("is_seen") val isSeen: Boolean,
        @SerializedName("translator_id") val translatorId: Int,
        @SerializedName("translator_name") val translatorName: String,
        @SerializedName("translation_date") val translationDate: String,
        @SerializedName("material_data") val materialData: TimelineItemMaterialData = TimelineItemMaterialData()
)