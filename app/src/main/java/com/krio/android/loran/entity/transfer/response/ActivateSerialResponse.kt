package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 2019-06-08.
 */
data class ActivateSerialResponse(
    @SerializedName("success") val success: Boolean? = null,
    @SerializedName("limit_reached") val limitReached: Boolean = false
)