package com.krio.android.loran.entity.presentation.displayitems.serial

import android.os.Parcelable

/**
 * Created by Dmitriy Kolmogorov on 17.02.2018.
 */
data class DisplayActorsItem(
        val actors: List<DisplayPersonItem>,
        var recyclerViewState: Parcelable? = null
)