package com.krio.android.loran.model.data.server.tmdb

import com.krio.android.loran.model.data.auth.AuthParameterHolder

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
object TMDbConfig : AuthParameterHolder {
    const val serverUrl = "https://api.themoviedb.org/"

    override val authParameterName = "api_key"
    override val authParameterValue = "fdecaebd2bfa8afdd6788d151a4d2203"
}