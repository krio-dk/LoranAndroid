package com.krio.android.loran.presentation.screens.favorite.list

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import at.favre.lib.dali.Dali
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.dataitems.main.ScheduleType
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteItem
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteTranslationItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import kotlinx.android.synthetic.main.item_favorite.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class FavoriteItemAdapterDelegate(
    private val isSubscriptionPresent: () -> Boolean,
    private val itemClickListener: (DisplayFavoriteItem) -> Unit,
    private val removeFromFavoriteClickListener: (DisplayFavoriteItem, DisplayFavoriteTranslationItem) -> Unit,
    private val translationClickListener: (DisplayFavoriteItem, DisplayFavoriteTranslationItem) -> Unit,
    private val checkMarkClickListener: (DisplayFavoriteItem, DisplayFavoriteTranslationItem) -> Unit,
    private val activateClickListener: (DisplayFavoriteItem) -> Unit,
    private val deactivateClickListener: (DisplayFavoriteItem) -> Unit
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(itemDisplays: MutableList<Any>, position: Int) = itemDisplays[position] is DisplayFavoriteItem

    override fun onCreateViewHolder(parent: ViewGroup): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favorite, parent, false)
        return FavoriteViewHolder(
            view,
            isSubscriptionPresent,
            itemClickListener,
            removeFromFavoriteClickListener,
            translationClickListener,
            checkMarkClickListener,
            activateClickListener,
            deactivateClickListener
        )
    }

    override fun onBindViewHolder(
        itemDisplays: MutableList<Any>,
        position: Int,
        viewHolder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (viewHolder as FavoriteViewHolder).bind(itemDisplays[position] as DisplayFavoriteItem)
    }

    class FavoriteViewHolder(
        private val view: View,
        private val isSubscriptionPresent: () -> Boolean,
        private val itemClickListener: (DisplayFavoriteItem) -> Unit,
        private val removeFromFavoriteClickListener: (DisplayFavoriteItem, DisplayFavoriteTranslationItem) -> Unit,
        private val translationClickListener: (DisplayFavoriteItem, DisplayFavoriteTranslationItem) -> Unit,
        private val checkMarkClickListener: (DisplayFavoriteItem, DisplayFavoriteTranslationItem) -> Unit,
        private val activateClickListener: (DisplayFavoriteItem) -> Unit,
        private val deactivateClickListener: (DisplayFavoriteItem) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        private lateinit var displayFavoriteItem: DisplayFavoriteItem

        init {
            view.setOnClickListener { itemClickListener.invoke(displayFavoriteItem) }
            view.activateButton.setOnClickListener { activateClickListener.invoke(displayFavoriteItem) }
            view.deactivateButton.setOnClickListener { deactivateClickListener.invoke(displayFavoriteItem) }
        }

        fun bind(favoriteItem: DisplayFavoriteItem) {
            this.displayFavoriteItem = favoriteItem

            view.backdrop.clearColorFilter()
            view.backdrop.setImageResource(R.drawable.placeholder_backdrop)

            view.serialName.text = favoriteItem.serialName

            val posterPath = favoriteItem.posterPath
            if (posterPath != null) {
                GlideApp.with(view.context)
                    .load(ImagesConfiguration.getPosterFullPath(PosterSize.POSTER_SIZE_342, posterPath))
                    .placeholder(R.drawable.placeholder_cover)
                    .into(view.imageView)
            }

            val backdropPath = favoriteItem.backdropPath
            if (backdropPath != null) {
                GlideApp.with(view.context)
                    .asBitmap()
                    .load(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_300, backdropPath))
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            if (backdropPath == favoriteItem.backdropPath) {
                                Dali.create(view.context)
                                    .load(resource)
                                    .placeholder(R.drawable.placeholder_backdrop)
                                    .blurRadius(10)
                                    .downScale(2)
                                    .concurrent()
                                    .reScale()
                                    .skipCache()
                                    .into(view.backdrop)
                            }
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            // do nothing
                        }
                    })
            }

            view.firstAirDate.text = favoriteItem.firstAirDate?.let {
                DateTimeUtils.getFormattedStringDate(favoriteItem.firstAirDate)
            } ?: view.context.getString(R.string.common_shruggie)

            view.score.text = favoriteItem.voteAverage?.toString() ?: view.context.getString(R.string.common_shruggie)

            view.status.text = favoriteItem.status?.description()?.let { statusRes ->
                view.context.getString(statusRes)
            } ?: view.context.getString(R.string.common_shruggie)

            view.originCountry.text =
                favoriteItem.originCountry?.joinToString { country -> Locale(favoriteItem.originalLanguage, country).displayCountry }
                    ?: view.context.getString(R.string.common_shruggie)
            view.originalLanguage.text = favoriteItem.originalLanguage?.let { Locale(favoriteItem.originalLanguage).displayLanguage }
                ?: view.context.getString(R.string.common_shruggie)

            if (favoriteItem.lastSeenSeasonNumber == null || favoriteItem.lastSeenEpisodeNumber == null) {
                view.lastSeenEpisodeContainer.visible(false)
                view.noSeenEpisodeContainer.visible(true)

            } else {
                view.seasonNumber.text = favoriteItem.lastSeenSeasonNumber?.toString() ?: view.context.getString(R.string.common_shruggie)
                view.episodeNumber.text = favoriteItem.lastSeenEpisodeNumber?.toString() ?: view.context.getString(R.string.common_shruggie)

                view.noSeenEpisodeContainer.visible(false)
                view.lastSeenEpisodeContainer.visible(true)
            }

            view.translationsContainer.removeAllViews()

            favoriteItem.translationItems.forEach { transition ->
                val translatorView = LayoutInflater.from(view.context).inflate(R.layout.item_favorite_translator, null)
                view.translationsContainer.addView(translatorView)

                translatorView.setOnClickListener {
                    translationClickListener.invoke(favoriteItem, transition)
                }

                translatorView.findViewById<ImageView>(R.id.favorite)?.apply {
                    setOnClickListener {
                        removeFromFavoriteClickListener.invoke(displayFavoriteItem, transition)
                    }

                    if (!isSubscriptionPresent() && !displayFavoriteItem.isActive) {
                        val colorMatrix = ColorMatrix().apply { setSaturation(0f) }
                        val filter = ColorMatrixColorFilter(colorMatrix)
                        colorFilter = filter
                    }
                }

                translatorView.findViewById<TextView>(R.id.translatorName)?.let {
                    it.text = transition.translatorName
                }

                translatorView.findViewById<TextView>(R.id.nextEpisodeDescription)?.let {
                    it.text = transition.nextEpisode?.let {
                        if (it.isReleased) {
                            view.resources.getString(R.string.favorite_text_next_episode_released)
                        } else {
                            if (it.scheduleDate != null && it.scheduleType != null) {
                                getScheduledNextEpisodeDescription(it.scheduleDate, it.scheduleType)
                            } else {
                                ""
                            }
                        }
                    } ?: view.resources.getString(R.string.favorite_text_next_episode_no_info)
                }

                translatorView.findViewById<TextView>(R.id.nextEpisodeDetails)?.apply {
                    text = transition.nextEpisode?.let {
                        view.resources.getString(R.string.favorite_text_next_episode_details, it.seasonNumber, it.episodeNumber)
                    } ?: ""

                    visible(text.isNotBlank())
                }

                val checkMarkContainer = translatorView.findViewById<CardView>(R.id.checkMarkContainer)?.apply {
                    visible(transition.nextEpisode?.isReleased == true)

                    val color = ContextCompat.getColor(
                        view.context,
                        if (transition.isChecked) R.color.colorWhiteSecondaryBackground else R.color.colorLinkText
                    )
                    setCardBackgroundColor(color)
                }

                translatorView.findViewById<ImageView>(R.id.checkMark)?.let { checkMark ->
                    val color = ContextCompat.getColor(
                        view.context,
                        if (transition.isChecked) R.color.colorLinkText else R.color.colorWhitePrimaryText
                    )
                    ImageViewCompat.setImageTintList(checkMark, ColorStateList.valueOf(color))

                    if (transition.nextEpisode?.isReleased == true) {
                        checkMark.setOnClickListener {
                            ImageViewCompat.setImageTintList(
                                checkMark,
                                ColorStateList.valueOf(ContextCompat.getColor(view.context, R.color.colorLinkText))
                            )
                            checkMarkContainer?.setCardBackgroundColor(ContextCompat.getColor(view.context, R.color.colorWhiteBackground))
                            checkMarkClickListener.invoke(favoriteItem, transition)
                        }
                    }
                }
            }

            view.backdrop.clearColorFilter()
            view.imageView.clearColorFilter()

            if (!isSubscriptionPresent() && !displayFavoriteItem.isActive) {
                view.statusContainer.visible(true)
                view.activeContainer.visible(displayFavoriteItem.isActive)
                view.notActiveContainer.visible(!displayFavoriteItem.isActive)

                val colorMatrix = ColorMatrix().apply { setSaturation(0f) }
                val filter = ColorMatrixColorFilter(colorMatrix)
                view.backdrop.colorFilter = filter
                view.imageView.colorFilter = filter

                view.activateButton.setTextColor(ContextCompat.getColor(view.context, R.color.colorWarning))

            } else {
                view.statusContainer.visible(false)
                view.activeContainer.visible(false)
                view.notActiveContainer.visible(false)
            }

            if (displayFavoriteItem.isUpdating) {
                Dali.create(view.context)
                    .load(view.content)
                    .blurRadius(15)
                    .downScale(2)
                    .concurrent()
                    .reScale()
                    .skipCache()
                    .into(view.loaderImage)

                view.loaderFrame.visibility = VISIBLE
                view.content.visibility = INVISIBLE
            } else {
                view.content.visibility = VISIBLE
                view.loaderFrame.visibility = GONE
            }
        }

        private fun getScheduledNextEpisodeDescription(date: String, scheduleType: ScheduleType): String {
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            val scheduleDate = dateFormatter.parse(date)
            val currentDate = dateFormatter.parse(dateFormatter.format(Date()))

            val diffInMillies = Math.abs(scheduleDate.time - currentDate.time)
            val nextEpisodeDaysCount = (TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)).toInt()

            return when (nextEpisodeDaysCount) {
                0 -> {
                    when (scheduleType) {
                        ScheduleType.CONCRETE -> view.resources.getString(R.string.schedule_text_today_concrete)
                        ScheduleType.CALCULATED -> view.resources.getString(R.string.schedule_text_today_calculated)
                        ScheduleType.ORIGINAL -> view.resources.getString(R.string.schedule_text_today_original)
                    }
                }
                1 -> {
                    when (scheduleType) {
                        ScheduleType.CONCRETE -> view.resources.getString(R.string.schedule_text_tomorrow_concrete)
                        ScheduleType.CALCULATED -> view.resources.getString(R.string.schedule_text_tomorrow_calculated)
                        ScheduleType.ORIGINAL -> view.resources.getString(R.string.schedule_text_tomorrow_original)
                    }
                }
                else -> {
                    val resource = when (scheduleType) {
                        ScheduleType.CONCRETE -> R.plurals.schedule_text_next_episode_concrete
                        ScheduleType.CALCULATED -> R.plurals.schedule_text_next_episode_calculated
                        ScheduleType.ORIGINAL -> R.plurals.schedule_text_next_episode_original
                    }

                    view.resources.getQuantityString(resource, nextEpisodeDaysCount, nextEpisodeDaysCount)
                }
            }
        }
    }
}