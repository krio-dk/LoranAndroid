package com.krio.android.loran.ui.global

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

/**
 * Created by Dmitriy Kolmogorov on 20.02.2018.
 */
class EndlessScrollListener(
        private val visibleThreshold: Int,
        private val progressViewType: Int? = null,
        private val loadMore: () -> Unit
) : RecyclerView.OnScrollListener() {

    private var previousItemCount = 0
    private var loading = false

    private fun getLastVisibleItem(lastVisibleItemPositions: IntArray): Int {
        var maxSize = 0
        for (i in lastVisibleItemPositions.indices) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i]
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i]
            }
        }
        return maxSize
    }

    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) {
            view.adapter?.let { adapter ->

                var itemCount = adapter.itemCount

                val layoutManager = view.layoutManager
                val lastVisibleItemPosition = when (layoutManager) {
                    is StaggeredGridLayoutManager -> getLastVisibleItem(layoutManager.findLastCompletelyVisibleItemPositions(null))
                    else -> (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                }

                if (lastVisibleItemPosition == itemCount - 1) {
                    view.stopScroll()
                }

                progressViewType?.let {
                    val lastViewType = adapter.getItemViewType(itemCount - 1)
                    if (lastViewType == progressViewType) {
                        itemCount--
                    }
                }

                if (previousItemCount != itemCount) {
                    loading = false
                }

                if (!loading && lastVisibleItemPosition + visibleThreshold >= itemCount - 1) {
                    previousItemCount = itemCount
                    loading = true
                    loadMore()
                }
            }
        }
    }

    fun reset() {
        previousItemCount = 0
        loading = false
    }
}