package com.krio.android.loran.entity.presentation.displayitems.favorite

/**
 * Created by Dmitriy Kolmogorov on 17.03.2018.
 */
data class DisplayFavoriteCaptionNotActive(val description: String = "caption not active")