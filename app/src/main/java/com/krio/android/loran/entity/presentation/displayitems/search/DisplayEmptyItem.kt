package com.krio.android.loran.entity.presentation.displayitems.search

import java.util.*

/**
 * Created by Dmitriy Kolmogorov on 14.04.2018.
 */
data class DisplayEmptyItem(private val uuid: UUID = UUID.randomUUID())