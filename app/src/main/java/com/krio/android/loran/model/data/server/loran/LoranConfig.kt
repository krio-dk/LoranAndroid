package com.krio.android.loran.model.data.server.loran

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
object LoranConfig {

    const val TRANSLATOR_ORIGINAL_ID = 3788

    // Yandex
    const val serverUrl = "https://loranproject.ru/"
    // const val serverUrl = "http://84.201.150.144/"

    // Macbook
    // const val serverUrl = "http://192.168.31.233/"

    // RaspberryPI remote
    // const val serverUrl = "http://91.108.27.213/"

    // RaspberryPI local
    // const val serverUrl = "http://192.168.31.187/"

}