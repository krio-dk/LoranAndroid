package com.krio.android.loran.ui.screens.serial


import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import at.favre.lib.dali.Dali
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayTranslationItem
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.screens.serial.SerialPresenter
import com.krio.android.loran.presentation.screens.serial.SerialView
import com.krio.android.loran.presentation.screens.serial.list.ActorsItemAdapterDelegate
import com.krio.android.loran.presentation.screens.serial.list.SerialDetailsItemAdapterDelegate
import com.krio.android.loran.presentation.screens.serial.list.TranslationItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.SerialModule
import com.krio.android.loran.toothpick.module.qualifier.ParentScope
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.RewardedVideoDialog
import com.krio.android.loran.ui.global.ZeroViewHolder
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_serial.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick
import toothpick.config.Module

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
class SerialFragment : BaseFragment(), SerialView {

    companion object {
        private const val ARGS_INIT_PARAMS = "init_params"
        private const val ARGS_PARAM_PARENT_SCOPE = "parent scope"

        private const val REWARDED_VIDEO_DIALOG_TAG = "rewarded_video_dialog"

        fun newInstance(parentScope: String, initParams: SerialPresenter.InitParams) = SerialFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGS_INIT_PARAMS, initParams)
                putString(ARGS_PARAM_PARENT_SCOPE, parentScope)
            }
        }
    }

    override val layoutRes = R.layout.fragment_serial

    private val adapter = SerialAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    @InjectPresenter
    lateinit var presenter: SerialPresenter

    @ProvidePresenter
    fun providePresenter(): SerialPresenter {
        val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String

        Toothpick.openScopes(parentScope, DI.serialRootScope(parentScope)).apply {
            installModules(SerialModule())
        }

        val scope = Toothpick.openScopes(DI.serialRootScope(parentScope), DI.serialDetailsScope(parentScope)).apply {
            installModules(object : Module() {
                init {
                    bind(SerialPresenter.InitParams::class.java).toInstance(arguments!![ARGS_INIT_PARAMS] as SerialPresenter.InitParams)
                    bind(String::class.java).withName(ParentScope::class.java).toInstance(parentScope)
                }
            })
        }

        return scope.getInstance(SerialPresenter::class.java).also {
            Toothpick.closeScope(DI.serialDetailsScope(parentScope))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@SerialFragment.adapter

            addItemDecoration(
                    HorizontalDividerItemDecoration.Builder(context)
                            .colorResId(R.color.colorDarkDividersBackground)
                            .sizeResId(R.dimen.divider_size)
                            .margin(resources.getDimensionPixelSize(R.dimen.divider_left_margin), 0)
                            .build()
            )
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.getData()
        }
    }

    override fun showTitle(title: String) {
        toolbarTitle?.text = title
        toolbarTitle?.typeface = null
    }

    override fun showData(data: List<Any>) {
        recyclerView?.visible(true)
        adapter.setData(data)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun showBackdrop(url: String) {
        GlideApp.with(this)
                .asBitmap()
                .load(url)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        Dali.create(activity)
                                .load(resource)
                                .placeholder(R.drawable.placeholder_backdrop)
                                .blurRadius(10)
                                .downScale(2)
                                .concurrent()
                                .reScale()
                                .skipCache()
                                .into(backdrop)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        // do nothing
                    }
                })
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun updateFavoriteStatus(translatorId: Int, isFavorite: Boolean) {
        adapter.updateFavoriteStatus(translatorId, isFavorite)
    }

    override fun showRewardedVideoDialog(serialName: String, translatorId: Int, translatorName: String, subscriptionCost: String) {
        if (isAdded) {
            val dialogFragment = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG)
            if (dialogFragment == null) {
                println("Rewarded Video: show dialog for $translatorName")
                val dialog = RewardedVideoDialog.newInstance(serialName, subscriptionCost)
                dialog.dismissListener = { presenter.onDismissRewardedVideoDialog() }
                dialog.subscriptionDetailsClickListener = { presenter.onSubscriptionDetailsClicked() }
                dialog.watchAdClickListener = {
                    dialog.showError(false)
                    dialog.showProgress(true)
                    dialog.enableWatchAdButton(false)
                    presenter.onWatchAdClicked(translatorId, translatorName)
                }
                dialog.isCancelable = false
                dialog.show(childFragmentManager, REWARDED_VIDEO_DIALOG_TAG)
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    override fun hideRewardedVideoDialog() {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.dismiss()
        }
    }

    override fun showRewardedVideoError(show: Boolean, message: String) {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.showError(show, message)
            dialog?.showProgress(false)
            dialog?.enableWatchAdButton(true)
        }
    }

    override fun setRewardedVideoProgress(value: Long) {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.setProgressValue(value)
        }
    }

    override fun onBackPressed() {
        val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String
        Toothpick.closeScope(DI.serialRootScope(parentScope))
        presenter.onBackPressed()
    }

    inner class SerialAdapter() : ListDelegationAdapter<MutableList<Any>>() {

        private val actorsDelegate = ActorsItemAdapterDelegate({ presenter.onPersonClicked(it) })

        init {
            delegatesManager.addDelegate(SerialDetailsItemAdapterDelegate())

            delegatesManager.addDelegate(
                    TranslationItemAdapterDelegate(
                            { presenter.onTranslationClicked(it) },
                            { presenter.onFavoriteClicked(it) }
                    )
            )
            delegatesManager.addDelegate(actorsDelegate)

            items = mutableListOf()
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }

        fun updateFavoriteStatus(translatorId: Int, isFavorite: Boolean) {
            items.find { (it as? DisplayTranslationItem)?.translatorId == translatorId }?.let {
                val index = items.indexOf(it)
                (it as DisplayTranslationItem).isFavorite = isFavorite
                notifyItemChanged(index)
            }
        }
    }
}