package com.krio.android.loran.entity.domain.dataitems.materialdata

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.main.SerialStatus

/**
 * Created by krio on 08.06.2018.
 */
class FavoriteItemMaterialData (
        @SerializedName("serial_name") var serialName: String? = null,
        @SerializedName("backdrop_path") var backdropPath: String? = null,
        @SerializedName("first_air_date") var firstAirDate: String? = null,
        @SerializedName("origin_country") var originCountry: List<String>? = null,
        @SerializedName("original_language") var originalLanguage: String? = null,
        @SerializedName("poster_path") var posterPath: String? = null,
        @SerializedName("status") var status: SerialStatus? = null,
        @SerializedName("vote_average") var voteAverage: Double? = null
)