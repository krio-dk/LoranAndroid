package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.loran.body.purchase.PurchaseBody

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
class InitBody (
        @SerializedName("active_purchases") val activePurchases: List<PurchaseBody>,
        @SerializedName("history") val history: List<PurchaseBody>?,
        @SerializedName("user_id") val userId: Int?,
        @SerializedName("language") val language: String
)
