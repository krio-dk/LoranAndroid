package com.krio.android.loran.presentation.screens.launch

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.BuildConfig
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.initialization.*
import com.krio.android.loran.model.system.billing.BadBillingResponseException
import com.krio.android.loran.model.system.billing.BillingClientNotReadyException
import com.krio.android.loran.model.system.billing.BillingManager
import com.krio.android.loran.model.system.billing.QueryType
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.screens.main.MainScreenLauncher
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@InjectViewState
class LaunchPresenter @Inject constructor(
    private val prefs: Prefs,
    @AppNavigation private val router: Router,
    private val initializer: Initializer,
    private val billingManager: BillingManager,
    private val resourceManager: ResourceManager,
    private val mainScreenLauncher: MainScreenLauncher
) : BasePresenter<LaunchView>() {

    private val initErrorHandler = InitErrorHandler(resourceManager)

    fun onInitialOnCreate() {
        startInitialization(initializer.init())
    }

    private fun startInitialization(single: Single<InitResult>) {
        single
            .doOnSubscribe {
                viewState.showInitAnimation()
            }
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { initResult ->
                    viewState.hideInitAnimation(500) {
                        initResult.message?.let {
                            viewState.showInitMessage(it)
                        } ?: showContent()
                    }
                },
                { error ->
                    viewState.hideInitAnimation(1000) {
                        initErrorHandler.proceed(error) {
                            viewState.showInitErrorDialog(it)
                        }
                    }
                })
            .connect()
    }

    private fun showContent() {
        if (!prefs.wasIntroShowing) {
            router.replaceScreen(Screens.IntroScreen(true))
            viewState.showMainContainer(true)
            viewState.setContentReadyFlag()
        } else {
            mainScreenLauncher.requestConsentOrLaunch(compositeDisposable) {
                viewState.showMainContainer(true)
                viewState.setContentReadyFlag()
            }
        }
    }

    fun onToSubscriptionClick(activity: Activity) {
        billingManager.queryActiveSubscriptions().blockingGet().firstOrNull()?.sku?.let {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/account/subscriptions?sku=$it&package=com.krio.android.loran")
            )
            activity.startActivity(browserIntent)
        }
    }

    fun onInitDialogDismiss() {
        showContent()
    }

    fun onRetryClick(error: Throwable) {
        when (error) {
            is BillingClientNotReadyException -> startInitialization(initializer.init())
            is BadBillingResponseException -> {
                when (error.queryType) {
                    QueryType.QUERY_ACTIVE_SUBSCRIPTIONS -> startInitialization(initializer.queryActiveSubscriptions())
                    QueryType.QUERY_PURCHASE_HISTORY -> startInitialization(initializer.querySubscriptionHistory())
                    QueryType.QUERY_PURCHASES_DETAILS -> throw UnsupportedOperationException("Querying purchases details is not a part of initialization process")
                }
            }
            is ServerErrorException -> startInitialization(initializer.sendPurchasesToServer())
            is SubscriptionsNotSupportedException -> startInitialization(initializer.requireSubscriptionsSupported())
            is PurchasesNotValidException -> startInitialization(initializer.sendPurchasesToServer())
            else -> startInitialization(initializer.init())
        }
    }

    fun onSupportClick() {
        val mailText = """
            |${resourceManager.getString(R.string.support_email_text_identification_data)}
            |User Id: ${prefs.userId}
            |Platform: Android
            |App Version: ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})
            |
            |${resourceManager.getString(R.string.support_email_text_feedback)}
            |
            |
        """.trimMargin()

        viewState.sendEmail(
            resourceManager.getString(R.string.support_email),
            resourceManager.getString(R.string.support_email_subject),
            mailText,
            resourceManager.getString(R.string.support_email_chooser_title)
        )

        router.exit()
    }

    fun onExitClick() = router.exit()

    fun onBackPressed() = router.exit()
}