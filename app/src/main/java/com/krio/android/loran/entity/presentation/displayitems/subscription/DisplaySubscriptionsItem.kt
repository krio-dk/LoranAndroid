package com.krio.android.loran.entity.presentation.displayitems.subscription

/**
 * Created by Dmitriy Kolmogorov on 15.09.2018.
 */
data class DisplaySubscriptionsItem(
        val availableSubscriptions: List<DisplaySubscriptionItem>,
        val activeSubscription: DisplaySubscriptionActiveItem?
)