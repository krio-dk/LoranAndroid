package com.krio.android.loran.entity.thirdparty.tmdb.season

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 02.03.2018.
 */
data class TmdbSeasonsItem(
        @SerializedName("air_date") val airDate: String,
        @SerializedName("episode_count") val episodeCount: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("poster_path") val posterPath: String?,
        @SerializedName("season_number") val seasonNumber: Int
)