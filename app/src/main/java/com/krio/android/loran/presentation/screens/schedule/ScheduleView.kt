package com.krio.android.loran.presentation.screens.schedule

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface ScheduleView : MvpView {
    fun showPageProgress(show: Boolean)
    fun showPageError(show: Boolean, message: String? = null)
    fun showEmptyProgress(show: Boolean)
    fun showEmptyView(show: Boolean, message: String? = null)
    fun showEmptyError(show: Boolean, message: String?)
    fun showData(show: Boolean, data: List<Any>)
    fun showRefreshProgress(show: Boolean)
    fun enableToolbarScrolling(enable: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun resetEndlessScroll()
}