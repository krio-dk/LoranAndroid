package com.krio.android.loran.model.repository

import android.content.Context
import android.graphics.drawable.Drawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import com.krio.android.loran.model.data.glide.GlideApp
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
class ImagesRepository @Inject constructor(private val context: Context) {

    fun preloadImages(urls: List<String>): Single<Boolean> {
        if (urls.isNotEmpty()) {
            val relay = BehaviorRelay.create<Boolean>()
            val requestListener = ImagesRequestListener(relay, urls.count())

            return relay.firstOrError().doOnSubscribe {
                urls.forEach { url ->
                    GlideApp.with(context)
                            .load(url)
                            .listener(requestListener)
                            .preload()
                }
            }
        } else {
            return Single.just(true)
        }
    }

    class ImagesRequestListener(
            private val relay: Relay<Boolean>,
            private val totalCount: Int
    ) : RequestListener<Drawable> {

        private var count = 0

        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
            processResult()
            return true
        }

        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            processResult()
            return true
        }

        private fun processResult() {
            count++
            if (count == totalCount) {
                relay.accept(true)
            }
        }
    }
}