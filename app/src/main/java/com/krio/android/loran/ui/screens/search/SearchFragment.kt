package com.krio.android.loran.ui.screens.search

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayProgressItem
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayRefreshItem
import com.krio.android.loran.entity.presentation.displayitems.search.DisplaySearchItem
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.presentation.global.list.ProgressItemAdapterDelegate
import com.krio.android.loran.presentation.global.list.RefreshItemAdapterDelegate
import com.krio.android.loran.presentation.screens.search.SearchPresenter
import com.krio.android.loran.presentation.screens.search.SearchView
import com.krio.android.loran.presentation.screens.search.list.EmptyItemAdapterDelegate
import com.krio.android.loran.presentation.screens.search.list.SearchItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.DiffCallback
import com.krio.android.loran.ui.global.EndlessScrollListener
import com.krio.android.loran.ui.global.ZeroViewHolder
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick

class SearchFragment : BaseFragment(), SearchView {

    companion object {
        const val VIEW_TYPE_SHOW = 1
        const val VIEW_TYPE_PROGRESS = 2
        const val VIEW_TYPE_REFRESH = 3
        const val VIEW_TYPE_EMPTY = 4

        const val VISIBLE_THRESHOLD = 9
    }

    override val layoutRes = R.layout.fragment_search

    private val adapter = SearchAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null


    @InjectPresenter
    lateinit var presenter: SearchPresenter

    @ProvidePresenter
    fun providePresenter(): SearchPresenter {
        return Toothpick.openScope(DI.CATALOG_SCOPE).getInstance(SearchPresenter::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = this@SearchFragment.adapter

            addOnScrollListener(endlessScrollListener)

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == SCROLL_STATE_DRAGGING) {
                        hideKeyboard()
                    }
                }
            })

            addItemDecoration(
                    HorizontalDividerItemDecoration.Builder(context)
                            .colorResId(R.color.colorDarkHintBackground)
                            .sizeResId(R.dimen.divider_size)
                            .showLastDivider()
                            .build()
            )
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.refreshShows()
        }

        swipeToRefresh.isEnabled = false

        searchView.setOnSearchViewListener(searchViewListener)
        searchView.showSearch(false)
    }

    private val endlessScrollListener = EndlessScrollListener(VISIBLE_THRESHOLD, VIEW_TYPE_PROGRESS) {
        presenter.loadNextPage()
    }

    private val searchViewListener = object : MaterialSearchView.SearchViewListener {
        override fun onSearchViewShown() {
            hideKeyboard()
        }

        override fun onSearchViewClosed() {
            presenter.onBackPressed()
        }
    }

    private val searchQueryTextListener = object : MaterialSearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            presenter.searchTvShow(newText)
            return false
        }
    }

    override fun enableSearchViewListener() {
        searchView?.setOnQueryTextListener(searchQueryTextListener)
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showHorizontalProgress(show: Boolean) {
        horizontalProgressView?.visible(show)
    }

    override fun showPageProgress(show: Boolean) {
        adapter.showProgress(show)
    }

    override fun showPageError(show: Boolean, message: String?) {
        adapter.showRefresh(show, message)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun showSearchResult(show: Boolean, tvShows: List<Any>) {
        recyclerView?.visible(true)
        adapter.setData(tvShows)
    }

    override fun scrollToTop() {
        recyclerView?.scrollToPosition(0)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun resetEndlessScroll() {
        endlessScrollListener.reset()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class SearchAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            setHasStableIds(true)
            items = mutableListOf()
            delegatesManager.addDelegate(VIEW_TYPE_SHOW, SearchItemAdapterDelegate {
                hideKeyboard()
                presenter.onShowClicked(it)
            })
            delegatesManager.addDelegate(VIEW_TYPE_PROGRESS, ProgressItemAdapterDelegate())
            delegatesManager.addDelegate(VIEW_TYPE_REFRESH, RefreshItemAdapterDelegate { presenter.loadNextPage() })
            delegatesManager.addDelegate(VIEW_TYPE_EMPTY, EmptyItemAdapterDelegate())
        }

        fun setData(shows: List<Any>) {
            val oldItems = ArrayList(items)
            val progress = isProgress()

            items.clear()
            items.addAll(shows)
            if (progress) items.add(DisplayProgressItem())

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }

        fun showProgress(isVisible: Boolean) {
            val oldItems = ArrayList(items)
            val currentProgress = isProgress()

            if (isVisible && !currentProgress) items.add(DisplayProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }

        fun showRefresh(show: Boolean, message: String?) {
            val oldItems = ArrayList(items)
            val currentRefresh = isRefresh()

            if (show && !currentRefresh) items.add(DisplayRefreshItem(message
                    ?: getString(R.string.error_unknown)))
            else if (!show && currentRefresh) items.remove(items.last())

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }


        private fun isProgress() = items.isNotEmpty() && items.last() is DisplayProgressItem

        private fun isRefresh() = items.isNotEmpty() && items.last() is DisplayRefreshItem

        override fun getItemId(position: Int): Long {
            super.getItemId(position)
            val item = items[position]
            return when (item) {
                is DisplaySearchItem -> item.serialTmdbId.toLong()
                else -> RecyclerView.NO_ID
            }
        }
    }
}