package com.krio.android.loran.presentation.screens.schedule

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.domain.core.ad.Ad
import com.krio.android.loran.entity.domain.core.ad.AdScreenConfiguration
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayDateItem
import com.krio.android.loran.entity.presentation.displayitems.schedule.DisplayScheduleItem
import com.krio.android.loran.model.interactor.AdInteractor
import com.krio.android.loran.model.interactor.FavoriteInteractor
import com.krio.android.loran.model.interactor.ScheduleInteractor
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.global.Paginator
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@InjectViewState
class SchedulePresenter @Inject constructor(
    private val innerRouter: Router,
    @AppNavigation private val appRouter: Router,
    private val scheduleInteractor: ScheduleInteractor,
    private val favoriteInteractor: FavoriteInteractor,
    private val adInteractor: AdInteractor,
    private val resourceManager: ResourceManager,
    private val refreshRelay: PublishRelay<Boolean>,
    private val errorHandler: ErrorHandler
) : BasePresenter<ScheduleView>() {

    var clearAdapterRelay = PublishRelay.create<Boolean>()

    private var cachedDisplayScheduleItems = listOf<DisplayScheduleItem>()

    private var adConfiguration: AdScreenConfiguration? = null

    var dataWasReceived = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        observeRefresh()
        refreshSchedule()
    }

    private val paginator: Paginator<DisplayScheduleItem> = Paginator(
        { page -> scheduleInteractor.getScheduleItems(page) },
        object : Paginator.ViewController<DisplayScheduleItem> {

            override fun showEmptyProgress(show: Boolean) {
                viewState.showEmptyProgress(show)
            }

            override fun showEmptyError(show: Boolean, error: Throwable?) {
                viewState.showRefreshProgress(false)
                if (error != null) {
                    errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                } else {
                    viewState.showEmptyError(show, null)
                }
            }

            override fun showErrorMessage(error: Throwable) {
                errorHandler.proceed(error) { viewState.showPageError(true, it) }
            }

            override fun showEmptyView(show: Boolean) {
                if (show) {
                    showEmptyViewAccordingToFavoritesExistence()
                } else {
                    viewState.showRefreshProgress(false)
                    viewState.showEmptyView(false)
                }
            }

            override fun showData(show: Boolean, data: List<DisplayScheduleItem>) {
                cachedDisplayScheduleItems = data

                viewState.showRefreshProgress(false)
                viewState.showData(show, getDataFilledWithDate(getDataFilledWithAd()))
                viewState.enableToolbarScrolling(show)

                dataWasReceived = true
            }

            override fun showRefreshProgress(show: Boolean) {
                viewState.showRefreshProgress(show)
            }

            override fun showPageProgress(show: Boolean) {
                viewState.showPageProgress(show)
            }
        }
    )

    private fun getDataFilledWithAd(): List<Any> {
        val dataFilledWithAd = mutableListOf<Any>()

        cachedDisplayScheduleItems.forEachIndexed { index, item ->
            adConfiguration?.let {
                if (index != 0 && (index == it.startIndex || ((index - it.startIndex) > 0 && (index - it.startIndex) % it.interval == 0))) {
                    dataFilledWithAd.add(Ad)
                }
            }

            dataFilledWithAd.add(item)
        }

        return dataFilledWithAd
    }

    private fun getDataFilledWithDate(inputData: List<Any>): List<Any> {
        val dataFilledWithDate = mutableListOf<Any>()

        var prevDate: String? = null

        inputData.forEach { item ->

            if (item is DisplayScheduleItem) {
                val date = item.scheduleDate.substring(0, 10)
                if (date != prevDate) {
                    dataFilledWithDate.add(DisplayDateItem(date))
                    prevDate = date
                }
            }

            dataFilledWithDate.add(item)
        }

        return dataFilledWithDate
    }

    override fun onDestroy() {
        paginator.release()
        super.onDestroy()
    }

    fun refreshSchedule() {
        getAdConfiguration {
            dataWasReceived = false
            viewState.resetEndlessScroll()
            paginator.refresh()
        }
    }

    private fun getAdConfiguration(callback: () -> Unit) {
        adInteractor.getAdScreenConfiguration("schedule_screen")
            .doOnSubscribe { viewState.showEmptyError(false, null) }
            .doAfterTerminate { callback.invoke() }
            .subscribe(
                { adConfigurationResponse ->
                    adConfiguration = adConfigurationResponse.adScreenConfiguration
                },
                {
                    // do nothing
                }
            )
            .connect()
    }

    fun loadNextPage() {
        viewState.showPageError(false)
        paginator.loadNewPage()
    }

    fun onSubscriptionDetailsClicked() {
        appRouter.navigateTo(Screens.SubscriptionScreen)
    }

    private fun showEmptyViewAccordingToFavoritesExistence() {
        favoriteInteractor
            .hasFavorites()
            .doAfterTerminate { viewState.showRefreshProgress(false) }
            .subscribe(
                { hasFavorites ->
                    val message = resourceManager.getString(if (hasFavorites) R.string.schedule_text_empty_message else R.string.schedule_text_no_favorites_message)
                    viewState.showEmptyView(true, message)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showEmptyError(true, it) }
                }
            )
            .connect()
    }

    private fun observeRefresh() {
        refreshRelay.subscribe {
            clearAdapterRelay.accept(true)
            refreshSchedule()

        }.connect()
    }

    fun onBackPressed() = innerRouter.exit()
}