package com.krio.android.loran.presentation.screens.serial.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayTranslationItem
import kotlinx.android.synthetic.main.item_translation.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class TranslationItemAdapterDelegate(
        private val translationClickListener: (DisplayTranslationItem) -> Unit,
        private val favoriteClickListener: (DisplayTranslationItem) -> Unit
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplayTranslationItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_translation, parent, false)
        return TranslationViewHolder(view, translationClickListener, favoriteClickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as TranslationViewHolder).bind(items[position] as DisplayTranslationItem)
    }

    class TranslationViewHolder(
            val view: View,
            translationClickListener: (DisplayTranslationItem) -> Unit,
            favoriteClickListener: (DisplayTranslationItem) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        private lateinit var displayTranslationItem: DisplayTranslationItem

        init {
            view.setOnClickListener { translationClickListener.invoke(displayTranslationItem) }
            view.favorite.setOnClickListener { favoriteClickListener.invoke(displayTranslationItem) }
        }

        fun bind(translationItem: DisplayTranslationItem) {
            this.displayTranslationItem = translationItem

            view.name.text = translationItem.translatorName

            view.seasonsCount.text = translationItem.seasonsCount.toString()
            view.episodesCount.text = translationItem.episodesCount.toString()

            view.seasonsCountCaption.text = view.resources.getQuantityString(R.plurals.translators_number_of_seasons, translationItem.seasonsCount)
            view.episodesCountCaption.text = view.resources.getQuantityString(R.plurals.translators_number_of_episodes, translationItem.episodesCount)

            view.favorite.setImageResource(if (translationItem.isFavorite) R.drawable.ic_favorite else R.drawable.ic_favorite_border_black_48dp)
        }
    }
}