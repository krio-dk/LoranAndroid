package com.krio.android.loran.model.system.ad

import android.annotation.SuppressLint
import android.content.Context
import com.google.ads.consent.ConsentInfoUpdateListener
import com.google.ads.consent.ConsentInformation
import com.google.ads.consent.ConsentStatus
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.jakewharton.rxrelay2.BehaviorRelay
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.presentation.global.ErrorHandler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
class ConsentManager @Inject constructor(
    private val context: Context,
    private val prefs: Prefs,
    private val errorHandler: ErrorHandler
) {

    companion object {
        // TODO: update publisher ids
        private val publisherIds = arrayOf("pub-9634394289726484")
    }

    private val consentInformation = ConsentInformation.getInstance(context)

    private val realy = BehaviorRelay.create<Boolean>()

    val needRequestConsent: Single<Boolean>
        get() = realy.firstOrError()

    val hasConsent: Single<Boolean>
        get() = isLimitAdTrackingEnabled()
            .map { isLimitAdTrackingEnabled ->
                if (isLimitAdTrackingEnabled) false
                else prefs.resultGdpr
            }

    init {
        requestConsentInfoUpdate()
    }

    fun update() {
        requestConsentInfoUpdate()
    }

    private fun requestConsentInfoUpdate() {
        consentInformation.requestConsentInfoUpdate(
            publisherIds,
            object : ConsentInfoUpdateListener {
                override fun onConsentInfoUpdated(consentStatus: ConsentStatus) {
                    if (ConsentInformation.getInstance(context).isRequestLocationInEeaOrUnknown) {
                        handleConsentInfoUpdated()
                    } else {
                        prefs.resultGdpr = true
                        realy.accept(false)
                    }
                }

                override fun onFailedToUpdateConsentInfo(errorDescription: String) {
                    realy.accept(false)
                }
            })
    }

    @SuppressLint("CheckResult")
    private fun handleConsentInfoUpdated() {
        isLimitAdTrackingEnabled()
            .subscribe(
                { isLimitAdTrackingEnabled ->
                    if (isLimitAdTrackingEnabled || prefs.wasConsentShowing) {
                        realy.accept(false)
                    } else {
                        realy.accept(true)
                    }
                },
                { error ->
                    realy.accept(false)
                    errorHandler.proceed(error)
                }
            )
    }

    private fun isLimitAdTrackingEnabled() =
        Single.fromCallable { AdvertisingIdClient.getAdvertisingIdInfo(context) }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .map { it.isLimitAdTrackingEnabled }
}