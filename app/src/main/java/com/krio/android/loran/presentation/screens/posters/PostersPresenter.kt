package com.krio.android.loran.presentation.screens.posters

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.presentation.displayitems.posters.DisplayPosterItem
import com.krio.android.loran.model.interactor.CatalogInteractor
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.global.Paginator
import com.krio.android.loran.presentation.screens.serial.SerialPresenter
import com.krio.android.loran.toothpick.DI
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@InjectViewState
class PostersPresenter @Inject constructor(
        private val router: Router,
        private val interactor: CatalogInteractor,
        private val errorHandler: ErrorHandler
) : BasePresenter<PostersView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refreshShows()
    }

    private val paginator: Paginator<DisplayPosterItem> = Paginator(
            { page -> interactor.getPopularSerialsPosterItems(page) },
            object : Paginator.ViewController<DisplayPosterItem> {

                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    if (error != null) {
                        errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                    } else {
                        viewState.showEmptyError(show, null)
                    }
                }

                override fun showErrorMessage(error: Throwable) {
                    errorHandler.proceed(error) { viewState.showPageError(true, it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showData(show: Boolean, data: List<DisplayPosterItem>) {
                    val excessCount = data.size % 3
                    val dataToShow = data.subList(0, data.size - excessCount)

                    viewState.showPosters(show, dataToShow)
                    viewState.showMenuItemSearch(show)
                    viewState.enableToolbarScrolling(show)

                    if (data.size < 40) {
                        Observable.timer(250, TimeUnit.MILLISECONDS)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe { loadNextPage() }
                    }
                }

                override fun showRefreshProgress(show: Boolean) {
                    // do nothing
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }

    fun refreshShows() {
        viewState.resetEndlessScroll()
        paginator.refresh()
    }

    fun loadNextPage() {
        viewState.showPageError(false)
        paginator.loadNewPage()
    }

    fun onSearchClicked() {
        router.navigateTo(Screens.SearchScreen)
    }

    fun onPosterClicked(displayPosterItem: DisplayPosterItem) {
        router.navigateTo(
            Screens.SerialScreen(
                DI.CATALOG_SCOPE,
                SerialPresenter.InitParams(displayPosterItem.serialTmdbId, displayPosterItem.serialName, displayPosterItem.backdropPath)
            )
        )
    }

    fun onBackPressed() = router.exit()
}
