package com.krio.android.loran.ui.global

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.ViewGroup
import com.krio.android.loran.R
import com.krio.android.loran.extensions.visible
import kotlinx.android.synthetic.main.dialog_subscription.*


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class SubscriptionDialog : DialogFragment() {

    companion object {
        private const val ARGS_PARAM_CAPTION = "caption"
        private const val ARGS_PARAM_EXTRA_DESCRIPTION = "extra_description"

        fun newInstance(caption: String, extraDescription: String? = null) = SubscriptionDialog().apply {
            arguments = Bundle().apply {
                putString(ARGS_PARAM_CAPTION, caption)
                putString(ARGS_PARAM_EXTRA_DESCRIPTION, extraDescription)
            }
        }
    }

    var subscriptionClickListener: () -> Unit = { }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        inflater.inflate(R.layout.dialog_subscription, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.getString(ARGS_PARAM_CAPTION)?.let { caption.text = it }
        arguments?.getString(ARGS_PARAM_EXTRA_DESCRIPTION)?.let {
            extraDescription.text = it
            extraContainer.visible(true)
        }

        cancelButton.setOnClickListener { dismiss() }
        detailsButton.setOnClickListener {
            subscriptionClickListener.invoke()
            dismiss()
        }
    }
}