package com.krio.android.loran

import com.krio.android.loran.presentation.screens.person.PersonPresenter
import com.krio.android.loran.presentation.screens.serial.SerialPresenter
import com.krio.android.loran.presentation.screens.web.WebPresenter
import com.krio.android.loran.ui.screens.about.AboutFragment
import com.krio.android.loran.ui.screens.episodes.EpisodesFragment
import com.krio.android.loran.ui.screens.favorite.FavoriteFragment
import com.krio.android.loran.ui.screens.gdpr.GdprFragment
import com.krio.android.loran.ui.screens.gdpr.GdprResultFragment
import com.krio.android.loran.ui.screens.intro.IntroFragment
import com.krio.android.loran.ui.screens.main.MainFragment
import com.krio.android.loran.ui.screens.person.PersonFragment
import com.krio.android.loran.ui.screens.posters.PostersFragment
import com.krio.android.loran.ui.screens.profile.ProfileFragment
import com.krio.android.loran.ui.screens.schedule.ScheduleFragment
import com.krio.android.loran.ui.screens.search.SearchFragment
import com.krio.android.loran.ui.screens.seasons.SeasonsFragment
import com.krio.android.loran.ui.screens.serial.SerialFragment
import com.krio.android.loran.ui.screens.subscription.SubscriptionFragment
import com.krio.android.loran.ui.screens.timeline.TimelineFragment
import com.krio.android.loran.ui.screens.web.WebFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Created by Dmitriy Kolmogorov on 13.02.2018.
 */
object Screens {

    // Intro
    data class IntroScreen(val initFlow: Boolean) : SupportAppScreen() {
        override fun getFragment() = IntroFragment.newInstance(initFlow)
    }

    // Gdpr
    object GdprScreen : SupportAppScreen() {
        override fun getFragment() = GdprFragment()
    }

    object GdprResultScreen : SupportAppScreen() {
        override fun getFragment() = GdprResultFragment()
    }

    // Main
    data class MainScreen(val initParams: MainFragment.InitParams) : SupportAppScreen() {
        override fun getFragment() = MainFragment.newInstance(initParams)
    }

    // Tabs
    object PostersScreen : SupportAppScreen() {
        override fun getFragment() = PostersFragment()
    }

    object ScheduleScreen : SupportAppScreen() {
        override fun getFragment() = ScheduleFragment()
    }

    object TimelineScreen : SupportAppScreen() {
        override fun getFragment() = TimelineFragment()
    }

    object FavoriteScreen : SupportAppScreen() {
        override fun getFragment() = FavoriteFragment()
    }

    object ProfileScreen : SupportAppScreen() {
        override fun getFragment() = ProfileFragment()
    }

    // Inner
    object SearchScreen : SupportAppScreen() {
        override fun getFragment() = SearchFragment()
    }

    data class SerialScreen(val parentScope: String, val initParams: SerialPresenter.InitParams) : SupportAppScreen() {
        override fun getFragment() = SerialFragment.newInstance(parentScope, initParams)
    }

    data class SeasonsScreen(val parentScope: String, val initParams: SeasonsFragment.InitParams) : SupportAppScreen() {
        override fun getFragment() = SeasonsFragment.newInstance(parentScope, initParams)
    }

    data class EpisodesScreen(val parentScope: String, val initParams: EpisodesFragment.InitParams) : SupportAppScreen() {
        override fun getFragment() = EpisodesFragment.newInstance(parentScope, initParams)
    }

    data class PersonScreen(val parentScope: String, val initParams: PersonPresenter.InitParams) : SupportAppScreen() {
        override fun getFragment() = PersonFragment.newInstance(parentScope, initParams)
    }

    // Profile
    object SubscriptionScreen: SupportAppScreen() {
        override fun getFragment() = SubscriptionFragment()
    }

    data class WebScreen(val parentScope: String, val initParams: WebPresenter.InitParams) : SupportAppScreen() {
        override fun getFragment() = WebFragment.newInstance(parentScope, initParams)
    }

    object AboutScreen: SupportAppScreen() {
        override fun getFragment() = AboutFragment()
    }
}