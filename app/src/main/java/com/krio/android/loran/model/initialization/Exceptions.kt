package com.krio.android.loran.model.initialization

/**
 * Created by Dmitriy Kolmogorov on 15.09.2018.
 */
class SubscriptionsNotSupportedException : Error()
class ServerErrorException(val reason: Throwable): Error()
class PurchasesNotValidException : Error()