package com.krio.android.loran.presentation.screens.favorite

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.domain.core.ad.Ad
import com.krio.android.loran.entity.domain.core.ad.AdScreenConfiguration
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteCaptionActive
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteItem
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteTranslationItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.*
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.screens.seasons.SeasonsPresenter
import com.krio.android.loran.presentation.screens.serial.SerialPresenter
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import com.krio.android.loran.ui.screens.seasons.SeasonsFragment
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@InjectViewState
class FavoritePresenter @Inject constructor(
        private val favoriteInteractor: FavoriteInteractor,
        private val checkMarkInteractor: CheckMarkInteractor,
        private val activateSerialInteractor: ActivateSerialInteractor,
        private val imagesInteractor: ImagesInteractor,
        private val subscriptionInteractor: SubscriptionInteractor,
        private val adInteractor: AdInteractor,
        private val prefs: Prefs,
        private val resourceManager: ResourceManager,
        private val refreshRelay: PublishRelay<Boolean>,
        private val innerRouter: Router,
        @AppNavigation private val appRouter: Router,
        private val appodealManager: AppodealManager,
        private val errorHandler: ErrorHandler
) : BasePresenter<FavoriteView>() {

    companion object {
        private const val TAG = "FavoritePresenter"
    }

    var clearAdapterRelay = PublishRelay.create<Boolean>()

    var refreshDisposable: Disposable? = null

    private var rewardedVideoDisposable: Disposable? = null

    private var cachedDisplayFavoriteItems = listOf<DisplayFavoriteItem>()

    private var maxActiveSerialsCount: Int? = null

    private var adConfiguration: AdScreenConfiguration? = null

    private var dataWasReceived = false

    var isSubscriptionPresent = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showEmptyProgress(true)
        observeRefresh()
        refreshFavorite()
    }

    private fun getData() {
        favoriteInteractor.getFavoriteItems()
            .flatMap { pair ->
                imagesInteractor
                    .preloadPosters(
                        pair.first.mapNotNull { it.posterPath },
                        PosterSize.POSTER_SIZE_342
                    )
                    .flatMap { Single.just(pair) }
            }
            .flatMap { pair ->
                imagesInteractor
                    .preloadBackdrops(
                        pair.first.mapNotNull { it.backdropPath },
                        BackdropSize.BACKDROP_SIZE_780
                    )
                    .flatMap { Single.just(pair) }
            }
            .flatMap { data ->
                Single.zip(
                    Single.just(data),
                    subscriptionInteractor.getActiveSubscription(),
                    BiFunction<Pair<List<DisplayFavoriteItem>, Int?>, String, Triple<List<DisplayFavoriteItem>, Int?, String>> { pair, currentSubscription ->
                        Triple(pair.first, pair.second, currentSubscription)
                    }
                )
            }
            .doOnSubscribe {
                viewState.showEmptyError(false, null)
            }
            .doAfterTerminate {
                viewState.showEmptyProgress(false)
                viewState.showRefreshProgress(false)
            }
            .subscribe(
                { result ->

                    val data = result.first
                    val maxActiveSerialsCount = result.second
                    val currentSubscription = result.third

                    if (data.isEmpty()) {
                        viewState.showEmptyView(true)
                    } else {
                        viewState.showEmptyView(false)

                        isSubscriptionPresent = currentSubscription.isNotEmpty()

                        val sortedData = when {
                            !isSubscriptionPresent -> data.sortedWith(
                                compareBy<DisplayFavoriteItem> { it.isActive }
                                    .thenBy { it.status }
                                    .thenByDescending { it.voteAverage }
                            )

                            else -> data.sortedWith(compareBy<DisplayFavoriteItem> { it.status }.thenByDescending { it.voteAverage })
                        }

                        this.cachedDisplayFavoriteItems = sortedData
                        this.maxActiveSerialsCount = maxActiveSerialsCount

                        val hasNotActiveSerials = data.find { !it.isActive } != null
                        val showWaring = if (isSubscriptionPresent) false else hasNotActiveSerials

                        viewState.showNotActiveWarning(showWaring)
                        viewState.showData(true, getDataFilledWithDelimiters(getDataFilledWithAd()))
                        viewState.enableToolbarScrolling(true)

                        dataWasReceived = true
                    }
                },
                { error ->
                    viewState.showNotActiveWarning(false)
                    viewState.showData(false, listOf())
                    errorHandler.proceed(error) { viewState.showEmptyError(true, it) }
                }
            )
            .connect()
    }

    private fun getDataFilledWithDelimiters(inputData: List<Any>): List<Any> {
        val dataFilledWithDelimiters = mutableListOf<Any>()

        /*
        if (!isSubscriptionPresent) {
            val list = mutableListOf<Any>().apply {
                add(
                    DisplayFavoriteCaptionActive(
                        inputData.filter { it is DisplayFavoriteItem && it.isActive }.count(),
                        maxActiveSerialsCount
                    )
                )
                addAll(inputData)

                val notActiveItemIndex = indexOfFirst { it is DisplayFavoriteItem && !it.isActive }

                if (notActiveItemIndex != -1) {
                    add(notActiveItemIndex, DisplayFavoriteCaptionNotActive())
                }
            }

            dataFilledWithDelimiters.addAll(list)

        } else {
            dataFilledWithDelimiters.addAll(inputData)
        }
         */

        dataFilledWithDelimiters.addAll(inputData)
        return dataFilledWithDelimiters
    }

    private fun getDataFilledWithAd(): List<Any> {
        val dataFilledWithAd = mutableListOf<Any>()

        cachedDisplayFavoriteItems.forEachIndexed { index, item ->
            adConfiguration?.let {
                if (index != 0 && (index == it.startIndex || ((index - it.startIndex) > 0 && (index - it.startIndex) % it.interval == 0))) {
                    dataFilledWithAd.add(Ad)
                }
            }

            dataFilledWithAd.add(item)
        }

        return dataFilledWithAd
    }

    fun refreshFavorite() {
        getAdConfiguration {
            dataWasReceived = false
            getData()
        }
    }

    private fun getAdConfiguration(callback: () -> Unit) {
        adInteractor.getAdScreenConfiguration("favorite_screen")
            .doOnSubscribe { viewState.showEmptyError(false, null) }
            .doAfterTerminate { callback.invoke() }
            .subscribe(
                { adConfigurationResponse ->
                    adConfiguration = adConfigurationResponse.adScreenConfiguration
                },
                {
                    // do nothing
                }
            )
            .connect()
    }

    fun onFavoriteItemClicked(displayFavoriteItem: DisplayFavoriteItem) {
        innerRouter.navigateTo(
            Screens.SerialScreen(
                DI.FAVORITE_SCOPE,
                SerialPresenter.InitParams(
                    displayFavoriteItem.serialTmdbId,
                    displayFavoriteItem.serialName,
                    displayFavoriteItem.backdropPath
                )
            )
        )
    }

    fun onRemoveFromFavoriteClicked(
        displayFavoriteItem: DisplayFavoriteItem,
        displayFavoriteTranslationItem: DisplayFavoriteTranslationItem
    ) {
        viewState.showRemoveFromFavoritesDialog {

            Analytics.logEvent("remove_from_favorite_clicked") {
                putString("serial_name", displayFavoriteItem.serialName)
                putString("translator_name", displayFavoriteTranslationItem.translatorName)
                putString("from", "favorite screen")
                prefs.userId?.let { putInt("user_id", it) }
            }

            favoriteInteractor
                .removeFromFavorite(
                    displayFavoriteItem.serialTmdbId,
                    displayFavoriteTranslationItem.translatorId
                )
                .subscribe(
                    { (success) ->
                        if (success == true) {
                            acceptRefresh()

                            displayFavoriteItem.translationItems.remove(
                                displayFavoriteTranslationItem
                            )

                            if (displayFavoriteItem.translationItems.isEmpty()) {
                                viewState.removeItem(displayFavoriteItem)
                            } else {
                                viewState.updateItem(displayFavoriteItem)
                            }
                        }
                    },
                    { error ->
                        errorHandler.proceed(error, { viewState.showMessage(it) })
                    }
                )
                .connect()
        }
    }

    fun onTranslationClicked(
        displayFavoriteItem: DisplayFavoriteItem,
        displayFavoriteTranslationItem: DisplayFavoriteTranslationItem
    ) {
        val presenterParams = SeasonsPresenter.InitParams(
            displayFavoriteItem.serialTmdbId,
            displayFavoriteItem.serialName,
            displayFavoriteItem.backdropPath,
            displayFavoriteTranslationItem.translatorId,
            displayFavoriteTranslationItem.translatorName,
            true
        )

        val params = SeasonsFragment.InitParams(
            closeSerialRootScopeOnExit = true,
            presenterParams = presenterParams
        )

        innerRouter.navigateTo(Screens.SeasonsScreen(DI.FAVORITE_SCOPE, params))
    }

    fun onCheckMarkClicked(
        displayFavoriteItem: DisplayFavoriteItem,
        displayFavoriteTranslationItem: DisplayFavoriteTranslationItem
    ) {
        displayFavoriteTranslationItem.nextEpisode?.let { nextEpisode ->

            Analytics.logEvent("add_check_mark_clicked") {
                putString("serial_name", displayFavoriteItem.serialName)
                putString("translator_name", displayFavoriteTranslationItem.translatorName)
                putInt("season_number", nextEpisode.seasonNumber)
                putInt("episode_number", nextEpisode.episodeNumber)
                putString("from", "favorite screen")
                prefs.userId?.let { putInt("user_id", it) }
            }

            addCheckMark(
                displayFavoriteItem.serialTmdbId,
                nextEpisode.seasonNumber,
                nextEpisode.episodeNumber
            )
        }
    }

    fun onSubscriptionDetailsClicked() = appRouter.navigateTo(Screens.SubscriptionScreen)

    fun onActivateClicked(displayFavoriteItem: DisplayFavoriteItem) {

        Analytics.logEvent("activate_serial_clicked") {
            putString("serial_name", displayFavoriteItem.serialName)
            prefs.userId?.let { putInt("user_id", it) }
        }

        if (isSubscriptionPresent) {
            performActivateSerial(displayFavoriteItem.serialTmdbId, displayFavoriteItem.serialName, adWatched = false)
        } else {
            checkIfNeedToShowRewardedVideoDialog(displayFavoriteItem)
        }
    }

    private fun checkIfNeedToShowRewardedVideoDialog(displayFavoriteItem: DisplayFavoriteItem) {
        adInteractor.needShowRewardedVideoDialog(displayFavoriteItem.serialTmdbId)
            .subscribe(
                { needShow ->
                    if (needShow) showRewardedVideoDialog(displayFavoriteItem)
                    else performActivateSerial(displayFavoriteItem.serialTmdbId, displayFavoriteItem.serialName, adWatched = false)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    private fun showRewardedVideoDialog(displayFavoriteItem: DisplayFavoriteItem) {
        appodealManager.cacheRewardedVideo()
        subscriptionInteractor
            .getLowestSubscriptionPrice()
            .subscribe(
                { totalPrice ->
                    viewState.showRewardedVideoDialog(
                        displayFavoriteItem.serialTmdbId,
                        displayFavoriteItem.serialName,
                        totalPrice
                    )

                    Analytics.logEvent("rewarded_video_dialog_shown") {
                        putString("serial_name", displayFavoriteItem.serialName)
                        putString("from", "favorite screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    fun onWatchAdClicked(serialTmdbId: Int, serialName: String) {

        Analytics.logEvent("watch_video_ad_clicked") {
            putString("serial_name", serialName)
            putString("from", "favorite screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        rewardedVideoDisposable = appodealManager.showRewardedVideo(viewState::setRewardedVideoProgress)
            .subscribe (
                { finished ->
                    println("Rewarded Video: presenter, finished = $finished")

                    Analytics.logEvent("rewarded_video_closed") {
                        putBoolean("finished", finished)
                        putString("from", "favorite screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    if (finished) {
                        performActivateSerial(serialTmdbId, serialName, adWatched = true, adShowTry = true)
                    } else {
                        viewState.showRewardedVideoError(true, resourceManager.getString(R.string.rewarded_video_dialog_error_not_finished))
                    }
                },
                { error ->

                    println("Rewarded Video: presenter, error = $error")

                    Analytics.logEvent("rewarded_video_failed") {
                        putString("from", "favorite screen")
                        putString("error", error.message)
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    if (error !is AppodealManager.AdDisabledException && error !is AppodealManager.RewardedVideoAdNotInitializedException) {
                        performActivateSerial(serialTmdbId, serialName, adWatched = false, adShowTry = true)
                    }

                    when (error) {
                        is AppodealManager.AdDisabledException,
                        is AppodealManager.RewardedVideoAdNotInitializedException -> {
                            viewState.hideRewardedVideoDialog()
                            viewState.showMessage(resourceManager.getString(R.string.common_restart_app))
                        }
                        is AppodealManager.CantShowRewardedVideoException -> {
                            // don't show anything
                        }
                        else -> {
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                    }
                }
            )
            .also { it.connect() }
    }

    fun onDismissRewardedVideoDialog() {
        rewardedVideoDisposable?.dispose()
    }

    private fun performActivateSerial(serialTmdbId: Int, serialName: String, adWatched: Boolean, adShowTry: Boolean = false) {
        activateSerialInteractor
            .activateSerial(serialTmdbId, adWatched)
            .doOnSubscribe {
                viewState.showNotActiveWarning(false)
                viewState.showData(false, emptyList())
                viewState.showEmptyProgress(true)
                viewState.enableToolbarScrolling(false)
            }
            .delay(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { (success, _) ->
                    if (success == true) {

                        Analytics.logEvent("activate_serial_success") {
                            putString("serial_name", serialName)
                            prefs.userId?.let { putInt("user_id", it) }
                        }

                        if (adShowTry && !adWatched) viewState.showMessage(resourceManager.getString(R.string.rewarded_video_message_lucky))

                        viewState.hideRewardedVideoDialog()
                        acceptRefresh()
                        refreshFavorite()

                    } else {

                        Analytics.logEvent("activate_serial_blocked") {
                            putString("serial_name", serialName)
                            prefs.userId?.let { putInt("user_id", it) }
                        }

                        viewState.showRewardedVideoError(true, resourceManager.getString(R.string.rewarded_video_dialog_error_show))
                        refreshFavorite()
                    }
                },
                { error ->

                    Analytics.logEvent("activate_serial_error") {
                        putString("serial_name", serialName)
                        prefs.userId?.let { putInt("user_id", it) }
                    }

                    viewState.hideRewardedVideoDialog()
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                    refreshFavorite()
                }
            )
            .connect()
    }

    // TODO: is need to switch scheduler to main?
    fun onDeactivateClicked(displayFavoriteItem: DisplayFavoriteItem) {

        Analytics.logEvent("deactivate_serial_clicked") {
            putString("serial_name", displayFavoriteItem.serialName)
            prefs.userId?.let { putInt("user_id", it) }
        }

        activateSerialInteractor
            .deactivateSerial(displayFavoriteItem.serialTmdbId)
            .doOnSubscribe {
                viewState.showNotActiveWarning(false)
                viewState.showData(false, emptyList())
                viewState.showEmptyProgress(true)
                viewState.enableToolbarScrolling(false)
            }
            .delay(500, TimeUnit.MILLISECONDS)
            .subscribe(
                { (success) ->
                    if (success == true) {
                        acceptRefresh()
                        refreshFavorite()
                    } else {
                        viewState.showMessage(resourceManager.getString(R.string.favorite_problem_deactivate_serial))
                        refreshFavorite()
                    }
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                    refreshFavorite()
                }
            )
            .connect()
    }

    fun onItemRemoved(items: List<Any>) {

        cachedDisplayFavoriteItems =
            items.filter { it is DisplayFavoriteItem }.map { it as DisplayFavoriteItem }

        val hasActiveItems = items.any { it is DisplayFavoriteItem && it.isActive }
        val hasNotActiveItems = items.any { it is DisplayFavoriteItem && !it.isActive }

        if (hasActiveItems || hasNotActiveItems) {
            viewState.updateCaptionActiveItem(
                DisplayFavoriteCaptionActive(
                    cachedDisplayFavoriteItems.filter { it.isActive }.count(),
                    maxActiveSerialsCount
                )
            )
        }

        when {
            !hasActiveItems && !hasNotActiveItems -> {
                viewState.showNotActiveWarning(false)
                viewState.showData(false, emptyList())
                viewState.showEmptyView(true)
                viewState.enableToolbarScrolling(false)
            }

            hasActiveItems && !hasNotActiveItems -> viewState.removeCaptionNotActiveItem()
        }

        if (!hasNotActiveItems) viewState.showNotActiveWarning(false)
    }

    private fun addCheckMark(tmdbId: Int, seasonNumber: Int, episodeNumber: Int) {
        checkMarkInteractor
            .addCheckMark(tmdbId, seasonNumber, episodeNumber)
            .subscribe(
                { response ->
                    if (response.success) {
                        acceptRefresh()
                        updateItem(tmdbId)
                    } else {
                        viewState.cancelUpdating()
                        viewState.showMessage(resourceManager.getString(R.string.episodes_problem_add_check_mark))
                    }
                },
                { error ->
                    viewState.cancelUpdating()
                    errorHandler.proceed(error, { viewState.showMessage(it) })
                }
            )
            .connect()
    }


    private fun updateItem(serialTmdbId: Int) {
        favoriteInteractor.getFavoriteItem(serialTmdbId)
            .subscribe(
                { displayFavoriteItem ->
                    viewState.updateItem(displayFavoriteItem)
                },
                { error ->
                    viewState.cancelUpdating()
                    errorHandler.proceed(error, { viewState.showMessage(it) })
                }
            )
            .connect()
    }

    private fun observeRefresh() {
        refreshDisposable = refreshRelay.subscribe {
            clearAdapterRelay.accept(true)
            viewState.showNotActiveWarning(false)
            viewState.showData(false, emptyList())
            viewState.showEmptyProgress(true)
            refreshFavorite()
        }.apply { connect() }
    }

    private fun acceptRefresh() {
        refreshDisposable?.dispose()
        refreshRelay.accept(true)
        observeRefresh()
    }

    fun onBackPressed() = innerRouter.exit()
}