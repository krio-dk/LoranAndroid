package com.krio.android.loran.entity.presentation.displayitems.episodes

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class DisplayEpisodeItem(
        val episodeNumber: Int,
        var isSeen: Boolean,
        val airDate: String?,
        val episodeName: String?,
        val overview: String?,
        val stillPath: String?,
        var isUpdating: Boolean = false
)