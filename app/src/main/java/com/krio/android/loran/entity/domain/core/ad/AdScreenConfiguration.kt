package com.krio.android.loran.entity.domain.core.ad

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 2019-06-06.
 */
data class AdScreenConfiguration(
    @SerializedName("start_index") val startIndex: Int,
    @SerializedName("interval") val interval: Int
)