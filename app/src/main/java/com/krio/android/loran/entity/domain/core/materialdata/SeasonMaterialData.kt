package com.krio.android.loran.entity.domain.core.materialdata

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class SeasonMaterialData(
        @SerializedName("air_date") var airDate: String? = null,
        @SerializedName("season_name") var seasonName: String? = null,
        @SerializedName("overview") var overview: String? = null,
        @SerializedName("poster_path") var posterPath: String? = null
)