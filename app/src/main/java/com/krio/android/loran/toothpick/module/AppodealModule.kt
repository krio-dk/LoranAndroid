package com.krio.android.loran.toothpick.module

import com.krio.android.loran.model.system.ad.AppodealManager
import toothpick.config.Module

/**
 * Created by krio on 27.05.2018.
 */
class AppodealModule(appodealManager: AppodealManager): Module()  {
    init {

        bind(AppodealManager::class.java).toInstance(appodealManager)
    }
}