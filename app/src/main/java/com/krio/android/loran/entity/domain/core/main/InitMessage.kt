package com.krio.android.loran.entity.domain.core.main

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
data class InitMessage(
        @SerializedName("title") val title: String,
        @SerializedName("text") val text: String,
        @SerializedName("type") val type: InitMessageType

)