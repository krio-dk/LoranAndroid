package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.main.InitMessage

/**
 * Created by Dmitriy Kolmogorov on 13.09.2018.
 */
data class InitResponse (
        @SerializedName("is_purchases_valid") var isPurchasesValid: Boolean,
        @SerializedName("user_id") var userId: Int?,
        @SerializedName("message") var message: InitMessage?,
        @SerializedName("vs") val verifySignature: Boolean?,
        @SerializedName("vi") val verifyInstaller: Boolean?,
        @SerializedName("vpa") val verifyPirateApps: Boolean?,
        @SerializedName("vtps") val verifyThirdPartyStore: Boolean?
)