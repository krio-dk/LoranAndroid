package com.krio.android.loran.ui.screens.subscription

import android.graphics.Rect
import android.os.Bundle
import com.google.android.material.appbar.AppBarLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.presentation.screens.subscription.SubscriptionPresenter
import com.krio.android.loran.presentation.screens.subscription.SubscriptionView
import com.krio.android.loran.presentation.screens.subscription.list.*
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.ZeroViewHolder
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_subscription.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick

class SubscriptionFragment : BaseFragment(), SubscriptionView {

    override val layoutRes = R.layout.fragment_subscription

    private val adapter = SubscriptionsAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    private var clearAdapterDisposable: Disposable? = null

    @InjectPresenter
    lateinit var presenter: SubscriptionPresenter

    @ProvidePresenter
    fun providePresenter(): SubscriptionPresenter {
        val parentScope = DI.APP_SCOPE
        val scope = Toothpick.openScopes(parentScope, DI.subscriptionScope(parentScope))

        return scope.getInstance(SubscriptionPresenter::class.java).apply {
            Toothpick.closeScope(DI.subscriptionScope(parentScope))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        clearAdapterDisposable = presenter.clearAdapterRelay.subscribe {
            adapter.setData(emptyList())
        }
    }

    override fun onDestroy() {
        clearAdapterDisposable?.dispose()
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.baseline_close_white_24)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbarTitle.setText(R.string.subscription_title)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = this@SubscriptionFragment.adapter

            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    parent.adapter?.let { adapter ->

                        val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)
                        val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)
                        val count = adapter.itemCount

                        val layoutParams = view.layoutParams as RecyclerView.LayoutParams

                        val firstPosition = 0
                        val lastPosition = count - 1

                        when (layoutParams.viewAdapterPosition) {
                            firstPosition -> outRect.set(0, 0, 0, 0)
                            lastPosition -> outRect.set(0, 0, 0, fullPadding)
                            else -> outRect.set(0, 0, 0, halfPadding)
                        }
                    }
                }
            })
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.refresh()
        }
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun enableToolbarScrolling(enable: Boolean) {
        toolbarFrameLayout?.let {
            val params = toolbarFrameLayout.layoutParams as AppBarLayout.LayoutParams
            params.scrollFlags = if (enable) {
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            } else 0

            toolbar?.visible(false)
            toolbar?.visible(true)
        }
    }

    override fun showData(show: Boolean, data: List<Any>) {
        recyclerView?.visible(show)
        adapter.setData(data)
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class SubscriptionsAdapter : ListDelegationAdapter<MutableList<Any>>() {

        init {
            items = mutableListOf()

            delegatesManager.addDelegate(SubscriptionDescriptionItemAdapterDelegate())
            delegatesManager.addDelegate(SubscriptionCaptionItemAdapterDelegate())
            delegatesManager.addDelegate(SubscriptionActiveItemAdapterDelegate { presenter.onDetailsClicked(it, requireActivity()) })
            delegatesManager.addDelegate(SubscriptionItemAdapterDelegate { presenter.onSubscribeClicked(it, requireActivity()) })
            delegatesManager.addDelegate(SubscriptionFooterItemAdapterDelegate { presenter.onLinkToGooglePlayClicked(requireActivity()) })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}
