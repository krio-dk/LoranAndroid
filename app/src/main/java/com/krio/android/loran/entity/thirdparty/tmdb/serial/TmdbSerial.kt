package com.krio.android.loran.entity.thirdparty.tmdb.serial

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.main.SerialStatus
import com.krio.android.loran.entity.thirdparty.tmdb.network.TmdbNetwork
import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeasonsItem

/**
 * Created by Dmitriy Kolmogorov on 02.03.2018.
 */
data class TmdbSerial(
        @SerializedName("backdrop_path") val backdropPath: String?,
        @SerializedName("episode_run_time") val episodeRunTime: List<Int>,
        @SerializedName("first_air_date") val firstAirDate: String,
        @SerializedName("homepage") val homepage: String,
        @SerializedName("id") val id: Int,
        @SerializedName("in_production") val inProduction: Boolean,
        @SerializedName("languages") val languages: List<String>,
        @SerializedName("last_air_date") val lastAirDate: String,
        @SerializedName("name") val name: String,
        @SerializedName("networks") var networks: List<TmdbNetwork>,
        @SerializedName("number_of_episodes") var numberOfEpisodes: Int,
        @SerializedName("number_of_seasons") val numberOfSeasons: Int,
        @SerializedName("origin_country") val originCountry: List<String>,
        @SerializedName("original_language") val originalLanguage: String,
        @SerializedName("original_name") val originalName: String,
        @SerializedName("overview") val overview: String,
        @SerializedName("popularity") val popularity: Double,
        @SerializedName("poster_path") var posterPath: String?,
        @SerializedName("seasons") val seasons: List<TmdbSeasonsItem>,
        @SerializedName("status") val status: SerialStatus,
        @SerializedName("type") val type: String,
        @SerializedName("vote_average") val voteAverage: Double,
        @SerializedName("vote_count") val voteCount: Int
)