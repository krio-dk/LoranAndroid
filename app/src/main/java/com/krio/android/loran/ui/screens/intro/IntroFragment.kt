package com.krio.android.loran.ui.screens.intro

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.presentation.screens.intro.IntroPresenter
import com.krio.android.loran.presentation.screens.intro.IntroView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_intro.*
import toothpick.Toothpick
import toothpick.config.Module


class IntroFragment : BaseFragment(), IntroView {

    companion object {
        private const val ARGS_PARAM_INIT_FLOW = "init flow"

        fun newInstance(initFlow: Boolean) = IntroFragment().apply {
            arguments = Bundle().apply {
                putBoolean(ARGS_PARAM_INIT_FLOW, initFlow)
            }
        }
    }

    override val layoutRes = R.layout.fragment_intro

    private lateinit var adapter: PagerAdapter

    @InjectPresenter
    lateinit var presenter: IntroPresenter

    @ProvidePresenter
    fun providePresenter(): IntroPresenter {
        val parentScope = DI.APP_SCOPE
        val initFlow = arguments!![ARGS_PARAM_INIT_FLOW] as Boolean
        val scope = Toothpick.openScopes(parentScope, DI.introScope(parentScope)).apply {
            installModules(object : Module() {
                init {
                    val initParams = IntroPresenter.InitParams(initFlow)
                    bind(IntroPresenter.InitParams::class.java).toInstance(initParams)
                }
            })
        }

        return scope.getInstance(IntroPresenter::class.java).apply {
            Toothpick.closeScope(DI.introScope(parentScope))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ScreenSlidePagerAdapter(childFragmentManager)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pager.adapter = adapter
        indicator.setViewPager(pager)

        nextButton.setOnClickListener { presenter.onNextClick(pager.currentItem, adapter.count) }
        closeButton.setOnClickListener { presenter.onBackPressed() }
    }

    override fun setCloseButtonText(text: String) {
        closeButton?.text = text
    }

    override fun showNextPage() {
        pager?.currentItem = pager.currentItem + 1
    }

    override fun showConfirmExitDialog(onExitConfirmed: () -> Unit) {
        activity?.let {
            android.app.AlertDialog.Builder(it, R.style.DialogTheme)
                    .setTitle(R.string.intro_alert_exit_confirm_title)
                    .setMessage(R.string.intro_alert_exit_confirm_text)
                    .setCancelable(true)
                    .setPositiveButton(R.string.intro_alert_exit_confirm_button_skip) { _, _ -> onExitConfirmed() }
                    .setNegativeButton(R.string.intro_alert_exit_confirm_button_cancel) { _, _ -> /* do nothing */ }
                    .create()
                    .show()
        }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val pagesCount = 5

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> IntroPageFragment.newInstance(R.drawable.intro_notification, R.string.intro_description_notification)
                1 -> IntroPageFragment.newInstance(R.drawable.intro_serial, R.string.intro_description_serial_translations)
                2 -> IntroPageFragment.newInstance(R.drawable.intro_timeline, R.string.intro_description_timeline)
                3 -> IntroPageFragment.newInstance(R.drawable.intro_schedule, R.string.intro_description_schedule)
                4 -> IntroPageFragment.newInstance(R.drawable.intro_favorites, R.string.intro_description_check_mark)
                else -> throw IndexOutOfBoundsException("There is only 5 pages present")
            }
        }

        override fun getCount(): Int {
            return pagesCount
        }
    }
}
