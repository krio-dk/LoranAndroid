package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.ad.AdScreenConfiguration

/**
 * Created by Dmitriy Kolmogorov on 06.06.2019.
 */
data class AdScreenConfigurationResponse(
    @SerializedName("ad_screen_configuration") val adScreenConfiguration: AdScreenConfiguration? = null
)