package com.krio.android.loran.ui.screens.web

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View.VISIBLE
import android.webkit.WebViewClient
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.presentation.screens.web.WebPresenter
import com.krio.android.loran.presentation.screens.web.WebView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_web.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick
import toothpick.config.Module

class WebFragment : BaseFragment(), WebView {

    companion object {
        private const val ARGS_INIT_PARAMS = "init_params"
        private const val ARGS_PARAM_PARENT_SCOPE = "parent scope"

        fun newInstance(parentScope: String, initParams: WebPresenter.InitParams) = WebFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGS_INIT_PARAMS, initParams)
                putString(ARGS_PARAM_PARENT_SCOPE, parentScope)
            }
        }
    }

    override val layoutRes = R.layout.fragment_web

    @InjectPresenter
    lateinit var presenter: WebPresenter

    @ProvidePresenter
    fun providePresenter(): WebPresenter {
        val parentScope = arguments!![WebFragment.ARGS_PARAM_PARENT_SCOPE] as String
        val scope = Toothpick.openScopes(parentScope, DI.webScope(parentScope)).apply {
            installModules(object : Module() {
                init {
                    bind(WebPresenter.InitParams::class.java).toInstance(arguments!![ARGS_INIT_PARAMS] as WebPresenter.InitParams)
                }
            })
        }

        return scope.getInstance(WebPresenter::class.java).apply {
            Toothpick.closeScope(DI.webScope(parentScope))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        webView.webViewClient = MyWebViewClient()
    }

    override fun onPause() {
        super.onPause()
        webView.onPause()
    }

    override fun onResume() {
        super.onResume()
        webView.onResume()
    }

    override fun showWebPage(url: String) {
        webView?.loadUrl(url)
    }

    override fun showTitle(title: String) {
        toolbarTitle?.text = title
    }

    override fun showSubtitle(subtitle: String) {
        toolbarSubtitle?.apply {
            text = subtitle
            visibility = VISIBLE
        }
    }

    override fun showProgress(show: Boolean) {
        progressView?.visible(show)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class MyWebViewClient : WebViewClient() {

        override fun onPageStarted(view: android.webkit.WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            presenter.onStartLoading()
        }

        override fun onPageFinished(view: android.webkit.WebView?, url: String?) {
            super.onPageFinished(view, url)
            presenter.onStopLoading()
        }
    }
}
