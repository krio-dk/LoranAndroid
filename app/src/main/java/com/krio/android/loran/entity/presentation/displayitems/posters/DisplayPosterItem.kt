package com.krio.android.loran.entity.presentation.displayitems.posters

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class DisplayPosterItem(
        val serialTmdbId: Int,
        val serialName: String,
        val posterPath: String,
        val backdropPath: String?
)