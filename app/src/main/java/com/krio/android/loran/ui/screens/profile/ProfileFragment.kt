package com.krio.android.loran.ui.screens.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.presentation.screens.profile.ProfilePresenter
import com.krio.android.loran.presentation.screens.profile.ProfileView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat.getColor
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat


class ProfileFragment : BaseFragment(), ProfileView {

    override val layoutRes = R.layout.fragment_profile

    @InjectPresenter
    lateinit var presenter: ProfilePresenter

    @ProvidePresenter
    fun providePresenter(): ProfilePresenter {
        return Toothpick.openScope(DI.PROFILE_SCOPE).getInstance(ProfilePresenter::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbarTitle.setText(R.string.profile_title)

        support.setOnClickListener { presenter.onSupportClick() }
        intro.setOnClickListener { presenter.onIntroClick() }
        about.setOnClickListener { presenter.onAboutClick() }
        termsOfUse.setOnClickListener { presenter.onTermsOfUseClick() }
        privacyPolicy.setOnClickListener { presenter.onPrivacyPolicyClick() }
        subscription.setOnClickListener { presenter.onSubscriptionClick() }
    }

    override fun setSubscriptionBageColor(@ColorRes color: Int) {
        for (drawable in subscription.compoundDrawablesRelative) {
            if (drawable != null) {
                drawable.mutate().colorFilter = PorterDuffColorFilter(getColor(requireContext(), color), PorterDuff.Mode.SRC_IN)
            }
        }
    }

    override fun showChromeTab(url: String) {
        val customTabsIntent = CustomTabsIntent.Builder()
                .setToolbarColor(getColor(requireContext(), R.color.colorBlackPrimary))
                .setShowTitle(true)
                .build()

        customTabsIntent.launchUrl(requireContext(), Uri.parse(url))
    }

    override fun sendEmail(email: String, subject: String, mailText: String, chooserText: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, mailText)
        }

        startActivity(Intent.createChooser(emailIntent, chooserText))
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
