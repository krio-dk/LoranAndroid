package com.krio.android.loran.entity.domain.dataitems.main

import com.google.gson.annotations.SerializedName

data class FavoriteNextEpisodeItem(
        @SerializedName("season_number") val seasonNumber: Int,
        @SerializedName("episode_number") val episodeNumber: Int,
        @SerializedName("is_released") val isReleased: Boolean,
        @SerializedName("schedule_date") val scheduleDate: String? = null,
        @SerializedName("schedule_type") val scheduleType: ScheduleType? = null
)