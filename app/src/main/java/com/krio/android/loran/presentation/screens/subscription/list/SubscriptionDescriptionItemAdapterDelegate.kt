package com.krio.android.loran.presentation.screens.subscription.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionDescriptionItem

/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
class SubscriptionDescriptionItemAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplaySubscriptionDescriptionItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subscription_description, parent, false)
        return SubscriptionDescriptionViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) = Unit

    class SubscriptionDescriptionViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}