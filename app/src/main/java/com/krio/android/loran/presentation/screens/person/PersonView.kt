package com.krio.android.loran.presentation.screens.person

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPerson

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface PersonView : MvpView {
    fun showTitle(title: String)

    fun showData(tmdbPerson: TmdbPerson)
    fun showPhotos(photos: List<String>)
    fun showBackdrop(url: String)
    fun showEmptyProgress(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}