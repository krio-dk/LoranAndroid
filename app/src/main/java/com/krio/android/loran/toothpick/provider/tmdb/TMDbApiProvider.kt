package com.krio.android.loran.toothpick.provider.tmdb

import com.google.gson.Gson
import com.krio.android.loran.model.data.server.tmdb.TMDbApi
import com.krio.android.loran.model.data.server.tmdb.TMDbConfig
import com.krio.android.loran.toothpick.qualifier.TMDbOkHttpClient
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider


/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class TMDbApiProvider @Inject constructor(
        @TMDbOkHttpClient private val okHttpClient: OkHttpClient,
        private val gson: Gson
) : Provider<TMDbApi> {

    override fun get() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(TMDbConfig.serverUrl)
                    .build()
                    .create(TMDbApi::class.java)
}
