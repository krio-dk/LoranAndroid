package com.krio.android.loran.ui.screens.main

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.krio.android.loran.R
import com.krio.android.loran.Tabs
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.presentation.screens.main.MainPresenter
import com.krio.android.loran.presentation.screens.main.MainView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.AppodealModule
import com.krio.android.loran.toothpick.module.InnerNavigationModule
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.containers.ContainerFragment
import com.krio.android.loran.ui.screens.schedule.ScheduleFragment
import com.krio.android.loran.ui.screens.timeline.TimelineFragment
import kotlinx.android.synthetic.main.fragment_main.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import toothpick.Toothpick
import toothpick.config.Module
import java.io.Serializable
import java.lang.ref.WeakReference
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
class MainFragment : BaseFragment(), MainView {

    companion object {
        private const val ARGS_INIT_PARAMS = "init_params"

        fun newInstance(initParams: InitParams) = MainFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGS_INIT_PARAMS, initParams)
            }
        }
    }

    data class InitParams(
        val appodealParams: AppodealManager.InitParams,
        val presenterParams: MainPresenter.InitParams
    ) : Serializable

    override val layoutRes = R.layout.fragment_main

    private var serialsContainerFragment: ContainerFragment? = null
    private var timelineFragment: TimelineFragment? = null
    private var scheduleFragment: ScheduleFragment? = null
    private var favoriteContainerFragment: ContainerFragment? = null
    private var profileContainerFragment: ContainerFragment? = null

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter(): MainPresenter = Toothpick.openScopes(DI.MAIN_SCOPE).getInstance(MainPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        val initParams = arguments!![ARGS_INIT_PARAMS] as InitParams

        val appodealManager = AppodealManager(
            WeakReference(requireActivity()),
            initParams.appodealParams,
            Toothpick.openScope(DI.APP_SCOPE).getInstance(Prefs::class.java)
        )

        Toothpick.openScopes(DI.APP_SCOPE, DI.MAIN_SCOPE).apply {
            installModules(AppodealModule(appodealManager))
            installModules(InnerNavigationModule())
            installModules(object : Module() {
                init {
                    bind(MainPresenter.InitParams::class.java).toInstance(initParams.presenterParams)
                }
            })

            Toothpick.inject(this@MainFragment, this)
        }

        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.onActivityCreated()
        initTabs()
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    private fun initTabs() {
        serialsContainerFragment = childFragmentManager.findFragmentByTag(Tabs.SerialTab.screenKey) as? ContainerFragment
        if (serialsContainerFragment == null) {
            serialsContainerFragment = ContainerFragment.newInstance(Tabs.SerialTab.screenKey).also {
                childFragmentManager
                    .beginTransaction()
                    .add(R.id.tabContainer, it, Tabs.SerialTab.screenKey)
                    .detach(it)
                    .commitNow()
            }
        }

        timelineFragment = childFragmentManager.findFragmentByTag(Tabs.TimelineTab.screenKey) as? TimelineFragment
        if (timelineFragment == null) {
            timelineFragment = TimelineFragment().also {
                childFragmentManager
                    .beginTransaction()
                    .add(R.id.tabContainer, it, Tabs.TimelineTab.screenKey)
                    .detach(it)
                    .commitNow()
            }
        }

        scheduleFragment = childFragmentManager.findFragmentByTag(Tabs.ScheduleTab.screenKey) as? ScheduleFragment
        if (scheduleFragment == null) {
            scheduleFragment = ScheduleFragment().also {
                childFragmentManager
                    .beginTransaction()
                    .add(R.id.tabContainer, it, Tabs.ScheduleTab.screenKey)
                    .detach(it)
                    .commitNow()
            }
        }

        favoriteContainerFragment = childFragmentManager.findFragmentByTag(Tabs.FavoritelTab.screenKey) as? ContainerFragment
        if (favoriteContainerFragment == null) {
            favoriteContainerFragment = ContainerFragment.newInstance(Tabs.FavoritelTab.screenKey).also {
                childFragmentManager
                    .beginTransaction()
                    .add(R.id.tabContainer, it, Tabs.FavoritelTab.screenKey)
                    .detach(it)
                    .commitNow()
            }
        }

        profileContainerFragment = childFragmentManager.findFragmentByTag(Tabs.ProfileTab.screenKey) as? ContainerFragment
        if (profileContainerFragment == null) {
            profileContainerFragment = ContainerFragment.newInstance(Tabs.ProfileTab.screenKey).also {
                childFragmentManager
                    .beginTransaction()
                    .add(R.id.tabContainer, it, Tabs.ProfileTab.screenKey)
                    .detach(it)
                    .commitNow()
            }
        }
    }

    override fun showMainContent(show: Boolean) {
        mainContent.visible(show)
    }

    override fun selectPostersTab() {
        bottomNavigation.selectedItemId = R.id.navigation_posters
    }

    override fun selectTimelineTab() {
        bottomNavigation.selectedItemId = R.id.navigation_timeline
    }

    override fun selectScheduleTab() {
        bottomNavigation.selectedItemId = R.id.navigation_schedule
    }

    override fun selectFavoritesTab() {
        bottomNavigation.selectedItemId = R.id.navigation_favorite
    }

    override fun selectProfileTab() {
        bottomNavigation.selectedItemId = R.id.navigation_profile
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_posters -> {
                presenter.onPostersSelected()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_timeline -> {
                presenter.onTimelineSelected()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_schedule -> {
                presenter.onScheduleSelected()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorite -> {
                presenter.onFavoriteSelected()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                presenter.onProfileSelected()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onBackPressed() {
        when (childFragmentManager.findFragmentById(R.id.tabContainer)) {
            is ContainerFragment -> {
                val currentFragment = childFragmentManager.findFragmentById(R.id.tabContainer) as ContainerFragment
                currentFragment.onBackPressed()
            }

            is BaseFragment -> {
                val currentFragment = childFragmentManager.findFragmentById(R.id.tabContainer) as BaseFragment
                currentFragment.onBackPressed()
            }

            else -> presenter.onBackPressed()
        }
    }

    private val navigator: SupportAppNavigator by lazy {
        object : SupportAppNavigator(activity, childFragmentManager, R.id.tabContainer) {

            override fun applyCommand(command: Command?) {
                when {
                    isReplaceTabCommand(command) -> replaceTab(command as Replace)
                    else -> super.applyCommand(command)
                }
            }

            private fun isReplaceTabCommand(command: Command?): Boolean {
                if (command is Replace) {
                    when (command.screen) {
                        Tabs.SerialTab,
                        Tabs.TimelineTab,
                        Tabs.ScheduleTab,
                        Tabs.FavoritelTab,
                        Tabs.ProfileTab -> return true
                    }
                }

                return false
            }

            private fun replaceTab(command: Replace) {
                when (command.screen) {
                    Tabs.SerialTab -> {
                        childFragmentManager.beginTransaction()
                            .detach(timelineFragment!!)
                            .detach(scheduleFragment!!)
                            .detach(favoriteContainerFragment!!)
                            .detach(profileContainerFragment!!)
                            .attach(serialsContainerFragment!!)
                            .commitNow()
                    }
                    Tabs.TimelineTab -> {
                        childFragmentManager.beginTransaction()
                            .detach(serialsContainerFragment!!)
                            .detach(scheduleFragment!!)
                            .detach(favoriteContainerFragment!!)
                            .detach(profileContainerFragment!!)
                            .attach(timelineFragment!!)
                            .commitNow()
                    }
                    Tabs.ScheduleTab -> {
                        childFragmentManager.beginTransaction()
                            .detach(serialsContainerFragment!!)
                            .detach(timelineFragment!!)
                            .detach(favoriteContainerFragment!!)
                            .detach(profileContainerFragment!!)
                            .attach(scheduleFragment!!)
                            .commitNow()
                    }
                    Tabs.FavoritelTab -> {
                        childFragmentManager.beginTransaction()
                            .detach(serialsContainerFragment!!)
                            .detach(timelineFragment!!)
                            .detach(scheduleFragment!!)
                            .detach(profileContainerFragment!!)
                            .attach(favoriteContainerFragment!!)
                            .commitNow()
                    }
                    Tabs.ProfileTab -> {
                        childFragmentManager.beginTransaction()
                            .detach(serialsContainerFragment!!)
                            .detach(timelineFragment!!)
                            .detach(scheduleFragment!!)
                            .detach(favoriteContainerFragment!!)
                            .attach(profileContainerFragment!!)
                            .commitNow()
                    }
                }
            }

            override fun activityBack() {
                Toothpick.openScope(DI.APP_SCOPE).getInstance(Router::class.java, "com.krio.android.loran.toothpick.module.qualifier.AppNavigation").exit()
            }
        }
    }
}