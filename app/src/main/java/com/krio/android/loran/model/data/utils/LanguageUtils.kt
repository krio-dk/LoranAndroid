package com.krio.android.loran.model.data.utils

import java.util.*

/**
 * Created by Dmitriy Kolmogorov on 06/04/2019.
 */
object LanguageUtils {

    val language: String
        get() = Locale.getDefault().language

    fun isRussianSpeaking() = when (language) {
        Locale("ru").language,
        Locale("uk").language,
        Locale("be").language,
        Locale("kk").language -> true
        else -> false
    }
}