package com.krio.android.loran.presentation.screens.favorite.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteCaptionActive
import com.krio.android.loran.extensions.visible
import kotlinx.android.synthetic.main.item_favorite_caption_active.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class CaptionActiveItemAdapterDelegate(private val removeRestrictionsClickListener: () -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
        items[position] is DisplayFavoriteCaptionActive

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favorite_caption_active, parent, false)
        return DateViewHolder(view, removeRestrictionsClickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as DateViewHolder).bind(items[position] as DisplayFavoriteCaptionActive)
    }

    class DateViewHolder(val view: View, removeRestrictionsClickListener: () -> Unit) : RecyclerView.ViewHolder(view) {

        init {
            view.removeRestrictionsButton.setOnClickListener { removeRestrictionsClickListener.invoke() }
        }

        fun bind(item: DisplayFavoriteCaptionActive) {
            item.maxActiveSerialCount?.let {
                view.monitoredCount.text = view.context.getString(
                    R.string.favorite_text_monitored_count,
                    item.currentActiveSerialsCount,
                    item.maxActiveSerialCount
                )
            } ?: view.monitoredCount.visible(false)

            view.emptyText.visible(item.currentActiveSerialsCount == 0)
        }
    }
}