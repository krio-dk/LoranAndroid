package com.krio.android.loran.entity.thirdparty.tmdb.network

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 16/03/2019.
 */
data class TmdbNetwork (
        @SerializedName("name") val name: String,
        @SerializedName("id") val id: Int,
        @SerializedName("logo_path") val logoPath: String,
        @SerializedName("origin_country") val originCountry: String
)