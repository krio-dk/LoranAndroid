package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.ad.AdAppConfiguration

/**
 * Created by Dmitriy Kolmogorov on 06.06.2019.
 */
data class AdAppConfigurationResponse(
    @SerializedName("ad_app_configuration") val adAppConfiguration: AdAppConfiguration? = null
)