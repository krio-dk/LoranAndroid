package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 10.06.2019.
 */
data class AdAppConfigurationBody(
    @SerializedName("user_id") val userId: Int?
)