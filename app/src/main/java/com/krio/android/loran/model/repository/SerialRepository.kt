package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.thirdparty.tmdb.season.TmdbSeason
import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerial
import com.krio.android.loran.entity.transfer.body.GetSerialBody
import com.krio.android.loran.entity.transfer.response.SerialResponse
import com.krio.android.loran.model.data.server.loran.LoranApi
import com.krio.android.loran.model.data.server.tmdb.TMDbApi
import com.krio.android.loran.model.data.utils.LanguageUtils
import com.krio.android.loran.model.system.resource.ResourceManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 15.04.2018.
 */
class SerialRepository @Inject constructor(
        private val loranApi: LoranApi,
        private val tmdbApi: TMDbApi,
        private val resourceManager: ResourceManager
) {

    private val defaultLanguage: String
        get() = LanguageUtils.language

    data class SerialKey(
            private val tmdbId: Int,
            private val language: String
    )

    data class SeasonKey(
            private val tmdbId: Int,
            private val seasonNumber: Int,
            private val language: String
    )

    private val cachedTmdbSerials = mutableMapOf<SerialKey, TmdbSerial>()
    private val cachedTmdbSeasons = mutableMapOf<SeasonKey, TmdbSeason>()

    init {
        Timber.d("Init")
    }

    fun getLoranSerial(
            serialTmdbId: Int,
            userId: Int?,
            language: String = this.defaultLanguage
    ): Single<SerialResponse> = loranApi
            .getSerial(GetSerialBody(serialTmdbId, userId, language))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getTmdbSerial(
            serialTmdbId: Int,
            language: String = this.defaultLanguage
    ): Single<TmdbSerial> {
        return cachedTmdbSerials[SerialKey(serialTmdbId, language)]?.let {
            Single.just(it)
        } ?: tmdbApi
                .getSerialDetails(serialTmdbId, language)
                .doAfterSuccess { cachedTmdbSerials.put(SerialKey(serialTmdbId, language), it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    fun getTmdbSeasonDetails(
            serialTmdbId: Int,
            seasonNumber: Int,
            language: String = this.defaultLanguage
    ): Single<TmdbSeason> {
        return cachedTmdbSeasons[SeasonKey(serialTmdbId, seasonNumber, language)]?.let {
            Single.just(it)
        } ?: tmdbApi
                .getSeasonDetails(serialTmdbId, seasonNumber, language)
                .doAfterSuccess { cachedTmdbSeasons.put(SeasonKey(serialTmdbId, seasonNumber, language), it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}