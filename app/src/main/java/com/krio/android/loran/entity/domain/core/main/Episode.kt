package com.krio.android.loran.entity.domain.core.main

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.materialdata.EpisodeMaterialData

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class Episode (
        @SerializedName("episode_number") val episodeNumber: Int,
        @SerializedName("is_seen") val isSeen: Boolean,
        @SerializedName("translations") val translations: MutableList<Translation> = mutableListOf(),
        @SerializedName("material_data") val materialData: EpisodeMaterialData = EpisodeMaterialData()
)