package com.krio.android.loran.presentation.screens.serial.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayPersonItem
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.ProfileSize
import com.krio.android.loran.model.data.glide.GlideApp
import kotlinx.android.synthetic.main.item_actor.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class PersonItemAdapterDelegate(private val clickListener: (DisplayPersonItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplayPersonItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_actor, parent, false)
        return PersonViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as PersonViewHolder).bind(items[position] as DisplayPersonItem)
    }

    class PersonViewHolder(val view: View, clickListener: (DisplayPersonItem) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var displayPersonItem: DisplayPersonItem

        init {
            view.setOnClickListener { clickListener.invoke(displayPersonItem) }
        }

        fun bind(personItem: DisplayPersonItem) {

            this.displayPersonItem = personItem

            GlideApp.with(view.context)
                    .load(ImagesConfiguration.getProfileFullPath(ProfileSize.PROFILE_SIZE_632, personItem.profilePath))
                    .placeholder(R.drawable.placeholder_cover)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(view.imageView)

            view.name.text = personItem.name
            view.character.text = personItem.character
        }
    }
}