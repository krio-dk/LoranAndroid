package com.krio.android.loran.toothpick.qualifier

import javax.inject.Qualifier

/**
 * Created by Dmitriy Kolmogorov on 09.02.2018.
 */
@Qualifier annotation class TMDbOkHttpClient

@Qualifier annotation class LoranOkHttpClient