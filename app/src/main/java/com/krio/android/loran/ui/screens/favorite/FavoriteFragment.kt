package com.krio.android.loran.ui.screens.favorite

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.appbar.AppBarLayout
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteCaptionActive
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteCaptionNotActive
import com.krio.android.loran.entity.presentation.displayitems.favorite.DisplayFavoriteItem
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.presentation.global.list.NativeAdAdapterDelegate
import com.krio.android.loran.presentation.screens.favorite.FavoritePresenter
import com.krio.android.loran.presentation.screens.favorite.FavoriteView
import com.krio.android.loran.presentation.screens.favorite.list.CaptionActiveItemAdapterDelegate
import com.krio.android.loran.presentation.screens.favorite.list.CaptionNotActiveItemAdapterDelegate
import com.krio.android.loran.presentation.screens.favorite.list.FavoriteItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.*
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_favorite.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class FavoriteFragment : BaseFragment(), FavoriteView {

    companion object {
        const val VIEW_TYPE_CAPTION_ACTIVE = 1
        const val VIEW_TYPE_CAPTION_NOT_ACTIVE = 2
        const val VIEW_TYPE_SERIAL = 3
        const val VIEW_TYPE_NATIVE_AD = 4

        private const val REWARDED_VIDEO_DIALOG_TAG = "rewarded_video_dialog"

        const val SUBSCRIPTION_DIALOG_TAG = "subscription_dialog"
        const val NOT_TRACKED_DETAILS_DIALOG_TAG = "not_tracked_details_dialog"
    }


    override val layoutRes = R.layout.fragment_favorite

    private val adapter = FavoriteAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    private var clearAdapterDisposable: Disposable? = null

    private val prefs = Toothpick.openScope(DI.APP_SCOPE).getInstance(Prefs::class.java)

    @InjectPresenter
    lateinit var presenter: FavoritePresenter

    @ProvidePresenter
    fun providePresenter(): FavoritePresenter {
        return Toothpick.openScopes(DI.FAVORITE_SCOPE).getInstance(FavoritePresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        clearAdapterDisposable = presenter.clearAdapterRelay.subscribe {
            adapter.setData(emptyList())
        }
    }

    override fun onDestroy() {
        clearAdapterDisposable?.dispose()
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbarTitle.setText(R.string.favorite_title)

        recyclerView.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(context)
            adapter = this@FavoriteFragment.adapter

            // Disable because of strange behaviour for now
            // addOnScrollListener(topButtonVisibilityController)

            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    parent.adapter?.let { adapter ->

                        val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)
                        val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)

                        val count = adapter.itemCount

                        val layoutParams = view.layoutParams as RecyclerView.LayoutParams

                        val firstPosition = 0
                        val secondPosition = 1
                        val lastPosition = count - 1

                        val viewAdapterPosition = layoutParams.viewAdapterPosition

                        if (viewAdapterPosition >=0 && adapter.getItemViewType(viewAdapterPosition) == VIEW_TYPE_NATIVE_AD) {
                            outRect.set(0, 0, 0, 0)
                        } else {
                            when (viewAdapterPosition) {
                                firstPosition -> {
                                    when (adapter.getItemViewType(firstPosition)) {
                                        VIEW_TYPE_CAPTION_ACTIVE -> outRect.set(0, 0, 0, halfPadding)
                                        VIEW_TYPE_SERIAL -> outRect.set(0, fullPadding, 0, halfPadding)
                                    }
                                }
                                lastPosition -> outRect.set(0, 0, 0, fullPadding)
                                else -> outRect.set(0, 0, 0, halfPadding)
                            }
                        }
                    }
                }
            })
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.refreshFavorite()
        }

        swipeToRefresh.setOnRefreshListener {
            presenter.refreshFavorite()
        }

        topButton.setOnClickListener { recyclerView.scrollToPosition(0) }
    }

    private val topButtonVisibilityController
        get() = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0)
                    topButton.visible(false)
                else if (dy < 0)
                    topButton.visible(true)

                val firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                if (firstVisibleItemPosition <= 1) {
                    topButton.visible(false)
                }
            }
        }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showEmptyView(show: Boolean) {
        if (show) zeroViewHolder?.showEmptyData(getString(R.string.favorite_text_empty_message), R.drawable.ic_favorite_svg)
        else zeroViewHolder?.hide()
        swipeToRefresh.visible(!show)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
        swipeToRefresh.visible(!show)
    }

    override fun showData(show: Boolean, data: List<Any>) {
        adapter.setData(data.filter { it !is DisplayFavoriteItem || it.translationItems.isNotEmpty() })
        swipeToRefresh?.visible(show)
    }

    override fun updateItem(item: DisplayFavoriteItem) {
        adapter.updateItem(item)
    }

    override fun cancelUpdating() {
        adapter.cancelUpdating()
    }

    override fun updateCaptionActiveItem(item: DisplayFavoriteCaptionActive) {
        adapter.updateCaptionActiveItem(item)
    }

    override fun removeItem(item: DisplayFavoriteItem) {
        adapter.removeItem(item)
    }

    override fun removeCaptionNotActiveItem() {
        adapter.removeCaptionNotActiveItem()
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh?.isRefreshing = show
    }

    override fun showNotActiveWarning(show: Boolean) {
        notActiveWarning.visible(show)
    }

    override fun enableToolbarScrolling(enable: Boolean) {
        toolbarFrameLayout?.let {
            val params = toolbarFrameLayout.layoutParams as AppBarLayout.LayoutParams
            params.scrollFlags = if (enable) {
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            } else 0

            toolbar?.visible(false)
            toolbar?.visible(true)
        }
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun showRemoveFromFavoritesDialog(onRemoveConfirmed: () -> Unit) {
        activity?.let {
            AlertDialog.Builder(it, R.style.DialogTheme)
                .setTitle(R.string.favorite_remove_alert_title)
                .setMessage(R.string.favorite_remove_alert_text)
                .setCancelable(true)
                .setPositiveButton(R.string.favorite_remove_alert_button_remove) { _, _ -> onRemoveConfirmed() }
                .setNegativeButton(R.string.favorite_remove_alert_button_cancel) { _, _ -> /* do nothing */ }
                .create()
                .show()
        }
    }

    override fun showActiveSerialsLimitReachedDialog() {
        showSubscriptionDialog(
            getString(R.string.subscription_dialog_caption_follow_more_shows),
            getString(R.string.subscription_dialog_description_follow_more_shows)
        )
    }

    private fun showSubscriptionDialog(caption: String, extraDescription: String? = null) {
        if (isAdded) {
            val subscriptionDialogFragment = childFragmentManager.findFragmentByTag(SUBSCRIPTION_DIALOG_TAG)
            if (subscriptionDialogFragment == null) {
                val dialog = SubscriptionDialog.newInstance(caption, extraDescription)
                dialog.show(childFragmentManager, SUBSCRIPTION_DIALOG_TAG)
                dialog.subscriptionClickListener = presenter::onSubscriptionDetailsClicked
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    private fun showNotTrackedDetailsDialog() {
        if (isAdded) {
            val notTrackedDetailsDialogFragment = childFragmentManager.findFragmentByTag(NOT_TRACKED_DETAILS_DIALOG_TAG)
            if (notTrackedDetailsDialogFragment == null) {
                val dialog = NotTrackedDetailsDialog()
                dialog.show(childFragmentManager, NOT_TRACKED_DETAILS_DIALOG_TAG)
                dialog.subscriptionClickListener = presenter::onSubscriptionDetailsClicked
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    override fun showRewardedVideoDialog(serialTmdbId: Int, serialName: String, subscriptionCost: String) {
        if (isAdded) {
            val dialogFragment = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG)
            if (dialogFragment == null) {
                val dialog = RewardedVideoDialog.newInstance(serialName, subscriptionCost)
                dialog.dismissListener = { presenter.onDismissRewardedVideoDialog() }
                dialog.subscriptionDetailsClickListener = { presenter.onSubscriptionDetailsClicked() }
                dialog.watchAdClickListener = {
                    dialog.showError(false)
                    dialog.showProgress(true)
                    dialog.enableWatchAdButton(false)
                    presenter.onWatchAdClicked(serialTmdbId, serialName)
                }
                dialog.isCancelable = false
                dialog.show(childFragmentManager, REWARDED_VIDEO_DIALOG_TAG)
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    override fun hideRewardedVideoDialog() {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.dismiss()
        }
    }

    override fun showRewardedVideoError(show: Boolean, message: String) {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.showError(show, message)
            dialog?.showProgress(false)
            dialog?.enableWatchAdButton(true)
        }
    }

    override fun setRewardedVideoProgress(value: Long) {
        if (isAdded) {
            val dialog = childFragmentManager.findFragmentByTag(REWARDED_VIDEO_DIALOG_TAG) as? RewardedVideoDialog
            dialog?.setProgressValue(value)
        }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    inner class FavoriteAdapter : ListDelegationAdapter<MutableList<Any>>() {

        private val appodealManager = Toothpick.openScope(DI.MAIN_SCOPE).getInstance(AppodealManager::class.java)

        init {

            items = mutableListOf()

            delegatesManager.addDelegate(VIEW_TYPE_CAPTION_ACTIVE, CaptionActiveItemAdapterDelegate {
                Analytics.logEvent("disable_restrictions_clicked") {
                    prefs.userId?.let { putInt("user_id", it) }
                }
                showSubscriptionDialog(getString(R.string.subscription_dialog_caption_disable_restrictions))
            })
            delegatesManager.addDelegate(VIEW_TYPE_CAPTION_NOT_ACTIVE, CaptionNotActiveItemAdapterDelegate {
                Analytics.logEvent("not_active_serials_details_clicked") {
                    prefs.userId?.let { putInt("user_id", it) }
                }
                showNotTrackedDetailsDialog()
            })
            delegatesManager.addDelegate(VIEW_TYPE_SERIAL, FavoriteItemAdapterDelegate(
                isSubscriptionPresent = {  presenter.isSubscriptionPresent },
                itemClickListener = { presenter.onFavoriteItemClicked(it) },
                removeFromFavoriteClickListener = { favoriteItem, favoriteTranslation ->
                    presenter.onRemoveFromFavoriteClicked(favoriteItem, favoriteTranslation)
                },
                translationClickListener = { favoriteItem, favoriteTranslation ->
                    presenter.onTranslationClicked(favoriteItem, favoriteTranslation)
                },
                checkMarkClickListener = { favoriteItem, favoriteTranslation ->
                    favoriteItem.isUpdating = true
                    favoriteTranslation.isChecked = true
                    notifyItemChanged(items.indexOf(favoriteItem))
                    presenter.onCheckMarkClicked(favoriteItem, favoriteTranslation)
                },
                activateClickListener = { presenter.onActivateClicked(it) },
                deactivateClickListener = { presenter.onDeactivateClicked(it) }
            ))

            delegatesManager.addDelegate(
                VIEW_TYPE_NATIVE_AD,
                NativeAdAdapterDelegate (appodealManager) {
                    Analytics.logEvent("close_ad_clicked") {
                        putString("from", "favorite screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }
                    showSubscriptionDialog(getString(R.string.subscription_dialog_caption_disable_ad))
                }
            )
        }

        fun setData(shows: List<Any>) {
            val oldItems = ArrayList(items)

            items.clear()
            items.addAll(shows)

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }

        fun updateItem(updatedItem: DisplayFavoriteItem) {
            view?.postDelayed({
                items
                    .filter { it is DisplayFavoriteItem }
                    .map { it as DisplayFavoriteItem }
                    .find { it.serialTmdbId == updatedItem.serialTmdbId }?.let { item ->
                        item.lastSeenSeasonNumber = updatedItem.lastSeenSeasonNumber
                        item.lastSeenEpisodeNumber = updatedItem.lastSeenEpisodeNumber
                        item.translationItems = updatedItem.translationItems
                        item.isUpdating = false
                        notifyItemChanged(items.indexOf(item))
                    }
            }, 500)
        }

        fun removeItem(updatedItem: DisplayFavoriteItem) {
            if (items.isNotEmpty()) {
                items
                    .filter { it is DisplayFavoriteItem }
                    .map { it as DisplayFavoriteItem }
                    .find { it.serialTmdbId == updatedItem.serialTmdbId }?.let { item ->
                        val index = items.indexOf(item)
                        items.removeAt(index)
                        notifyItemRemoved(index)
                    }

                presenter.onItemRemoved(items)
            }
        }

        fun updateCaptionActiveItem(item: DisplayFavoriteCaptionActive) {
            if (items.isNotEmpty() && items[0] is DisplayFavoriteCaptionActive) {
                items[0] = item
                notifyItemChanged(0)
            }
        }

        fun removeCaptionNotActiveItem() {
            items.find { it is DisplayFavoriteCaptionNotActive }?.let {
                val index = items.indexOf(it)
                items.removeAt(index)
                notifyItemRemoved(index)
            }
        }

        fun cancelUpdating() {
            view?.postDelayed({
                items
                    .filter { it is DisplayFavoriteItem }
                    .map { it as DisplayFavoriteItem }
                    .filter { it.isUpdating }.forEach { item ->
                        item.isUpdating = false
                        item.translationItems.forEach { it.isChecked = false }
                        notifyItemChanged(items.indexOf(item))
                    }
            }, 250)
        }

        override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
            super.onViewRecycled(holder)
            if (holder is NativeAdAdapterDelegate.NativeAdViewHolder) {
                holder.unregisterViewForInteraction()
            }
        }
    }
}