package com.krio.android.loran.entity.presentation.displayitems.favorite

import com.krio.android.loran.entity.domain.core.main.SerialStatus

/**
 * Created by Dmitriy Kolmogorov on 09.04.2018.
 */
data class DisplayFavoriteItem(
        val serialTmdbId: Int,
        val serialName: String,
        var lastSeenSeasonNumber: Int?,
        var lastSeenEpisodeNumber: Int?,
        var translationItems: MutableList<DisplayFavoriteTranslationItem>,
        val backdropPath: String?,
        val firstAirDate: String?,
        val originCountry: List<String>?,
        val originalLanguage: String?,
        val posterPath: String?,
        val status: SerialStatus?,
        val voteAverage: Double?,
        var isActive: Boolean,
        var isUpdating: Boolean = false
)