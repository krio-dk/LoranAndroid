package com.krio.android.loran.model.initialization

import com.krio.android.loran.entity.domain.core.main.InitMessage

/**
 * Created by Dmitriy Kolmogorov on 15.09.2018.
 */
data class InitResult(val message: InitMessage? = null)
