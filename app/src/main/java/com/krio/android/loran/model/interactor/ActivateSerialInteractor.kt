package com.krio.android.loran.model.interactor

import com.krio.android.loran.entity.transfer.response.ActivateSerialResponse
import com.krio.android.loran.entity.transfer.response.CheckActivationPermissionResponse
import com.krio.android.loran.entity.transfer.response.ResultResponse
import com.krio.android.loran.entity.transfer.response.UserIdResponse
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.ActivateSerialRepository
import com.krio.android.loran.model.repository.CheckMarkRepository
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 10.03.2018.
 */
class ActivateSerialInteractor @Inject constructor(
        private val repository: ActivateSerialRepository,
        private val prefs: Prefs
) {

    init {
        Timber.d("Init")
    }

    fun activateSerial(serialTmdbId: Int, adWatched: Boolean): Single<ActivateSerialResponse> {
        return prefs.userId?.let { userId ->
            repository.activateSerial(serialTmdbId, adWatched, userId)
        } ?: Single.just(ActivateSerialResponse(false))
    }

    fun deactivateSerial(serialTmdbId: Int): Single<ResultResponse> {
        return prefs.userId?.let { userId ->
            repository.deactivateSerial(serialTmdbId, userId)
        } ?: Single.just(ResultResponse(false))
    }

}