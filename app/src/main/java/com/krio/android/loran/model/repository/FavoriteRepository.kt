package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.transfer.body.AddFavoriteBody
import com.krio.android.loran.entity.transfer.response.*
import com.krio.android.loran.model.data.server.loran.LoranApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class FavoriteRepository @Inject constructor(private val loranApi: LoranApi) {

    init {
        Timber.d("Init")
    }

    fun getFavorites(userId: Int): Single<FavoritesResponse> = loranApi
            .getFavorites(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getFavorite(serialTmdbId: Int, userId: Int): Single<FavoriteItemResponse> = loranApi
            .getFavorite(serialTmdbId, userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun addSerialInTranslationToFavorite(
            serialTmdbId: Int,
            translatorId: Int,
            adWatched: Boolean,
            userId: Int?
    ): Single<UserIdResponse> = loranApi
            .addFavorite(AddFavoriteBody(serialTmdbId, translatorId, adWatched, userId))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun removeSerialInTranslationFromFavorite(
            serialTmdbId: Int,
            translatorId: Int,
            userId: Int
    ): Single<ResultResponse> = loranApi
            .removeFavorite(serialTmdbId, translatorId, userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun hasFavorites(userId: Int): Single<HasFavoritesResponse> = loranApi
            .hasFavorites(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getFavoritesIdentifiers(userId: Int): Single<List<Int>> = loranApi
        .getFavoritesIdentifiers(userId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}