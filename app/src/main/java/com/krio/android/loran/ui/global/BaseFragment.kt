package com.krio.android.loran.ui.global

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.arellomobile.mvp.MvpAppCompatFragment
import com.krio.android.loran.R
import com.krio.android.loran.ui.screens.launch.LaunchActivity
import kotlinx.android.synthetic.main.toolbar.*


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
abstract class BaseFragment : MvpAppCompatFragment() {

    companion object {
        private val PROGRESS_DIALOG_TAG = "progress_dialog"
    }

    abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
            = inflater.inflate(layoutRes, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()
        initSearchView()
    }

    override fun onResume() {
        super.onResume()
        (activity as LaunchActivity).configureSnackMargin()
    }

    private fun initToolbar() {
        toolbar?.let {
            toolbarTitle.typeface = Typeface.createFromAsset(activity?.assets, "fonts/din1451alt.ttf")
            (activity as? AppCompatActivity)?.let { activity ->
                activity.setSupportActionBar(toolbar)
                activity.supportActionBar?.setDisplayShowTitleEnabled(false)
            }
        }
    }

    private fun initSearchView() {
        searchView?.setCursorDrawable(R.drawable.search_cursor)
    }

    protected fun showProgressDialog() {
        if (isAdded) {
            val progressDialogFragment = childFragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG)
            if (progressDialogFragment == null) {
                ProgressDialog().show(childFragmentManager, PROGRESS_DIALOG_TAG)
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    protected fun hideProgressDialog() {
        if (isAdded) {
            val progressDialogFragment = childFragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG) as? ProgressDialog
            progressDialogFragment?.let {
                it.dismissAllowingStateLoss()
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    protected fun showSnackMessage(msg: String) {
        (activity as LaunchActivity).showSnackMessage(msg)
    }

    protected fun showKeyboard(view: View) {
        val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        inputMethodManager?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    protected fun hideKeyboard() {
        view?.let {
            val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            inputMethodManager?.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    open fun onBackPressed() {
        // default do nothing
    }
}