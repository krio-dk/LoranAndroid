package com.krio.android.loran.presentation.screens.about

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 04.09.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface AboutView : MvpView {
    fun showVersion(versionNumber: String)
}