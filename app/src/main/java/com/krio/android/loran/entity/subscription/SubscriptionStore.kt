package com.krio.android.loran.entity.subscription

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 09/03/2019.
 */
enum class SubscriptionStore {
    @SerializedName("Apple")
    APPLE,

    @SerializedName("Google")
    GOOGLE
}