package com.krio.android.loran.presentation.screens.gdpr

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.Screens
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.ad.ConsentManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import ru.terrakok.cicerone.Router
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
@InjectViewState
class GdprPresenter @Inject constructor(
    @AppNavigation private val router: Router,
    private val consentManager: ConsentManager,
    private val prefs: Prefs
) : BasePresenter<GdprView>() {

    fun onYesClicked() {
        prefs.resultGdpr = true
        consentManager.update()
        router.replaceScreen(Screens.GdprResultScreen)
    }

    fun onNoClicked() {
        prefs.resultGdpr = false
        consentManager.update()
        router.replaceScreen(Screens.GdprResultScreen)
    }

}
