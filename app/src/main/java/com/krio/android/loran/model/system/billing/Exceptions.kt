package com.krio.android.loran.model.system.billing

/**
 * Created by Dmitriy Kolmogorov on 15.09.2018.
 */
class BillingClientNotReadyException : Error()
class BadBillingResponseException(val queryType: QueryType) : Error()

enum class QueryType {
    QUERY_ACTIVE_SUBSCRIPTIONS,
    QUERY_PURCHASE_HISTORY,
    QUERY_PURCHASES_DETAILS
}
