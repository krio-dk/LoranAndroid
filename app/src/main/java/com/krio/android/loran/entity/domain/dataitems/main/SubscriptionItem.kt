package com.krio.android.loran.entity.domain.dataitems.main

/**
 * Created by Dmitriy Kolmogorov on 19.08.2018.
 */
data class SubscriptionItem (
        val id: String,
        val title: String,
        val totalPrice: String,
        val periodDescription: String,
        val freeTrial: String,
        val commonDescription: String,
        val monthPrice: String,
        var grade: Int = 0,
        var isActive: Boolean = false
)