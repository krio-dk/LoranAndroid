package com.krio.android.loran.model.repository

import com.krio.android.loran.entity.thirdparty.tmdb.serial.TmdbSerialItem
import com.krio.android.loran.model.data.server.tmdb.TMDbApi
import com.krio.android.loran.model.data.utils.LanguageUtils
import com.krio.android.loran.model.system.resource.ResourceManager
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by krio on 03.06.2018.
 */
class CatalogRepository @Inject constructor(
        private val tmdbApi: TMDbApi,
        private val resourceManager: ResourceManager
) {

    private val defaultLanguage: String
        get() = LanguageUtils.language

    init {
        Timber.d("Init")
    }

    fun getPopularTmdbSerials(
            page: Int,
            language: String = this.defaultLanguage
    ): Single<List<TmdbSerialItem>> = tmdbApi
            .getPopularSerials(page, language)
            .map({ it.results })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun getTopRatedTmdbSerials(
            page: Int,
            language: String = this.defaultLanguage
    ): Single<List<TmdbSerialItem>> = tmdbApi
            .getTopRatedSerials(page, language)
            .map({ it.results })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun searchTmdbSerials(
            query: String,
            page: Int = 1,
            language: String = this.defaultLanguage
    ): Single<List<TmdbSerialItem>> = tmdbApi
            .searchSerials(query, page, language)
            .map({ it.results })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}