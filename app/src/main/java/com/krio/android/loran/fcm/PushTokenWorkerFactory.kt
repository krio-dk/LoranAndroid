package com.krio.android.loran.fcm

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 2019-08-26.
 */
class PushTokenWorkerFactory @Inject constructor(private val pushTokenSender: PushTokenSender):  WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {

        val workerKlass = Class.forName(workerClassName).asSubclass(PushTokenWorker::class.java)
        val constructor = workerKlass.getDeclaredConstructor(Context::class.java, WorkerParameters::class.java)
        val instance = constructor.newInstance(appContext, workerParameters)

        when (instance) {
            is PushTokenWorker -> {
                instance.pushTokenSender = pushTokenSender
            }
            // optionally, handle other workers
        }

        return instance
    }

}