package com.krio.android.loran.toothpick.provider.loran

import com.google.gson.Gson
import com.krio.android.loran.model.data.server.loran.LoranApi
import com.krio.android.loran.model.data.server.loran.LoranConfig
import com.krio.android.loran.toothpick.qualifier.LoranOkHttpClient
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class LoranApiProvider @Inject constructor(
        @LoranOkHttpClient private val okHttpClient: OkHttpClient,
        private val gson: Gson
) : Provider<LoranApi> {

    override fun get() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(LoranConfig.serverUrl)
                    .build()
                    .create(LoranApi::class.java)
}