package com.krio.android.loran.entity.presentation.displayitems.subscription

/**
 * Created by Dmitriy Kolmogorov on 17.08.2018.
 */
data class DisplaySubscriptionItem (
        val id: String,
        val title: String,
        val totalPrice: String,
        val periodDescription: String,
        val freeTrial: String,
        val commonDescription: String,
        val monthPrice: String,
        val grade: Int
)