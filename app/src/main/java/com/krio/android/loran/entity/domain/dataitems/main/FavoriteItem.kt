package com.krio.android.loran.entity.domain.dataitems.main

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.materialdata.FavoriteItemMaterialData

/**
 * Created by krio on 05.06.2018.
 */
data class FavoriteItem(
        @SerializedName("serial_tmdb_id") val serialTmdbId: Int,
        @SerializedName("serial_name_en") val serialNameEn: String,
        @SerializedName("serial_name_ru") val serialNameRu: String?,
        @SerializedName("last_seen_season_number") val lastSeenSeasonNumber: Int?,
        @SerializedName("last_seen_episode_number") val lastSeenEpisodeNumber: Int?,
        @SerializedName("translations") val translations: List<FavoriteTranslationItem>,
        @SerializedName("is_active") val isActive: Boolean,
        @SerializedName("material_data") val materialData: FavoriteItemMaterialData = FavoriteItemMaterialData()
)