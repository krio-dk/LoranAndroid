package com.krio.android.loran.presentation.screens.timeline

import android.util.SparseArray
import com.appodeal.ads.NativeAd
import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.Screens
import com.krio.android.loran.entity.domain.core.ad.Ad
import com.krio.android.loran.entity.domain.core.ad.AdScreenConfiguration
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayDateItem
import com.krio.android.loran.entity.presentation.displayitems.timeline.DisplayTimelineItem
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.AdInteractor
import com.krio.android.loran.model.interactor.CheckMarkInteractor
import com.krio.android.loran.model.interactor.FavoriteInteractor
import com.krio.android.loran.model.interactor.TimelineInteractor
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.global.Paginator
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@InjectViewState
class TimelinePresenter @Inject constructor(
        private val innerRouter: Router,
        @AppNavigation private val appRouter: Router,
        private val timelineInteractor: TimelineInteractor,
        private val checkMarkInteractor: CheckMarkInteractor,
        private val favoriteInteractor: FavoriteInteractor,
        private val adInteractor: AdInteractor,
        private val prefs: Prefs,
        private val resourceManager: ResourceManager,
        private val refreshRelay: PublishRelay<Boolean>,
        private val errorHandler: ErrorHandler
) : BasePresenter<TimelineView>() {

    var clearAdapterRelay = PublishRelay.create<Boolean>()

    private var refreshDisposable: Disposable? = null

    private var cachedDisplayTimelineItems = listOf<DisplayTimelineItem>()

    private var adConfiguration: AdScreenConfiguration? = null

    var dataWasReceived = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        observeRefresh()
        refreshTimeline()
    }

    private val paginator: Paginator<DisplayTimelineItem> = Paginator(
        { page -> timelineInteractor.getTimelineItems(page) },
        object : Paginator.ViewController<DisplayTimelineItem> {

            override fun showEmptyProgress(show: Boolean) {
                viewState.showEmptyProgress(show)
            }

            override fun showEmptyError(show: Boolean, error: Throwable?) {
                viewState.showRefreshProgress(false)
                if (error != null) {
                    errorHandler.proceed(error) { viewState.showEmptyError(show, it) }
                } else {
                    viewState.showEmptyError(show, null)
                }
            }

            override fun showErrorMessage(error: Throwable) {
                errorHandler.proceed(error) { viewState.showPageError(true, it) }
            }

            override fun showEmptyView(show: Boolean) {
                if (show) {
                    showEmptyViewAccordingToFavoritesExistence()
                } else {
                    viewState.showRefreshProgress(false)
                    viewState.showEmptyView(false)
                }
            }

            override fun showData(show: Boolean, data: List<DisplayTimelineItem>) {
                cachedDisplayTimelineItems = data

                viewState.showRefreshProgress(false)
                viewState.showData(show, getDataFilledWithDate(getDataFilledWithAd()))
                viewState.enableToolbarScrolling(show)

                dataWasReceived = true
            }

            override fun showRefreshProgress(show: Boolean) {
                viewState.showRefreshProgress(show)
            }

            override fun showPageProgress(show: Boolean) {
                viewState.showPageProgress(show)
            }
        }
    )

    private fun getDataFilledWithAd(): List<Any> {
        val dataFilledWithAd = mutableListOf<Any>()

        cachedDisplayTimelineItems.forEachIndexed { index, item ->
            adConfiguration?.let {
                if (index != 0 && (index == it.startIndex || ((index - it.startIndex) > 0 && (index - it.startIndex) % it.interval == 0))) {
                    dataFilledWithAd.add(Ad)
                }
            }

            dataFilledWithAd.add(item)
        }

        return dataFilledWithAd
    }

    private fun getDataFilledWithDate(inputData: List<Any>): List<Any> {
        val dataFilledWithDate = mutableListOf<Any>()

        var prevDate: String? = null

        inputData.forEach { item ->

            if (item is DisplayTimelineItem) {
                val date = item.translationDate.substring(0, 10)
                if (date != prevDate) {
                    dataFilledWithDate.add(DisplayDateItem(date))
                    prevDate = date
                }
            }

            dataFilledWithDate.add(item)
        }

        return dataFilledWithDate
    }

    override fun onDestroy() {
        paginator.release()
        super.onDestroy()
    }

    fun refreshTimeline() {
        getAdConfiguration {
            dataWasReceived = false
            viewState.resetEndlessScroll()
            paginator.refresh()
        }
    }

    private fun getAdConfiguration(callback: () -> Unit) {
        adInteractor.getAdScreenConfiguration("timeline_screen")
            .doOnSubscribe { viewState.showEmptyError(false, null) }
            .doAfterTerminate { callback.invoke() }
            .subscribe(
                { adConfigurationResponse ->
                    adConfiguration = adConfigurationResponse.adScreenConfiguration
                },
                {
                   // do nothing
                }
            )
            .connect()
    }

    fun loadNextPage() {
        viewState.showPageError(false)
        paginator.loadNewPage()
    }

    fun onSubscriptionDetailsClicked() {
        appRouter.navigateTo(Screens.SubscriptionScreen)
    }

    fun onCheckMarkClicked(displayTimelineItem: DisplayTimelineItem) {
        if (displayTimelineItem.isSeen) {
            addCheckMark(displayTimelineItem)
        } else {
            removeCheckMark(displayTimelineItem)
        }
    }

    private fun addCheckMark(displayTimelineItem: DisplayTimelineItem) {

        Analytics.logEvent("add_check_mark_clicked") {
            putString("serial_name", displayTimelineItem.serialName)
            putString("translator_name", displayTimelineItem.translatorName)
            putInt("season_number", displayTimelineItem.seasonNumber)
            putInt("episode_number", displayTimelineItem.episodeNumber)
            putString("from", "timeline screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        checkMarkInteractor
            .addCheckMark(displayTimelineItem.tmdbId, displayTimelineItem.seasonNumber, displayTimelineItem.episodeNumber)
            .subscribe(
                { response ->
                    if (response.success) {
                        viewState.updateItem(displayTimelineItem)
                        acceptRefresh()

                    } else {
                        viewState.updateItem(displayTimelineItem.apply { isSeen = isSeen.not() })
                        viewState.showMessage(resourceManager.getString(R.string.episodes_problem_add_check_mark))
                    }
                },
                { error ->
                    viewState.updateItem(displayTimelineItem.apply { isSeen = isSeen.not() })
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    private fun removeCheckMark(displayTimelineItem: DisplayTimelineItem) {

        Analytics.logEvent("remove_check_mark_clicked") {
            putString("serial_name", displayTimelineItem.serialName)
            putString("translator_name", displayTimelineItem.translatorName)
            putInt("season_number", displayTimelineItem.seasonNumber)
            putInt("episode_number", displayTimelineItem.episodeNumber)
            putString("from", "timeline screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        checkMarkInteractor
            .removeCheckMark(displayTimelineItem.tmdbId, displayTimelineItem.seasonNumber, displayTimelineItem.episodeNumber)
            .subscribe(
                { (success) ->
                    if (success == true) {
                        viewState.updateItem(displayTimelineItem)
                        acceptRefresh()

                    } else {
                        viewState.updateItem(displayTimelineItem.apply { isSeen = isSeen.not() })
                        viewState.showMessage(resourceManager.getString(R.string.episodes_problem_remove_check_mark))
                    }
                },
                { error ->
                    viewState.updateItem(displayTimelineItem.apply { isSeen = isSeen.not() })
                    errorHandler.proceed(error) { viewState.showMessage(it) }
                }
            )
            .connect()
    }

    private fun showEmptyViewAccordingToFavoritesExistence() {
        favoriteInteractor
            .hasFavorites()
            .doAfterTerminate { viewState.showRefreshProgress(false) }
            .subscribe(
                { hasFavorites ->
                    val message = resourceManager.getString(if (hasFavorites) R.string.timeline_text_empty_message else R.string.timeline_text_no_favorites_message)
                    viewState.showEmptyView(true, message)
                },
                { error ->
                    errorHandler.proceed(error) { viewState.showEmptyError(true, it) }
                }
            )
            .connect()
    }

    private fun observeRefresh() {
        refreshDisposable = refreshRelay.subscribe {
            clearAdapterRelay.accept(true)
            refreshTimeline()
        }.apply { connect() }
    }

    private fun acceptRefresh() {
        refreshDisposable?.dispose()
        refreshRelay.accept(true)
        observeRefresh()
    }

    fun onBackPressed() = innerRouter.exit()
}