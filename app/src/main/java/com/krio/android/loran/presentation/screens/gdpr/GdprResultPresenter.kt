package com.krio.android.loran.presentation.screens.gdpr

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.Screens
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.AdInteractor
import com.krio.android.loran.model.system.ad.ConsentManager
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.screens.main.MainScreenLauncher
import ru.terrakok.cicerone.Router
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
@InjectViewState
class GdprResultPresenter @Inject constructor(
    private val prefs: Prefs,
    private val adInteractor: AdInteractor,
    private val mainScreenLauncher: MainScreenLauncher
) : BasePresenter<GdprResultView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showResult(prefs.resultGdpr)
    }

    fun onCloseClicked() {
        adInteractor
            .getAdAppConfiguration()
            .subscribe(
                {
                    mainScreenLauncher.launch(it.adAppConfiguration, compositeDisposable) {
                        // do nothing
                    }
                },
                {
                    mainScreenLauncher.launch(null, compositeDisposable) {
                        // do nothing
                    }
                }
            )
            .connect()
    }

}
