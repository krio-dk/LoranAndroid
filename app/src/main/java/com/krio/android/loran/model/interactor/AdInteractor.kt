package com.krio.android.loran.model.interactor

import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.AdRepository
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 06.06.2019.
 */
class AdInteractor @Inject constructor(
    private val repository: AdRepository,
    private val prefs: Prefs
) {

    init {
        Timber.d("Init")
    }

    fun getAdScreenConfiguration(screen: String) = repository.getAdScreenConfiguration(screen, prefs.userId)

    fun getAdAppConfiguration() = repository.getAdAppConfiguration(prefs.userId)

    fun needShowRewardedVideoDialog(serialTmdbId: Int): Single<Boolean> {
        return prefs.userId?.let { userId ->
            repository.needShowRewardedVideoDialog(serialTmdbId, userId).map { it.needShow }
        } ?: Single.just(false)
    }
}