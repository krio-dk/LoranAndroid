package com.krio.android.loran.entity.presentation.displayitems.serial

/**
 * Created by Dmitriy Kolmogorov on 12.04.2018.
 */
data class DisplayTranslationItem(
        val translatorId: Int,
        val translatorName: String,
        val seasonsCount: Int,
        val episodesCount: Int,
        var isFavorite: Boolean = false
)