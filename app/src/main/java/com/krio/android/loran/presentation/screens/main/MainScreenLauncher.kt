package com.krio.android.loran.presentation.screens.main

import com.krio.android.loran.Screens
import com.krio.android.loran.entity.domain.core.ad.AdAppConfiguration
import com.krio.android.loran.model.interactor.AdInteractor
import com.krio.android.loran.model.interactor.FavoriteInteractor
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.ad.ConsentManager
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import com.krio.android.loran.ui.screens.main.MainFragment
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 2019-06-01.
 */
class MainScreenLauncher @Inject constructor(
    private val favoriteInteractor: FavoriteInteractor,
    private val adInteractor: AdInteractor,
    @AppNavigation private val router: Router,
    private val consentManager: ConsentManager
) {

    fun requestConsentOrLaunch(
        compositeDisposable: CompositeDisposable,
        completeHandler: () -> Unit = { }
    ) {
        getAdAppConfiguration(compositeDisposable, completeHandler)
    }

    private fun getAdAppConfiguration(
        compositeDisposable: CompositeDisposable,
        completeHandler: () -> Unit
    ) {
        adInteractor.getAdAppConfiguration()
            .subscribe(
                { adAppConfigurationResponse ->
                    val adAppConfiguration = adAppConfigurationResponse.adAppConfiguration

                    if (adAppConfiguration == null) {
                        getTabIndexAndLaunch(false, null, compositeDisposable, completeHandler)
                    } else {
                        checkNeedRequestConsent(adAppConfiguration, compositeDisposable, completeHandler)
                    }
                },
                {
                    getTabIndexAndLaunch(false, null, compositeDisposable, completeHandler)
                }
            )
            .also { compositeDisposable.add(it) }
    }

    private fun checkNeedRequestConsent(
        adAppConfiguration: AdAppConfiguration?,
        compositeDisposable: CompositeDisposable,
        completeHandler: () -> Unit
    ) {
        consentManager.needRequestConsent
            .subscribe(
                { needRequestConsent ->
                    if (needRequestConsent) {
                        router.replaceScreen(Screens.GdprScreen)
                        completeHandler.invoke()
                    } else {
                        launch(adAppConfiguration, compositeDisposable, completeHandler)
                    }
                },
                {
                    getTabIndexAndLaunch(false, adAppConfiguration, compositeDisposable, completeHandler)
                }
            )
            .also { compositeDisposable.add(it) }
    }

    fun launch(
        adAppConfiguration: AdAppConfiguration?,
        compositeDisposable: CompositeDisposable,
        completeHandler: () -> Unit
    ) {
        consentManager.hasConsent
            .subscribe(
                { hasConsent ->
                    getTabIndexAndLaunch(
                        hasConsent = hasConsent,
                        adAppConfiguration = adAppConfiguration,
                        compositeDisposable = compositeDisposable,
                        completeHandler = completeHandler
                    )
                },
                {
                    getTabIndexAndLaunch(
                        hasConsent = false,
                        adAppConfiguration = adAppConfiguration,
                        compositeDisposable = compositeDisposable,
                        completeHandler = completeHandler
                    )
                }
            )
            .also { compositeDisposable.add(it) }
    }


    private fun getTabIndexAndLaunch(
        hasConsent: Boolean,
        adAppConfiguration: AdAppConfiguration?,
        compositeDisposable: CompositeDisposable,
        completeHandler: () -> Unit
    ) {
        favoriteInteractor.hasFavorites()
            .subscribe(
                { hasFavorites ->
                    launchMainScreen(
                        hasConsent = hasConsent,
                        adAppConfiguration = adAppConfiguration,
                        tabIndex = if (hasFavorites) 1 else 0,
                        completeHandler = completeHandler
                    )
                },
                {
                    launchMainScreen(
                        hasConsent = hasConsent,
                        adAppConfiguration = adAppConfiguration,
                        tabIndex = 1,
                        completeHandler = completeHandler
                    )
                }
            )
            .also { compositeDisposable.add(it) }
    }

    private fun launchMainScreen(
        hasConsent: Boolean,
        adAppConfiguration: AdAppConfiguration?,
        tabIndex: Int,
        completeHandler: () -> Unit
    ) {
        val params = MainFragment.InitParams(
            appodealParams = AppodealManager.InitParams(hasConsent, adAppConfiguration),
            presenterParams = MainPresenter.InitParams(tabIndex)
        )

        router.replaceScreen(Screens.MainScreen(params))
        completeHandler.invoke()
    }
}