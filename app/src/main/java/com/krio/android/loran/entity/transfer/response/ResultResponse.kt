package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 15.04.2018.
 */
data class ResultResponse(
        @SerializedName("success") val success: Boolean? = null
)