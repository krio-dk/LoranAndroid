package com.krio.android.loran.presentation.screens.search.list

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import at.favre.lib.dali.Dali
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.search.DisplaySearchItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.model.data.glide.GlideApp
import kotlinx.android.synthetic.main.item_search_serial.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class SearchItemAdapterDelegate(private val clickListener: (DisplaySearchItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplaySearchItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_serial, parent, false)
        return SearchViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as SearchViewHolder).bind(items[position] as DisplaySearchItem)
    }

    class SearchViewHolder(val view: View, clickListener: (DisplaySearchItem) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var serialItemDisplay: DisplaySearchItem

        init {
            view.setOnClickListener { clickListener.invoke(serialItemDisplay) }
        }

        fun bind(searchItem: DisplaySearchItem) {

            this.serialItemDisplay = searchItem

            view.backdrop.clearColorFilter()
            view.backdrop.setImageResource(R.drawable.placeholder_backdrop)

            val posterPath = searchItem.posterPath
            if (posterPath != null) {
                GlideApp.with(view.context)
                        .load(ImagesConfiguration.getPosterFullPath(PosterSize.POSTER_SIZE_342, posterPath))
                        .placeholder(R.drawable.placeholder_cover)
                        .into(view.imageView)
            }

            val backdropPath = searchItem.backdropPath
            if (backdropPath != null) {
                GlideApp.with(view.context)
                        .asBitmap()
                        .load(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_300, backdropPath))
                        .into(object : CustomTarget<Bitmap>() {

                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                if (backdropPath == serialItemDisplay.backdropPath) {
                                    Dali.create(view.context)
                                            .load(resource)
                                            .placeholder(R.drawable.placeholder_backdrop)
                                            .blurRadius(10)
                                            .downScale(2)
                                            .concurrent()
                                            .reScale()
                                            .skipCache()
                                            .into(view.backdrop)
                                }
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {
                                // do nothing
                            }
                        })
            }


            var year: Int? = null
            if (!searchItem.firstAirDate.isNullOrEmpty()) {
                val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                val date = format.parse(searchItem.firstAirDate)
                val cal = Calendar.getInstance()
                cal.time = date
                year = cal.get(Calendar.YEAR)
            }

            view.name.text = searchItem.serialName

            if (searchItem.serialName != searchItem.serialOriginalName) {
                view.originalName.text = when (year) {
                    null -> searchItem.serialOriginalName
                    else -> "${searchItem.serialOriginalName} ($year)"
                }

            } else if (year != null) {
                view.originalName.text = year.toString()
            }

            if (searchItem.overview.isNullOrEmpty()) {
                view.overview.text = view.resources.getString(R.string.common_text_no_overview)
            } else {
                view.overview.text = searchItem.overview
            }
        }
    }
}