package com.krio.android.loran.presentation.screens.episodes

import com.arellomobile.mvp.InjectViewState
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.episodes.DisplayEpisodeItem
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.interactor.CheckMarkInteractor
import com.krio.android.loran.model.interactor.ImagesInteractor
import com.krio.android.loran.model.interactor.SerialInteractor
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@InjectViewState
class EpisodesPresenter @Inject constructor(
        private val initParams: InitParams,
        private val serialInteractor: SerialInteractor,
        private val checkMarkInteractor: CheckMarkInteractor,
        private val imagesInteractor: ImagesInteractor,
        private val resourceManager: ResourceManager,
        private val refreshRelay: PublishRelay<Boolean>,
        private val prefs: Prefs,
        private val router: Router,
        private val errorHandler: ErrorHandler
) : BasePresenter<EpisodesView>() {

    data class InitParams(
            val serialTmdbId: Int,
            val serialName: String,
            val backdropPath: String?,
            val translatorId: Int,
            val translatorName: String,
            val seasonNumber: Int,
            val seasonName: String?
    ) : Serializable

    var refreshDisposable: Disposable? = null

    var originalEpisodeItems: List<DisplayEpisodeItem> = listOf()
    var isGroupMarkChecked = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        Analytics.logEvent("episodes_screen_opened") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            putInt("season_number", initParams.seasonNumber)
            prefs.userId?.let { putInt("user_id", it) }
        }

        viewState.showTitle(initParams.serialName)
        viewState.showTranslatorAndSeason("${initParams.translatorName} \u00b7 ${initParams.seasonName}")

        observeRefresh()
        getData(true)
    }

    fun getData(refresh: Boolean = false) {
        serialInteractor.getEpisodeItems(initParams.serialTmdbId, initParams.seasonNumber, initParams.translatorId, refresh)
                .flatMap { displayEpisodeItems ->
                    imagesInteractor
                            .preloadStills(displayEpisodeItems.mapNotNull { it.stillPath })
                            .flatMap { Single.just(displayEpisodeItems) }
                }
                .doOnSubscribe {
                    viewState.showEmptyError(false, null)
                    viewState.showEmptyProgress(true)
                }
                .doAfterTerminate {
                    viewState.showEmptyProgress(false)
                }
                .subscribe(
                        { episodeItems ->
                            this.originalEpisodeItems = episodeItems
                            updateGroupMark()
                            viewState.showData(episodeItems)
                        },
                        { error ->
                            errorHandler.proceed(error, { viewState.showEmptyError(true, it) })
                        }
                )
                .connect()
    }

    fun onCheckMarkClicked(displayEpisodeItem: DisplayEpisodeItem) {
        if (displayEpisodeItem.isSeen) {
            addCheckMark(displayEpisodeItem)
        } else {
            removeCheckMark(displayEpisodeItem)
        }
        updateGroupMark()
    }

    fun onGroupCheckMarkClicked() {
        viewState.showGroupCheckConfirmDialog(
            if (isGroupMarkChecked) R.style.DialogThemeRemove else R.style.DialogThemeAction,
            resourceManager.getString(if (isGroupMarkChecked) R.string.episodes_group_check_mark_confirm_remove else R.string.episodes_group_check_mark_confirm_add),
            resourceManager.getString(if (isGroupMarkChecked) R.string.episodes_group_check_mark_confirm_button_remove else R.string.episodes_group_check_mark_confirm_button_add)
        )
    }

    fun onGroupCheckMarkClickedConfirmed() {
        if (!isGroupMarkChecked) {
            val episodeItems = originalEpisodeItems.map { it.copy(isSeen = true) }
            viewState.showData(episodeItems)
            viewState.updateGroupMark(true)
            addGroupCheckMark(episodeItems)

        } else {
            val episodeItems = originalEpisodeItems.map { it.copy(isSeen = false) }
            viewState.showData(episodeItems)
            viewState.updateGroupMark(false)
            removeGroupCheckMark(episodeItems)
        }
        isGroupMarkChecked = !isGroupMarkChecked
    }

    private fun addCheckMark(displayEpisodeItem: DisplayEpisodeItem) {

        Analytics.logEvent("add_check_mark_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            putInt("season_number", initParams.seasonNumber)
            putInt("episode_number", displayEpisodeItem.episodeNumber)
            putString("from", "episodes screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        checkMarkInteractor
                .addCheckMark(
                        initParams.serialTmdbId,
                        initParams.seasonNumber,
                        displayEpisodeItem.episodeNumber
                )
                .subscribe(
                        { response ->
                            if (response.success) {
                                viewState.updateItem(displayEpisodeItem)
                                acceptRefresh()

                            } else {
                                viewState.updateItem(displayEpisodeItem.apply { isSeen = isSeen.not() })
                                viewState.showMessage(resourceManager.getString(R.string.episodes_problem_add_check_mark))
                            }
                        },
                        { error ->
                            viewState.updateItem(displayEpisodeItem.apply { isSeen = isSeen.not() })
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                )
                .connect()
    }

    private fun removeCheckMark(displayEpisodeItem: DisplayEpisodeItem) {

        Analytics.logEvent("remove_check_mark_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            putInt("season_number", initParams.seasonNumber)
            putInt("episode_number", displayEpisodeItem.episodeNumber)
            putString("from", "episodes screen")
            prefs.userId?.let { putInt("user_id", it) }
        }

        checkMarkInteractor
                .removeCheckMark(
                        initParams.serialTmdbId,
                        initParams.seasonNumber,
                        displayEpisodeItem.episodeNumber
                )
                .subscribe(
                        { (success) ->
                            if (success == true) {
                                viewState.updateItem(displayEpisodeItem)
                                acceptRefresh()

                            } else {
                                viewState.updateItem(displayEpisodeItem.apply { isSeen = isSeen.not() })
                                viewState.showMessage(resourceManager.getString(R.string.episodes_problem_remove_check_mark))
                            }
                        },
                        { error ->
                            viewState.updateItem(displayEpisodeItem.apply { isSeen = isSeen.not() })
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        })
                .connect()

    }

    private fun addGroupCheckMark(episodeItems: List<DisplayEpisodeItem>) {

        Analytics.logEvent("add_group_check_mark_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            putInt("season_number", initParams.seasonNumber)
            prefs.userId?.let { putInt("user_id", it) }
        }

        checkMarkInteractor
                .addGroupCheckMark(
                        initParams.serialTmdbId,
                        initParams.seasonNumber,
                        originalEpisodeItems.map { it.episodeNumber }
                )
                .subscribe(
                        { response ->
                            if (response.success) {
                                originalEpisodeItems = episodeItems
                                acceptRefresh()
                            } else {
                                updateGroupMark()
                                viewState.showData(originalEpisodeItems)
                                viewState.showMessage(resourceManager.getString(R.string.episodes_problem_add_group_check_mark))
                            }
                        },
                        { error ->
                            updateGroupMark()
                            viewState.showData(originalEpisodeItems)
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                )
                .connect()
    }

    private fun removeGroupCheckMark(episodeItems: List<DisplayEpisodeItem>) {

        Analytics.logEvent("remove_group_check_mark_clicked") {
            putString("serial_name", initParams.serialName)
            putString("translator_name", initParams.translatorName)
            putInt("season_number", initParams.seasonNumber)
            prefs.userId?.let { putInt("user_id", it) }
        }

        checkMarkInteractor
                .removeGroupCheckMark(
                        initParams.serialTmdbId,
                        initParams.seasonNumber
                )
                .subscribe(
                        { (success) ->
                            if (success == true) {
                                originalEpisodeItems = episodeItems
                                acceptRefresh()
                            } else {
                                updateGroupMark()
                                viewState.showData(originalEpisodeItems)
                                viewState.showMessage(resourceManager.getString(R.string.episodes_problem_remove_group_check_mark))
                            }
                        },
                        { error ->
                            updateGroupMark()
                            viewState.showData(originalEpisodeItems)
                            errorHandler.proceed(error) { viewState.showMessage(it) }
                        }
                )
                .connect()

    }

    private fun updateGroupMark() {
        isGroupMarkChecked = isGroupMarkShouldChecked()
        viewState.updateGroupMark(isGroupMarkChecked)
        viewState.showGroupMark(true)
    }

    private fun isGroupMarkShouldChecked() = originalEpisodeItems.count { it.isSeen } == originalEpisodeItems.count()

    private fun observeRefresh() {
        refreshDisposable = refreshRelay.subscribe {
            viewState.showData(emptyList())
            viewState.showGroupMark(false)
            getData(refresh = true)
        }.apply { connect() }
    }

    private fun acceptRefresh() {
        refreshDisposable?.dispose()
        refreshRelay.accept(true)
        observeRefresh()
    }

    fun onBackPressed() = router.exit()
}