package com.krio.android.loran.entity.thirdparty.tmdb

/**
 * Created by Dmitriy Kolmogorov on 10.03.2018.
 */
object ImagesConfiguration {

    private const val SECURE_BASE_URL = "https://image.tmdb.org/t/p/"

    fun getPosterFullPath(posterSize: PosterSize, posterPath: String) = SECURE_BASE_URL + posterSize.value + posterPath
    fun getBackdropFullPath(backdropSize: BackdropSize, backdropPath: String) = SECURE_BASE_URL + backdropSize.value + backdropPath
    fun getStillFullPath(stillSize: StillSize, stillPath: String) = SECURE_BASE_URL + stillSize.value + stillPath
    fun getProfileFullPath(profileSize: ProfileSize, profilePath: String) = SECURE_BASE_URL + profileSize.value + profilePath
}

enum class PosterSize(val value: String) {
    POSTER_SIZE_185("w185"),
    POSTER_SIZE_342("w342"),
    POSTER_SIZE_500("w500"),
    POSTER_SIZE_780("w780"),
    POSTER_SIZE_ORIGINAL("original")
}

enum class BackdropSize(val value: String) {
    BACKDROP_SIZE_300("w300"),
    BACKDROP_SIZE_780("w780"),
    BACKDROP_SIZE_1280("w1280"),
    BACKDROP_SIZE_ORIGINAL("original")
}

enum class StillSize(val value: String) {
    STILL_SIZE_300("w300"),
    STILL_SIZE_780("w780"),
    STILL_SIZE_ORIGINAL("original")
}

enum class ProfileSize(val value: String) {
    PROFILE_SIZE_185("w185"),
    PROFILE_SIZE_632("h632"),
    PROFILE_SIZE_ORIGINAL("original")
}