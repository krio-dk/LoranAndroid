package com.krio.android.loran.model.system.secure

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.github.javiersantos.piracychecker.PiracyChecker
import com.github.javiersantos.piracychecker.allow
import com.github.javiersantos.piracychecker.doNotAllow
import com.github.javiersantos.piracychecker.enums.InstallerID
import com.github.javiersantos.piracychecker.onError
import com.github.javiersantos.piracychecker.utils.apkSignatures
import com.krio.android.loran.BuildConfig
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.analytics.Analytics
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 01.05.2020.
 */
class PiracyManager @Inject constructor(
    private val context: Context,
    private val prefs: Prefs
) {

    companion object {
        private const val RELEASE_SIGNATURE = "4Bw5JuDccBFQG82h9/4Bgm65wcw="
    }

    var activity: WeakReference<Activity>? = null

    private var watchYoutubeDisposable: Disposable? = null

    init {
        if (!BuildConfig.DEBUG) checkInstaller(context)
    }

    fun check(verifySignature: Boolean, verifyInstaller: Boolean, verifyPirateApps: Boolean, verifyThirdPartyStore: Boolean): Completable {

        val relay = PublishSubject.create<Boolean>()

        val piracyChecker = PiracyChecker(context).apply {
            context.apkSignatures.forEach { Timber.d("Apk Signature: $it") }

            if (!BuildConfig.DEBUG) {
                if (verifySignature) enableSigningCertificates(RELEASE_SIGNATURE)
                if (verifyInstaller) enableInstallerId(InstallerID.GOOGLE_PLAY)
            }

            if (verifyPirateApps) enableUnauthorizedAppsCheck()
            if (verifyThirdPartyStore) enableStoresCheck()
        }

        return relay
            .ignoreElements()
            .doOnSubscribe {
                piracyChecker.apply {

                    allow { relay.onComplete() }

                    doNotAllow { piracyCheckerError, pirateApp ->
                        Timber.d("Piracy detected: $piracyCheckerError")
                        Analytics.logEvent("piracy_detected") {
                            putString("reason", piracyCheckerError.name)
                            putString("description", piracyCheckerError.toString())
                            prefs.userId?.let { putInt("user_id", it) }
                        }

                        watchYoutubeDisposable?.dispose()
                        watchYoutubeDisposable = Observable
                            .timer(15, TimeUnit.SECONDS)
                            .subscribe { watchYoutubeVideo(context) }

                        relay.onError(PiracyDetectedException(piracyCheckerError, pirateApp))
                    }

                    onError { piracyCheckerError ->
                        Timber.d("Piracy cehcker error: $piracyCheckerError")
                        Analytics.logEvent("piracy_checker_error") {
                            putString("error", piracyCheckerError.name)
                            putString("description", piracyCheckerError.toString())
                            prefs.userId?.let { putInt("user_id", it) }
                        }
                        relay.onError(PiracyErrorException())
                    }

                    start()
                }
            }
    }

    fun disposeWatchYoutube() {
        Timber.d("Dispose watch YouTube")
        watchYoutubeDisposable?.dispose()
    }

    private fun checkInstaller(context: Context) {
        val installer = context.packageManager.getInstallerPackageName(context.packageName)
        if (installer == null || !InstallerID.GOOGLE_PLAY.toIDs().contains(installer)) {
            Timber.d("Install not from Google Play")

            if (!prefs.wasInstallNotFromGooglePlayEventSend) {
                Analytics.logEvent("install_not_from_google_play") {
                    installer?.let { putString("installer", it) }
                    prefs.userId?.let { putInt("user_id", it) }
                }
                prefs.wasInstallNotFromGooglePlayEventSend = true
            }

            Analytics.logEvent("launch_installed_not_from_google_play") {
                installer?.let { putString("installer", it) }
                prefs.userId?.let { putInt("user_id", it) }
            }
        }
    }

    private fun watchYoutubeVideo(context: Context) {
        activity?.get()?.let {
            val id = "9rWai38hti8"
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
                it.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$id"))
                it.startActivity(intent)
            }
        }
    }
}