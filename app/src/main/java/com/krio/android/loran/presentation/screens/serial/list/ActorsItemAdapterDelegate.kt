package com.krio.android.loran.presentation.screens.serial.list

import android.graphics.Rect
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayActorsItem
import com.krio.android.loran.entity.presentation.displayitems.serial.DisplayPersonItem
import kotlinx.android.synthetic.main.item_cast.view.*


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class ActorsItemAdapterDelegate(
        private val clickListener: (DisplayPersonItem) -> Unit
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplayActorsItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cast, parent, false)
        return ActorsViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as ActorsViewHolder).bind(items[position] as DisplayActorsItem)
    }

    class ActorsViewHolder(
            private val view: View,
            private val clickListener: (DisplayPersonItem) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        var displayActorsItem: DisplayActorsItem? = null
        private val adapter = ActorAdapter()

        init {
            view.recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = this@ActorsViewHolder.adapter

                addItemDecoration(object : RecyclerView.ItemDecoration() {
                    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                        parent.adapter?.let { adapter ->

                            val layoutParams = view.layoutParams as RecyclerView.LayoutParams

                            val fullPadding = resources.getDimensionPixelSize(R.dimen.cover_full_padding)
                            val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)

                            val topPadding = fullPadding
                            val bottomPadding = fullPadding

                            val count = adapter.itemCount

                            when (layoutParams.viewAdapterPosition) {
                                0 -> outRect.set(fullPadding, topPadding, halfPadding, bottomPadding)
                                count - 1 -> outRect.set(0, topPadding, fullPadding, bottomPadding)
                                else -> outRect.set(0, topPadding, halfPadding, bottomPadding)
                            }
                        }
                    }
                })
            }

            view.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == SCROLL_STATE_IDLE) {
                        displayActorsItem?.recyclerViewState = view.recyclerView.layoutManager?.onSaveInstanceState()
                    }
                }
            })
        }

        fun bind(actorsItem: DisplayActorsItem) {
            this.displayActorsItem = actorsItem
            adapter.setData(actorsItem.actors)
            view.recyclerView.layoutManager?.onRestoreInstanceState(actorsItem.recyclerViewState)
        }

        inner class ActorAdapter : ListDelegationAdapter<MutableList<Any>>() {
            init {
                items = mutableListOf()
                delegatesManager.addDelegate(PersonItemAdapterDelegate({ clickListener.invoke(it) }))
            }

            fun setData(data: List<Any>) {
                items.clear()
                items.addAll(data)
                notifyDataSetChanged()
            }
        }
    }
}