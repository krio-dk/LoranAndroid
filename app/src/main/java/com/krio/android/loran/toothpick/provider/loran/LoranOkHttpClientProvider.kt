package com.krio.android.loran.toothpick.provider.loran

import com.krio.android.loran.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class LoranOkHttpClientProvider @Inject constructor() : Provider<OkHttpClient> {

    override fun get() = OkHttpClient.Builder().apply {
        readTimeout(30, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            addNetworkInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        }

    }.build()
}