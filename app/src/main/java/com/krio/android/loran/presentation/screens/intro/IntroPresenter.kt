package com.krio.android.loran.presentation.screens.intro

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.R
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.screens.main.MainScreenLauncher
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject


/**
 * Created by Dmitriy Kolmogorov on 09.09.2018.
 */
@InjectViewState
class IntroPresenter @Inject constructor(
    private val initParams: InitParams,
    @AppNavigation private val router: Router,
    private val prefs: Prefs,
    private val resourceManager: ResourceManager,
    private val mainScreenLauncher: MainScreenLauncher
) : BasePresenter<IntroView>() {

    data class InitParams(val isInitFlow: Boolean) : Serializable

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        if (initParams.isInitFlow) {
            viewState.setCloseButtonText(resourceManager.getString(R.string.intro_button_skip))
        } else {
            viewState.setCloseButtonText(resourceManager.getString(R.string.intro_button_close))
        }
    }

    fun onNextClick(currentItem: Int, pagesCount: Int) {

        Analytics.logEvent("intro_next_clicked") {
            prefs.userId?.let { putInt("user_id", it) }
        }

        if (currentItem + 1 < pagesCount) {
            viewState.showNextPage()
        } else {
            exit()
        }
    }

    fun onBackPressed() {
        if (initParams.isInitFlow) {
            viewState.showConfirmExitDialog { exit() }
        } else {
            exit()
        }
    }

    private fun exit() {

        Analytics.logEvent("intro_exit_clicked") {
            prefs.userId?.let { putInt("user_id", it) }
        }

        prefs.wasIntroShowing = true

        if (initParams.isInitFlow) mainScreenLauncher.requestConsentOrLaunch(compositeDisposable)
        else router.exit()
    }
}
