package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.core.main.Serial

/**
 * Created by Dmitriy Kolmogorov on 15.04.2018.
 */
data class SerialResponse(
        @SerializedName("serial") val serial: Serial? = null
)