package com.krio.android.loran.presentation.screens.episodes

import androidx.annotation.StyleRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.krio.android.loran.entity.presentation.displayitems.episodes.DisplayEpisodeItem

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface EpisodesView : MvpView {
    fun showTitle(title: String)
    fun showTranslatorAndSeason(text: String)
    fun showBackdrop(url: String)
    fun showData(displayEpisodes: List<DisplayEpisodeItem>)
    fun showEmptyProgress(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showGroupMark(show: Boolean)
    fun updateGroupMark(checked: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun updateItem(item: DisplayEpisodeItem)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(SkipStrategy::class)
    fun showGroupCheckConfirmDialog(@StyleRes themeResId: Int, message: String, positiveButtonText: String)
}