package com.krio.android.loran.presentation.screens.person

import com.arellomobile.mvp.InjectViewState
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.ProfileSize
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPerson
import com.krio.android.loran.entity.thirdparty.tmdb.person.TmdbPersonImages
import com.krio.android.loran.model.interactor.ImagesInteractor
import com.krio.android.loran.model.interactor.PersonInteractor
import com.krio.android.loran.presentation.global.BasePresenter
import com.krio.android.loran.presentation.global.ErrorHandler
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import ru.terrakok.cicerone.Router
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 26.02.2018.
 */
@InjectViewState
class PersonPresenter @Inject constructor(
        private val initParams: InitParams,
        private val router: Router,
        private val personInteractor: PersonInteractor,
        private val imagesInteractor: ImagesInteractor,
        private val errorHandler: ErrorHandler
) : BasePresenter<PersonView>() {

    data class InitParams(
            val personName: String,
            val profilePath: String,
            val personTmdbId: Int
    ) : Serializable

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showTitle(initParams.personName)
        showBackdrop()
        getData()
    }


    private fun showBackdrop() {
        viewState.showBackdrop(ImagesConfiguration.getProfileFullPath(ProfileSize.PROFILE_SIZE_632, initParams.profilePath))
    }

    fun getData() {
        getMergedData(initParams.personTmdbId)
                .flatMap { pair ->
                    imagesInteractor
                            .preloadProfiles(pair.second)
                            .flatMap { Single.just(pair) }
                }
                .doOnSubscribe {
                    viewState.showEmptyError(false, null)
                    viewState.showEmptyProgress(true)
                }
                .doAfterTerminate {
                    viewState.showEmptyProgress(false)
                }
                .subscribe(
                        { pair ->
                            viewState.showPhotos(pair.second)
                            viewState.showData(pair.first)
                        },
                        { error ->
                            errorHandler.proceed(error, { viewState.showEmptyError(true, it) })
                        }
                )
                .connect()
    }

    private fun getMergedData(personId: Int) =
            Single.zip(
                    personInteractor.getPerson(personId),
                    personInteractor.getPersonImages(personId),
                    BiFunction<TmdbPerson, TmdbPersonImages, Pair<TmdbPerson, List<String>>> { person, personImages ->
                        val photos = personImages.profiles
                                .map { it.filePath }
                                .filter { it.isNotEmpty() }
                        Pair(person, photos)
                    }
            )

    fun onBackPressed() {
        router.exit()
    }
}