package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName
import com.krio.android.loran.entity.domain.dataitems.main.FavoriteItem

data class FavoriteItemResponse (
        @SerializedName("favorite_item") val favoriteItem: FavoriteItem? = null
)