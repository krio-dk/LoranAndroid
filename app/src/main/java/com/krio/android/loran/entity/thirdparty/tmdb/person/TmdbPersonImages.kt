package com.krio.android.loran.entity.thirdparty.tmdb.person


import com.google.gson.annotations.SerializedName

data class TmdbPersonImages(
        @SerializedName("profiles") val profiles: List<TmdbProfilesItem>,
        @SerializedName("id") val id: Int
)



