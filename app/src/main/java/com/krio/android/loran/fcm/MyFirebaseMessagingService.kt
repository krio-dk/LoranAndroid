package com.krio.android.loran.fcm

import android.content.Context
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.krio.android.loran.extensions.timeZoneDateTime
import com.krio.android.loran.model.data.server.loran.LoranConfig.TRANSLATOR_ORIGINAL_ID
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.data.utils.LanguageUtils.isRussianSpeaking
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.model.system.notification.NotificationManager
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.toothpick.DI
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 30.12.2017.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var context: Context

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var prefs: Prefs

    @Inject
    lateinit var pushTokenSender: PushTokenSender

    @Inject
    lateinit var errorHandler: ErrorHandler

    init {
        Toothpick.inject(this, Toothpick.openScope(DI.APP_SCOPE))
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Analytics.logEvent("push_received") {
            prefs.userId?.let { putInt("user_id", it) }
        }

        mapRemoteMessageToSerialData(remoteMessage)?.let { serialData ->

            Analytics.logEvent("push_data_mapped") {
                prefs.userId?.let { putInt("user_id", it) }
                putString("serial_name", serialData.serialNameRu)
                putString("translator_name", serialData.translatorName)
                putInt("season_number", serialData.seasonNumber)
                putInt("episode_number", serialData.episodeNumber)
            }

            val translatorName = if (serialData.translatorId == TRANSLATOR_ORIGINAL_ID && serialData.networks.isNotBlank()) {
                when {
                    isRussianSpeaking() -> "${serialData.networks} (${serialData.translatorName})"
                    else -> serialData.networks
                }
            } else {
                serialData.translatorName
            }

            notificationManager.showNotification(
                serialName = if (isRussianSpeaking()) serialData.serialNameRu else serialData.serialNameEn,
                episodeName = if (isRussianSpeaking()) serialData.episodeNameRu else serialData.episodeNameEn,
                translator = translatorName,
                translationDate = serialData.translationDate,
                seasonNumber = serialData.seasonNumber,
                episodeNumber = serialData.episodeNumber,
                backdropPath = serialData.backdropPath,
                stillPath = serialData.stillPath
            )
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        pushTokenSender.sendPushToken()
    }

    private fun mapRemoteMessageToSerialData(remoteMessage: RemoteMessage?) = remoteMessage?.data?.let { data ->
        SerialData(
            serialTmdbId = data["serial_tmdb_id"]!!.toInt(),
            serialNameEn = data["serial_name_en"]!!,
            serialNameRu = data["serial_name_ru"]!!,
            episodeNameEn = data["episode_name_en"]!!,
            episodeNameRu = data["episode_name_ru"]!!,
            networks = data["networks"]!!,
            translatorId = data["translator_id"]!!.toInt(),
            translatorName = data["translator_name"]!!,
            translationDate = data["translation_date"]!!.timeZoneDateTime(),
            seasonNumber = data["season_number"]!!.toInt(),
            episodeNumber = data["episode_number"]!!.toInt(),
            backdropPath = data["backdrop_path"]!!,
            stillPath = data["still_path"]!!
        )
    }

    data class SerialData(
        val serialTmdbId: Int,
        val serialNameEn: String,
        val serialNameRu: String,
        val episodeNameEn: String,
        val episodeNameRu: String,
        val networks: String,
        val translatorId: Int,
        val translatorName: String,
        val translationDate: String,
        val seasonNumber: Int,
        val episodeNumber: Int,
        val backdropPath: String,
        val stillPath: String
    )
}

