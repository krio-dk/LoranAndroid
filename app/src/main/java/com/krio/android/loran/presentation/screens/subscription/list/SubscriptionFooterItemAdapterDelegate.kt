package com.krio.android.loran.presentation.screens.subscription.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionFooterItem
import kotlinx.android.synthetic.main.item_subscription_footer.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class SubscriptionFooterItemAdapterDelegate(private val linkClickListener: () -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) = items[position] is DisplaySubscriptionFooterItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_subscription_footer, parent, false)
        return SubscriptionFooterViewHolder(view, linkClickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {

    }

    class SubscriptionFooterViewHolder(val view: View, clickListener: () -> Unit) : RecyclerView.ViewHolder(view) {
        init {
            view.linkToSubscriptions.setOnClickListener {
                clickListener.invoke()
            }
        }
    }
}