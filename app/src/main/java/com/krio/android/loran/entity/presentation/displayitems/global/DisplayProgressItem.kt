package com.krio.android.loran.entity.presentation.displayitems.global

import java.util.*

/**
 * Created by Dmitriy Kolmogorov on 17.02.2018.
 */
data class DisplayProgressItem(val uuid: UUID = UUID.randomUUID())