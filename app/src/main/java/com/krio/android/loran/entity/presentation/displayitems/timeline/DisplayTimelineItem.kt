package com.krio.android.loran.entity.presentation.displayitems.timeline

/**
 * Created by Dmitriy Kolmogorov on 24.03.2018.
 */
data class DisplayTimelineItem(
        val tmdbId: Int,
        val serialName: String,
        val seasonNumber: Int,
        val episodeNumber: Int,
        var isSeen: Boolean,
        val translatorId: Int,
        val translatorName: String,
        val translationDate: String,
        var episodeName: String?,
        var originalAirDate: String?,
        var backdropPath: String?,
        var posterPath: String?,
        var stillPath: String?,
        var isUpdating: Boolean = false
)