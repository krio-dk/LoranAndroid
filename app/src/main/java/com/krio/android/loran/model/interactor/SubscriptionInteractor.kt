package com.krio.android.loran.model.interactor

import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetails
import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.dataitems.main.SubscriptionItem
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionActiveItem
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionItem
import com.krio.android.loran.entity.presentation.displayitems.subscription.DisplaySubscriptionsItem
import com.krio.android.loran.entity.transfer.response.VerifyPurchasesResponse
import com.krio.android.loran.fcm.PushTokenSender
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.repository.SubscriptionRepository
import com.krio.android.loran.model.system.billing.BillingManager
import com.krio.android.loran.model.system.resource.ResourceManager
import io.reactivex.Single
import org.threeten.bp.Period
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject


class SubscriptionInteractor @Inject constructor(
        private val prefs: Prefs,
        private val pushTokenSender: PushTokenSender,
        private val billingManager: BillingManager,
        private val resourceManager: ResourceManager,
        private val repository: SubscriptionRepository
) {


    fun getSubscriptions(): Single<DisplaySubscriptionsItem> = repository
            .getSubscriptionsIds()
            .flatMap { billingManager.querySubscriptionsDetails(it) }
            .map { mapDetailsToSubscriptionItems(it) }
            .map { fillGradeValues(it) }
            .flatMap { markActiveSubscription(it) }
            .map { mapItemsToDisplaySubscriptions(it) }

    fun getActiveSubscription(): Single<String> {
        return prefs.userId?.let { userId ->
            repository
                    .getActiveSubscription(userId)
                    .map { it.activeSubscription?.productId ?: "" }

        } ?: Single.just("")
    }

    fun getLowestSubscriptionPrice(): Single<String> = Single
            .just(listOf("subscription_origin_12_month"))
            .flatMap { billingManager.querySubscriptionsDetails(it) }
            .map { mapDetailsToSubscriptionItems(it) }
            .map { it.first().monthPrice }

    fun verifyPurchases(purchases: List<Purchase>): Single<VerifyPurchasesResponse> = repository
            .verifyPurchases(purchases, prefs.userId)
            .doOnSuccess { result ->
                if (result.verified) {
                    result.userId?.let { userId ->
                        prefs.userId = userId
                        pushTokenSender.sendPushToken()
                    }
                }
            }

    private fun mapDetailsToSubscriptionItems(skuDetailsList: List<SkuDetails>): List<SubscriptionItem> {
        val subscriptionItems = mutableListOf<SubscriptionItem>()

        skuDetailsList.sortedBy { it.priceAmountMicros }.forEach { skuDetails ->
            val subscriptionPeriod = Period.parse(skuDetails.subscriptionPeriod)
            val freeTrialPeriod = Period.parse(skuDetails.freeTrialPeriod)
            val currency = Currency.getInstance(skuDetails.priceCurrencyCode)

            val monthCount = when {
                subscriptionPeriod.years == 1 -> 12
                else -> subscriptionPeriod.months
            }

            val subscriptionItem = SubscriptionItem(
                    id = skuDetails.sku,
                    title = resourceManager.getQuantityString(R.plurals.subscription_period_title, monthCount),
                    totalPrice = getPrice(skuDetails.priceAmountMicros, skuDetails.priceCurrencyCode),
                    periodDescription = resourceManager.getQuantityString(R.plurals.subscription_period_description, monthCount),
                    freeTrial = resourceManager.getQuantityString(R.plurals.subscription_trial_period_value, freeTrialPeriod.days),
                    commonDescription = if (monthCount == 1) resourceManager.getString(R.string.subscription_text_monthly_payment) else resourceManager.getString(R.string.subscription_text_equivalent),
                    monthPrice = getPrice(skuDetails.priceAmountMicros, skuDetails.priceCurrencyCode, monthCount)
            )

            when (currency.currencyCode) {
                "DZD",
                "BDT",
                "BOB",
                "GEL",
                "KZT",
                "MMK",
                "PKR",
                "PYG",
                "RON",
                "LKR" -> {
                    if (subscriptionPeriod.months != 1) {
                        subscriptionItems.add(subscriptionItem)
                    }
                }

                else -> {
                    if (subscriptionPeriod.months != 3) {
                        subscriptionItems.add(subscriptionItem)
                    }
                }
            }
        }

        return subscriptionItems
    }

    private fun fillGradeValues(items: List<SubscriptionItem>): List<SubscriptionItem> {
        return items.apply { forEachIndexed { index, item -> item.grade = index + 1 } }
    }

    private fun markActiveSubscription(items: List<SubscriptionItem>): Single<List<SubscriptionItem>> {
        return billingManager.queryActiveSubscriptions()
                .flatMap { purchases ->
                    purchases.firstOrNull()?.sku?.let { subscriptionId ->
                        items.find { it.id == subscriptionId }?.isActive = true
                    }
                    Single.just(items)
                }
    }

    private fun mapItemsToDisplaySubscriptions(subscriptionItems: List<SubscriptionItem>): DisplaySubscriptionsItem {

        val availableSubscriptions = subscriptionItems
                .filterNot { it.isActive }
                .map {
                    DisplaySubscriptionItem(
                            id = it.id,
                            title = it.title,
                            totalPrice = it.totalPrice,
                            periodDescription = it.periodDescription,
                            freeTrial = it.freeTrial,
                            commonDescription = it.commonDescription,
                            monthPrice = it.monthPrice,
                            grade = it.grade
                    )
                }

        val activeSubscription = subscriptionItems
                .filter { it.isActive }
                .map {
                    DisplaySubscriptionActiveItem(
                            id = it.id,
                            title = it.title,
                            totalPrice = it.totalPrice,
                            periodDescription = it.periodDescription,
                            freeTrial = it.freeTrial,
                            commonDescription = it.commonDescription,
                            monthPrice = it.monthPrice,
                            grade = it.grade
                    )
                }
                .firstOrNull()

        return DisplaySubscriptionsItem(availableSubscriptions, activeSubscription)
    }

    private fun getPrice(priceAmountMicros: Long, priceCurrency: String, delimiter: Int = 1): String {
        val format = NumberFormat.getCurrencyInstance().apply {
            currency = Currency.getInstance(priceCurrency)
            minimumFractionDigits = 0
        }
        val price = priceAmountMicros.toDouble() / 1000000 / delimiter
        return format.format(price)
    }
}