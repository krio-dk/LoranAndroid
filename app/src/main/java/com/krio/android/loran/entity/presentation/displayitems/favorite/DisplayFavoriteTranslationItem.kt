package com.krio.android.loran.entity.presentation.displayitems.favorite

/**
 * Created by Dmitriy Kolmogorov on 09.04.2018.
 */
data class DisplayFavoriteTranslationItem(
        val translatorId: Int,
        val translatorName: String,
        val nextEpisode: DisplayFavoriteNextEpisodeItem?,
        var isChecked: Boolean = false
)