package com.krio.android.loran.presentation.screens.seasons.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.seasons.DisplaySeasonItem
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.entity.thirdparty.tmdb.PosterSize
import com.krio.android.loran.presentation.global.utils.DateTimeUtils
import kotlinx.android.synthetic.main.item_season.view.*

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class SeasonItemAdapterDelegate(private val clickListener: (DisplaySeasonItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplaySeasonItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_season, parent, false)
        return SeasonViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as SeasonViewHolder).bind(items[position] as DisplaySeasonItem)
    }

    class SeasonViewHolder(val view: View, clickListener: (DisplaySeasonItem) -> Unit) : RecyclerView.ViewHolder(view) {

        private lateinit var displaySeasonItem: DisplaySeasonItem

        init {
            view.setOnClickListener { clickListener.invoke(displaySeasonItem) }
        }

        fun bind(seasonItem: DisplaySeasonItem) {
            this.displaySeasonItem = seasonItem

            if (seasonItem.posterPath != null) {
                GlideApp.with(view.context)
                        .load(ImagesConfiguration.getPosterFullPath(PosterSize.POSTER_SIZE_780, seasonItem.posterPath))
                        .placeholder(R.drawable.placeholder_season_cover)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(view.imageView)
            } else {
                GlideApp.with(view.context)
                        .load(R.drawable.placeholder_season_cover)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(view.imageView)
            }

            view.name.text = seasonItem.seasonName

            if (seasonItem.seasonAirDate != null) {
                view.date.text = DateTimeUtils.getFormattedStringDate(seasonItem.seasonAirDate)
            }

            view.numberOfEpisodes.text = seasonItem.episodesCount.toString()
        }
    }
}