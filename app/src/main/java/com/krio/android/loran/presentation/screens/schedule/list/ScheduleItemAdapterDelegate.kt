package com.krio.android.loran.presentation.screens.schedule.list

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import at.favre.lib.dali.Dali
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.krio.android.loran.R
import com.krio.android.loran.entity.domain.dataitems.main.ScheduleType
import com.krio.android.loran.entity.presentation.displayitems.schedule.DisplayScheduleItem
import com.krio.android.loran.entity.thirdparty.tmdb.BackdropSize
import com.krio.android.loran.entity.thirdparty.tmdb.ImagesConfiguration
import com.krio.android.loran.model.data.glide.GlideApp
import kotlinx.android.synthetic.main.item_schedule_next_episode_date.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
class ScheduleItemAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is DisplayScheduleItem


    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_schedule_next_episode_date, parent, false)
        return ScheduleViewHolder(view)
    }

    override fun onBindViewHolder(items: MutableList<Any>, position: Int, viewHolder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (viewHolder as ScheduleViewHolder).bind(items[position] as DisplayScheduleItem)
    }

    class ScheduleViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private lateinit var displayScheduleItem: DisplayScheduleItem

        fun bind(scheduleItem: DisplayScheduleItem) {

            this.displayScheduleItem = scheduleItem

            view.backdrop.setImageResource(R.drawable.placeholder_backdrop)

            view.backdropContainer.foreground = when (scheduleItem.scheduleType) {
                ScheduleType.CONCRETE -> ContextCompat.getDrawable(view.context, R.color.colorScheduleItemConcreteForeground)
                ScheduleType.CALCULATED -> ContextCompat.getDrawable(view.context, R.color.colorScheduleItemCalculatedForeground)
                ScheduleType.ORIGINAL -> ContextCompat.getDrawable(view.context, R.color.colorScheduleItemOriginalForeground)
            }

            val backdropPath = scheduleItem.backdropPath
            if (backdropPath != null) {
                GlideApp.with(view.context)
                        .asBitmap()
                        .load(ImagesConfiguration.getBackdropFullPath(BackdropSize.BACKDROP_SIZE_300, backdropPath))
                        .into(object : CustomTarget<Bitmap>() {
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                if (backdropPath == scheduleItem.backdropPath) {
                                    Dali.create(view.context)
                                            .load(resource)
                                            .placeholder(R.drawable.placeholder_backdrop)
                                            .blurRadius(10)
                                            .downScale(2)
                                            .concurrent()
                                            .reScale()
                                            .skipCache()
                                            .into(view.backdrop)
                                }
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {
                                // do nothing
                            }
                        })
            }

            view.serialName.text = scheduleItem.serialName
            view.translator.text = scheduleItem.translatorName

            val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

            val scheduleDate = dateFormatter.parse(scheduleItem.scheduleDate)
            val currentDate = dateFormatter.parse(dateFormatter.format(Date()))

            val diffInMillies = Math.abs(scheduleDate.time - currentDate.time)
            val nextEpisodeDaysCount = (TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)).toInt()

            view.nextEpisodeDaysCount.text = when (nextEpisodeDaysCount) {
                0 -> {
                    when (scheduleItem.scheduleType) {
                        ScheduleType.CONCRETE -> view.resources.getString(R.string.schedule_text_today_concrete)
                        ScheduleType.CALCULATED -> view.resources.getString(R.string.schedule_text_today_calculated)
                        ScheduleType.ORIGINAL -> view.resources.getString(R.string.schedule_text_today_original)
                    }
                }
                1 -> {
                    when (scheduleItem.scheduleType) {
                        ScheduleType.CONCRETE -> view.resources.getString(R.string.schedule_text_tomorrow_concrete)
                        ScheduleType.CALCULATED -> view.resources.getString(R.string.schedule_text_tomorrow_calculated)
                        ScheduleType.ORIGINAL -> view.resources.getString(R.string.schedule_text_tomorrow_original)
                    }
                }
                else -> {
                    val resource = when (scheduleItem.scheduleType) {
                        ScheduleType.CONCRETE -> R.plurals.schedule_text_next_episode_concrete
                        ScheduleType.CALCULATED -> R.plurals.schedule_text_next_episode_calculated
                        ScheduleType.ORIGINAL -> R.plurals.schedule_text_next_episode_original
                    }

                    view.resources.getQuantityString(resource, nextEpisodeDaysCount, nextEpisodeDaysCount)
                }
            }

            view.nextEpisodeDate.text = SimpleDateFormat("d MMM yyyy", Locale.getDefault()).format(scheduleDate)

            view.seasonNumber.text = scheduleItem.seasonNumber.toString()
            view.episodeNumber.text = scheduleItem.episodeNumber.toString()
        }
    }
}