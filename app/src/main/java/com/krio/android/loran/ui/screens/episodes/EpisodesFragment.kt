package com.krio.android.loran.ui.screens.episodes

import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.annotation.StyleRes
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import at.favre.lib.dali.Dali
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.episodes.DisplayEpisodeItem
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.glide.GlideApp
import com.krio.android.loran.presentation.screens.episodes.EpisodesPresenter
import com.krio.android.loran.presentation.screens.episodes.EpisodesView
import com.krio.android.loran.presentation.screens.episodes.list.EpisodeItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.toothpick.module.SerialModule
import com.krio.android.loran.ui.global.BaseFragment
import com.krio.android.loran.ui.global.ZeroViewHolder
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_episodes.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick
import toothpick.config.Module
import java.io.Serializable

/**
 * Created by Dmitriy Kolmogorov on 07.03.2018.
 */
class EpisodesFragment : BaseFragment(), EpisodesView {

    companion object {
        private const val ARGS_INIT_PARAMS = "season"
        private const val ARGS_PARAM_PARENT_SCOPE = "parent scope"

        fun newInstance(parentScope: String, initParams: InitParams) = EpisodesFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGS_INIT_PARAMS, initParams)
                putString(ARGS_PARAM_PARENT_SCOPE, parentScope)
            }
        }
    }

    data class InitParams(
            val closeSerialRootScopeOnExit: Boolean,
            val presenterParams: EpisodesPresenter.InitParams
    ) : Serializable

    override val layoutRes = R.layout.fragment_episodes

    private val adapter = EpisodesAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    @InjectPresenter
    lateinit var presenter: EpisodesPresenter

    @ProvidePresenter
    fun providePresenter(): EpisodesPresenter {
        val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String
        val initParams = arguments!![ARGS_INIT_PARAMS] as InitParams

        Toothpick.openScopes(parentScope, DI.serialRootScope(parentScope)).apply {
            installModules(SerialModule())
        }

        val scope = Toothpick.openScopes(DI.serialRootScope(parentScope), DI.serialEpisodesScope(parentScope)).apply {
            installModules(object : Module() {
                init {
                    bind(EpisodesPresenter.InitParams::class.java).toInstance(initParams.presenterParams)
                }
            })
        }

        return scope.getInstance(EpisodesPresenter::class.java).also {
            Toothpick.closeScope(DI.serialEpisodesScope(parentScope))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationIcon(R.drawable.ic_action_navigation_arrow_back_inverted)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        groupCheckMark.setOnClickListener { presenter.onGroupCheckMarkClicked() }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@EpisodesFragment.adapter

            addItemDecoration(
                    HorizontalDividerItemDecoration.Builder(context)
                            .colorResId(R.color.colorDarkHintBackground)
                            .sizeResId(R.dimen.divider_size)
                            .showLastDivider()
                            .build()
            )
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.getData()
        }
    }

    override fun showTitle(title: String) {
        toolbarTitle?.text = title
        toolbarTitle?.typeface = null
    }

    override fun showTranslatorAndSeason(text: String) {
        translatorAndSeason?.text = text
    }

    override fun showBackdrop(url: String) {
            GlideApp.with(this)
                    .asBitmap()
                    .load(url)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            Dali.create(activity)
                                    .load(resource)
                                    .placeholder(R.drawable.placeholder_backdrop)
                                    .blurRadius(10)
                                    .downScale(2)
                                    .concurrent()
                                    .reScale()
                                    .skipCache()
                                    .into(backdrop)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            // do nothing
                        }
                    })
    }

    override fun showData(displayEpisodes: List<DisplayEpisodeItem>) {
        adapter.setData(displayEpisodes)
        recyclerView?.visible(true)
    }

    override fun showGroupMark(show: Boolean) {
        groupCheckMarkContainer.visible(show)
    }

    override fun updateItem(item: DisplayEpisodeItem) {
        adapter.updateItem(item)
    }

    override fun updateGroupMark(checked: Boolean) {
        val color = ContextCompat.getColor(requireContext(), if (checked) R.color.colorLinkText else R.color.colorWhitePrimaryText)
        ImageViewCompat.setImageTintList(groupCheckMark, ColorStateList.valueOf(color))

        groupCheckMarkContainer.setCardBackgroundColor(ContextCompat.getColor(requireContext(), if (checked) R.color.colorWhiteBackground else R.color.colorLinkText))
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun showGroupCheckConfirmDialog(@StyleRes themeResId: Int, message: String, positiveButtonText: String) {
        activity?.let {
            androidx.appcompat.app.AlertDialog.Builder(it, themeResId)
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton(positiveButtonText) { _, _ -> presenter.onGroupCheckMarkClickedConfirmed() }
                    .setNegativeButton(getString(R.string.common_button_cancel)) { _, _ -> /* do nothing */ }
                    .create()
                    .show()
        }
    }

    override fun onBackPressed() {
        val initParams = arguments!![ARGS_INIT_PARAMS] as EpisodesFragment.InitParams
        if (initParams.closeSerialRootScopeOnExit) {
            val parentScope = arguments!![ARGS_PARAM_PARENT_SCOPE] as String
            Toothpick.closeScope(DI.serialRootScope(parentScope))
        }

        presenter.onBackPressed()
    }

    inner class EpisodesAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(EpisodeItemAdapterDelegate { episodeItem ->
                episodeItem.isUpdating = true
                episodeItem.isSeen = episodeItem.isSeen.not()
                notifyItemChanged(items.indexOf(episodeItem))
                presenter.onCheckMarkClicked(episodeItem)
            })
        }

        fun updateItem(updatedItem: DisplayEpisodeItem) {
            items
                    .filter { it is DisplayEpisodeItem }
                    .map { it as DisplayEpisodeItem }
                    .find { it.episodeNumber == updatedItem.episodeNumber }?.let { item ->
                        item.isSeen = updatedItem.isSeen
                        item.isUpdating = false
                        notifyItemChanged(items.indexOf(item))
                    }
        }

        fun setData(seasons: List<DisplayEpisodeItem>) {
            items.clear()
            items.addAll(seasons)
            notifyDataSetChanged()
        }
    }

}