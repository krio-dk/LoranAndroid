package com.krio.android.loran.ui.screens.gdpr

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.UnderlineSpan
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.krio.android.loran.R
import com.krio.android.loran.presentation.screens.gdpr.GdprResultPresenter
import com.krio.android.loran.presentation.screens.gdpr.GdprResultView
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_gdpr_result.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
class GdprResultFragment : BaseFragment(), GdprResultView {

    override val layoutRes = R.layout.fragment_gdpr_result

    @InjectPresenter
    lateinit var presenter: GdprResultPresenter

    @ProvidePresenter
    fun providePresenter(): GdprResultPresenter = Toothpick.openScopes(DI.APP_SCOPE).getInstance(GdprResultPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbarTitle?.text = getString(R.string.common_app_name)
        toolbarTitle?.typeface = null
    }

    override fun showResult(resultGdpr: Boolean) {
        text.movementMethod = LinkMovementMethod.getInstance()

        if (resultGdpr) {
            text.text = getString(R.string.gdpr_agree_text)
        } else {
            text.text = getString(R.string.gdpr_disagree_text)
        }

        val close = getString(R.string.gdpr_close).toUpperCase()
        val spannableClose = SpannableString(close)
        spannableClose.setSpan(
            UnderlineSpan(),
            0,
            spannableClose.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        closeTextView.text = spannableClose
        closeButton.setOnClickListener { presenter.onCloseClicked() }
    }
}