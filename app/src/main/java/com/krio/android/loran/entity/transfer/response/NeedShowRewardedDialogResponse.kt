package com.krio.android.loran.entity.transfer.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dmitriy Kolmogorov on 03/05/2020.
 */
data class NeedShowRewardedDialogResponse(
        @SerializedName("need_show") val needShow: Boolean
)