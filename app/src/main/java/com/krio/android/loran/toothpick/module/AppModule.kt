package com.krio.android.loran.toothpick.module

import android.content.Context
import at.favre.lib.dali.Dali
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.rxrelay2.PublishRelay
import com.krio.android.loran.fcm.PushTokenSender
import com.krio.android.loran.fcm.PushTokenWorkerFactory
import com.krio.android.loran.model.data.auth.AuthParameterHolder
import com.krio.android.loran.model.data.server.loran.LoranApi
import com.krio.android.loran.model.data.server.tmdb.TMDbApi
import com.krio.android.loran.model.data.server.tmdb.TMDbConfig
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.initialization.Initializer
import com.krio.android.loran.model.interactor.*
import com.krio.android.loran.model.repository.*
import com.krio.android.loran.model.system.ad.ConsentManager
import com.krio.android.loran.model.system.billing.BillingManager
import com.krio.android.loran.model.system.notification.NotificationManager
import com.krio.android.loran.model.system.resource.ResourceManager
import com.krio.android.loran.model.system.secure.PiracyManager
import com.krio.android.loran.presentation.global.ErrorHandler
import com.krio.android.loran.presentation.screens.main.MainScreenLauncher
import com.krio.android.loran.toothpick.module.qualifier.AppNavigation
import com.krio.android.loran.toothpick.provider.DaliProvider
import com.krio.android.loran.toothpick.provider.loran.LoranApiProvider
import com.krio.android.loran.toothpick.provider.loran.LoranOkHttpClientProvider
import com.krio.android.loran.toothpick.provider.tmdb.TMDbApiProvider
import com.krio.android.loran.toothpick.provider.tmdb.TMDbOkHttpClientProvider
import com.krio.android.loran.toothpick.qualifier.LoranOkHttpClient
import com.krio.android.loran.toothpick.qualifier.TMDbOkHttpClient
import okhttp3.OkHttpClient
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module


/**
 * Created by Dmitriy Kolmogorov on 08.02.2018.
 */
class AppModule(context: Context) : Module() {
    init {
        // Global
        bind(Context::class.java).toInstance(context)
        bind(ResourceManager::class.java).singletonInScope()
        bind(NotificationManager::class.java).singletonInScope()
        bind(ConsentManager::class.java).singletonInScope()
        bind(PiracyManager::class.java).singletonInScope()
        bind(PushTokenSender::class.java).singletonInScope()
        bind(PushTokenWorkerFactory::class.java).singletonInScope()
        bind(MainScreenLauncher::class.java).singletonInScope()
        bind(Prefs::class.java).singletonInScope()
        bind(Gson::class.java).toInstance(GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create())

        // TMDb
        bind(AuthParameterHolder::class.java).toInstance(TMDbConfig)
        bind(OkHttpClient::class.java).withName(TMDbOkHttpClient::class.java).toProvider(TMDbOkHttpClientProvider::class.java).singletonInScope()
        bind(TMDbApi::class.java).toProvider(TMDbApiProvider::class.java).singletonInScope()

        // Loran
        bind(OkHttpClient::class.java).withName(LoranOkHttpClient::class.java).toProvider(LoranOkHttpClientProvider::class.java).singletonInScope()
        bind(LoranApi::class.java).toProvider(LoranApiProvider::class.java).singletonInScope()

        // Init
        bind(Initializer::class.java).singletonInScope()

        // Billing
        bind(BillingManager::class.java).singletonInScope()

        // Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java).withName(AppNavigation::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        // Error Handler
        bind(ErrorHandler::class.java).singletonInScope()

        // Interactor
        bind(CatalogInteractor::class.java).singletonInScope()
        bind(CheckMarkInteractor::class.java).singletonInScope()
        bind(FavoriteInteractor::class.java).singletonInScope()
        bind(PersonInteractor::class.java).singletonInScope()
        bind(ScheduleInteractor::class.java).singletonInScope()
        bind(TimelineInteractor::class.java).singletonInScope()
        bind(NotificationInteractor::class.java).singletonInScope()
        bind(AdInteractor::class.java).singletonInScope()

        // Repository
        bind(CheckMarkRepository::class.java).singletonInScope()
        bind(FavoriteRepository::class.java).singletonInScope()
        bind(PersonRepository::class.java).singletonInScope()
        bind(PushTokenRepository::class.java).singletonInScope()
        bind(ScheduleRepository::class.java).singletonInScope()
        bind(SerialRepository::class.java).singletonInScope()
        bind(CatalogRepository::class.java).singletonInScope()
        bind(TimelineRepository::class.java).singletonInScope()
        bind(ImagesRepository::class.java).singletonInScope()
        bind(AdRepository::class.java).singletonInScope()

        // Dali
        bind(Dali::class.java).toProvider(DaliProvider::class.java).singletonInScope()

        // Refresh relay
        val refreshRelay = PublishRelay.create<Boolean>()
        bind(PublishRelay::class.java).toInstance(refreshRelay)
    }
}