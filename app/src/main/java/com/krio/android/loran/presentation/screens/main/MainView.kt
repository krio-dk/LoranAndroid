package com.krio.android.loran.presentation.screens.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by Dmitriy Kolmogorov on 2019-05-31.
 */
@StateStrategyType(OneExecutionStateStrategy::class)
interface MainView : MvpView {
    fun showMainContent(show: Boolean)
    fun selectPostersTab()
    fun selectTimelineTab()
    fun selectScheduleTab()
    fun selectFavoritesTab()
    fun selectProfileTab()
}