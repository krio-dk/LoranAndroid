package com.krio.android.loran.presentation.screens.posters

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.krio.android.loran.entity.presentation.displayitems.posters.DisplayPosterItem

/**
 * Created by Dmitriy Kolmogorov on 15.02.2018.
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface PostersView : MvpView {
    fun showPageProgress(show: Boolean)
    fun showPageError(show: Boolean, message: String? = null)
    fun showEmptyProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showPosters(show: Boolean, displayPosters: List<DisplayPosterItem>)
    fun showMenuItemSearch(show: Boolean)
    fun enableToolbarScrolling(enable: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun resetEndlessScroll()
}