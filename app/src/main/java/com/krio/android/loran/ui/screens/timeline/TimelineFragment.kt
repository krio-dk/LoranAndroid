package com.krio.android.loran.ui.screens.timeline

import android.graphics.Rect
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.appbar.AppBarLayout
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.krio.android.loran.R
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayProgressItem
import com.krio.android.loran.entity.presentation.displayitems.global.DisplayRefreshItem
import com.krio.android.loran.entity.presentation.displayitems.timeline.DisplayTimelineItem
import com.krio.android.loran.extensions.visible
import com.krio.android.loran.model.data.storage.Prefs
import com.krio.android.loran.model.system.ad.AppodealManager
import com.krio.android.loran.model.system.analytics.Analytics
import com.krio.android.loran.presentation.global.list.DateItemAdapterDelegate
import com.krio.android.loran.presentation.global.list.NativeAdAdapterDelegate
import com.krio.android.loran.presentation.global.list.ProgressItemAdapterDelegate
import com.krio.android.loran.presentation.global.list.RefreshItemAdapterDelegate
import com.krio.android.loran.presentation.screens.timeline.TimelinePresenter
import com.krio.android.loran.presentation.screens.timeline.TimelineView
import com.krio.android.loran.presentation.screens.timeline.list.TimelineItemAdapterDelegate
import com.krio.android.loran.toothpick.DI
import com.krio.android.loran.ui.global.*
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_timeline.*
import kotlinx.android.synthetic.main.layout_zero_data.*
import kotlinx.android.synthetic.main.toolbar.*
import toothpick.Toothpick

class TimelineFragment : BaseFragment(), TimelineView {

    companion object {
        const val VIEW_TYPE_UPDATE = 1
        const val VIEW_TYPE_DATE = 2
        const val VIEW_TYPE_PROGRESS = 3
        const val VIEW_TYPE_REFRESH = 4
        const val VIEW_TYPE_NATIVE_AD = 5

        const val VISIBLE_THRESHOLD = 3

        const val SUBSCRIPTION_DIALOG_TAG = "subscription_dialog"
    }

    override val layoutRes = R.layout.fragment_timeline

    private val adapter = SerialsUpdatesAdapter()

    private var zeroViewHolder: ZeroViewHolder? = null

    private var clearAdapterDisposable: Disposable? = null

    private val prefs = Toothpick.openScope(DI.APP_SCOPE).getInstance(Prefs::class.java)

    @InjectPresenter
    lateinit var presenter: TimelinePresenter

    @ProvidePresenter
    fun providePresenter(): TimelinePresenter {
        return Toothpick.openScope(DI.MAIN_SCOPE).getInstance(TimelinePresenter::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        clearAdapterDisposable = presenter.clearAdapterRelay.subscribe {
            adapter.setData(emptyList())
        }
    }

    override fun onDestroy() {
        clearAdapterDisposable?.dispose()
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbarTitle.setText(R.string.timeline_title)

        recyclerView.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(context)
            adapter = this@TimelineFragment.adapter

            addOnScrollListener(endlessScrollListener)
            addOnScrollListener(topButtonVisibilityController)

            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    parent.adapter?.let { adapter ->
                        val halfPadding = resources.getDimensionPixelSize(R.dimen.cover_half_padding)
                        val count = adapter.itemCount

                        val layoutParams = view.layoutParams as RecyclerView.LayoutParams

                        val firstPosition = 0
                        val lastPosition = count - 1

                        val viewAdapterPosition = layoutParams.viewAdapterPosition

                        if (viewAdapterPosition >=0 && adapter.getItemViewType(viewAdapterPosition) == VIEW_TYPE_NATIVE_AD) {
                            outRect.set(0, 0, 0, 0)
                        } else {
                            when (viewAdapterPosition) {
                                firstPosition -> outRect.set(0, halfPadding, 0, halfPadding)
                                lastPosition -> outRect.set(0, 0, 0, halfPadding)
                                else -> outRect.set(0, 0, 0, halfPadding)
                            }
                        }
                    }
                }
            })
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout) {
            presenter.refreshTimeline()
        }

        swipeToRefresh.setOnRefreshListener {
            presenter.refreshTimeline()
        }

        topButton.setOnClickListener { recyclerView.scrollToPosition(0) }
    }

    private val endlessScrollListener = EndlessScrollListener(VISIBLE_THRESHOLD, VIEW_TYPE_PROGRESS) {
        presenter.loadNextPage()
    }

    private val topButtonVisibilityController
        get() = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0)
                    topButton.visible(false)
                else if (dy < 0)
                    topButton.visible(true)

                val firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
                if (firstVisibleItemPosition <= 3) {
                    topButton.visible(false)
                }
            }
        }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView?.visible(show)
    }

    override fun showPageProgress(show: Boolean) {
        adapter.showProgress(show)
    }

    override fun showEmptyView(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyData(message, R.drawable.ic_update_svg)
        else zeroViewHolder?.hide()
        swipeToRefresh.visible(!show)
    }

    override fun showPageError(show: Boolean, message: String?) {
        adapter.showRefresh(show, message)
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) zeroViewHolder?.showEmptyError(message)
        else zeroViewHolder?.hide()
        swipeToRefresh.visible(!show)
    }

    override fun showData(show: Boolean, data: List<Any>) {
        adapter.setData(data)
        swipeToRefresh?.visible(show)
    }

    override fun updateItem(item: DisplayTimelineItem) {
        adapter.updateItem(item)
    }

    override fun showRefreshProgress(show: Boolean) {
        swipeToRefresh?.isRefreshing = show
    }

    override fun enableToolbarScrolling(enable: Boolean) {
        toolbarFrameLayout?.let {
            val params = toolbarFrameLayout.layoutParams as AppBarLayout.LayoutParams
            params.scrollFlags = if (enable) {
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            } else 0

            toolbar?.visible(false)
            toolbar?.visible(true)
        }
    }

    override fun showMessage(message: String) {
        showSnackMessage(message)
    }

    override fun resetEndlessScroll() {
        endlessScrollListener.reset()
    }

    fun showSubscriptionDialog() {
        if (isAdded) {
            val subscriptionDialogFragment = childFragmentManager.findFragmentByTag(SUBSCRIPTION_DIALOG_TAG)
            if (subscriptionDialogFragment == null) {
                val dialog = SubscriptionDialog.newInstance(getString(R.string.subscription_dialog_caption_disable_ad))
                dialog.show(childFragmentManager, SUBSCRIPTION_DIALOG_TAG)
                dialog.subscriptionClickListener = presenter::onSubscriptionDetailsClicked
                childFragmentManager.executePendingTransactions()
            }
        }
    }

    override fun onBackPressed() = presenter.onBackPressed()

    inner class SerialsUpdatesAdapter : ListDelegationAdapter<MutableList<Any>>() {

        private val appodealManager = Toothpick.openScope(DI.MAIN_SCOPE).getInstance(AppodealManager::class.java)

        init {
            items = mutableListOf()

            delegatesManager.addDelegate(
                VIEW_TYPE_UPDATE,
                TimelineItemAdapterDelegate { displayTimelineItem ->
                    displayTimelineItem.isSeen = displayTimelineItem.isSeen.not()

                    items.filter { it is DisplayTimelineItem }.map { it as DisplayTimelineItem }
                        .filter { it.tmdbId == displayTimelineItem.tmdbId }
                        .filter { it.seasonNumber == displayTimelineItem.seasonNumber }
                        .filter { it.episodeNumber == displayTimelineItem.episodeNumber }
                        .forEach { timelineItem ->
                            timelineItem.isUpdating = true
                            timelineItem.isSeen = displayTimelineItem.isSeen
                            notifyItemChanged(items.indexOf(timelineItem))
                        }

                    presenter.onCheckMarkClicked(displayTimelineItem)
                }
            )

            delegatesManager.addDelegate(
                VIEW_TYPE_NATIVE_AD,
                NativeAdAdapterDelegate (appodealManager) {
                    Analytics.logEvent("close_ad_clicked") {
                        putString("from", "timeline screen")
                        prefs.userId?.let { putInt("user_id", it) }
                    }
                    showSubscriptionDialog()
                }
            )

            delegatesManager.addDelegate(VIEW_TYPE_DATE, DateItemAdapterDelegate())
            delegatesManager.addDelegate(VIEW_TYPE_PROGRESS, ProgressItemAdapterDelegate())
            delegatesManager.addDelegate(VIEW_TYPE_REFRESH, RefreshItemAdapterDelegate { presenter.loadNextPage() })
        }

        fun updateItem(updatedItem: DisplayTimelineItem) {
            items
                .filter { it is DisplayTimelineItem }
                .map { it as DisplayTimelineItem }
                .filter { it.tmdbId == updatedItem.tmdbId }
                .filter { it.seasonNumber == updatedItem.seasonNumber }
                .filter { it.episodeNumber == updatedItem.episodeNumber }
                .forEach { timelineItem ->
                    timelineItem.isUpdating = false
                    timelineItem.isSeen = updatedItem.isSeen
                    notifyItemChanged(items.indexOf(timelineItem))
                }
        }

        fun setData(shows: List<Any>) {
            val oldItems = ArrayList(items)
            val progress = isProgress()

            items.clear()
            items.addAll(shows)
            if (progress) items.add(DisplayProgressItem())

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }

        fun showProgress(isVisible: Boolean) {
            val oldItems = ArrayList(items)
            val progress = isProgress()

            if (isVisible && !progress) items.add(DisplayProgressItem())
            else if (!isVisible && progress) items.remove(items.last())

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }

        fun showRefresh(show: Boolean, message: String?) {
            val oldItems = ArrayList(items)
            val currentRefresh = isRefresh()

            if (show && !currentRefresh) items.add(DisplayRefreshItem(message ?: getString(R.string.error_unknown)))
            else if (!show && currentRefresh) items.remove(items.last())

            val newItems = ArrayList(items)

            val diffCallback = DiffCallback(oldItems, newItems)
            DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is DisplayProgressItem

        private fun isRefresh() = items.isNotEmpty() && items.last() is DisplayRefreshItem

        override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
            super.onViewRecycled(holder)
            if (holder is NativeAdAdapterDelegate.NativeAdViewHolder) {
                holder.unregisterViewForInteraction()
            }
        }
    }
}