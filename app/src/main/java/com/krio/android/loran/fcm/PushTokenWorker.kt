package com.krio.android.loran.fcm

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.krio.android.loran.toothpick.DI
import timber.log.Timber
import toothpick.Toothpick
import javax.inject.Inject

/**
 * Created by Dmitriy Kolmogorov on 2019-08-25.
 */
class PushTokenWorker (
    appContext: Context,
    workerParams: WorkerParameters
) : Worker(appContext, workerParams) {

    lateinit var pushTokenSender: PushTokenSender

    init {
        Timber.d("Init")
    }

    override fun doWork(): Result {
        Timber.d("doWork: start")
        pushTokenSender.sendPushToken()
        Timber.d("doWork: end")
        return Result.success()
    }

}