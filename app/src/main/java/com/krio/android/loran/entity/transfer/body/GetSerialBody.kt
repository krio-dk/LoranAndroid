package com.krio.android.loran.entity.transfer.body

import com.google.gson.annotations.SerializedName

/**
 * Created by krio on 08.06.2018.
 */
data class GetSerialBody(
    @SerializedName ("serial_tmdb_id") val serialTmdbId: Int,
    @SerializedName ("user_id") val userId: Int?,
    @SerializedName ("language") val language: String
)