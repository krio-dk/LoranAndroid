package com.krio.android.loran.model.system.secure

import com.github.javiersantos.piracychecker.enums.PiracyCheckerError
import com.github.javiersantos.piracychecker.enums.PirateApp

/**
 * Created by Dmitriy Kolmogorov on 01.05.2020.
 */
class PiracyDetectedException(val reason: PiracyCheckerError, val app: PirateApp?) : Error()
class PiracyErrorException : Error()